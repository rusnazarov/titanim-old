/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef NEWSFEEDMODEL_H
#define NEWSFEEDMODEL_H

#include <QAbstractListModel>
#include "vk/cvkStruct.h"

class newsfeedModel : public QAbstractListModel
{
Q_OBJECT

private:
    QVector<sNew> news;

public:
    explicit newsfeedModel(QObject *parent = 0);
    void appendNews(QVector<sNew> news);
    void replaceNews(QVector<sNew> news);
    QString dateText(QDateTime date) const;
    bool removeNew(int row, int count);
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant& value, int role = Qt::EditRole);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    void setLikesCount(const QString &ownerId, const QString &postId, const QString &countLikes, bool isAdd);
    QString getId(const int &index);
    QDateTime getDateTime(const int &index);
    int getIndex(const QString &id);

    enum NewsRole {
        typeRole = Qt::UserRole,
        sourceIdRole,
        dateRole,
        dateStrRole,
        postIdRole,
        copyOwnerIdRole,
        copyOwnerRole,
        copyPostIdRole,
        textRole,
        shortTextRole,
        likesRole,
        isLikedRole,
        commentsRole,
        canPostRole,
        photosRole,
        photosBigRole,
        photosWidthRole,
        photosHeightRole,
        geoRole,
        linkUrlRole
    };

public slots:

signals:
};

Q_DECLARE_METATYPE(QList<int>)

#endif // NEWSFEEDMODEL_H
