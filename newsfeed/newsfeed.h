/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef NEWSFEED_H
#define NEWSFEED_H

#include <QObject>
#include <QSortFilterProxyModel>
#include "newsfeedmodel.h"
#include "../vk/cvkStruct.h"

class Newsfeed : public QObject
{
Q_OBJECT

public:
    newsfeedModel *model;
    QSortFilterProxyModel *proxy;

public:
    Newsfeed(QObject *parent = 0);
    ~Newsfeed();

public slots:
    void appendNews(const QVector<sNew> &news);
    void replaceNews(const QVector<sNew> &news);
    QString getId(const int &index);
    QDateTime getDateTime(const int &index);
    int getIndex(const QString &id);

signals:
};

#endif // NEWSFEED_H
