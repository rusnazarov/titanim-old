/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "newsfeedmodel.h"

newsfeedModel::newsfeedModel(QObject *parent) :
    QAbstractListModel(parent)
{
    QHash<int, QByteArray> roles = roleNames();
    roles[typeRole] = "type";
    roles[sourceIdRole] = "sourceId";
    roles[dateRole] = "date";
    roles[dateStrRole] = "dateStr";
    roles[postIdRole] = "postId";
    roles[copyOwnerIdRole] = "copyOwnerId";
    roles[copyOwnerRole] = "copyOwner";
    roles[copyPostIdRole] = "copyPostId";
    roles[textRole] = "text";
    roles[shortTextRole] = "shortText";
    roles[likesRole] = "likes";
    roles[isLikedRole] = "isLiked";
    roles[commentsRole] = "comments";
    roles[canPostRole] = "canPost";
    roles[photosRole] = "photos";
    roles[photosBigRole] = "photosBig";
    roles[photosWidthRole] = "photosWidth";
    roles[photosHeightRole] = "photosHeight";
    roles[geoRole] = "geo";
    roles[linkUrlRole] = "linkUrl";
    setRoleNames(roles);
}

void newsfeedModel::appendNews(QVector<sNew> news){
    // ==========================================================================
    // добавление новостей
    // ==========================================================================

    if (!news.count())
        return;

    //добавляем новые
    beginInsertRows(QModelIndex(), this->news.count(), this->news.count() + news.count() - 1);
    this->news << news;
    endInsertRows();
}

void newsfeedModel::replaceNews(QVector<sNew> news){
    // ==========================================================================
    // замена новостей
    // ==========================================================================

    //очищаем модель
    removeNew(0, rowCount());

    //добавляем новые
    appendNews(news);
}

QString newsfeedModel::dateText(QDateTime date) const{
    // ==========================================================================
    // переводит дату в удобную для человеку
    // ==========================================================================

    int days = date.daysTo(QDateTime::currentDateTime());

    switch (days) {
    case 0:
        return tr("Today at %1").arg(date.time().toString("hh:mm"));
    case 1:
        return tr("Yesterday at %1").arg(date.time().toString("hh:mm"));
    default:
        return date.toString("dd.MM.yy hh:mm");
    }
}

bool newsfeedModel::removeNew(int row, int count){
    // ==========================================================================
    // удаляем с модели данные
    // ==========================================================================

    beginRemoveRows(QModelIndex(), row, row+count-1);

    for (int i = 0; i < count; ++i) {
        news.remove(row);
    }

    endRemoveRows();
    return true;
}

QVariant newsfeedModel::data(const QModelIndex &index, int role) const{
    // ==========================================================================
    // данные модели
    // ==========================================================================

    if (!index.isValid() || index.row() >= news.size()) {
        return QVariant();
    }

    switch (role) {
    case Qt::DisplayRole:
        return news.at(index.row()).firstName + " " + news.at(index.row()).lastName;

    case Qt::DecorationRole:
        return news.at(index.row()).avatarUrl;

    case typeRole:
        return news.at(index.row()).type;

    case sourceIdRole:
        return news.at(index.row()).sourceId;

    case dateRole:
        return news.at(index.row()).date;

    case dateStrRole:
        return dateText(news.at(index.row()).date);

    case postIdRole:
        return news.at(index.row()).postId;

    case copyOwnerIdRole:
        return news.at(index.row()).copyOwnerId;

    case copyOwnerRole:
        return news.at(index.row()).copyOwner;

    case copyPostIdRole:
        return news.at(index.row()).copyPostId;

    case textRole:
        return news.at(index.row()).text;

    case shortTextRole:
        return news.at(index.row()).text.length() > 200 ?
                news.at(index.row()).text.left(200) + "..." :
                news.at(index.row()).text;

    case likesRole:
        return news.at(index.row()).likes;

    case isLikedRole:
        return news.at(index.row()).isLiked;

    case commentsRole:
        return news.at(index.row()).comments;

    case canPostRole:
        return news.at(index.row()).canPost;

    case photosRole:
        return QVariant::fromValue(news.at(index.row()).photos);

    case photosBigRole:
        return QVariant::fromValue(news.at(index.row()).photosBig);

    case photosWidthRole:
        return QVariant::fromValue(news.at(index.row()).photosWidth);

    case photosHeightRole:
        return QVariant::fromValue(news.at(index.row()).photosHeight);

    case geoRole:
        return news.at(index.row()).geo;

    case linkUrlRole:
        return news.at(index.row()).linkUrl;
    }

    return QVariant();
}

bool newsfeedModel::setData(const QModelIndex &index, const QVariant &value, int role){
    // ==========================================================================
    // редактирование данных модели
    // ==========================================================================

    if (!index.isValid() && !value.isValid() && index.row() >= news.size()) {
        return false;
    }

    //изменяем количество лайков
    if (role == likesRole){
        news[index.row()].likes = value.toString();
        emit dataChanged(index, index);
        return true;
    }

    //изменяем isLiked
    if (role == isLikedRole){
        news[index.row()].isLiked = value.toBool();
        emit dataChanged(index, index);
        return true;
    }

    return false;
}

int newsfeedModel::rowCount(const QModelIndex &parent) const{
    // ==========================================================================
    // количество строк в модели
    // ==========================================================================

    Q_UNUSED(parent);
    return news.count();
}

Qt::ItemFlags newsfeedModel::flags(const QModelIndex &index) const{
    // ==========================================================================
    // флаги модели
    // ==========================================================================

    if (index.isValid())
      return (Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    else
        return Qt::NoItemFlags;
}

void newsfeedModel::setLikesCount(const QString &ownerId, const QString &postId, const QString &countLikes, bool isAdd){
    // ==========================================================================
    // обновляем количество лайков
    // ==========================================================================

    //ищем ownerId и postId записи и изменяем количество
    for (int i=0; i<news.count(); i++){
        if (news.at(i).sourceId == ownerId && news.at(i).postId == postId){
            QModelIndex idx = index(i, 0);
            setData(idx, countLikes, likesRole);
            setData(idx, isAdd, isLikedRole);
            break;
        }
    }
}

QString newsfeedModel::getId(const int &index){
    // ==========================================================================
    // отдаем id по индексу
    // ==========================================================================

    if (index >= 0 && index < news.count())
        return news.value(index).sourceId + "_" + QString::number(news.value(index).date.toTime_t());
    return "0";
}

QDateTime newsfeedModel::getDateTime(const int &index){
    // ==========================================================================
    // отдаем дату по индексу
    // ==========================================================================

    if (index >= 0 && index < news.count())
        return news.value(index).date;
    return QDateTime();
}

int newsfeedModel::getIndex(const QString &id){
    // ==========================================================================
    // отдаем индекс по id
    // ==========================================================================

    int idx = 0;
    QStringList idList = id.split("_");
    if (idList.count() < 2)
        return 0;

    for (int i=0; i<news.count(); i++){
        if (news.at(i).sourceId == idList.at(0) && QString::number(news.at(i).date.toTime_t()) == idList.at(1)){
            idx = i;
            break;
        }
    }

    return idx;
}
