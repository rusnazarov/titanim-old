/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "newsfeed.h"

Newsfeed::Newsfeed(QObject *parent) :
    QObject(parent)
{
    model = new newsfeedModel();

    proxy = new QSortFilterProxyModel(this);
    proxy->setDynamicSortFilter(true);
    proxy->setSortCaseSensitivity(Qt::CaseInsensitive);
    proxy->setFilterCaseSensitivity(Qt::CaseInsensitive);
    proxy->setSortRole(Qt::UserRole+2);
    proxy->sort(0, Qt::DescendingOrder);
    proxy->setSourceModel(model);
}

Newsfeed::~Newsfeed(){
    delete proxy;
    delete model;
}

void Newsfeed::appendNews(const QVector<sNew> &news){
    // ==========================================================================
    // пришли новости
    // ==========================================================================

    model->appendNews(news);
}

void Newsfeed::replaceNews(const QVector<sNew> &news){
    // ==========================================================================
    // пришли новости
    // ==========================================================================

    model->replaceNews(news);
}

QString Newsfeed::getId(const int &index){
    // ==========================================================================
    // отдаем id по индексу
    // ==========================================================================

    return model->getId(index);
}

QDateTime Newsfeed::getDateTime(const int &index){
    // ==========================================================================
    // отдаем дату по sourceId
    // ==========================================================================

    return model->getDateTime(index);
}

int Newsfeed::getIndex(const QString &id){
    // ==========================================================================
    // отдаем индекс по id
    // ==========================================================================

    return model->getIndex(id);
}
