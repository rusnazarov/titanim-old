/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0

Rectangle{
    id: highlightListView;
    smooth: true;
    radius: 5;
    gradient: Gradient{
        GradientStop{
            position: 0.0;
            color: "#4e79a1";
        }
        GradientStop{
            position: 1.0;
            color: "#174774"
        }
    }
}
