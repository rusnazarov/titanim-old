/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0
import "ContextualMenu.js" as CtxMenu

Rectangle{
    id: menuRoot;
    z:100;

    property ListModel model;
    property int posx: 0;
    property int posy: 0;
    property int itemFontSize: 10;
    property int button: Qt.RightButton;
    signal changeVisible(bool visible);
    signal itemClicked(int index);

    anchors.fill: parent;
    opacity: 0;
    color: "transparent";

    MouseArea{
        anchors.fill: parent;
        acceptedButtons: Qt.LeftButton | Qt.MiddleButton;
        onClicked: {
            CtxMenu.hideMenu();
            changeVisible(false);
        }
    }

    MouseArea{
        id: menuMouseArea;
        anchors.fill: parent;
        acceptedButtons: button;
        onPressed:{
            changeVisible(true);
            mouse.accepted = false;
            if (button == Qt.LeftButton)
                CtxMenu.showMenuButton(posx, posy);
            else
                CtxMenu.showMenu(mouse);
        }
    }

    Rectangle{
        id: menu;
        width: CtxMenu.getMenuWidth() + 1;
        height: CtxMenu.getMenuHeight();
        color: "#343434";
        border.color: "#696868"

        ListView{
            id: menuList;
            width: parent.width-3;
            height: parent.height-3;
            x:2;
            y:2;
            clip: true;
            boundsBehavior: Flickable.StopAtBounds;
            model: menuRoot.model;
            delegate: Rectangle{
                id: listViewItem;
                width: menu.width - 1;
                height: itemText.height;
                color: (activeFocus ? "#4e79a1" : "transparent");
                Row{
                    x: 5;
                    spacing: 9;
                    Image{
                        width: 16;
                        height: 16;
                        anchors.verticalCenter: parent.verticalCenter;
                        smooth: true;
                        source: icon;
                    }
                    Text{
                        id: itemText;
                        x: 10;
                        width: listViewItem.width;
                        color: "white";
                        font.pointSize: itemFontSize;
                        elide: Text.ElideRight;
                        text: name;
                    }
                }
                MouseArea{
                    anchors.fill: parent;
                    hoverEnabled: true;
                    onClicked:{
                        itemClicked(index);
                        CtxMenu.hideMenu();
                        changeVisible(false);
                    }
                    onEntered:{
                        menuList.currentIndex = index;
                    }
                }
                Keys.onReturnPressed:{
                    itemClicked(index);
                    CtxMenu.hideMenu();
                    changeVisible(false);
                }
            }
        }
    }

    states: State{
        name: "visible";
        PropertyChanges {
            target: menuRoot;
            opacity: 1;
        }
    }

    transitions: Transition{
        NumberAnimation{
            target:  menuRoot;
            properties: "opacity";
            duration: 250;
	}
    }

    Component.onCompleted: CtxMenu.initializeMenu()
}
