/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0

Item {
    id: statusBar;

    property string searchText;
    signal statusButtonClicked;

    BorderImage{
        width: parent.width;
        height: parent.height + 14;
        y: -7;
        source: "images/bar.sci";
    }

    Item{
        id: container;
        width: parent.width;
        height: parent.height;
        anchors.fill: parent;
        opacity: searchText.length ? 0 : 1;

        Row{
            id: statusRow;
            width: parent.width;
            anchors.left: parent.left;
            anchors.leftMargin: 2;
            anchors.verticalCenter: parent.verticalCenter;
            spacing: 1;

            Button{
                id: statusOnline;
                width: statusBar.width - statusButton.width - 6;
                height: 24;
                autoRaise: false;
                url: "qrc:/Icon_17.png";
                text: qsTr("Offline");

                ContextualMenu{
                    id: contextualStatusMenu;
                    posx: statusBar.x + statusRow.x + statusOnline.x + 2;
                    posy: statusBar.y + statusRow.y + statusOnline.y + statusOnline.height;
                    button: Qt.LeftButton;
                    ListModel{
                        id: statusMenu;
                        Component.onCompleted: {
                            statusMenu.append({icon: "qrc:/Icon_18.png", name: qsTr("Online")});
                            statusMenu.append({icon: "qrc:/Icon_17.png", name: qsTr("Offline")});
                        }
                    }
                    model: statusMenu;
                    onChangeVisible:{
                        if (visible)
                            rect.state = "dark";
                        else
                            if (!progressLabel.text.length) rect.state = "";
                    }
                    onItemClicked:{
                        titanim.on_statusCBox_currentIndexChanged(index);
                    }
                }

                Connections{
                    target: titanim;
                    onStatusChanged:{
                        if (isOnline){
                            statusOnline.url = "qrc:/Icon_18.png";
                            statusOnline.text = qsTranslate("StatusBar", "Online");
                            roster.enabled = true;
                        } else {
                            statusOnline.url = "qrc:/Icon_17.png";
                            statusOnline.text = qsTranslate("StatusBar", "Offline");
                            roster.enabled = false;
                        }
                    }
                }
            }

            Button{
                id: statusButton;
                width: 24;
                height: 24;
                autoRaise: false;
                url: "qrc:/balloon.png";
                onClicked: statusBar.statusButtonClicked();
            }
        }
    }

    Item {
        id: lineEdit;
        height: parent.height - 11;
        y: 5;
        anchors{
            left: parent.left;
            leftMargin: 3;
            right: parent.right;
            rightMargin: 3;
        }
        opacity: searchText.length ? 1 : 0;

        BorderImage{
            source: "images/lineedit.sci";
            anchors.fill: parent;
        }

        Image{
            id: iconSearch;
            anchors.right: parent.right;
            anchors.rightMargin: 5;
            anchors.verticalCenter: parent.verticalCenter;
            source: "qrc:/magnifier-zoom.png";
            MouseArea {
                anchors.fill: parent
                onClicked: searchText = "";
            }
        }

        TextInput{
            id: editor;
            anchors{
                left: parent.left;
                right: iconSearch.left;
                leftMargin: 10;
                rightMargin: 5;
                verticalCenter: parent.verticalCenter;
            }
            focus: searchText.length ? true : false;
            selectByMouse: true;
            color: "#4f4f4f";
            text: searchText;

            onTextChanged:{
                titanim.slotPressedKey(text);
                searchText = text;
            }

            Keys.onPressed:{
                switch (event.key){
                case Qt.Key_Up:
                    roster.decrementCurrentIndex();
                    break;
                case Qt.Key_Down:
                    roster.incrementCurrentIndex();
                    break;
                case Qt.Key_Escape:
                    searchText = "";
                    break;
                }
                event.accepted = false;
            }
        }
    }
}
