/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0

Item{
    id: chatDelegate;
    width: parent.width;
    height: avatarBorder.height > (messageName.height + messageText.height) ?  avatarBorder.height + 8 : messageName.height + messageText.height + 8;
    ListView.delayRemove: avatar.status == Image.Ready ? false : true;

    Rectangle{
        y: 1;
        anchors.fill: parent;
        opacity: index % 2 ? 0.1 : 0.2;
        color: (model.flags & 2) && (model.flags & 1) ? "#999999" : "black";
    }

    Row{
        id: message;
        height: parent.height;
        x: 5;
        spacing: 8;

        Rectangle{
            id: avatarBorder;
            width: (avatar.width + 2);
            height: (avatar.height + 2);
            y:4;
            scale: 0.0;
            color: avatar.height > 16 ? "white" : "transparent";
            Behavior on scale {
                NumberAnimation {
                    easing.type: Easing.InOutQuad;
                }
            }
            Image{
                id: avatar;
                width: 30;
                height: 30;
                anchors.centerIn: parent;
                smooth: true;
                source: model.decoration;
            }
        }

        Column{
            y: 2;
            Text{
                id: messageName;
                width: text.length ? chatDelegate.width - avatarBorder.width - message.x - message.spacing - 7 : 0;
                color: "white";
                font.pointSize: 8;
                font.bold: true;
                elide: Text.ElideRight;
                text: model.name;

                Text{
                    id: messageDate;
                    anchors.right: parent.right;
                    color: "white";
                    font.pointSize: 7;
                    text:  Qt.formatDateTime(model.date, "dd.MM.yyyy hh:mm:ss");
                }
            }
            Text{
                id: messageText;
                width: text.length ? chatDelegate.width - avatarBorder.width - message.x - message.spacing - 7 : 0;
                color: "white";
                wrapMode: Text.WordWrap;
                font.pointSize: 8;
                text: model.display;
            }
        }
    }

    MouseArea{
        anchors.fill: parent;
        acceptedButtons: Qt.LeftButton | Qt.RightButton;
        onClicked: chatListView.currentIndex = index;
//        onDoubleClicked:{
//            if (mouse.button == Qt.LeftButton){
//                titanim.slotOpenChatWindow(model.uid);
//                statusBar.searchText = "";
//            }
//        }
    }

    states:[
        State{
            name: "Show";
            when: avatar.status == Image.Ready;
            PropertyChanges {
                target: avatarBorder;
                scale: 1;
            }
        }
    ]
}
