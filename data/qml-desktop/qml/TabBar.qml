/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0

Rectangle {
    id: tabBar;
    width: parent.width;
    height: 25;
    color: "transparent";

    BorderImage{
        width: parent.width;
        height: parent.height + 14;
        y: -7;
        source: "images/bar.sci";
    }

    ListModel{
        id: tabsModel;
    }

    function addTab(icon, uid, label){
        tabsModel.append({icon: icon, uid: uid, label: label, inputEditText: ""});
    }

    function setTabIcon(uid, icon){
        for(var i = 0; i < tabsModel.count; i++){
            if (tabsModel.get(i).uid == uid){
                tabsModel.get(i).icon = icon;
                break;
            }
        }
    }

    function setInputEditText(uid, text){
        for(var i = 0; i < tabsModel.count; i++){
            if (tabsModel.get(i).uid == uid){
                tabsModel.get(i).inputEditText = text;
                break;
            }
        }
    }

    function currentTab(uid){
        if (tabsModel.get(tabs.currentIndex).uid == uid){
            chatListView.positionViewAtIndex(chatListView.count - 1, ListView.End);
            return;
        }
        chat.slotCurrentTab(uid);
        tabsModel.get(tabs.currentIndex).inputEditText = chatInputEdit.text;
        for(var i = 0; i < tabsModel.count; i++){
            if (tabsModel.get(i).uid == uid){
                chatInputEdit.text = "";
                tabs.currentIndex = i;
                chatListView.positionViewAtIndex(chatListView.count - 1, ListView.End);
                break;
            }
        }
    }

    function clearTab(){
        tabsModel.clear();
    }

    function nextTab(){
        if (tabs.count <= 1)
            return;

        if (tabs.currentIndex != tabs.count - 1)
            currentTab(tabsModel.get(tabs.currentIndex + 1).uid);
        else
            currentTab(tabsModel.get(0).uid);
    }

    function closeCurrentTab(){
        var index = tabs.currentIndex;
        chat.slotCloseTab(tabsModel.get(index).uid);
        if (tabsModel.count > 1){
            currentTab(tabsModel.get(index-1 >= 0 ? index-1 : index+1).uid);
        }
        tabsModel.remove(index);
    }

    Component{
        id: tabsDelegate;
        Item{
            width: rowItem.width + 10;
            height: parent.height;
            BorderImage{
                id: border;
                width: parent.width
                height: parent.height - 2;
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 1;
                source: "images/tab.sci";

                MouseArea{
                    anchors.fill: parent;
                    onClicked:{
                        currentTab(tabsModel.get(index).uid);
                    }
                }
            }

            Row{
                id: rowItem;
                anchors.left: parent.left;
                anchors.leftMargin: 5;
                anchors.verticalCenter: border.verticalCenter;
                spacing: 4;
                Image{
                    id: tabIcon;
                    width: 16;
                    height: 16;
                    source: model.icon;
                }

                Text{
                    id: tabText;
                    color: "white";
                    font.pointSize: 8;
                    font.bold: tabs.currentIndex == index;
                    text: model.label;
                }

                Button{
                    id: tabClose;
                    width: 18;
                    height: 18;
                    url: "qrc:/cross.png";
                    onClicked:{
                        if (tabsModel.count > 1 && index == tabs.currentIndex){
                            currentTab(tabsModel.get(index-1 >= 0 ? index-1 : index+1).uid);
                        }
                        chat.slotCloseTab(tabsModel.get(index).uid);
                        tabsModel.remove(index);
                    }
                }
            }
        }
    }

    ListView{
        id: tabs;
        width: parent.width;
        height: parent.height;
        orientation: ListView.Horizontal;
        spacing: -1;
        model: tabsModel;
        delegate: tabsDelegate;
        highlightMoveDuration:240
        onCurrentIndexChanged:{
            chatListView.positionViewAtIndex(chatListView.count - 1, ListView.End);
            chatInputEdit.text = tabsModel.get(tabs.currentIndex).inputEditText;
        }
    }
}
