/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0

Item{
    id: contactDelegate;
    width: parent.width;
    height: avatarBorder.height > (name.height + activity.height) ?  avatarBorder.height + 6 : name.height + activity.height + 6;
    ListView.delayRemove: avatar.status == Image.Ready ? false : true;

    Rectangle{
        y: 1;
        anchors.fill: parent;
        opacity: index % 2 ? 0.1 : 0.2;
        visible: titanim.alternatingRowColors;
        color: "black";
    }

    Row{
        id: contact;
        height: parent.height;
        x: 5;
        spacing: 12;

        Rectangle{
            id: avatarBorder;
            width: (avatar.width + 2);
            height: (avatar.height + 2);
            anchors.verticalCenter: parent.verticalCenter;
            scale: 0.0;
            color: avatar.height > 16 ? "white" : "transparent";
            Behavior on scale {
                NumberAnimation {
                    easing.type: Easing.InOutQuad;
                }
            }
            Image{
                id: avatar;
                width: titanim.iconSize;
                height: titanim.iconSize;
                anchors.centerIn: parent;
                smooth: true;
                source: model.decoration;
            }
        }

        Column{
            anchors.verticalCenter: parent.verticalCenter;
            Text{
                id: name;
                width: text.length ? contactDelegate.width - avatarBorder.width - contact.x - contact.spacing - 7 : 0;
                color: model.status == 1 ? "white" : "#999999";
                font.pointSize: titanim.fontPointSize;
                font.bold: true;
                elide: Text.ElideRight;
                text: model.display;
            }
            Text{
                id: activity;
                width: text.length ? contactDelegate.width - avatarBorder.width - contact.x - contact.spacing - 7 : 0;
                visible: titanim.showActivity;
                color: model.status == 1 ? "white" : "#999999";
                font.pointSize: titanim.fontPointSize - 1;
                elide: Text.ElideRight;
                text: model.activity;
            }
        }
    }

    MouseArea{
        anchors.fill: parent;
        acceptedButtons: Qt.LeftButton | Qt.RightButton;
        onClicked: roster.currentIndex = index;
        onDoubleClicked:{
            if (mouse.button == Qt.LeftButton){
                titanim.slotOpenChatWindow(model.uid);
                statusBar.searchText = "";
            }
        }
    }

    states:[
        State{
            name: "Show";
            when: avatar.status == Image.Ready;
            PropertyChanges {
                target: avatarBorder;
                scale: 1;
            }
        }
    ]
}
