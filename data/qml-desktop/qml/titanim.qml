/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0

Rectangle{
    id: mainView;
    color: "#343434";
    Image{
        anchors.fill: parent;
        fillMode: Image.Tile;
        opacity: 0.3;
        source: "images/stripes.png";
    }

    ToolBar{
        id: toolBar;
        width: parent.width;
        height: 30;
        z:1;
        anchors.top: parent.top;
        opacity: 0.9;
        onOfflineButtonClicked: titanim.on_offlineButton_clicked();
        onSoundButtonClicked: titanim.on_soundButton_clicked();
        onSettingsButtonClicked: titanim.on_settingsButton_clicked();
        onHomeButtonClicked: titanim.on_homeButton_clicked();
        onAboutButtonClicked: titanim.on_aboutButton_clicked();
    }

    ListView{
        id: roster;
        width: parent.width;
        height: parent.height;
        anchors.top: toolBar.bottom;
        anchors.bottom: statusBar.top;
        enabled: false;
        focus: roster.enabled ? statusBar.searchText.length ? false : true : false;
        highlight: HighlightListView{}
        highlightMoveSpeed: -1;
        model: rosterModel;
        delegate: ContactDelegate{}
        currentIndex: -1;

        Rectangle{
            id: rect;
            anchors.fill: parent;
            color: "#80000000";
            opacity: 0;

            Column{
                id: progress;
                width: parent.width;
                anchors.verticalCenter: parent.verticalCenter;
                spacing: 10;
                opacity: 0;

                Image{
                    id: progressIcon;
                    property bool on: progressLabel.text.length;
                    height: 30;
                    width: 30;
                    anchors.horizontalCenter: parent.horizontalCenter;
                    source: "images/busy.png";
                    visible: progressIcon.on;
                    NumberAnimation on rotation{
                        running: progressIcon.on;
                        from: 0;
                        to: 360;
                        loops: Animation.Infinite;
                        duration: 1200;
                    }
                }

                Text{
                    id: progressLabel;
                    width: parent.width - 20;
                    anchors.horizontalCenter: parent.horizontalCenter;
                    horizontalAlignment: Text.AlignHCenter;
                    verticalAlignment: Text.AlignVCenter;
                    wrapMode: Text.WordWrap;
                    color: "white";
                    font.pointSize: 12;
                }
            }

            states:[
                State{
                    name: "dark";
                    PropertyChanges{
                        target: rect;
                        opacity: 1;
                    }
                    PropertyChanges{
                        target: progress;
                        opacity: 0;
                    }
                },
                State{
                    name: "progress";
                    PropertyChanges{
                        target: rect;
                        opacity: 1;
                    }
                    PropertyChanges{
                        target: progress;
                        opacity: 1;
                    }
                }
            ]

            transitions: Transition{
                NumberAnimation{
                    target: rect;
                    properties: "opacity";
                    duration: 250;
                }
            }
        }

        ContextualMenu{
            id: contextualRosterMenu;
            ListModel{
                id: rosterMenu;
                Component.onCompleted: {
                    rosterMenu.append({icon: "qrc:/mail--arrow.png", name: qsTr("Send message")});
                    rosterMenu.append({icon: "qrc:/information.png", name: qsTr("Contact details")});
                    rosterMenu.append({icon: "qrc:/Icon_46.ico", name: qsTr("VK page")});
                    rosterMenu.append({icon: "qrc:/image.png", name: qsTr("Photo")});
                    rosterMenu.append({icon: "qrc:/music-beam-16.png", name: qsTr("Music")});
                    rosterMenu.append({icon: "qrc:/film.png", name: qsTr("Video")});
                }
            }
            model: rosterMenu;
            onChangeVisible:{
                if (visible)
                    rect.state = "dark";
                else
                    rect.state = "";
            }
            onItemClicked:{
                titanim.slotRosterMenu(index, roster.currentIndex);
            }
        }
    }

    ScrollBar{
        width: 7;
        height: roster.height;
        anchors.top: toolBar.bottom;
        anchors.right: roster.right;
        scrollArea: roster;
    }

    StatusBar{
        id: statusBar;
        width: parent.width;
        height: 30;
        z:1;
        anchors.bottom: parent.bottom;
        opacity: 0.9;
        onStatusButtonClicked: titanim.on_statusButton_clicked();
    }

    Keys.onPressed: {
        if (!roster.enabled){
            return;
        }

        if (event.modifiers == Qt.ControlModifier ||
            event.modifiers == Qt.AltModifier ||
            event.modifiers == Qt.MetaModifier ||
            event.key == Qt.Key_Escape ||
            event.key == Qt.Key_Delete ||
            event.key == Qt.Key_Backspace){
                return;
        }

        if (event.key == Qt.Key_Return){
            titanim.slotOpenChatWindow(roster.currentIndex);
            statusBar.searchText = "";
            return;
        }

        if (event.text.trim() !== ""){
            statusBar.searchText = event.text;
        }

        event.accepted = true;
    }

    Connections{
        target: titanim;
        onProgressChanged:{
            if (progressText.length){
                rect.state = "progress";
                progressLabel.text = progressText;
            } else {
                rect.state = "";
                progressLabel.text = "";
            }
        }

        onShowAuthForm:{
            titanim.setOnline("login", "pass");
        }
    }
}
