/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0

Rectangle {
    id: chatView;
    color: "#343434";
    Image{
        anchors.fill: parent;
        fillMode: Image.Tile;
        opacity: 0.3;
        source: "images/stripes.png";
    }

    TabBar{
        id: tabBar;
        z: 1;
    }

    ListView{
        id: chatListView;
        width: parent.width;
        height: parent.height;
        anchors.top: tabBar.bottom;
        anchors.bottom: toolBarChat.top;
        anchors.bottomMargin: 2;
        highlight: HighlightListView{}
        highlightMoveSpeed: -1;
        model: chatModel;
        delegate: ChatDelegate{}
        currentIndex: -1;
        onCountChanged: {
            chatListView.positionViewAtIndex(chatListView.count - 1, ListView.End);
        }

        Rectangle{
            id: rect;
            z:1;
            anchors.fill: parent;
            color: "#80000000";
            opacity: 0;

            states:[
                State{
                    name: "dark";
                    PropertyChanges{
                        target: rect;
                        opacity: 1;
                    }
                }
            ]

            transitions: Transition{
                NumberAnimation{
                    target: rect;
                    properties: "opacity";
                    duration: 250;
                }
            }
        }

        ContextualMenu{
            id: contextualChatListMenu;
            ListModel{
                id: chatListMenu;
                Component.onCompleted: {
                    chatListMenu.append({icon: "qrc:/Icon_36.ico", name: qsTr("Reply Quoted")});
                    chatListMenu.append({icon: "qrc:/clipboard.png", name: qsTr("Copy")});
                    chatListMenu.append({icon: "qrc:/clipboard-list.png", name: qsTr("Copy Text")});
                    chatListMenu.append({icon: "qrc:/mail--exclamation.png", name: qsTr("Mark as Unread")});
                    chatListMenu.append({icon: "qrc:/Icon_46.ico", name: qsTr("Open VK")});
                    chatListMenu.append({icon: "qrc:/cross-script.png", name: qsTr("Delete")});
                }
            }
            model: chatListMenu;
            onChangeVisible:{
                if (visible)
                    rect.state = "dark";
                else
                    rect.state = "";
            }
            onItemClicked:{
                chat.slotChatListMenu(index, chatListView.currentIndex);
            }
        }
    }

    ScrollBar{
        width: 7;
        height: chatListView.height;
        anchors.top: tabBar.bottom;
        anchors.right: parent.right;
        scrollArea: chatListView;
    }

    Item{
        id: toolBarChat;
        width: parent.width;
        height: 27;
        anchors.bottom: textEdit.top;
        BorderImage{
            width: parent.width;
            height: parent.height + 14;
            y: -7;
            source: "images/bar.sci";
        }

        Row{
            id: toolRow;
            anchors.left: parent.left;
            anchors.leftMargin: 3;
            y: 1;

            Button{
                id: historyButton;
                width: 24;
                height: 24;
                url: "qrc:/calendar-list.png";
                ContextualMenu{
                    id: contextualHistoryMenu;
                    posx: toolBarChat.x + toolRow.x + historyButton.x + 2;
                    posy: toolBarChat.y - 68;
                    button: Qt.LeftButton;
                    ListModel{
                        id: historyMenu;
                        Component.onCompleted:{
                            historyMenu.append({icon: "qrc:/calendar-list.png", name: qsTr("Download next 30 messages")});
                            historyMenu.append({icon: "qrc:/calendar-list.png", name: qsTr("Download next 50 messages")});
                            historyMenu.append({icon: "qrc:/calendar-list.png", name: qsTr("Download next 100 messages")});
                        }
                    }
                    model: historyMenu;
                    onChangeVisible:{
                        if (visible)
                            rect.state = "dark";
                        else
                            rect.state = "";
                    }
                    onItemClicked:{
                        chat.slotHistoryMenu(index);
                    }
                }
            }

            Button{
                id: smilesButton;
                width: 24;
                height: 24;
                url: "qrc:/smiley-grin.png";
            }

            Button{
                id: profileButton;
                width: 24;
                height: 24;
                url: "qrc:/information.png";
                onClicked: chat.on_profileButton_clicked();
            }

            Button{
                id: filterButton;
                width: 24;
                height: 24;
                url: "qrc:/magnifier-zoom.png";
                onClicked: toolBar.settingsButtonClicked();
            }

            Button{
                id: translitButton;
                width: 24;
                height: 24;
                url: "qrc:/translate.png";
                onClicked: {
                    var temp = chatInputEdit.text;
                    chatInputEdit.text = "";
                    chat.on_translitButton_clicked(temp);
                }
            }

            Button{
                id: quoteButton;
                width: 24;
                height: 24;
                url: "qrc:/Icon_36.ico";
                onClicked: chat.on_quoteButton_clicked(chatListView.currentIndex);
            }
        }

        Row{
            id: toolRowR;
            anchors.right: parent.right;
            anchors.rightMargin: 3;
            y: 1;
            Button{
                id: clearButton;
                width: 24;
                height: 24;
                url: "qrc:/broom.png";
                onClicked: chat.on_clearButton_clicked();
            }

            Button{
                id: closeButton;
                width: 24;
                height: 24;
                url: "qrc:/cross-button.png";
                onClicked: {
                    tabBar.closeCurrentTab();
                }
            }
        }

    }

    Rectangle{
        id: textEdit;
        z:1;
        width: parent.width;
        height: 50;
        anchors.bottom: chatBotton.top;
        color: "#272626"

        Flickable {
            id: flick
            anchors.fill: parent;
            anchors.leftMargin: 3;
            anchors.rightMargin: 3;
            contentWidth: chatInputEdit.paintedWidth
            contentHeight: chatInputEdit.paintedHeight
            boundsBehavior: Flickable.StopAtBounds;
            flickableDirection: Flickable.VerticalFlick;
            clip: true

            function ensureVisible(r)
            {
                if (contentX >= r.x)
                    contentX = r.x;
                else if (contentX+width <= r.x+r.width)
                    contentX = r.x+r.width-width;
                if (contentY >= r.y)
                    contentY = r.y;
                else if (contentY+height <= r.y+r.height)
                    contentY = r.y+r.height-height;
            }

            TextEdit{
                id: chatInputEdit;
                width: flick.width;
                height: flick.height;
                focus: true;
                wrapMode: TextEdit.Wrap;
                font.pointSize: 9;
                color: "#ffffff";
                onCursorRectangleChanged: flick.ensureVisible(cursorRectangle)
                onTextChanged:{
                    countChar.text = 4096 - chatInputEdit.text.length;
                    chatListView.currentIndex = -1;
                }
                Keys.onPressed:{
                    if ((event.key == Qt.Key_Return) || (event.key == Qt.Key_Enter)){
                        if ((chat.sendMsgByCtEnter && (event.modifiers & Qt.ControlModifier)) ||
                            (!chat.sendMsgByCtEnter && !(event.modifiers & Qt.ControlModifier))){
                            chat.slotSendMsg(chatInputEdit.text);
                            chatInputEdit.text = "";
                            event.accepted = true;
                        }
                    }

                    if ((event.key == Qt.Key_Tab) && (event.modifiers & Qt.ControlModifier)){
                        tabBar.nextTab();
                        event.accepted = true;
                    }
                }
            }
        }

        ScrollBar{
            width: 7;
            height: flick.height;
            anchors.top: flick.top;
            anchors.right: parent.right;
            scrollArea: flick;
        }
    }

    Item{
        id: chatBotton;
        z:1;
        width: parent.width;
        height: 27;
        anchors.bottom: parent.bottom;
        BorderImage{
            width: parent.width;
            height: parent.height + 14;
            y: -7;
            source: "images/bar.sci";
        }

        Row{
            id: bottonRow;
            anchors.left: parent.left;
            anchors.leftMargin: 3;
            y: 1;

            Button{
                id: userButton;
                width: 24;
                height: 24;
                url: "qrc:/Icon_10.ico";
                ContextualMenu{
                    id: contextualUserMenu;
                    posx: chatBotton.x + bottonRow.x + userButton.x + 2;
                    posy: chatBotton.y - 87;
                    button: Qt.LeftButton;
                    ListModel{
                        id: userMenu;
                        Component.onCompleted:{
                            userMenu.append({icon: "qrc:/Icon_46.ico", name: qsTr("VK page")});
                            userMenu.append({icon: "qrc:/image.png", name: qsTr("Photo")});
                            userMenu.append({icon: "qrc:/music-beam-16.png", name: qsTr("Music")});
                            userMenu.append({icon: "qrc:/film.png", name: qsTr("Video")});
                        }
                    }
                    model: userMenu;
                    onChangeVisible:{
                        if (visible)
                            rect.state = "dark";
                        else
                            rect.state = "";
                    }
                    onItemClicked:{
                        chat.slotUserMenu(index);
                    }
                }
            }

            Button{
                id: sendByCtEnterButton;
                width: 24;
                height: 24;
                url: "qrc:/key_enter.png";
                onClicked:{
                    chat.on_sendByCtEnterButton_clicked();
                }
            }

            Text{
                anchors.verticalCenter: parent.verticalCenter;
                color: "white";
                font.pointSize: 8;
                text: qsTr("Symbols left: ");
            }

            Text{
                id: countChar;
                anchors.verticalCenter: parent.verticalCenter;
                color: "white";
                font.pointSize: 8;
                text: "4096";
            }
        }

        Row{
            id: bottonRowR;
            anchors.right: parent.right;
            anchors.rightMargin: 3;
            y: 2;
            Button{
                id: sendButton;
                height: 23;
                autoRaise: false;
                url: "qrc:/mail--arrow.png";
                text: qsTr("Send");
                onClicked:{
                    chat.slotSendMsg(chatInputEdit.text);
                    chatInputEdit.text = "";
                }
            }
        }
    }

    Connections{
        target: chat;
        onScrollToBottom:{
            chatListView.positionViewAtIndex(chatListView.count - 1, ListView.End);
        }

        onAddTab:{
            tabBar.addTab(icon, uid, label);
        }

        onCurrentTab:{
            tabBar.currentTab(uid);
        }

        onInputEditAppend:{
            if (uid == "currentUid"){
                chatInputEdit.text = chatInputEdit.text + text;
                chatInputEdit.cursorPosition = chatInputEdit.text.length;
            } else {
                tabBar.setInputEditText(uid, text);
            }
        }

        onSetTabIcon:{
            tabBar.setTabIcon(uid, icon);
        }

        onClearTab:{
            tabBar.clearTab();
        }
    }
}
