/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0

Item{
    id: container;

    signal clicked;

    property string text;
    property string url;
    property bool autoRaise: true;
    property bool checked: false;

    width: parent.width ? parent.width : buttonImage.width + (label.width > 30 ? label.width : 40) + 15;
    height: parent.height;

    BorderImage{
        id: border;
        width: container.width;
        height: container.height;
        opacity: autoRaise ? 0 : 1;
        source: "images/toolbutton.sci";
    }

    Image{
        id: buttonImage;
        x: (container.height - height) / 2;
        y: (container.height - height) / 2;
        width: 16;
        height: 16;
        smooth: true;
        source: url;
    }

    Text{
        id: label;
        anchors.left: buttonImage.right;
        anchors.leftMargin: 5;
        anchors.verticalCenter: parent.verticalCenter;
        color: "white";
        font.pointSize: 8;
        font.bold: true;
        style: Text.Raised;
        styleColor: "black";
        text: container.text;
    }

    MouseArea{
        id: mouseRegion;
        anchors.fill: parent;
        hoverEnabled: true;
        onClicked:{
            container.clicked();
        }
        onEntered: container.state = "Entered";
        onExited: container.state = "";
    }

    states:[
        State {
            name: "Entered";
            PropertyChanges {
                target: border;
                opacity: 1;
            }
        },
        State{
            name: "Pressed";
            when: mouseRegion.pressed == true;
            PropertyChanges{
                target: buttonImage;
                scale: 1.2;
            }
        }
    ]

    transitions: Transition{
        NumberAnimation{
            target:  border;
            properties: "opacity";
            duration: 200;
        }
    }
}
