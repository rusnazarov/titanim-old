/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0

Item{
    id: toolBar;

    signal listsButtonClicked;
    signal offlineButtonClicked;
    signal soundButtonClicked;
    signal settingsButtonClicked;
    signal homeButtonClicked;
    signal aboutButtonClicked;

    BorderImage{
        width: parent.width;
        height: parent.height + 14;
        y: -7;
        source: "images/bar.sci";
    }

    Item{
        id: container;
        width: parent.width;
        height: parent.height;
        anchors.fill: parent;

        Row{
            id: toolRow;
            anchors.left: parent.left;
            anchors.leftMargin: 8;
            anchors.verticalCenter: parent.verticalCenter;

            Button{
                id: mainButton;
                width: 24;
                height: 24;
                url: "qrc:/logo.png";
                ContextualMenu{
                    id: contextualMainMenu;
                    posx: toolBar.x + toolRow.x + mainButton.x + 2;
                    posy: toolBar.y + toolRow.y + mainButton.y + mainButton.height;
                    button: Qt.LeftButton;
                    ListModel{
                        id: mainMenu;
                        Component.onCompleted: {
                            mainMenu.append({icon: "qrc:/wrench-screwdriver.png", name: qsTr("Settings")});
                            mainMenu.append({icon: "qrc:/broom.png", name: qsTr("Clear cache")});
                            mainMenu.append({icon: "qrc:/key.png", name: qsTr("Forget Password")});
                            mainMenu.append({icon: "qrc:/users.png", name: qsTr("Change profile")});
                            mainMenu.append({icon: "qrc:/information.png", name: qsTr("About TitanIM")});
                            mainMenu.append({icon: "qrc:/door.png", name: qsTr("Quit")});
                        }
                    }
                    model:mainMenu;
                    onChangeVisible:{
                        if (visible)
                            rect.state = "dark";
                        else
                            if (!progressLabel.text.length) rect.state = "";
                    }
                    onItemClicked:{
                        titanim.slotMainMenu(index);
                    }
                }
            }

            Button{
                id: listsButton;
                width: 24;
                height: 24;
                url: "qrc:/ui-menu-blue.png";
                onClicked: toolBar.listsButtonClicked();
            }

            Button{
                id: filterButton;
                width: 24;
                height: 24;
                url: "qrc:/magnifier-zoom.png";
            }

            Button{
                id: offlineButton;
                width: 24;
                height: 24;
                url: "qrc:/Icon_29.png";
                onClicked: toolBar.offlineButtonClicked();
            }

            Button{
                id: soundButton;
                width: 24;
                height: 24;
                url: titanim.sound ? "qrc:/Icon_9.ico" : "qrc:/Icon_7.ico";
                onClicked: toolBar.soundButtonClicked();
            }

            Button{
                id: settingsButton;
                width: 24;
                height: 24;
                url: "qrc:/wrench-screwdriver.png";
                onClicked: toolBar.settingsButtonClicked();
            }

            Button{
                id: homeButton;
                width: 24;
                height: 24;
                url: "qrc:/home.png";
                onClicked: toolBar.homeButtonClicked();
            }

            Button{
                id: aboutButton;
                width: 24;
                height: 24;
                url: "qrc:/information.png";
                onClicked: toolBar.aboutButtonClicked();
            }
        }
    }
}
