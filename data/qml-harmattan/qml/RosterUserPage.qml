/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0

Page{
    id: rosterUserPage;
    property bool friendsUserFinished: false;
    property string uid: "";
    property bool mutual: false;
    orientationLock: PageOrientation.LockPortrait;

    function load(userId, isMutual){
        if (userId != uid || isMutual != mutual){
            uid = userId;
            mutual = isMutual;
            clear();
            titanim.slotRosterUserGet(uid, mutual);
        }
    }

    function clear(){
        titanim.slotRosterUserClear();
        friendsUserFinished = false;
    }

    TitleBar{
        id: titleBar;

        MouseArea{
            anchors.fill: parent;
            onClicked: {
                rosterUser.positionViewAtBeginning();
            }
        }

        BorderImage{
            id: btnBack;
            width: 140;
            anchors.left: parent.left;
            anchors.leftMargin: 10;
            anchors.verticalCenter: parent.verticalCenter;
            source: "images/btn_back.sci";

            Text {
                anchors.centerIn: parent;
                color: "white";
                text: qsTr("Back");
                font.pixelSize: 21;
                font.bold: true;
            }
            MouseArea{
                anchors.fill: parent;
                onClicked: {
                    if (profilePage.uid != uid){
                        profilePage.clear();
                        profilePage.showProfile = true;
                        titanim.slotShowProfile(uid);
                        titanim.slotWallGet(uid);
                        profilePage.uid = uid;
                    }
                    pageStack.pop();
                }
            }
        }

        Label{
            id: labelName;
            anchors.left: btnBack.right;
            anchors.leftMargin: 5;
            anchors.right: parent.right;
            anchors.rightMargin: 5;
            anchors.verticalCenter: parent.verticalCenter;
            color: "white";
            text: mutual ? qsTr("Mutual friends") : qsTr("All friends");
            horizontalAlignment: Text.AlignHCenter;
            font.pixelSize: 27;
            font.bold: true;
        }
    }

    Component{
        id: heading;
        Item{
            height: textEdit.height;
            width: textEdit.width;
            Rectangle{
                id: textEdit;
                height: chatInputEdit.height + 16;
                width: rosterUserPage.width;
                color: "#C5C7CB";
                gradient: Gradient {
                    GradientStop {
                        position: 0.00;
                        color: "#e0e0e0";
                    }
                    GradientStop {
                        position: 1.00;
                        color: "#c0c1c4";
                    }
                }

                TextField {
                    id: chatInputEdit;
                    height: 50;
                    anchors.verticalCenter: parent.verticalCenter;
                    anchors.left: parent.left;
                    anchors.leftMargin: 10;
                    anchors.right: text.length ? clearButton.left : parent.right;
                    anchors.rightMargin: 10;
                    placeholderText: qsTr("Search");
                    inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText;
                    platformSipAttributes: SipAttributes {
                        actionKeyLabel: qsTr("Search");
                    }

                    Image{
                        id: iconSearch;
                        anchors.right: parent.right;
                        anchors.rightMargin: 15;
                        anchors.verticalCenter: parent.verticalCenter;
                        source: "images/ic_search.png";
                        MouseArea {
                            anchors.fill: parent
                            onClicked: chatInputEdit.text = "";
                        }
                    }

                    Keys.onReturnPressed: {
                        titanim.slotPressedKeyRosterUser(text);
                        clearButton.forceActiveFocus();
                    }

                    onTextChanged:{
                        if (!text.length)
                            titanim.slotPressedKeyRosterUser("");
                    }
                }

                Button {
                    id: clearButton;
                    platformStyle: ButtonStyle {
                        fontPixelSize: 20;
                    }
                    width: 100;
                    anchors.bottom: chatInputEdit.bottom;
                    anchors.right: parent.right;
                    anchors.rightMargin: 10;
                    visible: chatInputEdit.text.length;
                    text: qsTr("Cancel");
                    onClicked: {
                        chatInputEdit.text = "";
                        titanim.slotPressedKeyRosterUser("");
                        rosterUser.forceActiveFocus();
                    }
                }

                Rectangle{
                    width:  parent.width;
                    height: 1;
                    anchors.bottom: parent.bottom;
                    color: "#868686"
                }
            }
        }
    }

    ListView{
        id: rosterUser;
        width: parent.width;
        anchors.top: titleBar.bottom;
        anchors.bottom: parent.bottom;
        anchors.bottomMargin: 15;
        highlight: HighlightListView{}
        highlightMoveSpeed: -1;
        header: !mutual ? heading : null;
        model: rosterUserModel;
        delegate: ContactDelegate{}
        currentIndex: -1;

        Label{
            id: rosterUserLabel;
            anchors.centerIn: parent;
            visible: !rosterUser.count && friendsUserFinished;
            font.pixelSize: 25;
            color: "#606060";
            text: qsTr("No friends found");
        }
    }

    ScrollDecorator {
        flickableItem: rosterUser;
    }

    BusyIndicator {
        id: busyIndicator;
        platformStyle: BusyIndicatorStyle {
            size: "large";
        }
        anchors.centerIn: rosterUser;
        running: !rosterUser.count && !friendsUserFinished;
        visible: !rosterUser.count && !friendsUserFinished;
    }

    Connections{
        target: titanim;
        onFriendsUserFinished:{
            friendsUserFinished = true;
        }

        onStatusChanged:{
            if (!isOnline){
                friendsUserFinished = false;
            }
        }
    }
}
