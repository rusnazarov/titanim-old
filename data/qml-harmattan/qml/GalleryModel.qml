/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0
import QtMobility.gallery 1.1

DocumentGalleryModel{
    id: galleryModel;
    autoUpdate: true;
    limit: 100;
    rootType:DocumentGallery.Image;
    properties: ["url"];
    sortProperties: ["-dateTaken"];
}
