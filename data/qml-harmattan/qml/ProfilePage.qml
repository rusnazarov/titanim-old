/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0

Page{
    id: profilePage;
    orientationLock: PageOrientation.LockPortrait;

    property string uid: "";
    property bool wallUserFinished: false;
    property bool isNextLoad: false;
    property bool showProfile: false;

    function clear(){
        wallUserFinished = false;
        showProfile = false;
        titanim.slotPhotosClear();
        titanim.slotWallUserClear();
        profile.slotClear();
    }

    TitleBar{
        id: titleBar;

        MouseArea{
            anchors.fill: parent;
            onClicked: {
                wall.positionViewAtBeginning();
            }
        }

        BorderImage{
            id: btnBack;
            width: 140;
            anchors.left: parent.left;
            anchors.leftMargin: 10;
            anchors.verticalCenter: parent.verticalCenter;
            source: "images/btn_back.sci";

            Text {
                anchors.centerIn: parent;
                color: "white";
                text: qsTr("Back");
                font.pixelSize: 21;
                font.bold: true;
            }
            MouseArea{
                anchors.fill: parent;
                onClicked: {
                    pageStack.pop();
                    wallUserFinished = false;
                }
            }
        }

        Label{
            anchors.left: btnBack.right;
            anchors.leftMargin: 5;
            anchors.right: imgProfile.left;
            anchors.rightMargin: 5;
            anchors.verticalCenter: parent.verticalCenter;
            color: "white";
            text: showProfile ? qsTr("Profile") : qsTr("Page");
            horizontalAlignment: Text.AlignHCenter;
            font.pixelSize: 27;
            font.bold: true;
        }

        Image{
            id: imgProfile;
            anchors.right: parent.right;
            anchors.rightMargin: 7;
            anchors.verticalCenter: parent.verticalCenter;
            source: "images/ic_profile_more_sel.png";

            MouseArea{
                anchors.fill: parent;
                onClicked: {
                    showProfile = !showProfile;
                    wall.positionViewAtBeginning();
                }
            }
        }
    }

    Component{
        id: heading;
        Item{
            width: profilePage.width;
            height: photoRow.visible ? profileTop.height + photoRow.height + profileBottom.height + 20 : profileTop.height;
            Rectangle{
                id: profileTop;
                width: parent.width;
                height: (uid != titanim.profileUid) && (profile.canWriteMessage) ? avatar.height + btnSendMessage.height + 25 : avatar.height + 20;
                z: 1;
                color: "#C5C7CB";
                gradient: Gradient {
                    GradientStop {
                        position: 0.00;
                        color: "#e0e0e0";
                    }
                    GradientStop {
                        position: 1.00;
                        color: "#c0c1c4";
                    }
                }

                Rectangle{
                    id: avatar;
                    width: (avatarImg.width + 2);
                    height: (avatarImg.height + 2);
                    anchors.top: parent.top;
                    anchors.topMargin: 10;
                    anchors.left: parent.left;
                    anchors.leftMargin: 10;
                    color: "black";

                    Image{
                        id: avatarImg;
                        width: titanim.iconSize + 30;
                        height: titanim.iconSize + 30;
                        anchors.centerIn: parent;
                        smooth: true;
                        source: profile.photo;
                    }

                    MouseArea{
                        anchors.fill: parent;
                        onClicked: {
                            photoViewerPage.photoSource = profile.photoBig;
                            mainView.pageStack.push(photoViewerPage);
                        }
                    }
                }

                Column{
                    anchors.verticalCenter: avatar.verticalCenter;
                    anchors.left: avatar.right;
                    anchors.leftMargin: 20;
                    Text{
                        id: nameText;
                        width: text.length ? Math.min(nameText.implicitWidth, profilePage.width - avatar.width - 60) : 0;
                        font.pointSize: titanim.fontPointSize + 2;
                        font.bold: true;
                        elide: Text.ElideRight;
                        text: profile.name;

                        Image{
                            anchors.left: parent.right;
                            anchors.leftMargin: 7;
                            anchors.verticalCenter: parent.verticalCenter;
                            anchors.verticalCenterOffset: 2;
                            visible: profile.status;
                            source: "images/ic_online_up.png";
                        }

                        onTextChanged: { //bug qml
                            nameText.width = text.length ? Math.min(nameText.implicitWidth, profilePage.width - avatar.width - 60) : 0;
                        }
                    }
                    Text{
                        id: activity;
                        width: text.length ? profilePage.width - avatar.width - 35 : 0;
                        color: "#505050";
                        font.pointSize: titanim.fontPointSize + 1;
                        elide: Text.ElideRight;
                        text: profile.status ? profile.activity : profile.lastSeen;
                    }
                }

                Button{
                    id: btnSendMessage;
                    anchors.left: parent.left;
                    anchors.leftMargin: 10;
                    anchors.right: parent.right;
                    anchors.rightMargin: 10;
                    anchors.top: avatar.bottom;
                    anchors.topMargin: 10;
                    visible: (uid != titanim.profileUid) && (profile.canWriteMessage);
                    text: qsTr("Send message");
                    onClicked: {
                        titanim.slotOpenChatWindow(uid);
                        mainView.pageStack.push(chatPage);
                    }
                }

                Rectangle{
                    width:  parent.width;
                    height: 1;
                    anchors.bottom: parent.bottom;
                    color: "#868686"
                }
            }

            Row{
                id: photoRow;
                anchors.top: profileTop.bottom;
                anchors.topMargin: 10;
                anchors.left: parent.left;
                anchors.leftMargin: 11;
                anchors.right: parent.right;
                anchors.rightMargin: 11;
                height: visible ? implicitHeight : 0;
                visible: image1.status != Image.Null || image2.status != Image.Null || image3.status != Image.Null || image4.status != Image.Null;
                spacing: 10;

                Rectangle{
                    width: 107;
                    height: 107;
                    color: "black";
                    Image{
                        id: image1;
                        anchors.fill: parent;
                        smooth: true;
                        fillMode: Image.PreserveAspectFit;
                        source: photoModel.img1;
                    }
                }

                Rectangle{
                    width: 107;
                    height: 107;
                    color: "black";
                    Image{
                        id: image2;
                        anchors.fill: parent;
                        smooth: true;
                        fillMode: Image.PreserveAspectFit;
                        source: photoModel.img2;
                    }
                }

                Rectangle{
                    width: 107;
                    height: 107;
                    color: "black";
                    Image{
                        id: image3;
                        anchors.fill: parent;
                        smooth: true;
                        fillMode: Image.PreserveAspectFit;
                        source: photoModel.img3;
                    }
                }

                Rectangle{
                    width: 107;
                    height: 107;
                    color: "black";
                    Image{
                        id: image4;
                        anchors.fill: parent;
                        smooth: true;
                        fillMode: Image.PreserveAspectFit;
                        source: photoModel.img4;
                    }
                }

                MouseArea{
                    anchors.fill: parent;
                    onClicked: {
                        titanim.slotPhotosGetAll(uid);
                        photosPage.uid = uid;
                        mainView.pageStack.push(photosPage);
                    }
                }
            }

            Rectangle{
                id: profileBottom;
                width: parent.width;
                height: statusInputEdit.visible ? statusInputEdit.height + 6 : 37;
                anchors.bottom: parent.bottom;
                visible: photoRow.visible;
                color: "#C5C7CB";
                gradient: Gradient {
                    GradientStop {
                        position: 0.00;
                        color: "#e0e0e0";
                    }
                    GradientStop {
                        position: 1.00;
                        color: "#c0c1c4";
                    }
                }

                Rectangle{
                    width:  parent.width;
                    height: 1;
                    anchors.top: parent.top;
                    visible: !statusInputEdit.visible;
                    color: "#868686"
                }

                Text {
                    id: wallText;
                    anchors.left: parent.left;
                    anchors.leftMargin: 12;
                    anchors.verticalCenter: parent.verticalCenter;
                    visible: !statusInputEdit.visible;
                    color: "#505050";
                    font.pointSize: 15;
                    font.bold: true;
                    text: qsTr("Wall");
                }

                Image{
                    id: wallImgPlus;
                    anchors.right: parent.right;
                    anchors.rightMargin: 12;
                    anchors.verticalCenter: parent.verticalCenter;
                    visible: profile.canPost && !statusInputEdit.visible;
                    source: "images/ic_wall_plus_up.png";

                    MouseArea{
                        width: 50;
                        height: 50;
                        anchors.centerIn: parent;
                        onClicked: {
                            statusInputEdit.visible = true;
                            statusInputEdit.forceActiveFocus();
                        }
                    }
                }

                TextArea {
                    id: statusInputEdit;
                    height: 54;
                    anchors.left: parent.left;
                    anchors.leftMargin: 5;
                    anchors.right: sendButton.left;
                    anchors.rightMargin: 4;
                    anchors.bottom: parent.bottom;
                    anchors.bottomMargin: 7;
                    visible: false;
                    wrapMode: TextEdit.Wrap;
                    placeholderText: qsTr("Type your text here...");
                    inputMethodHints: Qt.ImhNoPredictiveText; //sorry

                    onActiveFocusChanged:{
                        if (!activeFocus){
                            visible = false;
                        }
                    }
                }

                Button {
                    id: sendButton;
                    width: 90;
                    anchors.bottom: statusInputEdit.bottom;
                    anchors.right: parent.right;
                    anchors.rightMargin: 6;
                    opacity: statusInputEdit.visible ? 1 : 0; //bug qml
                    text: statusInputEdit.text.length ? qsTr("Send") : "";
                    iconSource: statusInputEdit.text.length ? "" : "images/ic_add_photo.png";
                    onClicked: {
                        if (statusInputEdit.text.length){
                            titanim.wallPost(statusInputEdit.text, uid, "", 0);
                            statusInputEdit.text = "";
                        } else {
                            photosLocalSheet.uid = uid;
                            photosLocalSheet.state = "Grid";
                            timer.start();
                        }
                    }

                    Timer{
                        id: timer;
                        interval: 200;
                        onTriggered: {
                            photosLocalSheet.open();
                        }
                    }
                }

                Rectangle{
                    width:  parent.width;
                    height: 1;
                    anchors.bottom: parent.bottom;
                    color: "#868686"
                }
            }
        }
    }

    Component{
        id: footerUpd;
        Row{
            height: 70;
            anchors.left: parent.left;
            anchors.leftMargin: 35;
            spacing: 30;

            BusyIndicator {
                id: updIndicator;
                anchors.verticalCenter: parent.verticalCenter;
                platformStyle: BusyIndicatorStyle {
                    size: "medium";
                }
                running: isNextLoad;
            }

            Text {
                id: textUpdate;
                anchors.left: parent.left;
                anchors.leftMargin: 70;
                anchors.verticalCenter: parent.verticalCenter;
                text: qsTr("Loading...");
                font.pixelSize: 20
                font.bold: true;
                color: "#505050";
            }
        }
    }

    ListView{
        id: wall;
        width: parent.width;
        anchors.top: titleBar.bottom;
        anchors.bottom: parent.bottom;
        anchors.bottomMargin: 15;
        visible: !profileView.visible;
        enabled: !profileView.visible;
        highlight: HighlightListView{}
        highlightMoveSpeed: -1;
        header: heading;
        footer: isNextLoad ? footerUpd : null;
        model: wallUserModel;
        delegate: WallDelegate{}
        currentIndex: -1;

        onAtYEndChanged: {
            if (wall.atYEnd && !isNextLoad && wall.count >= 5){ //todo
                isNextLoad = true;
                titanim.slotWallGetNext(uid);
            }
        }

        Label{
            id: wallLabel;
            anchors.centerIn: parent;
            visible: !wall.count && wallUserFinished;
            font.pixelSize: 25;
            color: "#606060";
            text: qsTr("There are no posts on this wall yet");
        }

        ScrollDecorator {
            flickableItem: wall;
        }

        BusyIndicator {
            id: busyIndicator;
            platformStyle: BusyIndicatorStyle {
                size: "large";
            }
            anchors.centerIn: wall;
            anchors.verticalCenterOffset: 50;
            running: !wall.count && !wallUserFinished;
            visible: !wall.count && !wallUserFinished;
        }
    }

    Flickable{
        id: profileView;
        width: parent.width;
        anchors.top: titleBar.bottom;
        anchors.bottom: parent.bottom;
        anchors.bottomMargin: 15;
        visible: showProfile;
        enabled: showProfile;
        contentWidth: profileTop.width;
        contentHeight: profileTop.height;
        flickableDirection: Flickable.VerticalFlick;
        Item{
            width: profilePage.width;
            height: profileTop.height + labelCol.height + buttonCol.height + buttonCol2.height;
            Rectangle{
                id: profileTop;
                width: profilePage.width;
                height: avatar.height + 20;
                z: 1;
                color: "#C5C7CB";
                gradient: Gradient {
                    GradientStop {
                        position: 0.00;
                        color: "#e0e0e0";
                    }
                    GradientStop {
                        position: 1.00;
                        color: "#c0c1c4";
                    }
                }

                Rectangle{
                    id: avatar;
                    width: (avatarImg.width + 2);
                    height: (avatarImg.height + 2);
                    anchors.top: parent.top;
                    anchors.topMargin: 10;
                    anchors.left: parent.left;
                    anchors.leftMargin: 10;
                    color: "black";

                    Image{
                        id: avatarImg;
                        width: titanim.iconSize + 30;
                        height: titanim.iconSize + 30;
                        anchors.centerIn: parent;
                        smooth: true;
                        source: profile.photo;
                    }

                    MouseArea{
                        anchors.fill: parent;
                        onClicked: {
                            photoViewerPage.photoSource = profile.photoBig;
                            mainView.pageStack.push(photoViewerPage);
                        }
                    }
                }

                Column{
                    anchors.verticalCenter: avatar.verticalCenter;
                    anchors.left: avatar.right;
                    anchors.leftMargin: 20;
                    Text{
                        id: nameText;
                        width: text.length ? Math.min(nameText.implicitWidth, profilePage.width - avatar.width - 60) : 0;
                        font.pointSize: titanim.fontPointSize + 2;
                        font.bold: true;
                        elide: Text.ElideRight;
                        text: profile.name;

                        Image{
                            anchors.left: parent.right;
                            anchors.leftMargin: 7;
                            anchors.verticalCenter: parent.verticalCenter;
                            anchors.verticalCenterOffset: 2;
                            visible: profile.status;
                            source: "images/ic_online_up.png";
                        }

                        onTextChanged: { //bug qml
                            nameText.width = text.length ? Math.min(nameText.implicitWidth, profilePage.width - avatar.width - 60) : 0;
                        }
                    }
                    Text{
                        id: activity;
                        width: text.length ? profilePage.width - avatar.width - 35 : 0;
                        color: "#505050";
                        font.pointSize: titanim.fontPointSize + 1;
                        elide: Text.ElideRight;
                        text: profile.activity;
                    }
                }

                Rectangle{
                    width:  parent.width;
                    height: 1;
                    anchors.bottom: parent.bottom;
                    color: "#868686"
                }
            }

            Column{
                id: labelCol;
                anchors.top: profileTop.bottom;
                anchors.topMargin: 15;
                anchors.left: parent.left;
                anchors.leftMargin: 40;
                anchors.right: parent.right;
                anchors.rightMargin: 25;
                spacing: 15;

                Column{
                    width: profile.bDate ? parent.width : 0;
                    visible: profile.bDate;
                    Text{
                        width: parent.width;
                        color: "black";
                        font.bold: true;
                        font.pointSize: titanim.fontPointSize + 1;
                        text: qsTr("Birthday");
                    }
                    Text{
                        width: parent.width;
                        color: "#505050";
                        font.pointSize: titanim.fontPointSize + 1;
                        text: profile.bDate;
                    }
                }

                Column{
                    width: profile.mPhone ? parent.width : 0;
                    visible: profile.mPhone;
                    Text{
                        width: parent.width;
                        color: "black";
                        font.bold: true;
                        font.pointSize: titanim.fontPointSize + 1;
                        text: qsTr("Mobile phone");
                    }
                    Text{
                        width: parent.width;
                        color: "#505050";
                        font.pointSize: titanim.fontPointSize + 1;
                        text: profile.mPhone;

                        MouseArea{
                            anchors.fill: parent;
                            onClicked: {
                                titanim.slotCallPhone(profile.mPhone);
                            }
                        }
                    }
                }
            }

            ButtonColumn{
                id: buttonCol;
                anchors.top: labelCol.bottom;
                anchors.topMargin: 20;
                anchors.left: parent.left;
                anchors.leftMargin: 16;
                anchors.right: parent.right;
                anchors.rightMargin: 16;
                exclusive: false;

                Button{
                    height: 69;
                    platformStyle: ButtonStyle{ fontPixelSize: 24; horizontalAlignment: Text.AlignLeft }
                    iconSource: " ";
                    text: qsTr("Friends");
                    onClicked: {
                        if (!profile.friendsCount){
                            return;
                        }

                        if (uid != titanim.profileUid){
                            rosterUserPage.load(uid, false);
                            mainView.pageStack.push(rosterUserPage);
                        } else {
                            mainView.pageStack.clear();
                            mainView.pageStack.push(rosterPage);
                            tabBar.currentTab = 4;
                        }
                    }

                    Text{
                        id: friendsCount;
                        anchors.right: parent.right;
                        anchors.rightMargin: 30;
                        anchors.verticalCenter: parent.verticalCenter;
                        font.pixelSize: 23;
                        color: "#909090";
                        text: profile.friendsCount;
                    }
                }

                Button{
                    height: 69;
                    platformStyle: ButtonStyle{ fontPixelSize: 24; horizontalAlignment: Text.AlignLeft }
                    visible: uid != titanim.profileUid;
                    iconSource: " ";
                    text: qsTr("Mutual friends");
                    onClicked: {
                        if (!profile.mutualFriendsCount){
                            return;
                        }

                        rosterUserPage.load(uid, true);
                        mainView.pageStack.push(rosterUserPage);
                    }

                    Text{
                        id: mutualFriendsCount;
                        anchors.right: parent.right;
                        anchors.rightMargin: 30;
                        anchors.verticalCenter: parent.verticalCenter;
                        font.pixelSize: 23;
                        color: "#909090";
                        text: profile.mutualFriendsCount;
                    }
                }
            }

            ButtonColumn{
                id: buttonCol2;
                anchors.top: buttonCol.bottom;
                anchors.topMargin: 15;
                anchors.left: parent.left;
                anchors.leftMargin: 16;
                anchors.right: parent.right;
                anchors.rightMargin: 16;
                exclusive: false;

                Button{
                    height: 69;
                    platformStyle: ButtonStyle{ fontPixelSize: 24; horizontalAlignment: Text.AlignLeft }
                    iconSource: " ";
                    text: qsTr("Photos");
                    onClicked: {
                        if (!profile.photosCount){
                            return;
                        }

                        titanim.slotPhotosGetAll(uid);
                        photosPage.uid = uid;
                        mainView.pageStack.push(photosPage);
                    }

                    Text{
                        id: photosCount;
                        anchors.right: parent.right;
                        anchors.rightMargin: 30;
                        anchors.verticalCenter: parent.verticalCenter;
                        font.pixelSize: 23;
                        color: "#909090";
                        text: profile.photosCount;
                    }
                }

                Button{
                    height: 69;
                    platformStyle: ButtonStyle{ fontPixelSize: 24; horizontalAlignment: Text.AlignLeft }
                    iconSource: " ";
                    text: qsTr("Music");
                    onClicked: {
                        if (!profile.audiosCount){
                            return;
                        }

                        if (uid != titanim.profileUid){
                            audioPage.uid = uid;
                            mainView.pageStack.push(audioPage);
                        } else {
                            audioPage.uid = uid;
                            mainView.pageStack.clear();
                            mainView.pageStack.push(audioPage);
                            tabBar.currentTab = 3;
                        }
                    }

                    Text{
                        id: audiosCount;
                        anchors.right: parent.right;
                        anchors.rightMargin: 30;
                        anchors.verticalCenter: parent.verticalCenter;
                        font.pixelSize: 23;
                        color: "#909090";
                        text: profile.audiosCount;
                    }
                }

                Button{
                    height: 69;
                    platformStyle: ButtonStyle{ fontPixelSize: 24; horizontalAlignment: Text.AlignLeft }
                    iconSource: " ";
                    text: qsTr("Videos");
                    onClicked: {
                        if (!profile.videosCount){
                            return;
                        }

                        videoPage.clear();
                        titanim.slotVideoGet(uid);
                        videoPage.uid = uid;
                        mainView.pageStack.push(videoPage);
                    }

                    Text{
                        id: videosCount;
                        anchors.right: parent.right;
                        anchors.rightMargin: 30;
                        anchors.verticalCenter: parent.verticalCenter;
                        font.pixelSize: 23;
                        color: "#909090";
                        text: profile.videosCount;
                    }
                }
            }
        }
    }

    Connections{
        target: titanim;
        onWallUserFinished:{
            wallUserFinished = true;
            isNextLoad = false;
        }

        onStatusChanged:{
            if (!isOnline){
                wallUserFinished = false;
                showProfile = false;
            }
        }
    }
}
