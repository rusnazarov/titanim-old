/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0

Page{
    id: newsPage;
    property string dateUpdate: Qt.formatDateTime(new Date(), "dd.MM.yyyy hh:mm");
    property bool newsFinished: false;
    property bool isUpdate: false;
    property bool isNextLoad: false;
    property int currentViewIndex: -1;
    tools: tabBar;
    orientationLock: PageOrientation.LockPortrait;

    TitleBar{
        id: titleBar;
        Label{
            anchors.centerIn: parent;
            color: "white";
            text: qsTr("News");
            font.pixelSize: 27;
            font.bold: true;
        }

        MouseArea{
            anchors.fill: parent;
            onClicked: {
                news.positionViewAtBeginning();
                currentViewIndex = 0;
            }
        }
    }

    Row{
        id: update;
        property bool on: false;
        height: 70;
        y: -news.visibleArea.yPosition * Math.max(news.contentHeight, news.height);
        anchors.left: parent.left;
        anchors.leftMargin: 35;
        visible: !isUpdate && newsFinished;
        spacing: 30;

        Image{
            id: iconUpdate;
            anchors.top: parent.top;
            source: "images/ic_pull_arrow.png";

            Behavior on rotation {
                NumberAnimation {
                    easing.type: Easing.InCirc;
                }
            }
        }

        Column{
            spacing: 6;
            anchors.verticalCenter: parent.verticalCenter;
            Text {
                id: textUpdate;
                text: qsTr("Pull down to refresh...");
                font.pixelSize: 20
                font.bold: true;
                color: "#505050";
            }
            Text {
                id: textDateUpdate;
                text:  qsTr("Last updated: ") + dateUpdate;
                font.pixelSize: 18
                color: "#606060";
            }
        }

        onYChanged: {
            if(update.y > 110){
                iconUpdate.rotation = 180;
                on = true;
                textUpdate.text = qsTr("Release to refresh...");
            } else {
                if (on && !isUpdate && newsFinished){
                    isUpdate = true;
                    titanim.slotNewsfeedGet();
                    dateUpdate = Qt.formatDateTime(new Date(), "dd.MM.yyyy hh:mm");
                }
                iconUpdate.rotation = 0;
                on = false;
                textUpdate.text = qsTr("Pull down to refresh...");
            }
        }
    }

    Component{
        id: heading;
        Row{
            height: 70;
            anchors.left: parent.left;
            anchors.leftMargin: 35;
            spacing: 30;

            BusyIndicator {
                id: updIndicator;
                anchors.verticalCenter: parent.verticalCenter;
                platformStyle: BusyIndicatorStyle {
                    size: "medium";
                }
                running: isUpdate;
            }

            Column{
                spacing: 6;
                anchors.left: parent.left;
                anchors.leftMargin: 70;
                anchors.verticalCenter: parent.verticalCenter;
                Text {
                    id: textUpdate;
                    text: qsTr("Updating...");
                    font.pixelSize: 20
                    font.bold: true;
                    color: "#505050";
                }
                Text {
                    id: textDateUpdate;
                    text:  qsTr("Last updated: ") + dateUpdate;
                    font.pixelSize: 18
                    color: "#606060";
                }
            }
        }
    }

    Component{
        id: footerUpd;
        Row{
            height: 70;
            anchors.left: parent.left;
            anchors.leftMargin: 35;
            spacing: 30;

            BusyIndicator {
                id: updIndicator;
                anchors.verticalCenter: parent.verticalCenter;
                platformStyle: BusyIndicatorStyle {
                    size: "medium";
                }
                running: isNextLoad;
            }

            Text {
                id: textUpdate;
                anchors.left: parent.left;
                anchors.leftMargin: 70;
                anchors.verticalCenter: parent.verticalCenter;
                text: qsTr("Loading...");
                font.pixelSize: 20
                font.bold: true;
                color: "#505050";
            }
        }
    }

    ListView{
        id: news;
        width: parent.width;
        anchors.top: titleBar.bottom;
        anchors.bottom: parent.bottom;
        anchors.bottomMargin: 15;
        highlight: HighlightListView{}
        highlightMoveSpeed: -1;
        header: isUpdate ? heading : null;
        footer: isNextLoad ? footerUpd : null;
        model: newsModel;
        delegate: NewsDelegate{}
        currentIndex: -1;

        onAtYEndChanged: {
            if (news.atYEnd && !isNextLoad && news.count >= 5){ //todo
                isNextLoad = true;
                titanim.slotNewsfeedGetNext();
            }
        }

        onContentYChanged:{
            currentViewIndex = news.indexAt(0, news.contentY + 10);
        }

        Label{
            id: newsLabel;
            anchors.centerIn: parent;
            visible: !news.count && newsFinished;
            font.pixelSize: 25;
            color: "#606060";
            text: qsTr("No news");
        }
    }

    onCurrentViewIndexChanged:{
        if (currentViewIndex != -1 && currentViewIndex < tabBar.countNews){
            titanim.setIndexReadNews(currentViewIndex);
            tabBar.countNews = currentViewIndex;
        }
    }

    ScrollDecorator {
        flickableItem: news;
    }

    BusyIndicator {
        id: busyIndicator;
        platformStyle: BusyIndicatorStyle {
            size: "large";
        }
        anchors.centerIn: news;
        running: !news.count && !newsFinished;
        visible: !news.count && !newsFinished;
    }

    Connections{
        target: titanim;
        onNewsFinished:{
            if (!newsFinished && titanim.saveLastReadNew){
                currentViewIndex = titanim.indexLastNew;
                titanim.setIndexReadNews(currentViewIndex);
                tabBar.countNews = currentViewIndex;
                news.positionViewAtIndex(currentViewIndex, ListView.Beginning);
            }
            newsFinished = true;
            isUpdate = false;
            isNextLoad = false;
        }

        onStatusChanged:{
            if (!isOnline){
                newsFinished = false;
                isUpdate = false;
                isNextLoad = false;
                tabBar.countNews = 0;
            }
        }
    }
}
