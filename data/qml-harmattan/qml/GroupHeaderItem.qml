/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0

Item{
    id: groupHeaderItem;

    property string label: "";

    width: parent.width;
    height: 40;

    Text{
        id: headerLabel;
        anchors.left: parent.left;
        anchors.leftMargin: 3;
        anchors.bottom: parent.bottom;
        anchors.bottomMargin: 2;
        text: label;
        font.bold: true;
        font.pointSize: 18;
        color: theme.inverted ? "#4D4D4D" : "#3C3C3C";
    }

    Image{
        anchors.left: headerLabel.right;
        anchors.right: parent.right;
        anchors.verticalCenter: headerLabel.verticalCenter;
        anchors.leftMargin: 15;
        source: "image://theme/meegotouch-groupheader" + (theme.inverted ? "-inverted" : "") + "-background";
    }
}
