/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0
import com.nokia.meego 1.0

Item{
    id: audioItem;

    property string aid: "";
    property string oid: "";
    property string artist: "";
    property string title: "";
    property string durationStr: "";
    property int duration: 0;
    property string url: "";

    height: name.height + textBody.height + progressBar.height;

    MouseArea{
        anchors.fill: parent;
        onClicked: {
            audioPage.playUrlAudio(url);
        }
    }

    Row{
        id: audioRow;
        height: parent.height;
        width: parent.width;
        spacing: 15;

        Image{
            id: icon;
            anchors.verticalCenter: parent.verticalCenter;
            smooth: true;
            source: (audioPage.audioUrl == url) && (!audioPage.playMusicPaused) && (audioPage.musicPlaying) ?
                        "images/ic_pause_list_up.png" : "images/ic_play_list_up.png";
        }

        Column{
            width: parent.width - icon.width - audioRow.spacing - iconAdd.width;
            anchors.verticalCenter: parent.verticalCenter;
            Row{
                Text{
                    id: name;
                    width: textBody.width;
                    color: "black";
                    font.pointSize: titanim.fontPointSize - 1;
                    font.bold: true;
                    elide: Text.ElideRight;
                    text: title;
                }

                Text{
                    id: durationText;
                    anchors.bottom: name.bottom;
                    color: "#505050";
                    font.pointSize: titanim.fontPointSize - 2;
                    text: durationStr;
                }
            }

            Text{
                id: textBody;
                width: parent.width - durationText.width - 7;
                color: "#505050";
                font.pointSize: titanim.fontPointSize - 1;
                elide: Text.ElideRight;
                text: artist;
            }

            ProgressBar{
                id: progressBar;
                anchors.left: textBody.left;
                anchors.right: textBody.right;
                visible: audioPage.audioUrl == url;
                minimumValue: 0;
                maximumValue: duration * 1000;
                value: audioPage.audioPosition;
                indeterminate: audioPage.audioIndeterminate;
            }
        }
    }

    Image{
        id: iconAdd;
        anchors.verticalCenter: parent.verticalCenter;
        anchors.right: parent.right;
        smooth: true;
        source: "images/ic_attach_add_down.png";

        MouseArea{
            anchors.fill: parent;
            onClicked: {
                iconAdd.visible = false;
                audioCpp.slotAudioAdd(aid, oid, "");
            }
        }
    }
}
