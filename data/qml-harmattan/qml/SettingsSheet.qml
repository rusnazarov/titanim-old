/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.0

Sheet{
    id: settingsSheet;

    buttons:[
        SheetButton {
            id: rejectBtn;
            anchors.left: parent.left;
            anchors.leftMargin: 15;
            anchors.verticalCenter: parent.verticalCenter;
            text: qsTr("Close");
            onClicked:{
                settings.emitSettingsChanged();
                close();
            }
        },
        SheetButton {
            id: acceptBtn;
            anchors.right: parent.right;
            anchors.rightMargin: 15;
            anchors.verticalCenter: parent.verticalCenter;
            platformStyle: SheetButtonAccentStyle {}
            text: qsTr("Save");
            onClicked:{
                settings.emitSettingsChanged();
                close();
            }
        }
    ]

    content: Flickable{
        anchors.fill: parent;
        anchors.leftMargin: 10;
        anchors.rightMargin: 10;
        anchors.topMargin: 3;
        anchors.bottomMargin: 12;
        contentWidth: col.width;
        contentHeight: col.height;
        flickableDirection: Flickable.VerticalFlick;

        Column{
            id: col;
            width: settingsSheet.width - 20;
            anchors.top: parent.top;
            spacing: 10;

            GroupHeaderItem{
                label: qsTr("System");
            }

            CheckBox{
                width: parent.width;
                text: qsTr("Show status bar");
                checked: settings.showStatusBar;
                onCheckedChanged: {
                    mainView.showStatusBar = checked;
                    settings.setVisibleStatusBar(checked);
                }
            }

            CheckBox{
                width: parent.width;
                text: qsTr("Native");
                checked: settings.isNativToolBar;
                onCheckedChanged: {
                    settings.setVisibleNativToolBar(checked);
                }
            }

            CheckBox{
                width: parent.width;
                text: qsTr("Integrating event feed");
                checked: titanim.newsfeedEnabled;
                onCheckedChanged: {
                    settings.setNewsFeedEnabled(checked);
                }
            }

            Button{
                width: parent.width - 60;
                anchors.horizontalCenter: parent.horizontalCenter;
                text: qsTr("Clear cache");
                onClicked: {
                    titanim.slotDeleteCacheDir();
                }
            }

            TumblerButton{
                width: parent.width - 60;
                anchors.horizontalCenter: parent.horizontalCenter;
                text: qsTr("Language");

                SelectionDialog{
                    id: languageDialog;
                    titleText: qsTr("Language");
                    selectedIndex: settings.languageCurrentIndex;

                    model: ListModel{
                        id: languageModel;
                        Component.onCompleted: {
                            for (var i=0; i < settings.language.length; i++){
                                languageModel.append({name: settings.language[i]});
                            }
                        }
                    }

                    onAccepted:{
                        settings.setLanguage(settings.language[selectedIndex]);
                    }
                }

                onClicked: {
                    languageDialog.open();
                }
            }

            GroupHeaderItem{
                label: qsTr("General");
            }

            CheckBox{
                width: parent.width;
                text: qsTr("Save last read news");
                checked: titanim.saveLastReadNew;
                onCheckedChanged: {
                    settings.setSaveLastReadNew(checked);
                }
            }

            Column {
                width: parent.width;
                height: 65;
                spacing: -10;

                Label {
                    anchors.horizontalCenter: parent.horizontalCenter;
                    text: qsTr("Avatar size:");
                }

                Slider{
                    id: sizePhoto;
                    width: parent.width;
                    stepSize: 1;
                    valueIndicatorVisible: true;
                    minimumValue: 20;
                    maximumValue: 100;
                    value: titanim.iconSize;

                    onPressedChanged: {
                        if(!pressed)
                            settings.setIconSize(value);
                    }
                }
            }

            Column {
                width: parent.width;
                height: 65;
                spacing: -10;

                Label {
                    anchors.horizontalCenter: parent.horizontalCenter;
                    text: qsTr("Text size:");
                }

                Slider{
                    id: sizeText;
                    width: parent.width;
                    stepSize: 1;
                    valueIndicatorVisible: true;
                    minimumValue: 12;
                    maximumValue: 22;
                    value: titanim.fontPointSize;

                    onPressedChanged: {
                        if(!pressed)
                            settings.setTextSize(value);
                    }
                }
            }

            GroupHeaderItem{
                label: qsTr("Contact List");
            }

            CheckBox{
                width: parent.width;
                text: qsTr("Sort by name");
                checked: titanim.sortByName;
                onCheckedChanged: {
                    settings.setSortByName(checked);
                }
            }

            CheckBox{
                width: parent.width;
                text: qsTr("Show statuses");
                checked: titanim.showActivity;
                onCheckedChanged: {
                    settings.setShowActivity(checked);
                }
            }

            GroupHeaderItem{
                label: qsTr("Chat window");
            }

            CheckBox{
                width: parent.width;
                text: qsTr("Send typing notifications");
                checked: chat.messagesActivity;
                onCheckedChanged: {
                    settings.setMessagesActivity(checked);
                }
            }

            GroupHeaderItem{
                label: qsTr("Notifications");
            }

            CheckBox{
                width: parent.width;
                text: qsTr("Sound");
                checked: titanim.sound;
                onCheckedChanged: {
                    titanim.setSound(checked);
                }
            }

            CheckBox{
                width: parent.width;
                text: qsTr("Vibrate");
                checked: titanim.vibro;
                onCheckedChanged: {
                    settings.setVibro(checked);
                }
            }

            Button{
                width: parent.width;
                text: qsTr("Change profile");
                onClicked: {
                    titanim.slotMainMenu(3);
                    close();
                }
            }

        }
    }

    QueryDialog {
        id: restartDialog;
        icon: "images/logo.png";
        titleText: qsTr("Restart TitanIM");
        message: qsTr("You must restart TitanIM for the changes to take effect. Restart now?");
        acceptButtonText: qsTr("OK");
        rejectButtonText: qsTr("Cancel");

        onAccepted: {
            settings.restartApp();
        }
    }

    Connections{
        target: settings;
        onShowRestartDialog:{
            restartDialog.open();
        }
    }
}
