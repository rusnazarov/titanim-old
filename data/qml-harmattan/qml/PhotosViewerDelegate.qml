/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0

Rectangle{
    id: photosViewerDelegate;
    width: photosViewer.width;
    height: photosViewer.height;
//    ListView.delayRemove: photo.status == Image.Ready ? false : true;
    color: "black";

    Flickable {
        id: flick;
        z:2;
        anchors.fill: parent;
        contentWidth: parent.width;
        contentHeight: parent.height;

        PinchArea {
            width: Math.max(flick.contentWidth, flick.width)
            height: Math.max(flick.contentHeight, flick.height)

            property real initialWidth
            property real initialHeight
            onPinchStarted: {
                initialWidth = flick.contentWidth
                initialHeight = flick.contentHeight
            }

            onPinchUpdated: {
                flick.contentX += pinch.previousCenter.x - pinch.center.x
                flick.contentY += pinch.previousCenter.y - pinch.center.y
                flick.resizeContent(Math.max(initialWidth * pinch.scale, flick.width), Math.max(initialHeight * pinch.scale, flick.height), pinch.center)
            }

            onPinchFinished: {
                flick.returnToBounds()
            }

            Rectangle{
                width: flick.contentWidth
                height: flick.contentHeight
                color: "black";
                Image {
                    id: photo;
                    anchors.fill: parent;
                    opacity: 0;
                    smooth: true;
                    fillMode: Image.PreserveAspectFit;
                    source: model.srcBig ? model.srcBig : modelData;
                    Behavior on opacity {
                        NumberAnimation {
                            easing.type: Easing.InOutQuad;
                        }
                    }
                }

                MouseArea{
                    anchors.fill: parent;
                    onClicked: {
                        flick.contentWidth = flick.width;
                        flick.contentHeight = flick.height;
                        pageStack.pop();
                    }

                    onPressAndHold: {
                        photoMenu.src = model.srcBig ? model.srcBig : modelData;
                        photoMenu.open();
                    }
                }

                BusyIndicator{
                    id: busyIndicator;
                    platformStyle: BusyIndicatorStyle {
                        size: "large";
                        inverted: true;
                    }
                    anchors.centerIn: parent;
                    running: photo.status != Image.Ready;
                    visible: photo.status != Image.Ready;
                }
            }
        }
    }

    states:[
        State{
            name: "Show";
            when: photo.status == Image.Ready;
            PropertyChanges {
                target: photo;
                opacity: 1;
            }
        }
    ]
}
