/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0

Page{
    id: captchaPage;
    property string captchaSid: "";
    property string captchaUrl: "";

    TitleBar{
        id: titleBar;
        Label{
            anchors.centerIn: parent;
            color: "white";
            text: qsTr("Captcha");
            font.pixelSize: 27;
            font.bold: true;
        }
    }

    Column{
        spacing: 15;
        anchors.centerIn: parent;
        Image {
            id: captchaImg;
            width: sourceSize.width + 30;
            height: sourceSize.height + 30;
            anchors.horizontalCenter: captchaText.horizontalCenter;
            smooth: true;
            source: captchaUrl;
        }

        TextField {
            id: captchaText;
            width: 230;
            placeholderText: qsTr("Enter captcha");
            inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhEmailCharactersOnly | Qt.ImhNoPredictiveText;
            platformSipAttributes: SipAttributes {
                actionKeyLabel: qsTr("Send");
            }

            Keys.onReturnPressed: {
                titanim.slotCaptchaDone(captchaSid, captchaText.text);
                mainView.pageStack.pop();
            }
        }
    }
}
