/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0

Item{
    id: wallDelegate;
    width: wallDelegate.ListView.view.width;
    height: avatarBorder.height > column.height ? avatarBorder.height + 20 : column.height + 20;
    //ListView.delayRemove: avatar.status == Image.Ready ? false : true;

    MouseArea{
        anchors.fill: parent;
        onClicked: {
            wallCommentsPage.name = model.display;
            wallCommentsPage.avatarUrl = model.decoration;
            wallCommentsPage.date = model.dateStr;
            wallCommentsPage.textWall = model.text;
            wallCommentsPage.postId = model.id;
            wallCommentsPage.sourceId = model.toId;
            wallCommentsPage.fromId = model.fromId;
            wallCommentsPage.photosList = model.photos;
            wallCommentsPage.photosBigList = model.photosBig;
            wallCommentsPage.commentsCount = model.comments;
            wallCommentsPage.canPost = model.canPost;
            wallCommentsPage.likesCount = model.likes;
            wallCommentsPage.isLiked = model.isLiked;
            wallCommentsPage.isRetweet = false;
            titanim.slotWallClearComments();
            titanim.slotWallGetComments(model.id, model.toId);
            mainView.pageStack.push(wallCommentsPage);
        }
    }

    Row{
        id: wallRow;
        height: parent.height;
        x: 10;
        spacing: 15;

        Rectangle{
            id: avatarBorder;
            width: (avatar.width + 2);
            height: (avatar.height + 2);
            anchors.top: parent.top;
            anchors.topMargin: 10;
            scale: 0.0;
            color: avatar.height > 16 ? "black" : "transparent";
            Behavior on scale {
                NumberAnimation {
                    easing.type: Easing.InOutQuad;
                }
            }

            Image{
                id: avatar;
                width: titanim.iconSize + 10;
                height: titanim.iconSize + 10;
                anchors.centerIn: parent;
                smooth: true;
                source: model.decoration;
            }

            MouseArea{
                anchors.fill: parent;
                onClicked: {
                    if (model.fromId > 0){
                        profilePage.clear();
                        titanim.slotShowProfile(model.fromId);
                        titanim.slotWallGet(model.fromId);
                        profilePage.uid = model.fromId;
                        mainView.pageStack.push(profilePage);
                    }
                }
            }
        }

        Column{
            id: column;
            anchors.top: avatarBorder.top;
            spacing: 7;

            Text{
                id: name;
                width: text.length ? wallDelegate.width - avatarBorder.width - wallRow.x - wallRow.spacing - 40 : 0;
                color: "#254e84";
                font.pointSize: titanim.fontPointSize + 1;
                font.bold: true;
                elide: Text.ElideRight;
                text: model.display;

                MouseArea{
                    anchors.fill: parent;
                    onClicked: {
                        if (model.fromId > 0){
                            profilePage.clear();
                            titanim.slotShowProfile(model.fromId);
                            titanim.slotWallGet(model.fromId);
                            profilePage.uid = model.fromId;
                            mainView.pageStack.push(profilePage);
                        }
                    }
                }
            }

            Text{
                id: textBody;
                width: text.length ? wallDelegate.width - avatarBorder.width - wallRow.x - wallRow.spacing - 50 : 0;
                clip: true;
                color: "black";
                font.pointSize: titanim.fontPointSize - 1;
                wrapMode: Text.Wrap;
                text: model.shortText;
            }

            Flow{
                id: flowPhotos;
                property variant photosList: model.photos;
                property variant photosBigList: model.photosBig;
                width: wallDelegate.width - avatarBorder.width - wallRow.x - wallRow.spacing - 50;
                spacing: 6;

                Repeater{
                    model: parent.photosList;
                    Image{
                        id: photoAttach;
                        smooth: true;
                        source: modelData;

                        MouseArea{
                            anchors.fill: parent;
                            onClicked: {
                                mainView.pageStack.push(photosViewerPage);
                                photosViewerPage.photosBigList = flowPhotos.photosBigList;
                                photosViewerPage.curIndex = index;
                            }
                        }
                    }
                }
            }

            Row{
                spacing: 5;
                visible: link.text.length;
                Image{
                    id: linkImg;
                    source: "images/ic_link_up.png";
                }

                Text{
                    id: link;
                    width: text.length ? wallDelegate.width - avatarBorder.width - wallRow.x - wallRow.spacing - 60 : 0;
                    anchors.top: linkImg.top;
                    anchors.topMargin: -2;
                    color: "#445f82"
                    font.pointSize: titanim.fontPointSize - 1;
                    elide: Text.ElideRight;
                    text: model.linkUrl;

                    MouseArea{
                        anchors.fill: parent;
                        onClicked: {
                            Qt.openUrlExternally(model.linkUrl);
                        }
                    }
                }
            }

            Row{
                spacing: 15;
                Text{
                    id: messageDate;
                    color: "#505050";
                    font.pointSize: titanim.fontPointSize - 2;
                    text: model.dateStr;
                }

                Row{
                    visible: likesCount.text;
                    spacing: 3;
                    Image{
                        id: likes;
                        source: "images/ic_like_up.png";
                    }

                    Text{
                        id: likesCount;
                        anchors.verticalCenter: likes.verticalCenter;
                        color: "#505050";
                        font.pointSize: titanim.fontPointSize - 2;
                        text: model.likes;
                    }
                }

                Row{
                    visible: commentsCount.text;
                    spacing: 3;
                    Image{
                        id: comments;
                        source: "images/ic_comment_up.png";
                    }

                    Text{
                        id: commentsCount;
                        anchors.verticalCenter: comments.verticalCenter;
                        color: "#505050";
                        font.pointSize: titanim.fontPointSize - 2;
                        text: model.comments;
                    }
                }
            }
        }
    }

    Image{
        id: arrow;
        opacity: 0.5;
        source: "image://theme/icon-m-common-drilldown-arrow" + (theme.inverted ? "-inverse" : "")
        anchors.right: parent.right;
        anchors.rightMargin: 10;
        anchors.verticalCenter: parent.verticalCenter;
    }

    Rectangle{
        width:  parent.width;
        height: 1;
        y: wallDelegate.height - 1;
        color: "#c0c0c0";
    }

    states:[
        State{
            name: "Show";
            when: avatar.status == Image.Ready;
            PropertyChanges {
                target: avatarBorder;
                scale: 1;
            }
        }
    ]
}
