/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0

Page{
    id: wallPage;
    property bool wallFinished: false;
    property bool isNextLoad: false;
    tools: tabBar;
    orientationLock: PageOrientation.LockPortrait;

    TitleBar{
        id: titleBar;

        MouseArea{
            anchors.fill: parent;
            onClicked: {
                wall.positionViewAtBeginning();
            }
        }

        Label{
            anchors.centerIn: parent;
            color: "white";
            text: qsTr("Wall");
            font.pixelSize: 27;
            font.bold: true;
        }

        BorderImage{
            id: btnSetting;
            width: labelSettings.width + 22;
            height: 68;
            anchors.right: parent.right;
            anchors.rightMargin: 10;
            anchors.verticalCenter: parent.verticalCenter;
            source: "images/btn_title.sci";

            Text {
                id: labelSettings;
                anchors.centerIn: parent;
                color: "white";
                text: qsTr("Settings");
                font.pixelSize: 19;
                font.bold: true;
            }

            SettingsSheet{
                id: settingsSheet;
            }

            MouseArea{
                anchors.fill: parent;
                onClicked: {
                    settingsSheet.open();
                }
            }
        }
    }

    Component{
        id: heading;
        Rectangle{
            width: wallPage.width;
            height: 10 + avatar.height + 8 + statusInputEdit.height + 4;
            z: 1;
            color: "#C5C7CB";
            gradient: Gradient {
                GradientStop {
                    position: 0.00;
                    color: "#e0e0e0";
                }
                GradientStop {
                    position: 1.00;
                    color: "#c0c1c4";
                }
            }

            Image{
                id: avatar;
                width: Math.max (titanim.iconSize, 50);
                height: Math.max (titanim.iconSize, 50);
                anchors.top: parent.top;
                anchors.topMargin: 10;
                anchors.left: parent.left;
                anchors.leftMargin: 10;
                smooth: true;
                source: titanim.profilePhoto;
            }

            Text{
                id: nameText;
                anchors.top: avatar.top;
                anchors.left: avatar.right;
                anchors.leftMargin: 15;
                font.pointSize: titanim.fontPointSize + 2;
                font.bold: true;
                text: titanim.profileName;
            }

            ToolIcon {
                id: menu;
                anchors.right: parent.right;
                anchors.rightMargin: -5;
                anchors.verticalCenter: avatar.verticalCenter;
                opacity: 0.5;
                iconId: "toolbar-view-menu";
                onClicked: {
                    profilePage.clear();
                    titanim.slotShowProfile(titanim.profileUid);
                    titanim.slotWallGet(titanim.profileUid);
                    profilePage.uid = titanim.profileUid;
                    mainView.pageStack.push(profilePage);
                }
            }

            TextArea {
                id: statusInputEdit;
                height: 54;
                anchors.top: avatar.bottom;
                anchors.topMargin: 8;
                anchors.left: parent.left;
                anchors.leftMargin: 5;
                anchors.right: sendButton.left;
                anchors.rightMargin: 4;
                wrapMode: TextEdit.Wrap;
                placeholderText: qsTr("What's new?");
            }

            Button {
                id: sendButton;
                width: 90;
                anchors.bottom: statusInputEdit.bottom;
                anchors.right: parent.right;
                anchors.rightMargin: 6;
                text: statusInputEdit.activeFocus ? qsTr("Send") : "";
                iconSource: statusInputEdit.activeFocus ? "" : "images/ic_add_photo.png";
                onClicked: {
                    if (statusInputEdit.text.length){
                        titanim.wallPost(statusInputEdit.text);
                        statusInputEdit.text = "";
                    } else {
                        photosLocalSheet.uid = "";
                        photosLocalSheet.state = "Grid";
                        photosLocalSheet.open();
                    }
                }
            }

            Rectangle{
                width:  parent.width;
                height: 1;
                anchors.bottom: parent.bottom;
                color: "#868686"
            }
        }
    }

    Row{
        id: update;
        property bool on: false;
        property string dateUpdate: Qt.formatDateTime(new Date(), "dd.MM.yyyy hh:mm");
        height: 70;
        y: -wall.visibleArea.yPosition * Math.max(wall.contentHeight, wall.height);
        anchors.left: parent.left;
        anchors.leftMargin: 35;
        spacing: 30;

        Image{
            id: iconUpdate;
            anchors.top: parent.top;
            source: "images/ic_pull_arrow.png";

            Behavior on rotation {
                NumberAnimation {
                    easing.type: Easing.InCirc;
                }
            }
        }

        Column{
            spacing: 6;
            anchors.verticalCenter: parent.verticalCenter;
            Text {
                id: textUpdate;
                text: qsTr("Pull down to refresh...");
                font.pixelSize: 20
                font.bold: true;
                color: "#505050";
            }
            Text {
                id: textDateUpdate;
                text:  qsTr("Last updated: ") + update.dateUpdate;
                font.pixelSize: 18
                color: "#606060";
            }
        }

        onYChanged: {
            if(update.y > 110){
                iconUpdate.rotation = 180;
                on = true;
                textUpdate.text = qsTr("Release to refresh...");
            } else {
                if (on){
                    titanim.slotWallGet();
                    dateUpdate = Qt.formatDateTime(new Date(), "dd.MM.yyyy hh:mm");
                }
                iconUpdate.rotation = 0;
                on = false;
                textUpdate.text = qsTr("Pull down to refresh...");
            }
        }
    }

    Component{
        id: footerUpd;
        Row{
            height: 70;
            anchors.left: parent.left;
            anchors.leftMargin: 35;
            spacing: 30;

            BusyIndicator {
                id: updIndicator;
                anchors.verticalCenter: parent.verticalCenter;
                platformStyle: BusyIndicatorStyle {
                    size: "medium";
                }
                running: isNextLoad;
            }

            Text {
                id: textUpdate;
                anchors.left: parent.left;
                anchors.leftMargin: 70;
                anchors.verticalCenter: parent.verticalCenter;
                text: qsTr("Loading...");
                font.pixelSize: 20
                font.bold: true;
                color: "#505050";
            }
        }
    }

    ListView{
        id: wall;
        width: parent.width;
        anchors.top: titleBar.bottom;
        anchors.bottom: parent.bottom;
        anchors.bottomMargin: 15;
        highlight: HighlightListView{}
        highlightMoveSpeed: -1;
        header: heading;
        footer: isNextLoad ? footerUpd : null;
        model: wallModel;
        delegate: WallDelegate{}
        currentIndex: -1;

        onAtYEndChanged: {
            if (wall.atYEnd && !isNextLoad && wall.count >= 5){ //todo
                isNextLoad = true;
                titanim.slotWallGetNext();
            }
        }

        Label{
            id: wallLabel;
            anchors.centerIn: parent;
            visible: !wall.count && wallFinished;
            font.pixelSize: 25;
            color: "#606060";
            text: qsTr("There are no posts on this wall yet");
        }
    }

    ScrollDecorator {
        flickableItem: wall;
    }

    BusyIndicator {
        id: busyIndicator;
        platformStyle: BusyIndicatorStyle {
            size: "large";
        }
        anchors.centerIn: wall;
        running: !wall.count && !wallFinished;
        visible: !wall.count && !wallFinished;
    }

    Connections{
        target: tabBar;
        onWallPageClicked:{
            if (!wallFinished && wall.count == 0)
                titanim.slotWallGet();
        }
    }

    Connections{
        target: titanim;
        onWallFinished:{
            wallFinished = true;
            isNextLoad = false;
        }

        onStatusChanged:{
            if (!isOnline){
                wallFinished = false;
                isNextLoad = false;
            }
        }
    }
}
