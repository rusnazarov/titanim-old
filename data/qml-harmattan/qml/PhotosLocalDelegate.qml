/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0

Rectangle{
    id: photosLocalDelegate;
    width: 110;
    height: 110;
    //ListView.delayRemove: photo.status == Image.Ready ? false : true;
    color: "#fafafa";

    Image{
        id: photo;
        anchors.fill: parent;
        asynchronous: true;
        opacity: 0;
        smooth: true;
        fillMode: Image.PreserveAspectCrop;
        clip: true;
        sourceSize.width: 200;
        source: model.url;

        Behavior on opacity {
            NumberAnimation {
                easing.type: Easing.InOutQuad;
            }
        }

        MouseArea{
            anchors.fill: parent;
            onClicked: {
                photosLocalSheet.sourceImg = model.url;
                photosLocalSheet.state = "Image";
            }
        }
    }

    states:[
        State{
            name: "Show";
            when: photo.status == Image.Ready;
            PropertyChanges {
                target: photo;
                opacity: 1;
            }
        }
    ]
}
