/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0

Item{
    id: dialogsDelegate;
    width: dialogsDelegate.ListView.view.width;
    height: avatarBorder.height > (name.height + textBody.height) ?  avatarBorder.height + 20 : name.height + textBody.height + 20;
    //ListView.delayRemove: avatar.status == Image.Ready ? false : true;

    MouseArea{
        anchors.fill: parent;
        onClicked: {
            dialogs.currentIndex = index;
            titanim.slotOpenChatWindow(model.uid);
            mainView.pageStack.push(chatPage);
        }
    }

    Row{
        id: dialog;
        height: parent.height;
        x: 10;
        spacing: 15;

        Rectangle{
            id: avatarBorder;
            width: (avatar.width + 2);
            height: (avatar.height + 2);
            anchors.verticalCenter: parent.verticalCenter;
            scale: 0.0;
            color: avatar.height > 16 ? "black" : "transparent";
            Behavior on scale {
                NumberAnimation {
                    easing.type: Easing.InOutQuad;
                }
            }
            Image{
                id: avatar;
                width: titanim.iconSize;
                height: titanim.iconSize;
                anchors.centerIn: parent;
                smooth: true;
                source: model.decoration;
            }

            MouseArea{
                anchors.fill: parent;
                onClicked: {
                    if (model.uid > 0){
                        profilePage.clear();
                        titanim.slotShowProfile(model.uid);
                        titanim.slotWallGet(model.uid);
                        profilePage.uid = model.uid;
                        mainView.pageStack.push(profilePage);
                    }
                }
            }
        }

        Column{
            anchors.verticalCenter: parent.verticalCenter;
            Text{
                id: name;
                width: text.length ? dialogsDelegate.width - avatarBorder.width - dialog.x - dialog.spacing - 40 : 0;
                color: "black";
                font.pointSize: titanim.fontPointSize;
                font.bold: true;
                elide: Text.ElideRight;
                text: model.display;

                Text{
                    id: messageDate;
                    anchors.right: parent.right;
                    anchors.bottom: name.bottom;
                    color: "#2b497a"
                    font.pointSize: titanim.fontPointSize - 2;
                    text: model.dateStr;
                }
            }
            Text{
                id: textBody;
                width: text.length ? dialogsDelegate.width - avatarBorder.width - dialog.x - dialog.spacing - 50 : 0;
                clip: true;
                color: "#505050";
                font.pointSize: titanim.fontPointSize - 1;
                wrapMode: Text.Wrap;
                text: model.text;
            }
        }
    }

    Image{
        id: arrow;
        opacity: 0.5;
        source: "image://theme/icon-m-common-drilldown-arrow" + (theme.inverted ? "-inverse" : "")
        anchors.right: parent.right;
        anchors.rightMargin: 10;
        anchors.verticalCenter: parent.verticalCenter
    }

    Rectangle {
        width:  parent.width;
        height: 1;
        y: dialogsDelegate.height - 1;
        color: "#c0c0c0"
    }

    states:[
        State{
            name: "Show";
            when: avatar.status == Image.Ready;
            PropertyChanges {
                target: avatarBorder;
                scale: 1;
            }
        }
    ]
}
