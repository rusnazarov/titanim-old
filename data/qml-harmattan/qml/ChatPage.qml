/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0

Page{
    id: chatPage;

    TitleBar{
        id: titleBar;
        visible: inPortrait;

        MouseArea{
            anchors.fill: parent;
            onClicked: {
                chatListView.positionViewAtBeginning();
            }
        }

        BorderImage{
            id: btnBack;
            width: 140;
            anchors.left: parent.left;
            anchors.leftMargin: 10;
            anchors.verticalCenter: parent.verticalCenter;
            source: "images/btn_back.sci";

            Text {
                anchors.centerIn: parent;
                color: "white";
                text: qsTr("Back");
                font.pixelSize: 21;
                font.bold: true;
            }
            MouseArea{
                anchors.fill: parent;
                onClicked: {
                    chatInputEdit.text = "";
                    pageStack.pop();
                    chat.slotCloseTab();
                }
            }
        }

        Label{
            id: labelName;
            anchors.left: btnBack.right;
            anchors.leftMargin: 5;
            anchors.right: imageTyping.left;
            anchors.rightMargin: 5;
            anchors.verticalCenter: parent.verticalCenter;
            color: "white";
            text: chat.nameCurrentContact;
            horizontalAlignment: Text.AlignHCenter;
            font.pixelSize: 27;
            font.bold: true;
        }

        Image{
            id: imageTyping;
            width: visible ? implicitWidth : 0;
            anchors.right: parent.right;
            anchors.rightMargin: visible ? 15 : 0;
            anchors.verticalCenter: parent.verticalCenter;
            visible: false;
            smooth: true;
            source: "images/ic_typing.png";
        }
    }

    ListView{
        id: chatListView;
        width: parent.width;
        anchors.top: titleBar.visible ? titleBar.bottom : parent.top;
        anchors.bottom: textEdit.top;
        highlight: HighlightListView{}
        highlightResizeSpeed: -1;
        highlightMoveSpeed: -1;
        model: chatModel;
        delegate: ChatDelegate{}
        currentIndex: -1;
        onCountChanged: {
            chatListView.positionViewAtIndex(chatListView.count - 1, ListView.End);
        }
        onHeightChanged: {
            chatListView.positionViewAtIndex(chatListView.count - 1, ListView.End);
        }

        Menu{
            id: chatMenu;
            visualParent: pageStack;
            MenuLayout{
                MenuItem {text: qsTr("Reply Quoted"); onClicked: chat.slotChatListMenu(0, chatListView.currentIndex);}
                MenuItem {text: qsTr("Copy"); onClicked: chat.slotChatListMenu(1, chatListView.currentIndex);}
                MenuItem {text: qsTr("Mark as Unread"); onClicked: chat.slotChatListMenu(3, chatListView.currentIndex);}
                MenuItem {text: qsTr("Open VK"); onClicked: chat.slotChatListMenu(4, chatListView.currentIndex);}
                MenuItem {text: qsTr("Delete"); onClicked: chat.slotChatListMenu(5, chatListView.currentIndex);}
            }
        }
    }

    ScrollDecorator {
        flickableItem: chatListView;
    }

    Rectangle{
        id: textEdit;
        height: chatInputEdit.height + 16;
        width: parent.width;
        anchors.bottom: parent.bottom;
        anchors.left: parent.left;
        color: "#C3C5C9";

        TextArea {
            id: chatInputEdit;
            height: 54;
            anchors.verticalCenter: parent.verticalCenter;
            anchors.left: parent.left;
            anchors.leftMargin: 5;
            anchors.right: sendButton.left;
            anchors.rightMargin: 4;
            wrapMode: TextEdit.Wrap;
            placeholderText: qsTr("Type your text here...");
            errorHighlight: text.length > 4096;

            onTextChanged:{
                if (!timerMessagesActivity.running){
                    chat.slotMessagesSetActivity();
                    timerMessagesActivity.start();
                }
            }
        }

        Button {
            id: sendButton;
            width: 100;
            anchors.bottom: chatInputEdit.bottom;
            anchors.right: parent.right;
            anchors.rightMargin: 6;
            text: qsTr("Send");
            onClicked: {
                chat.slotSendMsg(chatInputEdit.text);
                chatInputEdit.text = "";
            }
        }
    }

    Timer{
        id: timerTyping;
        interval: 10000;
        onTriggered: {
            imageTyping.visible = false;
        }
    }

    Timer{
        id: timerMessagesActivity;
        interval: 5000;
    }

    Connections{
        target: chat;
        onScrollToBottom:{
            chatListView.positionViewAtIndex(chatListView.count - 1, ListView.End);
        }

        onInputEditAppend:{
            chatInputEdit.text = chatInputEdit.text + text;
            chatInputEdit.cursorPosition = chatInputEdit.text.length;
            chatInputEdit.forceActiveFocus();
        }

        onUserTyping:{
            if (flag == 1){
                imageTyping.visible = true;
                timerTyping.restart();
            } else {
                imageTyping.visible = false;
                timerTyping.stop();
            }
        }
    }
}
