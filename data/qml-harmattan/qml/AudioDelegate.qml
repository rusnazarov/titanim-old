/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0
import com.nokia.meego 1.0

Item{
    id: audioDelegate;
    width: audioDelegate.ListView.view.width;
    height: progressBar.visible ? name.height + textBody.height + 30 : name.height + textBody.height + 20;

    MouseArea{
        anchors.fill: parent;
        onClicked: {
            audio.currentIndex = index;
            playAudio(index);
        }
    }

    Row{
        id: audioRow;
        height: parent.height;
        x: 12;
        spacing: 15;

        Image{
            id: avatarBorder;
            anchors.verticalCenter: parent.verticalCenter;
            smooth: true;
            source: (audioPage.audioUrl == model.url) && (!playMusic.paused) ? "images/ic_pause_list_up.png" : "images/ic_play_list_up.png";
        }

        Column{
            anchors.verticalCenter: parent.verticalCenter;
            Text{
                id: name;
                width: text.length ? audioDelegate.width - avatarBorder.width - audioRow.x - audioRow.spacing - durationText.width - iconAdd.width - 25 : 0;
                color: "black";
                font.pointSize: titanim.fontPointSize;
                font.bold: true;
                elide: Text.ElideRight;
                text: model.title;

                Text{
                    id: durationText;
                    anchors.right: parent.right;
                    anchors.rightMargin: - 45;
                    anchors.bottom: name.bottom;
                    color: "#505050";
                    font.pointSize: titanim.fontPointSize - 2;
                    text: model.durationStr;
                }
            }

            Text{
                id: textBody;
                width: text.length ? audioDelegate.width - avatarBorder.width - audioRow.x - audioRow.spacing - durationText.width - iconAdd.width - 40: 0;
                color: "#505050";
                font.pointSize: titanim.fontPointSize - 1;
                elide: Text.ElideRight;
                text: model.artist;
            }

            Item{
                height: progressBar.visible ? progressBar.implicitHeight + 5 : 0;
                anchors.left: name.left;
                anchors.right: parent.right;
                anchors.rightMargin: 5;
                ProgressBar{
                    id: progressBar;
                    width: parent.width;
                    anchors.bottom: parent.bottom;
                    visible: audioPage.audioUrl == model.url;
                    minimumValue: 0;
                    maximumValue: model.duration * 1000;
                    value: audioPage.audioPosition;
                    indeterminate: audioPage.audioIndeterminate;
                }
            }
        }
    }

    Image{
        id: iconAdd;
        width: visible ? 38 : 0;
        anchors.verticalCenter: parent.verticalCenter;
        anchors.right: parent.right;
        anchors.rightMargin: 7;
        smooth: true;
        visible: audioPage.isSearch || !audioPage.myAudio ? true : false;
        source: "images/ic_attach_add_down.png";

        MouseArea{
            anchors.fill: parent;
            onClicked: {
                iconAdd.visible = false;
                audioCpp.slotAudioAdd(model.aid, model.ownerId, "");
            }
        }
    }

    Rectangle {
        width:  parent.width;
        height: 1;
        y: audioDelegate.height - 1;
        color: "#c0c0c0"
    }
}
