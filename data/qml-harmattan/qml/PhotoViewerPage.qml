/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0

Page{
    id: photoViewerPage;

    property string photoSource: "";

    Rectangle{
        anchors.fill: parent;
        color: "black";
    }

    Image {
        id: viewer;
        anchors.fill: parent;
        fillMode: Image.PreserveAspectFit;
        smooth: true;
        source: photoSource;
    }

    MouseArea{
        anchors.fill: parent;
        onClicked: {
            pageStack.pop();
        }
    }

    BusyIndicator {
        id: busyIndicator;
        platformStyle: BusyIndicatorStyle {
            size: "large";
            inverted: true;
        }
        anchors.centerIn: parent;
        running: viewer.status != Image.Ready;
        visible: viewer.status != Image.Ready;
    }
}
