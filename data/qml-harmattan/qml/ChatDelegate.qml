/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0

Item{
    id: chatDelegate;
    width: parent.width;
    height: avatarBorder.height > column.height ?  avatarBorder.height + 12 : column.height + 12;
    //ListView.delayRemove: avatar.status == Image.Ready ? false : true;

    function replaceURLWithHTMLLinks(text){
        //thanks Sauron
        var exp = /(\b(https?|ftp|file):\/\/([-A-Z0-9+&@#%?=~_|!:,.;]*)([-A-Z0-9+&@#%?\/=~_|!:,.;]*)[-A-Z0-9+&@#\/%=~_|])/ig;
        return text.replace(exp, "<a href='$1' target='_blank'>$1</a>");
    }

    MouseArea{
        anchors.fill: parent;
        acceptedButtons: Qt.LeftButton | Qt.RightButton;
        onPressed: chatListView.currentIndex = index;
        onPressAndHold: {
            chatMenu.open();
        }
    }

    Rectangle{
        y: 1;
        anchors.fill: parent;
        opacity: (model.flags & 2) && (model.flags & 1) ? 0.2 : 0;
        color: "#999999";
    }

    Row{
        id: message;
        height: parent.height;
        x: 5;
        spacing: 8;

        Rectangle{
            id: avatarBorder;
            width: (avatar.width + 2);
            height: (avatar.height + 2);
            y:4;
            scale: 0.0;
            color: avatar.height > 16 ? "black" : "transparent";
            Behavior on scale {
                NumberAnimation {
                    easing.type: Easing.InOutQuad;
                }
            }
            Image{
                id: avatar;
                width: Math.max (titanim.iconSize - 10, 30);
                height: Math.max (titanim.iconSize - 10, 30);
                anchors.centerIn: parent;
                smooth: true;
                source: model.decoration;
            }

            MouseArea{
                anchors.fill: parent;
                onClicked: {
                    if (model.uid > 0){
                        profilePage.clear();
                        titanim.slotShowProfile(model.uid);
                        titanim.slotWallGet(model.uid);
                        profilePage.uid = model.uid;
                        mainView.pageStack.push(profilePage);
                    }
                }
            }
        }

        Column{
            id: column;
            y: 2;
            spacing: 3;
            Text{
                id: messageName;
                width: text.length ? chatDelegate.width - avatarBorder.width - message.x - message.spacing - 7 : 0;
//                color: model.flags & 2 ? "#254e84" : "#be4444";
                color: "#254e84";
                font.pointSize: titanim.fontPointSize;
                font.bold: true;
                elide: Text.ElideRight;
                text: model.name;

                MouseArea{
                    anchors.fill: parent;
                    onClicked: {
                        if (model.uid > 0){
                            profilePage.clear();
                            titanim.slotShowProfile(model.uid);
                            titanim.slotWallGet(model.uid);
                            profilePage.uid = model.uid;
                            mainView.pageStack.push(profilePage);
                        }
                    }
                }

                Text{
                    id: messageDate;
                    anchors.right: parent.right;
                    color: "#505050";
                    font.pointSize: titanim.fontPointSize - 2;
                    text:  Qt.formatDateTime(model.date, "dd.MM.yyyy hh:mm:ss");
                }
            }
            Text{
                id: messageText;
                width: text.length ? chatDelegate.width - avatarBorder.width - message.x - message.spacing - 7 : 0;
                color: "black";
                wrapMode: Text.Wrap;
                font.pointSize: titanim.fontPointSize;
                text: replaceURLWithHTMLLinks(model.display);
                onLinkActivated: Qt.openUrlExternally(link);
            }

            Flow{
                id: flowPhotos;
                property variant photosList: model.photos;
                property variant photosBigList: model.photosBig;
                width: chatDelegate.width - avatarBorder.width - message.x - message.spacing - 7;
                spacing: 6;

                Repeater{
                    model: parent.photosList;
                    Image{
                        id: photoAttach;
                        smooth: true;
                        source: modelData;

                        MouseArea{
                            anchors.fill: parent;
                            onClicked: {
                                mainView.pageStack.push(photosViewerPage);
                                photosViewerPage.photosBigList = flowPhotos.photosBigList;
                                photosViewerPage.curIndex = index;
                            }
                        }
                    }
                }
            }

            Column{
                id: columnAudios;
                property variant audiosList: model.audio;
                width: parent.width;
                spacing: 6;

                Repeater{
                    model: parent.audiosList;
                    AudioItem{
                        id: audioItem;
                        width: parent.width;
                        aid: modelData["aid"];
                        oid: modelData["oid"];
                        title: modelData["title"];
                        artist: modelData["artist"];
                        duration: modelData["duration"];
                        durationStr: modelData["durationStr"];
                        url: modelData["url"];
                    }
                }
            }
        }
    }

    states:[
        State{
            name: "Show";
            when: avatar.status == Image.Ready;
            PropertyChanges {
                target: avatarBorder;
                scale: 1;
            }
        }
    ]
}
