/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.0

Item{
    id: tools;

    property bool nativ: false;
    property bool labelVisible: true;
    property int currentTab: 0;
    property int countNews: 0;
    property int countMessages: 0;
    signal wallPageClicked;
    signal messagesPageClicked;
    signal audioPageClicked;

    width: parent.width;
    height: nativ || !labelVisible ? parent.height : 87;
    anchors.bottom: parent.bottom;

    Loader {
        anchors.fill: parent;
        sourceComponent: nativ ? nativTools : iosTools;
    }

    Component {
        id: iosTools;
        Rectangle{
            id: tabBar;
            width: parent.width;
            height: parent.height;
            z: 100;
            anchors.bottom: parent.bottom;
            color: "black";

            Row{
                x: 3;
                width: parent.width - 6;
                spacing: 7;

                Rectangle{
                    width: 89;
                    height: tabBar.height;
                    color: "transparent";
                    Column{
                        width: parent.width;
                        anchors.verticalCenter: parent.verticalCenter;
                        Image {
                            id: iconNews;
                            anchors.horizontalCenter: parent.horizontalCenter;
                            source: tools.currentTab == 0 ? "images/tab_news_down.png" : "images/tab_news_up.png";
                        }
                        Text {
                            id: textNews;
                            visible: tools.labelVisible;
                            anchors.horizontalCenter: iconNews.horizontalCenter;
                            text: qsTr("News");
                            color: tools.currentTab == 0 ? "white" : "#999999";
                            font.pixelSize: 17;
                        }
                    }
                    MouseArea{
                        anchors.fill: parent;
                        onClicked: {
                            mainView.pageStack.clear();
                            mainView.pageStack.push(newsPage);
                            tools.currentTab = 0;
                        }
                        onPressAndHold: {
                            titanim.slotMainMenu(5);
                        }
                    }

                    BorderImage{
                        id: badgeBackground;
                        source: "images/badge.png";
                        visible: countNews > 0;
                        smooth: true;
                        anchors.right: parent.right;
                        anchors.top: parent.top;
                        anchors.topMargin: 2;
                        width: Math.max(badgeBackground.sourceSize.width, badgeLabel.implicitWidth + 16);
                        height: Math.max(badgeBackground.sourceSize.height, badgeLabel.implicitHeight);
                        border {left: 10; top: 10; right: 10; bottom: 10}

                        Text{
                            id: badgeLabel;
                            font.pixelSize: 16;
                            anchors.centerIn: parent;
                            color: "white";
                            text: countNews;
                        }
                    }
                }

                Rectangle{
                    width: 89;
                    height: tabBar.height;
                    color: "transparent";
                    Column {
                        width: parent.width;
                        anchors.verticalCenter: parent.verticalCenter;
                        Image {
                            id: iconWall;
                            anchors.horizontalCenter: parent.horizontalCenter;
                            source: tools.currentTab == 1 ? "images/tab_profile_down.png" : "images/tab_profile_up.png";
                        }
                        Text {
                            id: textWall;
                            visible: tools.labelVisible;
                            anchors.horizontalCenter: iconWall.horizontalCenter;
                            text: qsTr("Wall");
                            color: tools.currentTab == 1 ? "white" : "#999999";
                            font.pixelSize: 17;
                        }
                    }
                    MouseArea{
                        anchors.fill: parent;
                        onClicked: {
                            mainView.pageStack.clear();
                            mainView.pageStack.push(wallPage);
                            tools.currentTab = 1;
                            tools.wallPageClicked();
                        }
                        onPressAndHold: {
                            titanim.slotMainMenu(0);
                        }
                    }
                }

                Rectangle{
                    width: 89;
                    height: tabBar.height;
                    color: "transparent";
                    Column {
                        width: parent.width;
                        anchors.verticalCenter: parent.verticalCenter;
                        Image {
                            id: iconMessages;
                            anchors.horizontalCenter: parent.horizontalCenter;
                            source: tools.currentTab == 2 ? "images/tab_messages_down.png" : "images/tab_messages_up.png";
                        }
                        Text {
                            id: textMessages;
                            visible: tools.labelVisible;
                            anchors.horizontalCenter: iconMessages.horizontalCenter;
                            text: qsTr("Messages");
                            color: tools.currentTab == 2 ? "white" : "#999999";
                            font.pixelSize: 17;
                        }
                    }
                    MouseArea{
                        anchors.fill: parent;
                        onClicked: {
                            mainView.pageStack.clear();
                            mainView.pageStack.push(messagesPage);
                            tools.currentTab = 2;
                            bannerMsg.hide();
                            tools.messagesPageClicked();
                        }
                        onPressAndHold: {
                            titanim.slotMainMenu(3);
                        }
                    }

                    BorderImage{
                        id: msgBackground;
                        source: "images/badge.png";
                        visible: countMessages > 0;
                        smooth: true;
                        anchors.right: parent.right;
                        anchors.top: parent.top;
                        anchors.topMargin: 2;
                        width: Math.max(msgBackground.sourceSize.width, badgeMsgLabel.implicitWidth + 16);
                        height: Math.max(msgBackground.sourceSize.height, badgeMsgLabel.implicitHeight);
                        border {left: 10; top: 10; right: 10; bottom: 10}

                        Text{
                            id: badgeMsgLabel;
                            font.pixelSize: 16;
                            anchors.centerIn: parent;
                            color: "white";
                            text: countMessages;
                        }
                    }
                }

                Rectangle{
                    width: 89;
                    height: tabBar.height;
                    color: "transparent";
                    Column {
                        width: parent.width;
                        anchors.verticalCenter: parent.verticalCenter;
                        Image {
                            id: iconAudio;
                            anchors.horizontalCenter: parent.horizontalCenter;
                            source: tools.currentTab == 3 ? "images/tab_audio_down.png" : "images/tab_audio_up.png";
                        }
                        Text {
                            id: textAudio;
                            visible: tools.labelVisible;
                            anchors.horizontalCenter: iconAudio.horizontalCenter;
                            text: qsTr("Audio");
                            color: tools.currentTab == 3 ? "white" : "#999999";
                            font.pixelSize: 17;
                        }
                    }
                    MouseArea{
                        anchors.fill: parent;
                        onClicked: {
                            audioPage.uid = titanim.profileUid;
                            mainView.pageStack.clear();
                            mainView.pageStack.push(audioPage);
                            tools.currentTab = 3;
                            tools.audioPageClicked();
                        }
                        onPressAndHold: {
                            titanim.slotMainMenu(4);
                        }
                    }
                }

                Rectangle{
                    width: 89;
                    height: tabBar.height;
                    color: "transparent";
                    Column {
                        width: parent.width;
                        anchors.verticalCenter: parent.verticalCenter;
                        Image {
                            id: iconFriends;
                            anchors.horizontalCenter: parent.horizontalCenter;
                            source: tools.currentTab == 4 ? "images/tab_friends_down.png" : "images/tab_friends_up.png";
                        }
                        Text {
                            id: textFriends;
                            visible: tools.labelVisible;
                            anchors.horizontalCenter: iconFriends.horizontalCenter;
                            text: qsTr("Friends");
                            color: tools.currentTab == 4 ? "white" : "#999999";
                            font.pixelSize: 17;
                        }
                    }
                    MouseArea{
                        anchors.fill: parent;
                        onClicked: {
                            mainView.pageStack.clear();
                            mainView.pageStack.push(rosterPage);
                            tools.currentTab = 4;
                        }
                        onPressAndHold: {
                            titanim.slotMainMenu(4);
                        }
                    }
                }
            }
        }
    }

    Component {
        id: nativTools;
        ToolBarLayout{
            id: commonTools;
            ButtonRow {
                TabButton{
                    checked: tools.currentTab == 0 ? true : false;
                    iconSource: "images/tab_nativ_news.png";

                    CountBubble{
                        anchors.right: parent.right;
                        anchors.rightMargin: 2;
                        anchors.top: parent.top;
                        anchors.topMargin: -4;
                        largeSized: true;
                        visible: countNews;
                        value: countNews;
                    }

                    onClicked: {
                        mainView.pageStack.clear();
                        mainView.pageStack.push(newsPage);
                        tools.currentTab = 0;
                    }
                }
                TabButton{
                    checked: tools.currentTab == 1 ? true : false;
                    iconSource: "images/tab_nativ_profile.png";
                    onClicked: {
                        mainView.pageStack.clear();
                        mainView.pageStack.push(wallPage);
                        tools.currentTab = 1;
                        tools.wallPageClicked();
                    }
                }
                TabButton{
                    iconSource: "images/tab_nativ_messages.png";
                    onClicked: {
                        mainView.pageStack.clear();
                        mainView.pageStack.push(messagesPage);
                        tools.currentTab = 2;
                        tools.messagesPageClicked();
                    }
                }
                TabButton{
                    iconSource: "images/tab_nativ_audio.png";
                    onClicked: {
                        audioPage.uid = titanim.profileUid;
                        mainView.pageStack.clear();
                        mainView.pageStack.push(audioPage);
                        tools.currentTab = 3;
                        tools.audioPageClicked();
                    }
                }
                TabButton{
                    iconSource: "images/tab_nativ_friends.png";
                    onClicked: {
                        mainView.pageStack.clear();
                        mainView.pageStack.push(rosterPage);
                        tools.currentTab = 4;
                    }
                }
            }
        }
    }
}
