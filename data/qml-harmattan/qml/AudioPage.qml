/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0
import QtMultimediaKit 1.1

Page{
    id: audioPage;
    property bool audioFinished: false;
    property bool isNextLoad: false;
    property string uid: "";
    property bool myAudio: true;
    tools: myAudio ? tabBar : null;
    orientationLock: PageOrientation.LockPortrait;

    property int audioIndex: -1;
    property int audioPosition: 0;
    property bool audioIndeterminate: false;
    property string audioUrl: "";
    property bool audioAutoPlay: false;
    property alias playMusicPaused: playMusic.paused;
    property alias musicPlaying: playMusic.playing;
    property bool isAudioUrl: false;
    property bool isSearch: false;
    property bool _isSearch: false;
    property string _searchText: "";

    function clear(){
        audioFinished = false;
        titanim.slotAudiosClear();
    }

    function playAudio(index){
        isAudioUrl = false;
        if (!audio.count)
            return;
        if (index >= audio.count || index < 0)
            index = 0;
        audioIndex = index;
        if (audioModel.urlAt(audioIndex) == audioUrl){
            if (playMusic.paused)
                playMusic.play();
            else
                playMusic.pause();
        } else {
            audioUrl = audioModel.urlAt(audioIndex);
            playMusic.source = audioUrl;
            playMusic.play();
        }
    }

    function playUrlAudio(url){
        isAudioUrl = true;
        if (url == audioUrl){
            if (playMusic.paused)
                playMusic.play();
            else
                playMusic.pause();
        } else {
            audioUrl = url;
            playMusic.source = audioUrl;
            playMusic.play();
        }
    }

    function playNext(){
        var idx;
        idx = audioIndex + 1;
        if (idx >= audio.count)
            idx = 0;
        audio.currentIndex = idx;
        playAudio(idx);
    }

    function playPrev(){
        var idx;
        idx = audioIndex - 1;
        if (idx < 0)
            idx = audio.count - 1;
        audio.currentIndex = idx;
        playAudio(idx);
    }

    TitleBar{
        id: titleBar;

        MouseArea{
            anchors.fill: parent;
            onClicked: {
                audio.positionViewAtBeginning();
            }
        }

        BorderImage{
            id: btnBack;
            width: 140;
            visible: !myAudio;
            anchors.left: parent.left;
            anchors.leftMargin: 10;
            anchors.verticalCenter: parent.verticalCenter;
            source: "images/btn_back.sci";

            Text {
                anchors.centerIn: parent;
                color: "white";
                text: qsTr("Back");
                font.pixelSize: 21;
                font.bold: true;
            }
            MouseArea{
                anchors.fill: parent;
                onClicked: {
                    pageStack.pop();
                }
            }
        }

        Label{
            anchors.left: btnBack.visible ? btnBack.right :  parent.left;
            anchors.leftMargin: 5;
            anchors.right: parent.right;
            anchors.rightMargin: 5;
            anchors.verticalCenter: parent.verticalCenter;
            color: "white";
            text: qsTr("Audio");
            horizontalAlignment: Text.AlignHCenter;
            font.pixelSize: 27;
            font.bold: true;
        }
    }

    Row{
        id: update;
        property bool on: false;
        property string dateUpdate: Qt.formatDateTime(new Date(), "dd.MM.yyyy hh:mm");
        height: 70;
        y: -audio.visibleArea.yPosition * Math.max(audio.contentHeight, audio.height);
        anchors.left: parent.left;
        anchors.leftMargin: 35;
        spacing: 30;

        Image{
            id: iconUpdate;
            anchors.top: parent.top;
            source: "images/ic_pull_arrow.png";

            Behavior on rotation {
                NumberAnimation {
                    easing.type: Easing.InCirc;
                }
            }
        }

        Column{
            spacing: 6;
            anchors.verticalCenter: parent.verticalCenter;
            Text {
                id: textUpdate;
                text: qsTr("Pull down to refresh...");
                font.pixelSize: 20
                font.bold: true;
                color: "#505050";
            }
            Text {
                id: textDateUpdate;
                text:  qsTr("Last updated: ") + update.dateUpdate;
                font.pixelSize: 18
                color: "#606060";
            }
        }

        onYChanged: {
            if(update.y > 110){
                iconUpdate.rotation = 180;
                on = true;
                textUpdate.text = qsTr("Release to refresh...");
            } else {
                if (on){
                    titanim.slotAudioGet(uid);
                    _isSearch = false;
                    dateUpdate = Qt.formatDateTime(new Date(), "dd.MM.yyyy hh:mm");
                }
                iconUpdate.rotation = 0;
                on = false;
                textUpdate.text = qsTr("Pull down to refresh...");
            }
        }
    }

    Component{
        id: heading;
        Item{
            height: textEdit.height;
            width: textEdit.width;
            Rectangle{
                id: textEdit;
                height: chatInputEdit.height + 16;
                width: audioPage.width;
                color: "#C5C7CB";
                gradient: Gradient {
                    GradientStop {
                        position: 0.00;
                        color: "#e0e0e0";
                    }
                    GradientStop {
                        position: 1.00;
                        color: "#c0c1c4";
                    }
                }

                TextField {
                    id: chatInputEdit;
                    height: 50;
                    anchors.verticalCenter: parent.verticalCenter;
                    anchors.left: parent.left;
                    anchors.leftMargin: 10;
                    anchors.right: text.length ? clearButton.left : parent.right;
                    anchors.rightMargin: 10;
                    placeholderText: qsTr("Search");
                    inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText;
                    platformSipAttributes: SipAttributes {
                        actionKeyLabel: qsTr("Search");
                    }

                    Image{
                        id: iconSearch;
                        anchors.right: parent.right;
                        anchors.rightMargin: 15;
                        anchors.verticalCenter: parent.verticalCenter;
                        source: "images/ic_search.png";
                        MouseArea {
                            anchors.fill: parent
                            onClicked: chatInputEdit.text = "";
                        }
                    }

                    Keys.onReturnPressed: {
                        clear();
                        _searchText = text;
                        titanim.slotAudioSearch(text);
                        _isSearch = true;
                        clearButton.forceActiveFocus();
                    }
                }

                Button {
                    id: clearButton;
                    platformStyle: ButtonStyle {
                        fontPixelSize: 20;
                    }
                    width: 100;
                    anchors.bottom: chatInputEdit.bottom;
                    anchors.right: parent.right;
                    anchors.rightMargin: 10;
                    visible: chatInputEdit.text.length;
                    text: qsTr("Cancel");
                    onClicked: {
                        chatInputEdit.text = "";
                        clear();
                        titanim.slotAudioGet(uid);
                        _isSearch = false;
                        _searchText = "";
                        audio.forceActiveFocus();
                    }
                }

                Rectangle{
                    width:  parent.width;
                    height: 1;
                    anchors.bottom: parent.bottom;
                    color: "#868686"
                }
            }
        }
    }

    Component{
        id: footerUpd;
        Row{
            height: 70;
            anchors.left: parent.left;
            anchors.leftMargin: 35;
            spacing: 30;

            BusyIndicator {
                id: updIndicator;
                anchors.verticalCenter: parent.verticalCenter;
                platformStyle: BusyIndicatorStyle {
                    size: "medium";
                }
                running: isNextLoad;
            }

            Text {
                id: textUpdate;
                anchors.left: parent.left;
                anchors.leftMargin: 70;
                anchors.verticalCenter: parent.verticalCenter;
                text: qsTr("Loading...");
                font.pixelSize: 20
                font.bold: true;
                color: "#505050";
            }
        }
    }

    ListView{
        id: audio;
        width: parent.width;
        anchors.top: titleBar.bottom;
        anchors.bottom: parent.bottom;
        anchors.bottomMargin: 15;
        highlight: HighlightListView{}
        highlightMoveSpeed: -1;
        header: heading;
        footer: isNextLoad ? footerUpd : null;
        model: audioModel;
        delegate: AudioDelegate{}
        currentIndex: -1;

        onAtYEndChanged: {
            if (audio.atYEnd && !isNextLoad && audio.count >= 20){ //todo
                isNextLoad = true;
                if (!isSearch)
                    titanim.slotAudioGetNext(uid);
                else
                    titanim.slotAudioSearchNext(_searchText);
            }
        }

        Label{
            id: audioLabel;
            anchors.centerIn: parent;
            visible: !audio.count && audioFinished;
            font.pixelSize: 25;
            color: "#606060";
            text: qsTr("You have no audio files yet");
        }
    }

    ScrollDecorator {
        flickableItem: audio;
    }

    BusyIndicator {
        id: busyIndicator;
        platformStyle: BusyIndicatorStyle {
            size: "large";
        }
        anchors.centerIn: audio;
        running: !audio.count && !audioFinished;
        visible: !audio.count && !audioFinished;
    }

    Connections{
        target: titanim;
        onAudioFinished:{
            audioFinished = true;
            isNextLoad = false;
            isSearch = _isSearch;
            if (audioAutoPlay){
                playAudio(audioIndex);
                audioAutoPlay = false;
            }
        }

        onStatusChanged:{
            if (!isOnline){
                audioFinished = false;
                isNextLoad = false;
                playMusic.pause();
                uid = "";
                myAudio = true;
                _searchText = "";
            }
        }
    }

    onUidChanged: {
        if (uid != ""){
            myAudio = (uid == titanim.profileUid);
            clear();
            titanim.slotAudioGet(uid);
            _isSearch = false;
        }
    }

    Connections{
        target: audioCpp;
        onHeadsetButtonPressed:{
            if (!audio.count && !audioFinished){
                audioAutoPlay = true;
                uid = titanim.profileUid;
            }
            playAudio(audioIndex);
        }
        onForwardButtonPressed:{
            playNext();
        }
        onRewindButtonPressed:{
            playPrev();
        }
    }

    Audio{
        id: playMusic;

        onPositionChanged:{
            audioPosition = position;
        }
        onStatusChanged:{
            if (playMusic.status == Audio.Stalled){
                audioIndeterminate = true;
            }
            if (playMusic.status == Audio.Buffered){
                audioIndeterminate = false;
            }
            if (playMusic.status == Audio.EndOfMedia && !isAudioUrl){
                playNext();
            }
        }
    }
}
