/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0

Page{
    id: rosterPage;
    property bool friendsFinished: false;
    tools: tabBar;
    orientationLock: PageOrientation.LockPortrait;

    TitleBar{
        id: titleBar;

        MouseArea{
            anchors.fill: parent;
            onClicked: {
                roster.positionViewAtBeginning();
            }
        }

        ButtonRow {
            width: 300;
            anchors.centerIn: parent;
            Button {
                id: btnAll;
                platformStyle: ButtonStyle {
                    fontPixelSize: 17;
                }
                text: qsTr("All Friends");
                onClicked: {
                    titanim.on_offlineButton_clicked(2);
                }
            }
            Button {
                id: btnOnline;
                platformStyle: ButtonStyle {
                    fontPixelSize: 17;
                }
                checked: true;
                text: qsTr("Friends Online")
                onClicked: {
                    titanim.on_offlineButton_clicked(1);
                }
            }
        }
    }

    Component{
        id: heading;
        Item{
            height: textEdit.height;
            width: textEdit.width;
            Rectangle{
                id: textEdit;
                height: chatInputEdit.height + 16;
                width: rosterPage.width;
                color: "#C5C7CB";
                gradient: Gradient {
                    GradientStop {
                        position: 0.00;
                        color: "#e0e0e0";
                    }
                    GradientStop {
                        position: 1.00;
                        color: "#c0c1c4";
                    }
                }

                TextField {
                    id: chatInputEdit;
                    height: 50;
                    anchors.verticalCenter: parent.verticalCenter;
                    anchors.left: parent.left;
                    anchors.leftMargin: 10;
                    anchors.right: text.length ? clearButton.left : parent.right;
                    anchors.rightMargin: 10;
                    placeholderText: qsTr("Search");
                    inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText;
                    platformSipAttributes: SipAttributes {
                        actionKeyLabel: qsTr("Search");
                    }

                    Image{
                        id: iconSearch;
                        anchors.right: parent.right;
                        anchors.rightMargin: 15;
                        anchors.verticalCenter: parent.verticalCenter;
                        source: "images/ic_search.png";
                        MouseArea {
                            anchors.fill: parent
                            onClicked: chatInputEdit.text = "";
                        }
                    }

                    Keys.onReturnPressed: {
                        titanim.slotPressedKey(text);
                        clearButton.forceActiveFocus();
                    }

                    onTextChanged:{
                        if (text.length){
                            btnAll.checked = true;
                        } else {
                            btnOnline.checked = true;
                            titanim.slotPressedKey("");
                        }
                    }
                }

                Button {
                    id: clearButton;
                    platformStyle: ButtonStyle {
                        fontPixelSize: 20;
                    }
                    width: 100;
                    anchors.bottom: chatInputEdit.bottom;
                    anchors.right: parent.right;
                    anchors.rightMargin: 10;
                    visible: chatInputEdit.text.length;
                    text: qsTr("Cancel");
                    onClicked: {
                        chatInputEdit.text = "";
                        titanim.slotPressedKey("");
                        roster.forceActiveFocus();
                    }
                }

                Rectangle{
                    width:  parent.width;
                    height: 1;
                    anchors.bottom: parent.bottom;
                    color: "#868686"
                }
            }
        }
    }

    ListView{
        id: roster;
        width: parent.width;
        anchors.top: titleBar.bottom;
        anchors.bottom: parent.bottom;
        anchors.bottomMargin: 15;
        highlight: HighlightListView{}
        highlightMoveSpeed: -1;
        header: heading;
        model: rosterModel;
        delegate: ContactDelegate{}
        currentIndex: -1;

        Label{
            id: rosterLabel;
            anchors.centerIn: parent;
            visible: !roster.count && friendsFinished;
            font.pixelSize: 25;
            color: "#606060";
            text: qsTr("No friends found");
        }
    }

    ScrollDecorator {
        flickableItem: roster;
    }

    BusyIndicator {
        id: busyIndicator;
        platformStyle: BusyIndicatorStyle {
            size: "large";
        }
        anchors.centerIn: roster;
        running: !roster.count && !friendsFinished;
        visible: !roster.count && !friendsFinished;
    }

    Connections{
        target: titanim;
        onFriendsFinished:{
            friendsFinished = true;
        }

        onStatusChanged:{
            if (!isOnline){
                friendsFinished = false;
                btnOnline.checked = true;
            }
        }
    }
}
