/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0

Item{
    id: contactDelegate;
    width: parent.width;
    height: avatarBorder.height > (name.height + activity.height) ?  avatarBorder.height + 20 : name.height + activity.height + 20;
    //ListView.delayRemove: avatar.status == Image.Ready ? false : true;

    Row{
        id: contact;
        height: parent.height;
        x: 10;
        spacing: 15;

        Rectangle{
            id: avatarBorder;
            width: (avatar.width + 2);
            height: (avatar.height + 2);
            anchors.verticalCenter: parent.verticalCenter;
            scale: 0.0;
            color: avatar.height > 16 ? "black" : "transparent";
            Behavior on scale {
                NumberAnimation {
                    easing.type: Easing.InOutQuad;
                }
            }
            Image{
                id: avatar;
                width: titanim.iconSize;
                height: titanim.iconSize;
                anchors.centerIn: parent;
                smooth: true;
                source: model.decoration;
            }
        }

        Column{
            anchors.verticalCenter: parent.verticalCenter;
            Text{
                id: name;
                width: text.length ? contactDelegate.width - avatarBorder.width - contact.x - contact.spacing - 30 : 0;
                color: "black";
                font.pointSize: titanim.fontPointSize;
                font.bold: true;
                elide: Text.ElideRight;
                text: model.display;
            }
            Text{
                id: activity;
                width: text.length ? contactDelegate.width - avatarBorder.width - contact.x - contact.spacing - 30 : 0;
                visible: titanim.showActivity;
                color: "#505050";
                font.pointSize: titanim.fontPointSize - 1;
                elide: Text.ElideRight;
                text: model.activity;
            }
        }
    }

    Image{
        anchors.right: parent.right;
        anchors.rightMargin: 15;
        anchors.verticalCenter: parent.verticalCenter;
        visible: model.status;
        source: "images/ic_online_up.png";
    }

    Rectangle {
        width:  parent.width;
        height: 1;
        y: contactDelegate.height - 1;
        color: "#c0c0c0"
    }

    MouseArea{
        anchors.fill: parent;
        onClicked: {
            profilePage.clear();
            titanim.slotShowProfile(model.uid);
            titanim.slotWallGet(model.uid);
            profilePage.uid = model.uid;
            mainView.pageStack.push(profilePage);
        }
    }

    states:[
        State{
            name: "Show";
            when: avatar.status == Image.Ready;
            PropertyChanges {
                target: avatarBorder;
                scale: 1;
            }
        }
    ]
}
