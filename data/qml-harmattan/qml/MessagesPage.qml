/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0

Page{
    id: messagesPage;
    property bool dialogsFinished: false;
    property bool isNextLoad: false;
    tools: tabBar;
    orientationLock: PageOrientation.LockPortrait;

    TitleBar{
        id: titleBar;

        MouseArea{
            anchors.fill: parent;
            onClicked: {
                dialogs.positionViewAtBeginning();
            }
        }

        Label{
            anchors.centerIn: parent;
            color: "white";
            text: qsTr("Messages");
            font.pixelSize: 27;
            font.bold: true;
        }
    }

    Row{
        id: update;
        property bool on: false;
        property string dateUpdate: Qt.formatDateTime(new Date(), "dd.MM.yyyy hh:mm");
        height: 70;
        y: -dialogs.visibleArea.yPosition * Math.max(dialogs.contentHeight, dialogs.height);
        anchors.left: parent.left;
        anchors.leftMargin: 35;
        spacing: 30;

        Image{
            id: iconUpdate;
            anchors.top: parent.top;
            source: "images/ic_pull_arrow.png";

            Behavior on rotation {
                NumberAnimation {
                    easing.type: Easing.InCirc;
                }
            }
        }

        Column{
            spacing: 6;
            anchors.verticalCenter: parent.verticalCenter;
            Text {
                id: textUpdate;
                text: qsTr("Pull down to refresh...");
                font.pixelSize: 20
                font.bold: true;
                color: "#505050";
            }
            Text {
                id: textDateUpdate;
                text:  qsTr("Last updated: ") + update.dateUpdate;
                font.pixelSize: 18
                color: "#606060";
            }
        }

        onYChanged: {
            if(update.y > 110){
                iconUpdate.rotation = 180;
                on = true;
                textUpdate.text = qsTr("Release to refresh...");
            } else {
                if (on){
                    titanim.slotMessagesGetDialogs();
                    dateUpdate = Qt.formatDateTime(new Date(), "dd.MM.yyyy hh:mm");
                }
                iconUpdate.rotation = 0;
                on = false;
                textUpdate.text = qsTr("Pull down to refresh...");
            }
        }
    }

    Component{
        id: footerUpd;
        Row{
            height: 70;
            anchors.left: parent.left;
            anchors.leftMargin: 35;
            spacing: 30;

            BusyIndicator {
                id: updIndicator;
                anchors.verticalCenter: parent.verticalCenter;
                platformStyle: BusyIndicatorStyle {
                    size: "medium";
                }
                running: isNextLoad;
            }

            Text {
                id: textUpdate;
                anchors.left: parent.left;
                anchors.leftMargin: 70;
                anchors.verticalCenter: parent.verticalCenter;
                text: qsTr("Loading...");
                font.pixelSize: 20
                font.bold: true;
                color: "#505050";
            }
        }
    }

    ListView{
        id: dialogs;
        width: parent.width;
        anchors.top: titleBar.bottom;
        anchors.bottom: parent.bottom;
        anchors.bottomMargin: 15;
        highlight: HighlightListView{}
        highlightMoveSpeed: -1;
        footer: isNextLoad ? footerUpd : null;
        model: dialogsModel;
        delegate: DialogsDelegate{}
        currentIndex: -1;

        onAtYEndChanged: {
            if (dialogs.atYEnd && !isNextLoad && dialogs.count >= 9){ //todo
                isNextLoad = true;
                titanim.slotMessagesGetNextDialogs();
            }
        }

        Label{
            id: dialogsLabel;
            anchors.centerIn: parent;
            visible: !dialogs.count && dialogsFinished;
            font.pixelSize: 25;
            color: "#606060";
            text: qsTr("No messages");
        }
    }

    ScrollDecorator {
        flickableItem: dialogs;
    }

    BusyIndicator {
        id: busyIndicator;
        platformStyle: BusyIndicatorStyle {
            size: "large";
        }
        anchors.centerIn: dialogs;
        running: !dialogs.count && !dialogsFinished;
        visible: !dialogs.count && !dialogsFinished;
    }

    Connections{
        target: tabBar;
        onMessagesPageClicked:{
            if (!dialogsFinished && dialogs.count == 0)
                titanim.slotMessagesGetDialogs();
        }
    }

    Connections{
        target: titanim;
        onDialogsFinished:{
            dialogsFinished = true;
            isNextLoad = false;
        }

        onStatusChanged:{
            if (!isOnline){
                dialogsFinished = false;
                isNextLoad = false;
            }
        }
    }
}
