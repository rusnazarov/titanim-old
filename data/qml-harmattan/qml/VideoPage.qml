/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0

Page{
    id: videoPage;
    property string uid: "";
    property bool videoFinished: false;
    property bool isNextLoad: false;
    property bool isSearch: false;
    property string _searchText: "";
    //orientationLock: PageOrientation.LockPortrait;

    function clear(){
        titanim.slotVideosClear();
        videoFinished = false;
        isSearch = false;
        _searchText = "";
    }

    TitleBar{
        id: titleBar;
        visible: inPortrait;

        MouseArea{
            anchors.fill: parent;
            onClicked: {
                videoListView.positionViewAtBeginning();
            }
        }

        BorderImage{
            id: btnBack;
            width: 140;
            anchors.left: parent.left;
            anchors.leftMargin: 10;
            anchors.verticalCenter: parent.verticalCenter;
            source: "images/btn_back.sci";

            Text {
                anchors.centerIn: parent;
                color: "white";
                text: qsTr("Back");
                font.pixelSize: 21;
                font.bold: true;
            }
            MouseArea{
                anchors.fill: parent;
                onClicked: {
                    pageStack.pop();
                }
            }
        }

        Label{
            id: labelName;
            anchors.left: btnBack.right;
            anchors.leftMargin: 5;
            anchors.right: parent.right;
            anchors.rightMargin: 5;
            anchors.verticalCenter: parent.verticalCenter;
            color: "white";
            text: qsTr("Video");
            horizontalAlignment: Text.AlignHCenter;
            font.pixelSize: 27;
            font.bold: true;
        }
    }

    Component{
        id: heading;
        Item{
            height: textEdit.height;
            width: textEdit.width;
            Rectangle{
                id: textEdit;
                height: chatInputEdit.height + 16;
                width: videoPage.width;
                color: "#C5C7CB";
                gradient: Gradient {
                    GradientStop {
                        position: 0.00;
                        color: "#e0e0e0";
                    }
                    GradientStop {
                        position: 1.00;
                        color: "#c0c1c4";
                    }
                }

                TextField {
                    id: chatInputEdit;
                    height: 50;
                    anchors.verticalCenter: parent.verticalCenter;
                    anchors.left: parent.left;
                    anchors.leftMargin: 10;
                    anchors.right: text.length ? clearButton.left : parent.right;
                    anchors.rightMargin: 10;
                    placeholderText: qsTr("Search");
                    inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText;
                    platformSipAttributes: SipAttributes {
                        actionKeyLabel: qsTr("Search");
                    }

                    Image{
                        id: iconSearch;
                        anchors.right: parent.right;
                        anchors.rightMargin: 15;
                        anchors.verticalCenter: parent.verticalCenter;
                        source: "images/ic_search.png";
                        MouseArea {
                            anchors.fill: parent
                            onClicked: chatInputEdit.text = "";
                        }
                    }

                    Keys.onReturnPressed: {
                        videoPage.clear();
                        isSearch = true;
                        _searchText = text;
                        titanim.slotVideoSearch(text);
                        clearButton.forceActiveFocus();
                    }
                }

                Button {
                    id: clearButton;
                    platformStyle: ButtonStyle {
                        fontPixelSize: 20;
                    }
                    width: 100;
                    anchors.bottom: chatInputEdit.bottom;
                    anchors.right: parent.right;
                    anchors.rightMargin: 10;
                    visible: chatInputEdit.text.length;
                    text: qsTr("Cancel");
                    onClicked: {
                        chatInputEdit.text = "";
                        videoPage.clear();
                        isSearch = false;
                        _searchText = "";
                        titanim.slotVideoGet(uid);
                        videoListView.forceActiveFocus();
                    }
                }

                Rectangle{
                    width:  parent.width;
                    height: 1;
                    anchors.bottom: parent.bottom;
                    color: "#868686"
                }
            }
        }
    }

    Component{
        id: footerUpd;
        Row{
            height: 70;
            anchors.left: parent.left;
            anchors.leftMargin: 35;
            spacing: 30;

            BusyIndicator {
                id: updIndicator;
                anchors.verticalCenter: parent.verticalCenter;
                platformStyle: BusyIndicatorStyle {
                    size: "medium";
                }
                running: isNextLoad;
            }

            Text {
                id: textUpdate;
                anchors.left: parent.left;
                anchors.leftMargin: 70;
                anchors.verticalCenter: parent.verticalCenter;
                text: qsTr("Loading...");
                font.pixelSize: 20
                font.bold: true;
                color: "#505050";
            }
        }
    }

    ListView{
        id: videoListView;
        width: parent.width;
        anchors.top: titleBar.visible ? titleBar.bottom : parent.top;
        anchors.bottom: parent.bottom;
        anchors.bottomMargin: 15;
        highlight: HighlightListView{}
        highlightMoveSpeed: -1;
        header: heading;
        footer: isNextLoad ? footerUpd : null;
        model: videoModel;
        delegate: VideoDelegate{}
        currentIndex: -1;

        onAtYEndChanged: {
            if (videoListView.atYEnd && !isNextLoad && videoListView.count >= 20){ //todo
                isNextLoad = true;
                if (!isSearch)
                    titanim.slotVideoGetNext(uid);
                else
                    titanim.slotVideoSearchNext(_searchText);
            }
        }

        Label{
            id: videoLabel;
            anchors.centerIn: parent;
            visible: !videoListView.count && videoFinished;
            font.pixelSize: 25;
            color: "#606060";
            text: qsTr("No videos");
        }
    }

    ScrollDecorator {
        flickableItem: videoListView;
    }

    BusyIndicator {
        id: busyIndicator;
        platformStyle: BusyIndicatorStyle {
            size: "large";
        }
        anchors.centerIn: videoListView;
        running: !videoListView.count && !videoFinished;
        visible: !videoListView.count && !videoFinished;
    }

    Connections{
        target: titanim;
        onVideoFinished:{
            videoFinished = true;
            isNextLoad = false;
        }

        onStatusChanged:{
            if (!isOnline){
                videoFinished = false;
                isNextLoad = false;
                isSearch = false;
                _searchText = "";
            }
        }
    }
}
