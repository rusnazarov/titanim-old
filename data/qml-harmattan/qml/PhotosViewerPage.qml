/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0

Page{
    id: photosViewerPage;
    property alias curIndex: photosViewer.currentIndex
    property variant photosBigList: 0;

    Rectangle{
        anchors.fill: parent;
        color: "black";
    }

    ListView{
        id: photosViewer;
        anchors.fill: parent;
        clip: true;
        highlightMoveSpeed: -1;
        model: photosBigList ? photosBigList : photoModel;
        spacing: 15;
        orientation: ListView.Horizontal;
        snapMode: ListView.SnapOneItem;
        cacheBuffer: 960;
        delegate: PhotosViewerDelegate{}
    }

    Menu{
        id: photoMenu;
        property string src: "";
        visualParent: pageStack;
        MenuLayout{
            MenuItem { text: qsTr("Save photo"); onClicked: titanim.slotSavePhoto(photoMenu.src); }
            MenuItem { text: qsTr("Share photo"); onClicked: titanim.slotShare(photoMenu.src); }
        }
    }
}
