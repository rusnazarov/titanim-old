/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0

Item{
    id: wallCommentsDelegate;
    width: wallCommentsDelegate.ListView.view.width;
    height: avatarBorder.height > column.height ? avatarBorder.height + 20 : column.height + 20;
    //ListView.delayRemove: avatar.status == Image.Ready ? false : true;

    function replaceURLWithHTMLLinks(text){
        //thanks Sauron
        var exp = /(\b(https?|ftp|file):\/\/([-A-Z0-9+&@#%?=~_|!:,.;]*)([-A-Z0-9+&@#%?\/=~_|!:,.;]*)[-A-Z0-9+&@#\/%=~_|])/ig;
        return text.replace(exp, "<a href='$1' target='_blank'>$1</a>");
    }

    Row{
        id: wallCommentsRow;
        height: parent.height;
        x: 10;
        spacing: 15;

        Rectangle{
            id: avatarBorder;
            width: (avatar.width + 2);
            height: (avatar.height + 2);
            anchors.top: parent.top;
            anchors.topMargin: 10;
            scale: 0.0;
            color: avatar.height > 16 ? "black" : "transparent";
            Behavior on scale {
                NumberAnimation {
                    easing.type: Easing.InOutQuad;
                }
            }

            Image{
                id: avatar;
                width: titanim.iconSize + 10;
                height: titanim.iconSize + 10;
                anchors.centerIn: parent;
                smooth: true;
                source: model.decoration;
            }

            MouseArea{
                anchors.fill: parent;
                onClicked: {
                    if (model.uid > 0){
                        profilePage.clear();
                        titanim.slotShowProfile(model.uid);
                        titanim.slotWallGet(model.uid);
                        profilePage.uid = model.uid;
                        mainView.pageStack.push(profilePage);
                    }
                }
            }
        }

        Column{
            id: column;
            anchors.top: avatarBorder.top;
            spacing: 7;

            Text{
                id: name;
                width: text.length ? wallCommentsDelegate.width - avatarBorder.width - wallCommentsRow.x - wallCommentsRow.spacing - 40 : 0;
                color: "#254e84";
                font.pointSize: titanim.fontPointSize + 1;
                font.bold: true;
                elide: Text.ElideRight;
                text: model.display;

                MouseArea{
                    anchors.fill: parent;
                    onClicked: {
                        if (model.uid > 0){
                            profilePage.clear();
                            titanim.slotShowProfile(model.uid);
                            titanim.slotWallGet(model.uid);
                            profilePage.uid = model.uid;
                            mainView.pageStack.push(profilePage);
                        }
                    }
                }
            }

            Text{
                id: textBody;
                width: text.length ? wallCommentsDelegate.width - avatarBorder.width - wallCommentsRow.x - wallCommentsRow.spacing - 50 : 0;
                clip: true;
                color: "black";
                font.pointSize: titanim.fontPointSize - 1;
                wrapMode: Text.Wrap;
                text: replaceURLWithHTMLLinks(model.text);
                onLinkActivated: Qt.openUrlExternally(link);
            }

            Row{
                spacing: 15;
                Text{
                    id: messageDate;
                    color: "#505050";
                    font.pointSize: titanim.fontPointSize - 2;
                    text: model.dateStr;
                }
            }
        }
    }

    Rectangle{
        width:  parent.width;
        height: 1;
        y: wallCommentsDelegate.height - 1;
        color: "#c0c0c0";
    }

    states:[
        State{
            name: "Show";
            when: avatar.status == Image.Ready;
            PropertyChanges {
                target: avatarBorder;
                scale: 1;
            }
        }
    ]
}
