/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0

Page{
    id: wallCommentsPage;
    orientationLock: PageOrientation.LockPortrait;

    property string name: "";
    property string avatarUrl: "";
    property string date: "";
    property string textWall: "";
    property string postId: "";
    property string sourceId: "";
    property string fromId: "";
    property variant photosList: 0;
    property variant photosBigList: 0;
    property int commentsCount: 0;
    property bool canPost: true;
    property int likesCount: 0;
    property bool isLiked: false;
    property bool isRetweet: false;

    function replaceURLWithHTMLLinks(text){
        //thanks Sauron
        var exp = /(\b(https?|ftp|file):\/\/([-A-Z0-9+&@#%?=~_|!:,.;]*)([-A-Z0-9+&@#%?\/=~_|!:,.;]*)[-A-Z0-9+&@#\/%=~_|])/ig;
        return text.replace(exp, "<a href='$1' target='_blank'>$1</a>");
    }

    TitleBar{
        id: titleBar;

        MouseArea{
            anchors.fill: parent;
            onClicked: {
                wallComments.positionViewAtBeginning();
            }
        }

        BorderImage{
            id: btnBack;
            width: 140;
            anchors.left: parent.left;
            anchors.leftMargin: 10;
            anchors.verticalCenter: parent.verticalCenter;
            source: "images/btn_back.sci";

            Text {
                anchors.centerIn: parent;
                color: "white";
                text: qsTr("Back");
                font.pixelSize: 21;
                font.bold: true;
            }

            MouseArea{
                anchors.fill: parent;
                onClicked: {
                    pageStack.pop();
                    chatInputEdit.text = "";
                }
            }
        }

        Label{
            anchors.left: btnBack.right;
            anchors.leftMargin: 5;
            anchors.right: btnWallDelete.left;
            anchors.rightMargin: 5;
            anchors.verticalCenter: parent.verticalCenter;
            color: "white";
            text: qsTr("Comments");
            horizontalAlignment: Text.AlignHCenter;
            font.pixelSize: 27;
            font.bold: true;
        }

        BorderImage{
            id: btnWallDelete;
            width: visible ? imageWallDelete.width + 30 : 0;
            height: 72;
            anchors.right: parent.right;
            anchors.rightMargin: visible ? 11 : 0;
            anchors.verticalCenter: parent.verticalCenter;
            visible: fromId == titanim.profileUid;
            smooth: true;
            source: "images/btn_title.sci";

            Image{
                id: imageWallDelete;
                anchors.centerIn: parent;
                smooth: true;
                source: "images/ic_delete.png";
            }

            QueryDialog {
                id: wallDeleteDialog;
                icon: "images/ic_delete_big.png";
                titleText: qsTr("Delete wall post?");
                message: textWall;
                acceptButtonText: qsTr("Delete");
                rejectButtonText: qsTr("Cancel");

                onAccepted: {
                    titanim.slotWallDelete(postId, sourceId);
                    pageStack.pop();
                    chatInputEdit.text = "";
                }
            }

            MouseArea{
                anchors.fill: parent;
                onClicked: {
                    wallDeleteDialog.open();
                }
            }
        }
    }

    Component{
        id: headerComments;
        Item {
            width: wallCommentsPage.width;
            height: heading.height + headingTextWall.height + likesText.height + flowPhotos.height + 50;
            Rectangle{
                id: heading;
                width: parent.width;
                height: 10 + avatar.height + 8;
                z: 1;
                color: "#C5C7CB";
                gradient: Gradient {
                    GradientStop {
                        position: 0.00;
                        color: "#e0e0e0";
                    }
                    GradientStop {
                        position: 1.00;
                        color: "#c0c1c4";
                    }
                }

                Image{
                    id: avatar;
                    width: Math.max (titanim.iconSize, 50);
                    height: Math.max (titanim.iconSize, 50);
                    anchors.top: parent.top;
                    anchors.topMargin: 10;
                    anchors.left: parent.left;
                    anchors.leftMargin: 10;
                    smooth: true;
                    source: avatarUrl;
                }

                Column{
                    anchors.left: avatar.right;
                    anchors.leftMargin: 15;
                    anchors.verticalCenter: parent.verticalCenter;

                    Text{
                        id: nameText;
                        width: wallCommentsPage.width - avatar.width - 25;
                        font.pointSize: titanim.fontPointSize + 2;
                        font.bold: true;
                        elide: Text.ElideRight;
                        text: name;
                    }

                    Text{
                        id: dateText;
                        color: "#505050";
                        font.pointSize: titanim.fontPointSize - 1;
                        text: date;
                    }
                }

                Image{
                    id: arrow;
                    opacity: 0.5;
                    visible: fromId > 0;
                    source: "image://theme/icon-m-common-drilldown-arrow" + (theme.inverted ? "-inverse" : "")
                    anchors.right: parent.right;
                    anchors.rightMargin: 10;
                    anchors.verticalCenter: parent.verticalCenter;
                }

                Rectangle{
                    width:  parent.width;
                    height: 1;
                    anchors.bottom: parent.bottom;
                    color: "#868686"
                }

                MouseArea{
                    anchors.fill: parent;
                    onClicked: {
                        if (fromId > 0){
                            profilePage.clear();
                            titanim.slotShowProfile(fromId);
                            titanim.slotWallGet(fromId);
                            profilePage.uid = fromId;
                            mainView.pageStack.push(profilePage);
                        }
                    }
                }
            }

            Text{
                id: headingTextWall;
                width: wallCommentsPage.width - 24;
                height: textWall.length ? headingTextWall.implicitHeight : 0;
                x: 12;
                anchors.top: heading.bottom;
                anchors.topMargin: 12;
                visible: textWall.length;
                font.pointSize: titanim.fontPointSize - 1;
                wrapMode: Text.Wrap;
                text: replaceURLWithHTMLLinks(textWall);
                onLinkActivated: Qt.openUrlExternally(link);
            }

            Flow{
                id: flowPhotos;
                anchors.top: headingTextWall.bottom;
                anchors.topMargin: 10;
                anchors.left: parent.left;
                anchors.leftMargin: 12;
                width: wallCommentsPage.width - 24;
                spacing: 6;

                Repeater{
                    model: wallCommentsPage.photosList;
                    Image{
                        id: photoAttach;
                        width: wallCommentsPage.photosList.length == 1 ? wallCommentsPage.width - 24 : null;
                        anchors.centerIn: wallCommentsPage.photosList.length == 1 ? parent : null;
                        fillMode: Image.PreserveAspectFit;
                        smooth: true;
                        source: wallCommentsPage.photosList.length == 1 ? wallCommentsPage.photosBigList[index] : modelData;

                        MouseArea{
                            anchors.fill: parent;
                            onClicked: {
                                mainView.pageStack.push(photosViewerPage);
                                photosViewerPage.photosBigList = wallCommentsPage.photosBigList;
                                photosViewerPage.curIndex = index;
                            }
                        }
                    }
                }
            }

            Row{
                anchors.bottom: parent.bottom;
                anchors.bottomMargin: 10;
                anchors.left: parent.left;
                anchors.leftMargin: 12;
                spacing: 10;
                Image{
                    id: likes;
                    source: "images/ic_like_up.png";
                }

                Text{
                    id: likesText;
                    anchors.verticalCenter: likes.verticalCenter;
                    color: "#505050";
                    font.pointSize: titanim.fontPointSize - 2;
                    text: qsTr("Like") + " " + likesCount + " " + qsTr("people"); // :(
                }
            }

            Rectangle{
                width:  parent.width - 20;
                anchors.horizontalCenter: parent.horizontalCenter;
                height: 1;
                anchors.bottom: parent.bottom;
                color: "#868686"
            }
        }
    }

    Row{
        id: update;
        property bool on: false;
        property string dateUpdate: Qt.formatDateTime(new Date(), "dd.MM.yyyy hh:mm");
        height: 70;
        y: -wallComments.visibleArea.yPosition * Math.max(wallComments.contentHeight, wallComments.height);
        anchors.left: parent.left;
        anchors.leftMargin: 35;
        spacing: 30;

        Image{
            id: iconUpdate;
            anchors.top: parent.top;
            source: "images/ic_pull_arrow.png";

            Behavior on rotation {
                NumberAnimation {
                    easing.type: Easing.InCirc;
                }
            }
        }

        Column{
            spacing: 6;
            anchors.verticalCenter: parent.verticalCenter;
            Text {
                id: textUpdate;
                text: qsTr("Pull down to refresh...");
                font.pixelSize: 20
                font.bold: true;
                color: "#505050";
            }
            Text {
                id: textDateUpdate;
                text:  qsTr("Last updated: ") + update.dateUpdate;
                font.pixelSize: 18
                color: "#606060";
            }
        }

        onYChanged: {
            if(update.y > 110){
                iconUpdate.rotation = 180;
                on = true;
                textUpdate.text = qsTr("Release to refresh...");
            } else {
                if (on){
                    titanim.slotWallGetComments(postId, sourceId);
                    dateUpdate = Qt.formatDateTime(new Date(), "dd.MM.yyyy hh:mm");
                }
                iconUpdate.rotation = 0;
                on = false;
                textUpdate.text = qsTr("Pull down to refresh...");
            }
        }
    }

    ListView{
        id: wallComments;
        width: parent.width;
        anchors.top: titleBar.bottom;
        anchors.bottom: textEdit.top;
        highlight: HighlightListView{}
        highlightMoveSpeed: -1;
        cacheBuffer: 960;
        header: headerComments;
        model: wallCommentsModel;
        delegate: WallCommentsDelegate{}
        currentIndex: -1;
    }

    ScrollDecorator {
        flickableItem: wallComments;
    }

    BusyIndicator {
        id: busyIndicator;
        platformStyle: BusyIndicatorStyle {
            size: "large";
        }
        anchors.centerIn: wallComments;
        running: commentsCount && !wallComments.count;
        visible: commentsCount && !wallComments.count;
    }

    Rectangle{
        id: textEdit;
        height: chatInputEdit.visible ? chatInputEdit.height + 16 : 65;
        width: parent.width;
        anchors.bottom: parent.bottom;
        anchors.left: parent.left;
        color: "#C3C5C9";

        Row{
            anchors.centerIn: parent;
            visible: !chatInputEdit.visible;
            spacing: 110;

            Image {
                id: likeImg;
                source: isLiked ? "images/ic_post_like_down.png" : "images/ic_post_like_up.png";
                MouseArea{
                    anchors.fill: parent;
                    onClicked: {
                        titanim.slotLikesGet(sourceId, "post", postId, !isLiked, false);
                        isLiked = !isLiked;
                        if (!isLiked)
                            isRetweet = false;
                    }
                }
            }

            Image {
                id: retweetImg;
                source: isRetweet && isLiked ? "images/ic_post_retweet_done.png" : "images/ic_post_retweet_up.png";
                MouseArea{
                    anchors.fill: parent;
                    onClicked: {
                        if (!isRetweet){
                            titanim.slotLikesGet(sourceId, "post", postId, true, true);
                            isLiked = true;
                            isRetweet = true;
                        } else {
                            titanim.slotLikesGet(sourceId, "post", postId, false, false);
                            isLiked = false;
                            isRetweet = false;
                        }
                    }
                }
            }

            Image {
                id: commentImg;
                source: canPost ? "images/ic_post_comment_up.png" : "images/ic_post_comment_disabled.png";
                enabled: canPost;
                MouseArea{
                    anchors.fill: parent;
                    onClicked: {
                        chatInputEdit.visible = true;
                        chatInputEdit.forceActiveFocus();
                    }
                }
            }
        }

        TextArea {
            id: chatInputEdit;
            height: 54;
            anchors.verticalCenter: parent.verticalCenter;
            anchors.left: parent.left;
            anchors.leftMargin: 5;
            anchors.right: sendButton.left;
            anchors.rightMargin: 4;
            visible: false;
            wrapMode: TextEdit.Wrap;
            placeholderText: qsTr("Comment...");
            errorHighlight: text.length > 4096;

            onActiveFocusChanged: {
                chatInputEdit.visible = chatInputEdit.activeFocus;
            }
        }

        Button {
            id: sendButton;
            width: 100;
            anchors.bottom: chatInputEdit.bottom;
            anchors.right: parent.right;
            anchors.rightMargin: 6;
            visible: chatInputEdit.visible;
            text: qsTr("Send");
            onPressedChanged:{
                titanim.slotWallAddComment(postId, chatInputEdit.text, sourceId);
                titanim.slotWallGetComments(postId, sourceId);
                chatInputEdit.text = "";
            }
        }

        Rectangle{
            width:  parent.width;
            height: 1;
            anchors.top: parent.top;
            color: "#868686"
        }
    }

    Connections{
        target: titanim;
        onLikes:{
            if (ownerId == sourceId && itemId == postId){
                likesCount = countLikes;
                isLiked = isAdd;
            }
        }
    }
}
