/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0

Sheet {
    id: photosLocalSheet;
    property variant galleryModel;
    property string uid: "";
    property string sourceImg: "";
    property string textImg: "";
    property int rotationImg: 0;
    signal send;

    buttons:[
        SheetButton {
            id: rejectBtn;
            anchors.left: parent.left;
            anchors.leftMargin: 15;
            anchors.verticalCenter: parent.verticalCenter;
            text: qsTr("Cancel");
            onClicked:{
                close();
                uid = "";
                statusInputEdit.text = "";
                img.rotation = 0;
            }
        },
        SheetButton {
            id: acceptBtn;
            anchors.right: parent.right;
            anchors.rightMargin: 15;
            anchors.verticalCenter: parent.verticalCenter;
            platformStyle: SheetButtonAccentStyle {}
            visible: photos.visible ? false : true;
            text: qsTr("Send");
            onClicked:{
                close();
                textImg = statusInputEdit.text;
                rotationImg = img.rotation;
                statusInputEdit.text = "";
                img.rotation = 0;
                send();
            }
        }
    ]

    function loadGalleryModel(){
        var component = Qt.createComponent("GalleryModel.qml");
        if (component.status == Component.Ready){
            galleryModel = component.createObject(photosLocalSheet);
            errorLabel.visible = false;
        } else {
            galleryModel = null;
            errorLabel.visible = true;
        }
    }

    content: Rectangle{
        anchors.fill: parent;
        color: photos.visible ? "white" : "black";

        GridView{
            id: photos;
            anchors.fill: parent;
            anchors.topMargin: 8;
            anchors.leftMargin: 8;
            visible: true;
            cellHeight: 118;
            cellWidth: 118;
            model: galleryModel;
            delegate: PhotosLocalDelegate{}

            ScrollDecorator {
                flickableItem: photos;
            }
        }

        Item{
            id: statusView;
            anchors.fill: parent;
            visible: false;

            Image{
                id: img;
                anchors.top: parent.top;
                anchors.topMargin: 15;
                anchors.left: parent.left;
                anchors.leftMargin: 10;
                anchors.right: parent.right;
                anchors.rightMargin: 10;
                anchors.bottom: textEdit.top;
                anchors.bottomMargin: 15;
                asynchronous: true;
                smooth: true;
                fillMode: Image.PreserveAspectFit;
                sourceSize.width: 480;
                source: sourceImg;

                MouseArea{
                    anchors.fill: parent;
                    onClicked: {
                        img.rotation += 90;
                    }
                }
            }

            Rectangle{
                id: textEdit;
                height: statusInputEdit.height + 16;
                width: parent.width;
                anchors.bottom: parent.bottom;
                color: "#C3C5C9";

                TextArea {
                    id: statusInputEdit;
                    height: Math.max (50, Math.min(implicitHeight, 300));
                    anchors.verticalCenter: parent.verticalCenter;
                    anchors.left: parent.left;
                    anchors.leftMargin: 10;
                    anchors.right: parent.right;
                    anchors.rightMargin: 10;
                    wrapMode: TextEdit.Wrap;
                    placeholderText: qsTr("Description");
                }

                Rectangle{
                    width:  parent.width;
                    height: 1;
                    anchors.top: parent.top;
                    color: "#c0c0c0";
                }
            }
        }

        Label{
            id: errorLabel;
            width: parent.width - 20;
            anchors.centerIn: parent;
            visible: false;
            font.pixelSize: 45;
            color: "#606060";
            wrapMode: Text.Wrap;
            horizontalAlignment: Text.AlignHCenter;
            text: qsTr("Gallery is not available for your firmware");
        }
    }

    states:[
        State{
            name: "Grid";
            PropertyChanges {
                target: photos;
                visible: true;
            }
            PropertyChanges {
                target: statusView;
                visible: false;
            }
        },
        State{
            name: "Image";
            PropertyChanges {
                target: photos;
                visible: false;
            }
            PropertyChanges {
                target: statusView;
                visible: true;
            }
        }
    ]

    Component.onCompleted:{
        loadGalleryModel();
    }
}
