/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0

Page{
    id: authorizePage;
    orientationLock: PageOrientation.LockPortrait;

    Rectangle {
        anchors.fill: parent;
        color: "#4e729a"
        gradient: Gradient {
             GradientStop { position: 0.0; color: "#42658c" }
             GradientStop { position: 1.0; color: "#5a7da5" }
        }
    }

    Image {
        id: logo;
        source: "images/logo_full.png";
        anchors.horizontalCenter: parent.horizontalCenter;
        anchors.bottom: column.top;
        anchors.bottomMargin: 15;
    }

    Column{
        id: column;
        width: parent.width - 40;
        anchors.centerIn: parent;
        spacing: 3;
        TextField {
            id: login;
            width: parent.width;
            placeholderText: qsTr("Email");
            inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhEmailCharactersOnly | Qt.ImhNoPredictiveText;
            platformSipAttributes: SipAttributes {
                actionKeyLabel: qsTr("Next");
            }

            Keys.onReturnPressed: {
                pass.forceActiveFocus();
            }
        }

        TextField {
            id: pass;
            width: parent.width;
            placeholderText: qsTr("Password");
            echoMode: TextInput.Password;
            inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText;
            platformSipAttributes: SipAttributes {
                actionKeyLabel: qsTr("Log In");
            }

            Keys.onReturnPressed: {
                if (login.text.length == 0){
                    login.forceActiveFocus();
                    return;
                }
                if (pass.text.length == 0){
                    pass.forceActiveFocus();
                    return;
                }
                parent.focus = true;
                titanim.setOnline(login.text, pass.text);
                mainView.pageStack.replace(newsPage);
            }
        }
    }

    QueryDialog {
        id: privacyDialog;
        titleText: "Privacy Policy";
        message: "Your privacy is important to us. To better protect your privacy, we provide this "
        + "notice explaining our online information practices. The text does not fit entirely on screen, "
        + "please use the scroll wheel for a complete reading\n\n"
        + "TitanIM application work is related to the network and some of data can be sent and received "
        + "from the network at any moment.\n\n"
        + "Hereby we state that we never collect or sent any of your data with or without your permission. "
        + "Your account information is stored locally only in mobile device. We never send any of this "
        + "information to our server or use it in way other than implementation of your network interaction.\n\n"
        + "When you use social networking service, the personal information you share is visible to other users "
        + "and can be read, collected, or used by them. You are responsible for the personal information you "
        + "choose to submit in these instances. For example, if you list your name and email address in a forum "
        + "posting, that information is public. Please take care when using these features.\n\n"
        + "Distribution of Information.\n"
        + "We do not share your personally identifiable information to any third party for marketing purposes. "
        + "However, we may share information with governmental agencies or other companies assisting us in fraud "
        + "prevention or investigation. We may do so when: (1) permitted or required by law; or, (2) trying to "
        + "protect against or prevent actual or potential fraud or unauthorized transactions; or, (3) investigating "
        + "fraud which has already taken place.\n\n"
        + "How To Contact Us.\n"
        + "Should you have other questions or concerns about these privacy policies, please send us an email at "
        + "818151@mail.ru";
        acceptButtonText: "Send email";
        rejectButtonText: "OK";

        onAccepted: {
            titanim.slotOpenUrl("mailto:818151@mail.ru");
        }
    }

    QueryDialog {
        id: joinDialog;
        icon: "images/logo.png";
        titleText: "How to Create a Profile";
        message: "You can become a VK user by receiving an invitation from one of the members.\n\n"
        + "When one of your friends invites you, a message with the login and password will be sent "
        + "to your email. No additional actions are required – you can start using your new profile "
        + "right away by entering your login and password.";
        acceptButtonText: "OK";
    }

    QueryDialog {
        id: aboutDialog;
        titleText: "Ruslan Nazarov";
        message: "Twitter: @rnazarov\n"
        + "Jabber: 818151@jabber.ru\n"
        + "Mail: 818151@mail.ru\n"
        + "VK: vk.com/pause";
        acceptButtonText: "OK";
    }

    Text{
        id: privacyPolicy;
        anchors.top: column.bottom;
        anchors.topMargin: 12;
        anchors.horizontalCenter: parent.horizontalCenter;
        color: "white";
        text: qsTr("Privacy Policy");
        font.pixelSize: 20;
        MouseArea{
            anchors.fill: parent;
            onClicked: {
                privacyDialog.open();
            }
        }
    }

    Text{
        anchors.top: privacyPolicy.bottom;
        anchors.topMargin: 7;
        anchors.horizontalCenter: parent.horizontalCenter;
        color: "white";
        text: qsTr("Sign up for VKontakte");
        font.pixelSize: 20;
        MouseArea{
            anchors.fill: parent;
            onClicked: {
                joinDialog.open();
            }
        }
    }

    Text{
        width: parent.width;
        anchors.bottom: parent.bottom;
        anchors.bottomMargin: 7;
        anchors.horizontalCenter: parent.horizontalCenter;
        color: "white";
        horizontalAlignment: Text.AlignHCenter;
        text: "© 2012 Ruslan Nazarov";
        font.pixelSize: 15;
        MouseArea{
            width: parent.width;
            height: 100;
            anchors.bottom: parent.bottom;
            onClicked: {
                aboutDialog.open();
            }
        }
    }
}
