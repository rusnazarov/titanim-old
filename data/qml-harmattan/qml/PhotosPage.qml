/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0

Page{
    id: photosPage;
    property string uid: "";
    property bool isNextLoad: false;
    orientationLock: PageOrientation.LockPortrait;

    TitleBar{
        id: titleBar;

        MouseArea{
            anchors.fill: parent;
            onClicked: {
                photos.positionViewAtBeginning();
            }
        }

        BorderImage{
            id: btnBack;
            width: 140;
            anchors.left: parent.left;
            anchors.leftMargin: 10;
            anchors.verticalCenter: parent.verticalCenter;
            source: "images/btn_back.sci";

            Text {
                anchors.centerIn: parent;
                color: "white";
                text: qsTr("Back");
                font.pixelSize: 21;
                font.bold: true;
            }
            MouseArea{
                anchors.fill: parent;
                onClicked: {
                    pageStack.pop();
                }
            }
        }

        Label{
            anchors.left: btnBack.right;
            anchors.leftMargin: 5;
            anchors.right: parent.right;
            anchors.rightMargin: 5;
            anchors.verticalCenter: parent.verticalCenter;
            color: "white";
            text: qsTr("Photos");
            horizontalAlignment: Text.AlignHCenter;
            font.pixelSize: 27;
            font.bold: true;
        }
    }

    Component{
        id: footerUpd;
        Row{
            height: 70;
            anchors.left: parent.left;
            anchors.leftMargin: 35;
            spacing: 30;

            BusyIndicator {
                id: updIndicator;
                anchors.verticalCenter: parent.verticalCenter;
                platformStyle: BusyIndicatorStyle {
                    size: "medium";
                }
                running: isNextLoad;
            }

            Text {
                id: textUpdate;
                anchors.left: parent.left;
                anchors.leftMargin: 70;
                anchors.verticalCenter: parent.verticalCenter;
                text: qsTr("Loading...");
                font.pixelSize: 20
                font.bold: true;
                color: "#505050";
            }
        }
    }

    Rectangle{
        width: parent.width;
        anchors.top: titleBar.bottom;
        anchors.bottom: parent.bottom;
        color: "white";

        GridView{
            id: photos;
            anchors.fill: parent;
            anchors.topMargin: mainView.showStatusBar ? 8 : 10; //qml bug
            anchors.leftMargin: 8;
            cellHeight: 118;
            cellWidth: 118;
            footer: isNextLoad ? footerUpd : null;
            model: photoModel;
            delegate: PhotosDelegate{}

            onAtYEndChanged: {
                if (photos.atYEnd && !isNextLoad && photos.count >= 28){ //todo
                    isNextLoad = true;
                    titanim.slotPhotosGetAllNext(uid);
                }
            }

            ScrollDecorator {
                flickableItem: photos;
            }
        }
    }

    Connections{
        target: titanim;

        onPhotoFinished:{
            isNextLoad = false;
        }

        onStatusChanged:{
            if (!isOnline){
                isNextLoad = false;
            }
        }
    }
}
