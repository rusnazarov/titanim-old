/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.0

PageStackWindow{
    id: mainView;
    showStatusBar: settings.showStatusBar;
    initialPage: titanim.autoConnect ? newsPage : authorizePage;

    AuthorizePage{
        id:authorizePage;
    }

    NewsPage{
        id: newsPage;
    }

    PhotoViewerPage{
        id: photoViewerPage;
    }

    WallPage{
        id: wallPage;
    }

    WallCommentsPage{
        id: wallCommentsPage;
    }

    MessagesPage{
        id: messagesPage;
    }

    ChatPage{
        id: chatPage;
    }

    AudioPage{
        id: audioPage;
    }

    RosterPage{
        id: rosterPage;
    }

    RosterUserPage{
        id: rosterUserPage;
    }

    TabBar{
        id: tabBar;
        visible: false;
        nativ: settings.isNativToolBar;
        countMessages: chat.countUnreadMessages;
    }

    CaptchaPage{
        id: captchaPage;
    }

    ProfilePage{
        id: profilePage;
    }

    PhotosPage{
        id: photosPage;
    }

    PhotosViewerPage{
        id: photosViewerPage;
    }

    VideoPage{
        id: videoPage;
    }

    PhotosLocalSheet{
        id: photosLocalSheet;
        onSend:{
            titanim.wallPost(photosLocalSheet.textImg, photosLocalSheet.uid, photosLocalSheet.sourceImg, photosLocalSheet.rotationImg);
            photosLocalSheet.uid = "";
            photosLocalSheet.textImg = "";
            photosLocalSheet.sourceImg = "";
            photosLocalSheet.rotationImg = 0;
        }
    }

    InfoBanner{
        id: bannerMsg;
        property string uid: "";
        text: qsTr("New message");
        iconSource: "images/tab_messages_up.png";
        topMargin: 120;
        leftMargin: 7;
        MouseArea{
            anchors.fill: parent;
            onClicked: {
                bannerMsg.hide();
                if(bannerMsg.uid != ""){
                    titanim.slotOpenChatWindow(bannerMsg.uid);
                    mainView.pageStack.push(chatPage);
                } else {
                    tabBar.currentTab = 2;
                    mainView.pageStack.clear();
                    mainView.pageStack.push(messagesPage);
                }
            }
        }
    }

    InfoBanner{
        id: bannerNotification;
        topMargin: 120;
        leftMargin: 7;
        MouseArea{
            anchors.fill: parent;
            onClicked: {
                bannerNotification.hide();
            }
        }
    }

    Connections{
        target: titanim;
        onStatusChanged:{
            if (isOnline && mainView.pageStack.currentPage == authorizePage){
                mainView.pageStack.replace(newsPage);
            }
            if (isOnline)
                bannerNotification.hide();
        }

        onMessageReceived:{
            if (mainView.pageStack.currentPage != messagesPage &&
                mainView.pageStack.currentPage != chatPage){
                  bannerMsg.uid = uid;
                  bannerMsg.show();
            }
        }

        onShowAuthForm:{
            mainView.pageStack.clear();
            tabBar.currentTab = 0;
            mainView.pageStack.push(authorizePage);
        }

        onShowNotification:{
            bannerNotification.text = text;
            bannerNotification.show();
        }

        onCaptcha:{
            captchaPage.captchaSid = captchaSid;
            captchaPage.captchaUrl = captchaImg;
            mainView.pageStack.push(captchaPage);
        }
    }
}
