/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

import QtQuick 1.0

Item{
    id: videoDelegate;
    width: parent.width;
    height: imgVideo.height > (title.height + duration.height + domain.height) ?  imgVideo.height + 20 : title.height + duration.height + domain.height + 20;
    //ListView.delayRemove: avatar.status == Image.Ready ? false : true;

    Row{
        id: video;
        height: parent.height;
        x: 10;
        spacing: 15;

        Image{
            id: imgVideo;
            width: 120;
            anchors.verticalCenter: parent.verticalCenter;
            fillMode: Image.PreserveAspectFit;
            smooth: true;
            source: model.decoration;
        }

        Column{
            anchors.verticalCenter: parent.verticalCenter;
            Text{
                id: title;
                width: text.length ? videoDelegate.width - imgVideo.width - video.x - video.spacing - 7 : 0;
                color: "#254e84";
                font.pointSize: titanim.fontPointSize;
                font.bold: true;
                wrapMode: Text.Wrap;
                text: model.display;
            }
            Text{
                id: duration;
                width: text.length ? videoDelegate.width - imgVideo.width - video.x - video.spacing - 7 : 0;
                color: "black";
                font.pointSize: titanim.fontPointSize - 3;
                font.bold: true;
                text: model.duration;
            }
            Text{
                id: domain;
                width: text.length ? videoDelegate.width - imgVideo.width - video.x - video.spacing - 7 : 0;
                color: "#505050";
                font.pointSize: titanim.fontPointSize - 4;
                text: model.domain;
            }
        }
    }

    Rectangle {
        width:  parent.width;
        height: 1;
        y: videoDelegate.height - 1;
        color: "#c0c0c0"
    }

    MouseArea{
        anchors.fill: parent;
        onClicked: {
            videoCpp.playVideo(model.url);
        }
    }
}
