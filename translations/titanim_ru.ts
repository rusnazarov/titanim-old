<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>About</name>
    <message>
        <location filename="../about.ui" line="26"/>
        <location filename="../ui_about.h" line="162"/>
        <source>TitanIM</source>
        <translation>TitanIM</translation>
    </message>
    <message>
        <location filename="../about.ui" line="45"/>
        <location filename="../ui_about.h" line="175"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../about.ui" line="54"/>
        <location filename="../ui_about.h" line="163"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TitanIM is a multiplatform client of the social network VK&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; All logotypes and trademarks, used in application, belong to corresponding owners&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TitanIM, кросплатформенный клиент социальной сети ВКонтакте&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Все логотипы и торговые марки используемые в приложении принадлежат соответствующим владельцам&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message utf8="true">
        <location filename="../about.ui" line="79"/>
        <location filename="../ui_about.h" line="170"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;© 2010, Ruslan Nazarov&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;© 2010, Руслан Назаров&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="94"/>
        <location filename="../ui_about.h" line="186"/>
        <source>Authors</source>
        <translation>Авторы</translation>
    </message>
    <message>
        <location filename="../about.ui" line="109"/>
        <location filename="../ui_about.h" line="176"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Ruslan Nazarov (http://vkontakte.ru/id6230054)&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Main developer and Project founder&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Yusuke Kamiyamane (http://yusukekamiyamane.com)&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Icons of the program&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Руслан Назаров (http://vkontakte.ru/id6230054)&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Главный разработчик и основатель Проекта&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Yusuke Kamiyamane (http://yusukekamiyamane.com)&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Иконки программы&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="125"/>
        <location filename="../ui_about.h" line="201"/>
        <source>Thanks To</source>
        <translation>Благодарности</translation>
    </message>
    <message utf8="true">
        <location filename="../about.ui" line="140"/>
        <location filename="../ui_about.h" line="187"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Руслан Нигматуллин&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Алексей Сидоров&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Виталий Петров&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Алексей Онуфрийчук&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Алексей Чернышов&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Артём Казаков&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Тимур Исхаков&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;theMIROn&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../about.ui" line="159"/>
        <location filename="../ui_about.h" line="208"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="../about.ui" line="171"/>
        <location filename="../ui_about.h" line="202"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://titanim.ru&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;The official website of the program&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://vk.com/titanim&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;The official VK group of the program&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://titanim.ru&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Официальный сайт программы&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://vk.com/titanim&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Официальная группа ВКонтакте&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="208"/>
        <location filename="../ui_about.h" line="209"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>AudioPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="117"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="136"/>
        <source>Audio</source>
        <translation>Аудиозаписи</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="170"/>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="196"/>
        <source>Pull down to refresh...</source>
        <translation>Потяните вниз, чтобы обновить...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="177"/>
        <source>Last updated: </source>
        <translation>Обновлено: </translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="187"/>
        <source>Release to refresh...</source>
        <translation>Отпустите, чтобы обновить...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="230"/>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="233"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="267"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="310"/>
        <source>Loading...</source>
        <translation>Загрузка...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="348"/>
        <source>You have no audio files yet</source>
        <translation>Ни одной аудиозаписи не найдено</translation>
    </message>
</context>
<context>
    <name>AuthorizePage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/AuthorizePage.qml" line="45"/>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AuthorizePage.qml" line="48"/>
        <source>Next</source>
        <translation>Далее</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AuthorizePage.qml" line="59"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AuthorizePage.qml" line="63"/>
        <source>Log In</source>
        <translation>Вход</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AuthorizePage.qml" line="108"/>
        <source>Sign up for VKontakte</source>
        <translation>Регистрация ВКонтакте</translation>
    </message>
</context>
<context>
    <name>CaptchaPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/CaptchaPage.qml" line="27"/>
        <source>Captcha</source>
        <translation>Captcha</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/CaptchaPage.qml" line="48"/>
        <source>Enter captcha</source>
        <translation>Введите код</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/CaptchaPage.qml" line="51"/>
        <source>Send</source>
        <translation>Отправить</translation>
    </message>
</context>
<context>
    <name>ChatForm</name>
    <message>
        <location filename="../chat/chatform.ui" line="14"/>
        <location filename="../ui_chatform.h" line="75"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../chat/chatform.cpp" line="123"/>
        <source>Status change: Online</source>
        <translation>Смена статуса: подключен(а)</translation>
    </message>
    <message>
        <location filename="../chat/chatform.cpp" line="135"/>
        <source>Status change: Offline</source>
        <translation>Смена статуса: отключен(а)</translation>
    </message>
    <message>
        <location filename="../chat/chatform.cpp" line="145"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
</context>
<context>
    <name>ChatPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/ChatPage.qml" line="42"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ChatPage.qml" line="104"/>
        <source>Reply Quoted</source>
        <translation>Ответить с цитатой</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ChatPage.qml" line="105"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ChatPage.qml" line="106"/>
        <source>Mark as Unread</source>
        <translation>Отметить как новое</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ChatPage.qml" line="107"/>
        <source>Open VK</source>
        <translation>Открыть ВКонтакте</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ChatPage.qml" line="108"/>
        <source>Delete</source>
        <translation>Удалить с сервера</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ChatPage.qml" line="134"/>
        <source>Type your text here...</source>
        <translation>Введите Ваше сообщение...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ChatPage.qml" line="151"/>
        <source>Send</source>
        <translation>Отпр.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <location filename="../ui_mainwindow.h" line="340"/>
        <source>TitanIM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="45"/>
        <location filename="../ui_mainwindow.h" line="342"/>
        <source>Main menu</source>
        <translation>Главное меню</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="48"/>
        <location filename="../mainwindow.ui" line="77"/>
        <location filename="../mainwindow.ui" line="112"/>
        <location filename="../mainwindow.ui" line="144"/>
        <location filename="../mainwindow.ui" line="173"/>
        <location filename="../mainwindow.ui" line="203"/>
        <location filename="../mainwindow.ui" line="229"/>
        <location filename="../mainwindow.ui" line="255"/>
        <location filename="../mainwindow.ui" line="596"/>
        <location filename="../ui_mainwindow.h" line="344"/>
        <location filename="../ui_mainwindow.h" line="348"/>
        <location filename="../ui_mainwindow.h" line="352"/>
        <location filename="../ui_mainwindow.h" line="357"/>
        <location filename="../ui_mainwindow.h" line="361"/>
        <location filename="../ui_mainwindow.h" line="365"/>
        <location filename="../ui_mainwindow.h" line="369"/>
        <location filename="../ui_mainwindow.h" line="373"/>
        <location filename="../ui_mainwindow.h" line="405"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="74"/>
        <location filename="../ui_mainwindow.h" line="346"/>
        <source>List of friends</source>
        <translation>Списки друзей</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="109"/>
        <location filename="../ui_mainwindow.h" line="350"/>
        <source>Contacts filter</source>
        <translation>Фильтр контактов</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="119"/>
        <location filename="../ui_mainwindow.h" line="353"/>
        <source>Ctrl+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="141"/>
        <location filename="../ui_mainwindow.h" line="355"/>
        <source>Show/hide offline</source>
        <translation>Показать/скрыть отключенных</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="170"/>
        <location filename="../ui_mainwindow.h" line="359"/>
        <source>Sound on/off</source>
        <translation>Звук вкл/выкл</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="200"/>
        <location filename="../mainwindow.cpp" line="58"/>
        <location filename="../ui_mainwindow.h" line="363"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="226"/>
        <location filename="../ui_mainwindow.h" line="367"/>
        <source>VK page</source>
        <translation>Страница ВКонтакте</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="252"/>
        <location filename="../ui_mainwindow.h" line="371"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="364"/>
        <location filename="../ui_mainwindow.h" line="376"/>
        <source>Friends</source>
        <translation>Друзья</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="367"/>
        <location filename="../mainwindow.ui" line="396"/>
        <location filename="../mainwindow.ui" line="425"/>
        <location filename="../mainwindow.ui" line="454"/>
        <location filename="../mainwindow.ui" line="483"/>
        <location filename="../mainwindow.ui" line="512"/>
        <location filename="../ui_mainwindow.h" line="378"/>
        <location filename="../ui_mainwindow.h" line="382"/>
        <location filename="../ui_mainwindow.h" line="386"/>
        <location filename="../ui_mainwindow.h" line="390"/>
        <location filename="../ui_mainwindow.h" line="394"/>
        <location filename="../ui_mainwindow.h" line="398"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="393"/>
        <location filename="../ui_mainwindow.h" line="380"/>
        <source>Photos</source>
        <translation>Фотографии</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="422"/>
        <location filename="../ui_mainwindow.h" line="384"/>
        <source>Videos</source>
        <translation>Видеозаписи</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="451"/>
        <location filename="../ui_mainwindow.h" line="388"/>
        <source>Notes</source>
        <translation>Заметки</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="480"/>
        <location filename="../ui_mainwindow.h" line="392"/>
        <source>Gifts</source>
        <translation>Подарки</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="509"/>
        <location filename="../ui_mainwindow.h" line="396"/>
        <source>Groups</source>
        <translation>Группы</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="563"/>
        <location filename="../mainwindow.cpp" line="53"/>
        <location filename="../mainwindow.cpp" line="1162"/>
        <location filename="../ui_mainwindow.h" line="399"/>
        <source>Online</source>
        <translation>В сети</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="572"/>
        <location filename="../mainwindow.cpp" line="54"/>
        <location filename="../mainwindow.cpp" line="1175"/>
        <location filename="../ui_mainwindow.h" line="400"/>
        <source>Offline</source>
        <translation>Не в сети</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="593"/>
        <location filename="../ui_mainwindow.h" line="403"/>
        <source>Change Status</source>
        <translation>Изменить статус</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="60"/>
        <source>Change profile</source>
        <translation>Сменить профиль</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="62"/>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="52"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="56"/>
        <source>Edit text</source>
        <translation>Изменить текст</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="59"/>
        <source>Quiet mode</source>
        <translation>Тихий режим</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="690"/>
        <source>Downloading settings</source>
        <translation>Загружаю настройки</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="714"/>
        <source>Connecting...</source>
        <translation>Подключаюсь...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="865"/>
        <source>Server is not available</source>
        <translation>Сервер недоступен</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="898"/>
        <source>Receiving Contact List</source>
        <translation>Получаю контакт лист</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1451"/>
        <source>Cache clear: %1 MB</source>
        <translation>Кеш очищен: %1 MB</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1945"/>
        <source>Upload image</source>
        <translation>Отправка изображения</translation>
    </message>
    <message>
        <source>Dialogs</source>
        <translation type="obsolete">Диалоги</translation>
    </message>
    <message>
        <source>All friends</source>
        <translation type="obsolete">Все друзья</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1058"/>
        <source>Privacy error</source>
        <translation>Ошибка приватности</translation>
    </message>
    <message>
        <source>New message</source>
        <translation type="obsolete">Новое сообщение</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1211"/>
        <source>My Friends (%1)</source>
        <translation>Мои Друзья (%1)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1219"/>
        <source>My Photos (%1)</source>
        <translation>Мои Фотографии (%1)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1227"/>
        <source>My Videos (%1)</source>
        <translation>Мои Видеозаписи (%1)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1235"/>
        <source>My Notes (%1)</source>
        <translation>Мои Заметки (%1)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1243"/>
        <source>My Gifts (%1)</source>
        <translation>Мои Подарки (%1)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1251"/>
        <source>My Groups (%1)</source>
        <translation>Мои Группы (%1)</translation>
    </message>
</context>
<context>
    <name>MessagesPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/MessagesPage.qml" line="37"/>
        <source>Messages</source>
        <translation>Сообщения</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/MessagesPage.qml" line="70"/>
        <location filename="../data/qml-harmattan/qml/MessagesPage.qml" line="95"/>
        <source>Pull down to refresh...</source>
        <translation>Потяните вниз, чтобы обновить...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/MessagesPage.qml" line="77"/>
        <source>Last updated: </source>
        <translation>Обновлено: </translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/MessagesPage.qml" line="87"/>
        <source>Release to refresh...</source>
        <translation>Отпустите, чтобы обновить...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/MessagesPage.qml" line="122"/>
        <source>Loading...</source>
        <translation>Загрузка...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/MessagesPage.qml" line="156"/>
        <source>No messages</source>
        <translation>Нет сообщений</translation>
    </message>
</context>
<context>
    <name>NewsPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="32"/>
        <source>News</source>
        <translation>Новости</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="73"/>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="99"/>
        <source>Pull down to refresh...</source>
        <translation>Потяните вниз, чтобы обновить...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="80"/>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="135"/>
        <source>Last updated: </source>
        <translation>Обновлено: </translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="90"/>
        <source>Release to refresh...</source>
        <translation>Отпустите, чтобы обновить...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="128"/>
        <source>Updating...</source>
        <translation>Идет обновление...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="165"/>
        <source>Loading...</source>
        <translation>Загрузка...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="204"/>
        <source>No news</source>
        <translation>Новостей пока нет</translation>
    </message>
</context>
<context>
    <name>PhotosLocalSheet</name>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosLocalSheet.qml" line="32"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosLocalSheet.qml" line="47"/>
        <source>Send</source>
        <translation>Отправить</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosLocalSheet.qml" line="135"/>
        <source>Description</source>
        <translation>Введите описание</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosLocalSheet.qml" line="156"/>
        <source>Gallery is not available for your firmware</source>
        <translation>Галерея недоступна для вашей прошивки</translation>
    </message>
</context>
<context>
    <name>PhotosPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosPage.qml" line="44"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosPage.qml" line="63"/>
        <source>Photos</source>
        <translation>Фотографии</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosPage.qml" line="92"/>
        <source>Loading...</source>
        <translation>Загрузка...</translation>
    </message>
</context>
<context>
    <name>PhotosViewerPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosViewerPage.qml" line="45"/>
        <source>Save photo</source>
        <translation>Сохранить фотографию</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosViewerPage.qml" line="46"/>
        <source>Share photo</source>
        <translation>Поделиться фотографией</translation>
    </message>
</context>
<context>
    <name>ProfilePage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="55"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="75"/>
        <source>Profile</source>
        <translation>Профиль</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="75"/>
        <source>Page</source>
        <translation>Страница</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="192"/>
        <source>Send message</source>
        <translation>Написать сообщение</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="316"/>
        <source>Wall</source>
        <translation>Стена</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="349"/>
        <source>Type your text here...</source>
        <translation>Введите Ваше сообщение...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="366"/>
        <source>Send</source>
        <translation>Отпр.</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="420"/>
        <source>Loading...</source>
        <translation>Загрузка...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="457"/>
        <source>There are no posts on this wall yet</source>
        <translation>На стене пока нет ни одной записи</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="596"/>
        <source>Birthday</source>
        <translation>Дата рождения</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="614"/>
        <source>Mobile phone</source>
        <translation>Мобильный телефон</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="646"/>
        <source>Friends</source>
        <translation>Друзья</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="678"/>
        <source>Mutual friends</source>
        <translation>Общие друзья</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="714"/>
        <source>Photos</source>
        <translation>Фотографии</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="740"/>
        <source>Music</source>
        <translation>Аудиозаписи</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="772"/>
        <source>Videos</source>
        <translation>Видеозаписи</translation>
    </message>
</context>
<context>
    <name>RosterPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterPage.qml" line="41"/>
        <source>All Friends</source>
        <translation>Все друзья</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterPage.qml" line="52"/>
        <source>Friends Online</source>
        <translation>Друзья онлайн</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterPage.qml" line="89"/>
        <location filename="../data/qml-harmattan/qml/RosterPage.qml" line="92"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterPage.qml" line="132"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterPage.qml" line="169"/>
        <source>No friends found</source>
        <translation>Ни одного друга не найдено</translation>
    </message>
</context>
<context>
    <name>RosterUserPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterUserPage.qml" line="59"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterUserPage.qml" line="86"/>
        <source>Mutual friends</source>
        <translation>Общие друзья</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterUserPage.qml" line="86"/>
        <source>All friends</source>
        <translation>Все друзья</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterUserPage.qml" line="122"/>
        <location filename="../data/qml-harmattan/qml/RosterUserPage.qml" line="125"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterUserPage.qml" line="161"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterUserPage.qml" line="198"/>
        <source>No friends found</source>
        <translation>Ни одного друга не найдено</translation>
    </message>
</context>
<context>
    <name>SettingsSheet</name>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="27"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="39"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="64"/>
        <source>System</source>
        <translation>Система</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="69"/>
        <source>Show status bar</source>
        <translation>Показывать статус бар</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="79"/>
        <source>Native</source>
        <translation>Нативный вид</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="88"/>
        <source>Integrating event feed</source>
        <translation>Интеграция в канал новостей</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="98"/>
        <source>Clear cache</source>
        <translation>Очистить кеш</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="107"/>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="111"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="134"/>
        <source>General</source>
        <translation>Общее</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="213"/>
        <source>Show statuses</source>
        <translation>Показывать статусы</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="139"/>
        <source>Save last read news</source>
        <translation>Запоминать положение в ленте новостей</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="153"/>
        <source>Avatar size:</source>
        <translation>Размер аватара:</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="179"/>
        <source>Text size:</source>
        <translation>Размер текста:</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="199"/>
        <source>Contact List</source>
        <translation>Список контактов</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="204"/>
        <source>Sort by name</source>
        <translation>Сортировать по имени</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="221"/>
        <source>Chat window</source>
        <translation>Окно сообщений</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="226"/>
        <source>Send typing notifications</source>
        <translation>Отправка уведомлений о наборе текста</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="234"/>
        <source>Notifications</source>
        <translation>Оповещения</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="239"/>
        <source>Sound</source>
        <translation>Звук</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="248"/>
        <source>Vibrate</source>
        <translation>Вибрация</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="257"/>
        <source>Change profile</source>
        <translation>Сменить профиль</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="270"/>
        <source>Restart TitanIM</source>
        <translation>Перезапуск TitanIM</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="271"/>
        <source>You must restart TitanIM for the changes to take effect. Restart now?</source>
        <translation>Изменения вступят в силу после перезапуска программы. Перезапустить сейчас?</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="272"/>
        <source>OK</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="273"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>SpellTextEdit</name>
    <message>
        <location filename="../spellchecker/SpellTextEdit.cpp" line="202"/>
        <source>Ignore</source>
        <translation>Пропустить</translation>
    </message>
    <message>
        <location filename="../spellchecker/SpellTextEdit.cpp" line="203"/>
        <source>Add</source>
        <translation>Добавить в словарь</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <location filename="../data/qml-desktop/qml/StatusBar.qml" line="50"/>
        <location filename="../data/qml-desktop/qml/StatusBar.qml" line="61"/>
        <location filename="../data/qml-desktop/qml/StatusBar.qml" line="85"/>
        <location filename="../share/qml/StatusBar.qml" line="50"/>
        <location filename="../share/qml/StatusBar.qml" line="61"/>
        <location filename="../share/qml/StatusBar.qml" line="85"/>
        <source>Offline</source>
        <translation>Не в сети</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/StatusBar.qml" line="60"/>
        <location filename="../data/qml-desktop/qml/StatusBar.qml" line="81"/>
        <location filename="../share/qml/StatusBar.qml" line="60"/>
        <location filename="../share/qml/StatusBar.qml" line="81"/>
        <source>Online</source>
        <translation>В сети</translation>
    </message>
</context>
<context>
    <name>TabBar</name>
    <message>
        <location filename="../data/qml-harmattan/qml/TabBar.qml" line="70"/>
        <source>News</source>
        <translation>Новости</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/TabBar.qml" line="125"/>
        <source>Wall</source>
        <translation>Стена</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/TabBar.qml" line="160"/>
        <source>Messages</source>
        <translation>Сообщения</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/TabBar.qml" line="217"/>
        <source>Audio</source>
        <translation>Аудио</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/TabBar.qml" line="253"/>
        <source>Friends</source>
        <translation>Друзья</translation>
    </message>
</context>
<context>
    <name>TabForm</name>
    <message>
        <location filename="../chat/tabform.ui" line="23"/>
        <location filename="../ui_tabform.h" line="285"/>
        <source>Chat window</source>
        <translation>Окно сообщений</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="103"/>
        <location filename="../ui_tabform.h" line="287"/>
        <source>Contact history</source>
        <translation>История контакта</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="138"/>
        <location filename="../ui_tabform.h" line="291"/>
        <source>Smilies</source>
        <translation>Смайлы</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="173"/>
        <location filename="../ui_tabform.h" line="295"/>
        <source>Contact details</source>
        <translation>Профиль пользователя</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="183"/>
        <location filename="../ui_tabform.h" line="298"/>
        <source>Ctrl+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="208"/>
        <location filename="../ui_tabform.h" line="300"/>
        <source>Message filter</source>
        <translation>Фильтр сообщений</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="218"/>
        <location filename="../ui_tabform.h" line="303"/>
        <source>Ctrl+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="246"/>
        <location filename="../ui_tabform.h" line="305"/>
        <source>Swap layout</source>
        <translation>Смена раскладки</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="256"/>
        <location filename="../ui_tabform.h" line="308"/>
        <source>Ctrl+T</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="281"/>
        <location filename="../ui_tabform.h" line="310"/>
        <source>Quote</source>
        <translation>Цитировать</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="291"/>
        <location filename="../ui_tabform.h" line="313"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="329"/>
        <location filename="../ui_tabform.h" line="315"/>
        <source>Clear chat log</source>
        <translation>Очистить окно чата</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="361"/>
        <location filename="../ui_tabform.h" line="319"/>
        <source>Close Tab</source>
        <translation>Закрыть вкладку</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="371"/>
        <location filename="../ui_tabform.h" line="322"/>
        <source>Ctrl+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="417"/>
        <location filename="../ui_tabform.h" line="325"/>
        <source>VK page</source>
        <translation>Страница ВКонтакте</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="452"/>
        <location filename="../ui_tabform.h" line="329"/>
        <source>Send message on enter</source>
        <translation>Отправлять сообщения по Enter</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="472"/>
        <location filename="../ui_tabform.h" line="332"/>
        <source>Symbols left: </source>
        <translation>Осталось: </translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="479"/>
        <location filename="../ui_tabform.h" line="333"/>
        <source>4096</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="520"/>
        <location filename="../ui_tabform.h" line="335"/>
        <source>Send message</source>
        <translation>Отправить сообщение</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="523"/>
        <location filename="../ui_tabform.h" line="337"/>
        <source>Send</source>
        <translation>Отправить</translation>
    </message>
    <message>
        <source>Send to wall</source>
        <translation type="obsolete">Отправить на стену</translation>
    </message>
    <message>
        <source>Invite to TitanIM</source>
        <translation type="obsolete">Пригласить в TitanIM</translation>
    </message>
    <message>
        <source>Photo</source>
        <translation type="obsolete">Фото</translation>
    </message>
    <message>
        <source>Music</source>
        <translation type="obsolete">Музыка</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="obsolete">Видео</translation>
    </message>
    <message>
        <source>VK instant messenger</source>
        <translation type="obsolete">вот клиент для обмена мгновенными сообщениями</translation>
    </message>
    <message>
        <location filename="../chat/tabform.cpp" line="408"/>
        <location filename="../chat/tabform.cpp" line="781"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../chat/tabform.cpp" line="493"/>
        <source>Status change: Online</source>
        <translation>Смена статуса: подключен(а)</translation>
    </message>
    <message>
        <location filename="../chat/tabform.cpp" line="510"/>
        <source>Status change: Offline</source>
        <translation>Смена статуса: отключен(а)</translation>
    </message>
    <message>
        <location filename="../chat/tabform.cpp" line="846"/>
        <source>qwertyuiop[]asdfghjkl;&apos;zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:&quot;ZXCVBNM&lt;&gt;?</source>
        <translation>йцукенгшщзхъфывапролджэячсмитьбю.ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ,</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <location filename="../data/qml-desktop/qml/ToolBar.qml" line="58"/>
        <location filename="../share/qml/ToolBar.qml" line="58"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/ToolBar.qml" line="59"/>
        <location filename="../share/qml/ToolBar.qml" line="59"/>
        <source>Clear cache</source>
        <translation>Очистить кеш</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/ToolBar.qml" line="60"/>
        <location filename="../share/qml/ToolBar.qml" line="60"/>
        <source>Forget Password</source>
        <translation>Забыть пароль</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/ToolBar.qml" line="61"/>
        <location filename="../share/qml/ToolBar.qml" line="61"/>
        <source>Change profile</source>
        <translation>Сменить профиль</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/ToolBar.qml" line="62"/>
        <location filename="../share/qml/ToolBar.qml" line="62"/>
        <source>About TitanIM</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/ToolBar.qml" line="63"/>
        <location filename="../share/qml/ToolBar.qml" line="63"/>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
</context>
<context>
    <name>VideoPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/VideoPage.qml" line="55"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/VideoPage.qml" line="75"/>
        <source>Video</source>
        <translation>Видеозаписи</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/VideoPage.qml" line="111"/>
        <location filename="../data/qml-harmattan/qml/VideoPage.qml" line="114"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/VideoPage.qml" line="148"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/VideoPage.qml" line="191"/>
        <source>Loading...</source>
        <translation>Загрузка...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/VideoPage.qml" line="229"/>
        <source>No videos</source>
        <translation>Ни одной видеозаписи не найдено</translation>
    </message>
</context>
<context>
    <name>WallCommentsPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="63"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="84"/>
        <source>Comments</source>
        <translation>Комментарии</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="111"/>
        <source>Delete wall post?</source>
        <translation>Удалить запись?</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="113"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="114"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="280"/>
        <source>Like</source>
        <translation>Понравилось</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="280"/>
        <source>people</source>
        <translation>чел.</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="321"/>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="346"/>
        <source>Pull down to refresh...</source>
        <translation>Потяните вниз, чтобы обновить...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="328"/>
        <source>Last updated: </source>
        <translation>Обновлено: </translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="338"/>
        <source>Release to refresh...</source>
        <translation>Отпустите, чтобы обновить...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="449"/>
        <source>Comment...</source>
        <translation>Комментировать...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="464"/>
        <source>Send</source>
        <translation>Отпр.</translation>
    </message>
</context>
<context>
    <name>WallPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="37"/>
        <source>Wall</source>
        <translation>Стена</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">Выход</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="55"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="139"/>
        <source>What&apos;s new?</source>
        <translation>Что у Вас нового?</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="148"/>
        <source>Send</source>
        <translation>Отпр.</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="198"/>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="223"/>
        <source>Pull down to refresh...</source>
        <translation>Потяните вниз, чтобы обновить...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="205"/>
        <source>Last updated: </source>
        <translation>Обновлено: </translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="215"/>
        <source>Release to refresh...</source>
        <translation>Отпустите, чтобы обновить...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="250"/>
        <source>Loading...</source>
        <translation>Загрузка...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="285"/>
        <source>There are no posts on this wall yet</source>
        <translation>На стене пока нет ни одной записи</translation>
    </message>
</context>
<context>
    <name>captchaView</name>
    <message>
        <location filename="../vk/captchaview.cpp" line="64"/>
        <source>Send</source>
        <translation>Отправить</translation>
    </message>
</context>
<context>
    <name>chat</name>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="78"/>
        <location filename="../share/qml/chat.qml" line="78"/>
        <source>Reply Quoted</source>
        <translation>Ответить с цитатой</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="79"/>
        <location filename="../share/qml/chat.qml" line="79"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="80"/>
        <location filename="../share/qml/chat.qml" line="80"/>
        <source>Copy Text</source>
        <translation>Копировать текст</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="81"/>
        <location filename="../share/qml/chat.qml" line="81"/>
        <source>Mark as Unread</source>
        <translation>Отметить как новое</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="82"/>
        <location filename="../share/qml/chat.qml" line="82"/>
        <source>Open VK</source>
        <translation>Открыть ВКонтакте</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="83"/>
        <location filename="../share/qml/chat.qml" line="83"/>
        <source>Delete</source>
        <translation>Удалить с сервера</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="138"/>
        <location filename="../share/qml/chat.qml" line="138"/>
        <source>Download next 30 messages</source>
        <translation>Загрузить еще 30 сообщений</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="139"/>
        <location filename="../share/qml/chat.qml" line="139"/>
        <source>Download next 50 messages</source>
        <translation>Загрузить еще 50 сообщений</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="140"/>
        <location filename="../share/qml/chat.qml" line="140"/>
        <source>Download next 100 messages</source>
        <translation>Загрузить еще 100 сообщений</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="329"/>
        <location filename="../share/qml/chat.qml" line="329"/>
        <source>VK page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="330"/>
        <location filename="../share/qml/chat.qml" line="330"/>
        <source>Photo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="331"/>
        <location filename="../share/qml/chat.qml" line="331"/>
        <source>Music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="332"/>
        <location filename="../share/qml/chat.qml" line="332"/>
        <source>Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="362"/>
        <location filename="../share/qml/chat.qml" line="362"/>
        <source>Symbols left: </source>
        <translation>Осталось: </translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="384"/>
        <location filename="../share/qml/chat.qml" line="384"/>
        <source>Send</source>
        <translation>Отправить</translation>
    </message>
</context>
<context>
    <name>contactListModel</name>
    <message>
        <location filename="../roster/contactlistmodel.cpp" line="143"/>
        <source>Nickname</source>
        <translation>Никнейм</translation>
    </message>
    <message>
        <location filename="../roster/contactlistmodel.cpp" line="149"/>
        <source>Birthday</source>
        <translation>Дата рождения</translation>
    </message>
    <message>
        <location filename="../roster/contactlistmodel.cpp" line="155"/>
        <source>Mobile phone</source>
        <translation>Моб. телефон</translation>
    </message>
    <message>
        <location filename="../roster/contactlistmodel.cpp" line="161"/>
        <source>Home phone</source>
        <translation>Дом. телефон</translation>
    </message>
</context>
<context>
    <name>cvk</name>
    <message>
        <source>VK | Authorization</source>
        <translation type="obsolete">ВКонтакте | Авторизация</translation>
    </message>
    <message>
        <location filename="../vk/cvk.cpp" line="193"/>
        <location filename="../vk/cvk.cpp" line="351"/>
        <location filename="../vk/cvk.cpp" line="1010"/>
        <source>Server is not available</source>
        <translation>Сервер недоступен</translation>
    </message>
    <message>
        <location filename="../vk/cvk.cpp" line="238"/>
        <source>User authorization failed</source>
        <translation>Ошибка авторизации</translation>
    </message>
    <message>
        <source>Authorization canceled</source>
        <translation type="obsolete">Авторизация отменена</translation>
    </message>
</context>
<context>
    <name>dialogsModel</name>
    <message>
        <location filename="../chat/dialogsmodel.cpp" line="64"/>
        <source>Monday</source>
        <translation>понедельник</translation>
    </message>
    <message>
        <location filename="../chat/dialogsmodel.cpp" line="65"/>
        <source>Tuesday</source>
        <translation>вторник</translation>
    </message>
    <message>
        <location filename="../chat/dialogsmodel.cpp" line="66"/>
        <source>Wednesday</source>
        <translation>среда</translation>
    </message>
    <message>
        <location filename="../chat/dialogsmodel.cpp" line="67"/>
        <source>Thursday</source>
        <translation>четверг</translation>
    </message>
    <message>
        <location filename="../chat/dialogsmodel.cpp" line="68"/>
        <source>Friday</source>
        <translation>пятница</translation>
    </message>
    <message>
        <location filename="../chat/dialogsmodel.cpp" line="69"/>
        <source>Saturday</source>
        <translation>суббота</translation>
    </message>
    <message>
        <location filename="../chat/dialogsmodel.cpp" line="70"/>
        <source>Sunday</source>
        <translation>воскресенье</translation>
    </message>
    <message>
        <location filename="../chat/dialogsmodel.cpp" line="78"/>
        <source>yesterday</source>
        <translation>вчера</translation>
    </message>
</context>
<context>
    <name>newsfeedModel</name>
    <message>
        <source>Monday</source>
        <translation type="obsolete">понедельник</translation>
    </message>
    <message>
        <source>Tuesday</source>
        <translation type="obsolete">вторник</translation>
    </message>
    <message>
        <source>Wednesday</source>
        <translation type="obsolete">среда</translation>
    </message>
    <message>
        <source>Thursday</source>
        <translation type="obsolete">четверг</translation>
    </message>
    <message>
        <source>Friday</source>
        <translation type="obsolete">пятница</translation>
    </message>
    <message>
        <source>Saturday</source>
        <translation type="obsolete">суббота</translation>
    </message>
    <message>
        <source>Sunday</source>
        <translation type="obsolete">воскресенье</translation>
    </message>
    <message>
        <location filename="../newsfeed/newsfeedmodel.cpp" line="78"/>
        <source>Today at %1</source>
        <translation>сегодня в %1</translation>
    </message>
    <message>
        <location filename="../newsfeed/newsfeedmodel.cpp" line="80"/>
        <source>Yesterday at %1</source>
        <translation>вчера в %1</translation>
    </message>
</context>
<context>
    <name>popupWindow</name>
    <message>
        <location filename="../notification/popupwindow.ui" line="14"/>
        <location filename="../ui_popupwindow.h" line="60"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>profileForm</name>
    <message>
        <location filename="../profileform.ui" line="26"/>
        <location filename="../ui_profileform.h" line="262"/>
        <source>Contact details</source>
        <translation>Профиль пользователя</translation>
    </message>
    <message>
        <location filename="../profileform.ui" line="74"/>
        <location filename="../profileform.ui" line="117"/>
        <location filename="../profileform.ui" line="163"/>
        <location filename="../profileform.ui" line="209"/>
        <location filename="../profileform.ui" line="255"/>
        <location filename="../profileform.ui" line="301"/>
        <location filename="../profileform.ui" line="350"/>
        <location filename="../ui_profileform.h" line="264"/>
        <location filename="../ui_profileform.h" line="266"/>
        <location filename="../ui_profileform.h" line="268"/>
        <location filename="../ui_profileform.h" line="270"/>
        <location filename="../ui_profileform.h" line="272"/>
        <location filename="../ui_profileform.h" line="274"/>
        <location filename="../ui_profileform.h" line="276"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../profileform.ui" line="107"/>
        <location filename="../ui_profileform.h" line="265"/>
        <source>Sex:</source>
        <translation>Пол:</translation>
    </message>
    <message>
        <location filename="../profileform.ui" line="153"/>
        <location filename="../ui_profileform.h" line="267"/>
        <source>Birthday:</source>
        <translation>Дата рождения:</translation>
    </message>
    <message>
        <location filename="../profileform.ui" line="199"/>
        <location filename="../ui_profileform.h" line="269"/>
        <source>Mobile phone:</source>
        <translation>Моб. телефон:</translation>
    </message>
    <message>
        <location filename="../profileform.ui" line="245"/>
        <location filename="../ui_profileform.h" line="271"/>
        <source>Home phone:</source>
        <translation>Дом. телефон:</translation>
    </message>
    <message>
        <location filename="../profileform.ui" line="291"/>
        <location filename="../ui_profileform.h" line="273"/>
        <source>University:</source>
        <translation>Вуз:</translation>
    </message>
    <message>
        <location filename="../profileform.ui" line="340"/>
        <location filename="../ui_profileform.h" line="275"/>
        <source>Faculty:</source>
        <translation>Факультет:</translation>
    </message>
    <message>
        <location filename="../profileform.ui" line="400"/>
        <location filename="../ui_profileform.h" line="277"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <source>Contact details - %1</source>
        <translation type="obsolete">Профиль пользователя - %1</translation>
    </message>
    <message>
        <source>Male</source>
        <translation type="obsolete">мужской</translation>
    </message>
    <message>
        <source>Female</source>
        <translation type="obsolete">женский</translation>
    </message>
    <message>
        <location filename="../profileform.cpp" line="116"/>
        <source>last seen</source>
        <comment>woman</comment>
        <translation>заходила</translation>
    </message>
    <message>
        <location filename="../profileform.cpp" line="116"/>
        <source>last seen</source>
        <comment>man</comment>
        <translation>заходил</translation>
    </message>
    <message>
        <location filename="../profileform.cpp" line="127"/>
        <source>minutes ago</source>
        <comment>5</comment>
        <translation>минут назад</translation>
    </message>
    <message>
        <location filename="../profileform.cpp" line="129"/>
        <source>minute ago</source>
        <comment>1</comment>
        <translation>минуту назад</translation>
    </message>
    <message>
        <location filename="../profileform.cpp" line="131"/>
        <source>minutes ago</source>
        <comment>2</comment>
        <translation>минуты назад</translation>
    </message>
    <message>
        <location filename="../profileform.cpp" line="139"/>
        <source>today at %1</source>
        <translation>сегодня в %1</translation>
    </message>
    <message>
        <location filename="../profileform.cpp" line="143"/>
        <source>yesterday at %1</source>
        <translation>вчера в %1</translation>
    </message>
</context>
<context>
    <name>settingsForm</name>
    <message>
        <location filename="../settingsform.ui" line="69"/>
        <location filename="../ui_settingsform.h" line="926"/>
        <source>General</source>
        <translation>Главные</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="78"/>
        <location filename="../ui_settingsform.h" line="928"/>
        <source>Contact List</source>
        <translation>Список контактов</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="87"/>
        <location filename="../ui_settingsform.h" line="930"/>
        <source>Chat window</source>
        <translation>Окно сообщений</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="96"/>
        <location filename="../ui_settingsform.h" line="932"/>
        <source>Notification</source>
        <translation>Оповещение</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="123"/>
        <location filename="../ui_settingsform.h" line="938"/>
        <source>Sound</source>
        <translation>Звук</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="132"/>
        <location filename="../ui_settingsform.h" line="940"/>
        <source>Spell checker</source>
        <translation>Орфография</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="141"/>
        <location filename="../ui_settingsform.h" line="942"/>
        <source>Proxy</source>
        <translation>Прокси</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="180"/>
        <location filename="../ui_settingsform.h" line="945"/>
        <source>Autoconnect on start</source>
        <translation>Автоматически подключаться при запуске</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="187"/>
        <location filename="../ui_settingsform.h" line="946"/>
        <source>Reconnect after disconnect</source>
        <translation>Подключаться после обрыва связи</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="194"/>
        <location filename="../ui_settingsform.h" line="947"/>
        <source>Hide on startup</source>
        <translation>Скрывать при запуске</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="201"/>
        <location filename="../ui_settingsform.h" line="948"/>
        <source>Thick window title</source>
        <translation>Обычный заголовок окна</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="208"/>
        <location filename="../ui_settingsform.h" line="949"/>
        <source>Always on top</source>
        <translation>Всегда наверху</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="220"/>
        <location filename="../ui_settingsform.h" line="950"/>
        <source> Language:</source>
        <translation> Язык (Language):</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1084"/>
        <location filename="../ui_settingsform.h" line="1010"/>
        <source>Russian (RU)</source>
        <translation>Русский (RU)</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="278"/>
        <location filename="../ui_settingsform.h" line="951"/>
        <source>Show offline friends</source>
        <translation>Показывать отключенных пользователей</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="105"/>
        <location filename="../ui_settingsform.h" line="934"/>
        <source>Events</source>
        <translation>События</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="114"/>
        <location filename="../settingsform.ui" line="936"/>
        <location filename="../ui_settingsform.h" line="936"/>
        <location filename="../ui_settingsform.h" line="1000"/>
        <source>Smilies</source>
        <translation>Смайлы</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="285"/>
        <location filename="../ui_settingsform.h" line="952"/>
        <source>Draw the background with using alternating colors</source>
        <translation>Рисовать фон альтернативными цветами</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="292"/>
        <location filename="../ui_settingsform.h" line="953"/>
        <source>Show avatars</source>
        <translation>Показывать аватары</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="299"/>
        <location filename="../ui_settingsform.h" line="954"/>
        <source>Show statuses</source>
        <translation>Показывать статусы</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="311"/>
        <location filename="../ui_settingsform.h" line="955"/>
        <source> Avatar size:</source>
        <translation>Размер аватара:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="343"/>
        <location filename="../ui_settingsform.h" line="956"/>
        <source>30</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="409"/>
        <location filename="../ui_settingsform.h" line="957"/>
        <source>Your name in the chat window:</source>
        <translation>Ваше имя в окне сообщений:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="416"/>
        <location filename="../ui_settingsform.h" line="958"/>
        <source>You</source>
        <translation>Я</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="428"/>
        <location filename="../ui_settingsform.h" line="959"/>
        <source>Don&apos;t play incoming message sound if message window is active</source>
        <translation>Без звука при входящем сообщении, если вкладка активна</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="435"/>
        <location filename="../ui_settingsform.h" line="960"/>
        <source>Send message on Ctrl+Enter</source>
        <translation>Отправлять по Ctrl + Enter</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="442"/>
        <location filename="../ui_settingsform.h" line="961"/>
        <source>Close the chat window after sending a message</source>
        <translation>Закрывать окно чата после отправки сообщения</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="449"/>
        <location filename="../ui_settingsform.h" line="962"/>
        <source>Show the events in the chat window</source>
        <translation>Отображать события в окне сообщений</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="480"/>
        <location filename="../ui_settingsform.h" line="963"/>
        <source>Colors</source>
        <translation>Цвета</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="495"/>
        <location filename="../ui_settingsform.h" line="964"/>
        <source>Your name</source>
        <translation>Своё имя</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="502"/>
        <location filename="../ui_settingsform.h" line="965"/>
        <source>Contact&apos;s name</source>
        <translation>Имя контакта</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="509"/>
        <location filename="../ui_settingsform.h" line="966"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="516"/>
        <location filename="../ui_settingsform.h" line="967"/>
        <source>Your background</source>
        <translation>Свой фон</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="523"/>
        <location filename="../ui_settingsform.h" line="968"/>
        <source>Contact&apos;s background</source>
        <translation>Фон контакта</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="530"/>
        <location filename="../ui_settingsform.h" line="969"/>
        <source>Separator</source>
        <translation>Разделитель</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="577"/>
        <location filename="../ui_settingsform.h" line="970"/>
        <source>Sound notifications</source>
        <translation>Звуковые уведомления</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="592"/>
        <location filename="../settingsform.ui" line="791"/>
        <location filename="../ui_settingsform.h" line="971"/>
        <location filename="../ui_settingsform.h" line="992"/>
        <source>Incoming message</source>
        <translation>Входящее сообщение</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="648"/>
        <location filename="../ui_settingsform.h" line="972"/>
        <source>Tray messages window</source>
        <translation>Всплывающие окна</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="684"/>
        <location filename="../ui_settingsform.h" line="973"/>
        <source>Show time:</source>
        <translation>Время показа:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="691"/>
        <location filename="../ui_settingsform.h" line="974"/>
        <source> s</source>
        <translation> сек</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="707"/>
        <location filename="../ui_settingsform.h" line="975"/>
        <source>Position:</source>
        <translation>Позиция:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="718"/>
        <location filename="../ui_settingsform.h" line="978"/>
        <source>Upper left corner</source>
        <translation>Верхний левый угол</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="723"/>
        <location filename="../ui_settingsform.h" line="979"/>
        <source>Upper right corner</source>
        <translation>Верхний правый угол</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="728"/>
        <location filename="../ui_settingsform.h" line="980"/>
        <source>Lower left corner</source>
        <translation>Нижний левый угол</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="733"/>
        <location filename="../ui_settingsform.h" line="981"/>
        <source>Lower right corner</source>
        <translation>Нижний правый угол</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="741"/>
        <location filename="../ui_settingsform.h" line="983"/>
        <source>Style:</source>
        <translation>Стиль:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="752"/>
        <location filename="../ui_settingsform.h" line="986"/>
        <source>No slide</source>
        <translation>Без скольжения</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="757"/>
        <location filename="../ui_settingsform.h" line="987"/>
        <source>Slide vertically</source>
        <translation>Вертикальное скольжение</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="762"/>
        <location filename="../ui_settingsform.h" line="988"/>
        <source>Slide horizontally</source>
        <translation>Горизонтальное скольжение</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="777"/>
        <location filename="../ui_settingsform.h" line="990"/>
        <source>Online contact</source>
        <translation>Контакт онлайн</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="784"/>
        <location filename="../ui_settingsform.h" line="991"/>
        <source>Offline contact</source>
        <translation>Контакт оффлайн</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="833"/>
        <location filename="../ui_settingsform.h" line="993"/>
        <source>Events VK</source>
        <translation>События ВКонтакте</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="860"/>
        <location filename="../ui_settingsform.h" line="994"/>
        <source>My Friends</source>
        <translation>Мои Друзья</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="867"/>
        <location filename="../ui_settingsform.h" line="995"/>
        <source>My Photos</source>
        <translation>Мои Фотографии</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="874"/>
        <location filename="../ui_settingsform.h" line="996"/>
        <source>My Videos</source>
        <translation>Мои Видеозаписи</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="881"/>
        <location filename="../ui_settingsform.h" line="997"/>
        <source>My Notes</source>
        <translation>Мои Заметки</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="888"/>
        <location filename="../ui_settingsform.h" line="998"/>
        <source>My Groups</source>
        <translation>Мои Группы</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="895"/>
        <location filename="../ui_settingsform.h" line="999"/>
        <source>My Gifts</source>
        <translation>Мои Подарки</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="948"/>
        <location filename="../ui_settingsform.h" line="1001"/>
        <source>Select smilies:</source>
        <translation>Выбрать смайлы:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1006"/>
        <location filename="../ui_settingsform.h" line="1003"/>
        <source>Custom sound player</source>
        <translation>Внешняя программа воспроизведения звуков</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1015"/>
        <location filename="../ui_settingsform.h" line="1004"/>
        <source>Command:</source>
        <translation>Команда:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1022"/>
        <location filename="../ui_settingsform.h" line="1005"/>
        <source>aplay -q &quot;%1&quot;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1064"/>
        <location filename="../ui_settingsform.h" line="1006"/>
        <source>Spell checker (Hunspell)</source>
        <translation>Проверять орфографию (Hunspell)</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1076"/>
        <location filename="../ui_settingsform.h" line="1007"/>
        <source>Select dictionary:</source>
        <translation>Словарь:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1089"/>
        <location filename="../ui_settingsform.h" line="1011"/>
        <source>English (En)</source>
        <translation>Английский (En)</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="975"/>
        <location filename="../ui_settingsform.h" line="1002"/>
        <source>*You must restart TitanIM for the changes to take effect</source>
        <translation>*Изменения вступят в силу после перезапуска программы</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1140"/>
        <location filename="../ui_settingsform.h" line="1013"/>
        <source>Type:</source>
        <translation>Тип:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1148"/>
        <location filename="../ui_settingsform.h" line="1016"/>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1153"/>
        <location filename="../ui_settingsform.h" line="1017"/>
        <source>HTTP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1158"/>
        <location filename="../ui_settingsform.h" line="1018"/>
        <source>SOCKS 5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1166"/>
        <location filename="../ui_settingsform.h" line="1020"/>
        <source>Host:</source>
        <translation>Хост:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1180"/>
        <location filename="../ui_settingsform.h" line="1021"/>
        <source>Port:</source>
        <translation>Порт:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1211"/>
        <location filename="../ui_settingsform.h" line="1022"/>
        <source>Authentication (optionally)</source>
        <translation>Аутентификация (опционально)</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1220"/>
        <location filename="../ui_settingsform.h" line="1023"/>
        <source>User name:</source>
        <translation>Имя пользователя:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1234"/>
        <location filename="../ui_settingsform.h" line="1024"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1286"/>
        <location filename="../ui_settingsform.h" line="1025"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1293"/>
        <location filename="../ui_settingsform.h" line="1026"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1300"/>
        <location filename="../ui_settingsform.h" line="1027"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="20"/>
        <location filename="../ui_settingsform.h" line="921"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../settingsform.cpp" line="291"/>
        <source>You must restart TitanIM for the changes to take effect. Restart now?</source>
        <translation>Изменения вступят в силу после перезапуска программы. Перезапустить сейчас?</translation>
    </message>
</context>
<context>
    <name>statusForm</name>
    <message>
        <location filename="../statusform.ui" line="20"/>
        <location filename="../statusform.ui" line="106"/>
        <location filename="../ui_statusform.h" line="113"/>
        <location filename="../ui_statusform.h" line="120"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../statusform.ui" line="72"/>
        <location filename="../ui_statusform.h" line="115"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../statusform.ui" line="116"/>
        <location filename="../ui_statusform.h" line="121"/>
        <source>Wall</source>
        <translation>Стена</translation>
    </message>
    <message>
        <location filename="../statusform.ui" line="126"/>
        <location filename="../ui_statusform.h" line="122"/>
        <source>Send</source>
        <translation>Отправить</translation>
    </message>
</context>
<context>
    <name>titanim</name>
    <message>
        <location filename="../data/qml-desktop/qml/titanim.qml" line="135"/>
        <location filename="../share/qml/titanim.qml" line="135"/>
        <source>Send message</source>
        <translation>Открыть диалог</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/titanim.qml" line="136"/>
        <location filename="../share/qml/titanim.qml" line="136"/>
        <source>Contact details</source>
        <translation>Профиль</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/titanim.qml" line="137"/>
        <location filename="../share/qml/titanim.qml" line="137"/>
        <source>VK page</source>
        <translation>Страница</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/titanim.qml" line="138"/>
        <location filename="../share/qml/titanim.qml" line="138"/>
        <source>Photo</source>
        <translation>Фотографии</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/titanim.qml" line="139"/>
        <location filename="../share/qml/titanim.qml" line="139"/>
        <source>Music</source>
        <translation>Аудиозаписи</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/titanim.qml" line="140"/>
        <location filename="../share/qml/titanim.qml" line="140"/>
        <source>Video</source>
        <translation>Видеозаписи</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/titanim.qml" line="104"/>
        <source>New message</source>
        <translation>Новое сообщение</translation>
    </message>
</context>
<context>
    <name>wallCommentsModel</name>
    <message>
        <location filename="../wall/wallcommentsmodel.cpp" line="54"/>
        <source>Today at %1</source>
        <translation>сегодня в %1</translation>
    </message>
    <message>
        <location filename="../wall/wallcommentsmodel.cpp" line="56"/>
        <source>Yesterday at %1</source>
        <translation>вчера в %1</translation>
    </message>
</context>
<context>
    <name>wallModel</name>
    <message>
        <source>Monday</source>
        <translation type="obsolete">понедельник</translation>
    </message>
    <message>
        <source>Tuesday</source>
        <translation type="obsolete">вторник</translation>
    </message>
    <message>
        <source>Wednesday</source>
        <translation type="obsolete">среда</translation>
    </message>
    <message>
        <source>Thursday</source>
        <translation type="obsolete">четверг</translation>
    </message>
    <message>
        <source>Friday</source>
        <translation type="obsolete">пятница</translation>
    </message>
    <message>
        <source>Saturday</source>
        <translation type="obsolete">суббота</translation>
    </message>
    <message>
        <source>Sunday</source>
        <translation type="obsolete">воскресенье</translation>
    </message>
    <message>
        <source>yesterday</source>
        <translation type="obsolete">вчера</translation>
    </message>
    <message>
        <location filename="../wall/wallmodel.cpp" line="75"/>
        <source>Today at %1</source>
        <translation>сегодня в %1</translation>
    </message>
    <message>
        <location filename="../wall/wallmodel.cpp" line="77"/>
        <source>Yesterday at %1</source>
        <translation>вчера в %1</translation>
    </message>
</context>
</TS>
