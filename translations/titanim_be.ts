<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="be_BY">
<context>
    <name>About</name>
    <message>
        <location filename="../about.ui" line="26"/>
        <location filename="../ui_about.h" line="162"/>
        <source>TitanIM</source>
        <translation>TitanIM</translation>
    </message>
    <message>
        <location filename="../about.ui" line="45"/>
        <location filename="../ui_about.h" line="175"/>
        <source>About</source>
        <translation>Пра праграму</translation>
    </message>
    <message>
        <location filename="../about.ui" line="54"/>
        <location filename="../ui_about.h" line="163"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TitanIM is a multiplatform client of the social network VK&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; All logotypes and trademarks, used in application, belong to corresponding owners&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TitanIM, кросплатформавы кліент сацыяльнай сеткі Ўкантакце&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Усе лагатыпы і гандлёвыя маркі, выкарыстаныя ў праграме, прыналежаць адпаведным уладальнікам&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message utf8="true">
        <location filename="../about.ui" line="79"/>
        <location filename="../ui_about.h" line="170"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;© 2010, Ruslan Nazarov&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;© 2010, Руслан Назараў&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="94"/>
        <location filename="../ui_about.h" line="186"/>
        <source>Authors</source>
        <translation>Аўтары</translation>
    </message>
    <message>
        <location filename="../about.ui" line="109"/>
        <location filename="../ui_about.h" line="176"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Ruslan Nazarov (http://vkontakte.ru/id6230054)&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Main developer and Project founder&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Yusuke Kamiyamane (http://yusukekamiyamane.com)&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Icons of the program&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Руслан Назараў (http://vkontakte.ru/id6230054)&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Галоўны распрацоўнік і заснавальнік Праекта&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Yusuke Kamiyamane (http://yusukekamiyamane.com)&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Абразкі праграмы&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="125"/>
        <location filename="../ui_about.h" line="201"/>
        <source>Thanks To</source>
        <translation>Падзякі</translation>
    </message>
    <message utf8="true">
        <location filename="../about.ui" line="140"/>
        <location filename="../ui_about.h" line="187"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Руслан Нигматуллин&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Алексей Сидоров&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Виталий Петров&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Алексей Онуфрийчук&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Алексей Чернышов&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Артём Казаков&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Тимур Исхаков&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;theMIROn&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../about.ui" line="159"/>
        <location filename="../ui_about.h" line="208"/>
        <source>Help</source>
        <translation>Дапамога</translation>
    </message>
    <message>
        <location filename="../about.ui" line="171"/>
        <location filename="../ui_about.h" line="202"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://titanim.ru&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;The official website of the program&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://vk.com/titanim&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;The official VK group of the program&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://titanim.ru&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Афіцыйны сайт праграмы&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://vk.com/titanim&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Афіцыйная група Ўкантакце&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="208"/>
        <location filename="../ui_about.h" line="209"/>
        <source>Close</source>
        <translation>Закрыць</translation>
    </message>
</context>
<context>
    <name>AudioPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="117"/>
        <source>Back</source>
        <translation>Таму</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="136"/>
        <source>Audio</source>
        <translation>Аўдыёзапісы</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="170"/>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="196"/>
        <source>Pull down to refresh...</source>
        <translation>Пацягніце ўніз, каб абнавіць...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="177"/>
        <source>Last updated: </source>
        <translation>Абноўлена: </translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="187"/>
        <source>Release to refresh...</source>
        <translation>Адпусціце, каб абнавіць...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="230"/>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="233"/>
        <source>Search</source>
        <translation>Пошук</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="267"/>
        <source>Cancel</source>
        <translation>Адмена</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="310"/>
        <source>Loading...</source>
        <translation>Загрузка...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AudioPage.qml" line="348"/>
        <source>You have no audio files yet</source>
        <translation>У вас яшчэ няма аўдыёзапісаў</translation>
    </message>
</context>
<context>
    <name>AuthorizePage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/AuthorizePage.qml" line="45"/>
        <source>Email</source>
        <translation>Эл. пошта</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AuthorizePage.qml" line="48"/>
        <source>Next</source>
        <translation>Далей</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AuthorizePage.qml" line="59"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AuthorizePage.qml" line="63"/>
        <source>Log In</source>
        <translation>Вход</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/AuthorizePage.qml" line="108"/>
        <source>Sign up for VKontakte</source>
        <translation>Рэгістрацыя ВКонтакте</translation>
    </message>
</context>
<context>
    <name>CaptchaPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/CaptchaPage.qml" line="27"/>
        <source>Captcha</source>
        <translation>Captcha</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/CaptchaPage.qml" line="48"/>
        <source>Enter captcha</source>
        <translation>Увядзіце код</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/CaptchaPage.qml" line="51"/>
        <source>Send</source>
        <translation>Адправіць</translation>
    </message>
</context>
<context>
    <name>ChatForm</name>
    <message>
        <location filename="../chat/chatform.ui" line="14"/>
        <location filename="../ui_chatform.h" line="75"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../chat/chatform.cpp" line="123"/>
        <source>Status change: Online</source>
        <translation>Змена статусу: падключаны</translation>
    </message>
    <message>
        <location filename="../chat/chatform.cpp" line="135"/>
        <source>Status change: Offline</source>
        <translation>Змена статусу: адключаны</translation>
    </message>
    <message>
        <location filename="../chat/chatform.cpp" line="145"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
</context>
<context>
    <name>ChatPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/ChatPage.qml" line="42"/>
        <source>Back</source>
        <translation>Таму</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ChatPage.qml" line="104"/>
        <source>Reply Quoted</source>
        <translation>Адказаць з цытатай</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ChatPage.qml" line="105"/>
        <source>Copy</source>
        <translation>Капіраваць</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ChatPage.qml" line="106"/>
        <source>Mark as Unread</source>
        <translation>Пазначыць як новае</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ChatPage.qml" line="107"/>
        <source>Open VK</source>
        <translation>Адкрыць Ўкантакце</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ChatPage.qml" line="108"/>
        <source>Delete</source>
        <translation>Выдаліць з сервера</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ChatPage.qml" line="134"/>
        <source>Type your text here...</source>
        <translation>Напішыце сваё паведамленьне…</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ChatPage.qml" line="151"/>
        <source>Send</source>
        <translation>Адпр.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <location filename="../ui_mainwindow.h" line="340"/>
        <source>TitanIM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="45"/>
        <location filename="../ui_mainwindow.h" line="342"/>
        <source>Main menu</source>
        <translation>Галоўнае меню</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="48"/>
        <location filename="../mainwindow.ui" line="77"/>
        <location filename="../mainwindow.ui" line="112"/>
        <location filename="../mainwindow.ui" line="144"/>
        <location filename="../mainwindow.ui" line="173"/>
        <location filename="../mainwindow.ui" line="203"/>
        <location filename="../mainwindow.ui" line="229"/>
        <location filename="../mainwindow.ui" line="255"/>
        <location filename="../mainwindow.ui" line="596"/>
        <location filename="../ui_mainwindow.h" line="344"/>
        <location filename="../ui_mainwindow.h" line="348"/>
        <location filename="../ui_mainwindow.h" line="352"/>
        <location filename="../ui_mainwindow.h" line="357"/>
        <location filename="../ui_mainwindow.h" line="361"/>
        <location filename="../ui_mainwindow.h" line="365"/>
        <location filename="../ui_mainwindow.h" line="369"/>
        <location filename="../ui_mainwindow.h" line="373"/>
        <location filename="../ui_mainwindow.h" line="405"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="74"/>
        <location filename="../ui_mainwindow.h" line="346"/>
        <source>List of friends</source>
        <translation>Сьпісы сяброў</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="109"/>
        <location filename="../ui_mainwindow.h" line="350"/>
        <source>Contacts filter</source>
        <translation>Фільтр кантактаў</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="119"/>
        <location filename="../ui_mainwindow.h" line="353"/>
        <source>Ctrl+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="141"/>
        <location filename="../ui_mainwindow.h" line="355"/>
        <source>Show/hide offline</source>
        <translation>Паказаць/схаваць адключаных</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="170"/>
        <location filename="../ui_mainwindow.h" line="359"/>
        <source>Sound on/off</source>
        <translation>Гук уключ/выкл</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="200"/>
        <location filename="../mainwindow.cpp" line="58"/>
        <location filename="../ui_mainwindow.h" line="363"/>
        <source>Settings</source>
        <translation>Наладкі</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="226"/>
        <location filename="../ui_mainwindow.h" line="367"/>
        <source>VK page</source>
        <translation>Старонка Ўкантакце</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="252"/>
        <location filename="../ui_mainwindow.h" line="371"/>
        <source>About</source>
        <translation>Пра праграму</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="364"/>
        <location filename="../ui_mainwindow.h" line="376"/>
        <source>Friends</source>
        <translation>Сябры</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="367"/>
        <location filename="../mainwindow.ui" line="396"/>
        <location filename="../mainwindow.ui" line="425"/>
        <location filename="../mainwindow.ui" line="454"/>
        <location filename="../mainwindow.ui" line="483"/>
        <location filename="../mainwindow.ui" line="512"/>
        <location filename="../ui_mainwindow.h" line="378"/>
        <location filename="../ui_mainwindow.h" line="382"/>
        <location filename="../ui_mainwindow.h" line="386"/>
        <location filename="../ui_mainwindow.h" line="390"/>
        <location filename="../ui_mainwindow.h" line="394"/>
        <location filename="../ui_mainwindow.h" line="398"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="393"/>
        <location filename="../ui_mainwindow.h" line="380"/>
        <source>Photos</source>
        <translation>Фатаздымкі</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="422"/>
        <location filename="../ui_mainwindow.h" line="384"/>
        <source>Videos</source>
        <translation>Відэазапісы</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="451"/>
        <location filename="../ui_mainwindow.h" line="388"/>
        <source>Notes</source>
        <translation>Нататкі</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="480"/>
        <location filename="../ui_mainwindow.h" line="392"/>
        <source>Gifts</source>
        <translation>Падарункі</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="509"/>
        <location filename="../ui_mainwindow.h" line="396"/>
        <source>Groups</source>
        <translation>Суполкі</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="563"/>
        <location filename="../mainwindow.cpp" line="53"/>
        <location filename="../mainwindow.cpp" line="1162"/>
        <location filename="../ui_mainwindow.h" line="399"/>
        <source>Online</source>
        <translation>У сетцы</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="572"/>
        <location filename="../mainwindow.cpp" line="54"/>
        <location filename="../mainwindow.cpp" line="1175"/>
        <location filename="../ui_mainwindow.h" line="400"/>
        <source>Offline</source>
        <translation>Адключаны ад сеткі</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="593"/>
        <location filename="../ui_mainwindow.h" line="403"/>
        <source>Change Status</source>
        <translation>Зьмяніць статус</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="60"/>
        <source>Change profile</source>
        <translation>Зьмяніць профіль</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="62"/>
        <source>Quit</source>
        <translation>Выйсьці</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="52"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="56"/>
        <source>Edit text</source>
        <translation>Зьмяніць тэкст</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="59"/>
        <source>Quiet mode</source>
        <translation>Ціхі рэжым</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="690"/>
        <source>Downloading settings</source>
        <translation>Загружаю наладкі</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="714"/>
        <source>Connecting...</source>
        <translation>Падключаюся...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="865"/>
        <source>Server is not available</source>
        <translation>Сервер недаступны</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="898"/>
        <source>Receiving Contact List</source>
        <translation>Атрымліваю кантакт ліст</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1451"/>
        <source>Cache clear: %1 MB</source>
        <translation>Кэш ачышчаны: %1 MB</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1945"/>
        <source>Upload image</source>
        <translation>Отправка</translation>
    </message>
    <message>
        <source>Dialogs</source>
        <translation type="obsolete">Дыялёґі</translation>
    </message>
    <message>
        <source>All friends</source>
        <translation type="obsolete">Усе сябры</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1058"/>
        <source>Privacy error</source>
        <translation>Памылка прыватнасці</translation>
    </message>
    <message>
        <source>New message</source>
        <translation type="obsolete">Новае паведамленне</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1211"/>
        <source>My Friends (%1)</source>
        <translation>Мае Сябры (%1)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1219"/>
        <source>My Photos (%1)</source>
        <translation>Мае Фатаздымкі (%1)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1227"/>
        <source>My Videos (%1)</source>
        <translation>Мае Відэазапісы (%1)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1235"/>
        <source>My Notes (%1)</source>
        <translation>Мае Нататкі (%1)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1243"/>
        <source>My Gifts (%1)</source>
        <translation>Мае Падарункі (%1)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1251"/>
        <source>My Groups (%1)</source>
        <translation>Мае Суполкі (%1)</translation>
    </message>
</context>
<context>
    <name>MessagesPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/MessagesPage.qml" line="37"/>
        <source>Messages</source>
        <translation>Паведамленьні</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/MessagesPage.qml" line="70"/>
        <location filename="../data/qml-harmattan/qml/MessagesPage.qml" line="95"/>
        <source>Pull down to refresh...</source>
        <translation>Пацягніце ўніз, каб абнавіць...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/MessagesPage.qml" line="77"/>
        <source>Last updated: </source>
        <translation>Абноўлена: </translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/MessagesPage.qml" line="87"/>
        <source>Release to refresh...</source>
        <translation>Адпусціце, каб абнавіць...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/MessagesPage.qml" line="122"/>
        <source>Loading...</source>
        <translation>Загрузка...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/MessagesPage.qml" line="156"/>
        <source>No messages</source>
        <translation>Няма паведамленняў</translation>
    </message>
</context>
<context>
    <name>NewsPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="32"/>
        <source>News</source>
        <translation>Навіны</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="73"/>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="99"/>
        <source>Pull down to refresh...</source>
        <translation>Пацягніце ўніз, каб абнавіць...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="80"/>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="135"/>
        <source>Last updated: </source>
        <translation>Абноўлена: </translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="90"/>
        <source>Release to refresh...</source>
        <translation>Адпусціце, каб абнавіць...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="128"/>
        <source>Updating...</source>
        <translation>Iдзе абнаўленне...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="165"/>
        <source>Loading...</source>
        <translation>Загрузка...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/NewsPage.qml" line="204"/>
        <source>No news</source>
        <translation>Няма навін</translation>
    </message>
</context>
<context>
    <name>PhotosLocalSheet</name>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosLocalSheet.qml" line="32"/>
        <source>Cancel</source>
        <translation>Адмена</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosLocalSheet.qml" line="47"/>
        <source>Send</source>
        <translation>Даслаць</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosLocalSheet.qml" line="135"/>
        <source>Description</source>
        <translation>Апiшыце фатаздымак</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosLocalSheet.qml" line="156"/>
        <source>Gallery is not available for your firmware</source>
        <translation>Галерэя недаступная для Вашай прашыўкі</translation>
    </message>
</context>
<context>
    <name>PhotosPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosPage.qml" line="44"/>
        <source>Back</source>
        <translation>Таму</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosPage.qml" line="63"/>
        <source>Photos</source>
        <translation>Фатаздымкі</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosPage.qml" line="92"/>
        <source>Loading...</source>
        <translation>Загрузка...</translation>
    </message>
</context>
<context>
    <name>PhotosViewerPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosViewerPage.qml" line="45"/>
        <source>Save photo</source>
        <translation>Захаваць фатаграфію</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/PhotosViewerPage.qml" line="46"/>
        <source>Share photo</source>
        <translation>Падзяліцца фатаграфіяй</translation>
    </message>
</context>
<context>
    <name>ProfilePage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="55"/>
        <source>Back</source>
        <translation>Таму</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="75"/>
        <source>Profile</source>
        <translation>Профіль</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="75"/>
        <source>Page</source>
        <translation>Старонка</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="192"/>
        <source>Send message</source>
        <translation>Даслаць паведамленьне</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="316"/>
        <source>Wall</source>
        <translation>Сьцяна</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="349"/>
        <source>Type your text here...</source>
        <translation>Напішыце сваё паведамленьне…</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="366"/>
        <source>Send</source>
        <translation>Адпр.</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="420"/>
        <source>Loading...</source>
        <translation>Загрузка...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="457"/>
        <source>There are no posts on this wall yet</source>
        <translation>На сьцяне пакуль няма аніводнага запісу</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="596"/>
        <source>Birthday</source>
        <translation>Дзень народзінаў</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="614"/>
        <source>Mobile phone</source>
        <translation>Мабільны тэлефон</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="646"/>
        <source>Friends</source>
        <translation>Сябры</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="678"/>
        <source>Mutual friends</source>
        <translation>Супольныя сябры</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="714"/>
        <source>Photos</source>
        <translation>Фатаздымкі</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="740"/>
        <source>Music</source>
        <translation>Музыка</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/ProfilePage.qml" line="772"/>
        <source>Videos</source>
        <translation>Відэазапісы</translation>
    </message>
</context>
<context>
    <name>RosterPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterPage.qml" line="41"/>
        <source>All Friends</source>
        <translation>Усе сябры</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterPage.qml" line="52"/>
        <source>Friends Online</source>
        <translation>Зараз тут</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterPage.qml" line="89"/>
        <location filename="../data/qml-harmattan/qml/RosterPage.qml" line="92"/>
        <source>Search</source>
        <translation>Пошук</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterPage.qml" line="132"/>
        <source>Cancel</source>
        <translation>Адмена</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterPage.qml" line="169"/>
        <source>No friends found</source>
        <translation>Ня знойдзена аніводнага сябра</translation>
    </message>
</context>
<context>
    <name>RosterUserPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterUserPage.qml" line="59"/>
        <source>Back</source>
        <translation>Таму</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterUserPage.qml" line="86"/>
        <source>Mutual friends</source>
        <translation>Супольныя сябры</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterUserPage.qml" line="86"/>
        <source>All friends</source>
        <translation>Усе сябры</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterUserPage.qml" line="122"/>
        <location filename="../data/qml-harmattan/qml/RosterUserPage.qml" line="125"/>
        <source>Search</source>
        <translation>Пошук</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterUserPage.qml" line="161"/>
        <source>Cancel</source>
        <translation>Адмена</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/RosterUserPage.qml" line="198"/>
        <source>No friends found</source>
        <translation>Ня знойдзена аніводнага сябра</translation>
    </message>
</context>
<context>
    <name>SettingsSheet</name>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="27"/>
        <source>Close</source>
        <translation>Закрыць</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="39"/>
        <source>Save</source>
        <translation>Захаваць</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="64"/>
        <source>System</source>
        <translation>Сістэма</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="69"/>
        <source>Show status bar</source>
        <translation>Паказаць радок стану</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="79"/>
        <source>Native</source>
        <translation>Натыўных выгляд</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="88"/>
        <source>Integrating event feed</source>
        <translation>Інтэграцыя ў канал навін</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="98"/>
        <source>Clear cache</source>
        <translation>Ачысціць кэш</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="107"/>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="111"/>
        <source>Language</source>
        <translation>Мова</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="134"/>
        <source>General</source>
        <translation>Галоўныя</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="213"/>
        <source>Show statuses</source>
        <translation>Паказваць статусы</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="139"/>
        <source>Save last read news</source>
        <translation>Запамінаць становішча ў стужцы навін</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="153"/>
        <source>Avatar size:</source>
        <translation>Памер аватара:</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="179"/>
        <source>Text size:</source>
        <translation>Памер тэксту:</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="199"/>
        <source>Contact List</source>
        <translation>Спіс кантактаў</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="204"/>
        <source>Sort by name</source>
        <translation>Cартаваць па імі</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="221"/>
        <source>Chat window</source>
        <translation>Акно паведамленняў</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="226"/>
        <source>Send typing notifications</source>
        <translation>Адпраўка паведамленняў аб наборы тэксту</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="234"/>
        <source>Notifications</source>
        <translation>Апавяшчэння</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="239"/>
        <source>Sound</source>
        <translation>Гук</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="248"/>
        <source>Vibrate</source>
        <translation>Вібрацыя</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="257"/>
        <source>Change profile</source>
        <translation>Зьмяніць профіль</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="270"/>
        <source>Restart TitanIM</source>
        <translation>Перазапуск TitanIM</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="271"/>
        <source>You must restart TitanIM for the changes to take effect. Restart now?</source>
        <translation>Змены набудуць моц пасля перазапуску праграмы. Перазапусціць цяпер?</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="272"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/SettingsSheet.qml" line="273"/>
        <source>Cancel</source>
        <translation>Адмена</translation>
    </message>
</context>
<context>
    <name>SpellTextEdit</name>
    <message>
        <location filename="../spellchecker/SpellTextEdit.cpp" line="202"/>
        <source>Ignore</source>
        <translation>Прапусціць</translation>
    </message>
    <message>
        <location filename="../spellchecker/SpellTextEdit.cpp" line="203"/>
        <source>Add</source>
        <translation>Дадаць у слоўнік</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <location filename="../data/qml-desktop/qml/StatusBar.qml" line="50"/>
        <location filename="../data/qml-desktop/qml/StatusBar.qml" line="61"/>
        <location filename="../data/qml-desktop/qml/StatusBar.qml" line="85"/>
        <location filename="../share/qml/StatusBar.qml" line="50"/>
        <location filename="../share/qml/StatusBar.qml" line="61"/>
        <location filename="../share/qml/StatusBar.qml" line="85"/>
        <source>Offline</source>
        <translation>Адключаны ад сеткі</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/StatusBar.qml" line="60"/>
        <location filename="../data/qml-desktop/qml/StatusBar.qml" line="81"/>
        <location filename="../share/qml/StatusBar.qml" line="60"/>
        <location filename="../share/qml/StatusBar.qml" line="81"/>
        <source>Online</source>
        <translation>У сетцы</translation>
    </message>
</context>
<context>
    <name>TabBar</name>
    <message>
        <location filename="../data/qml-harmattan/qml/TabBar.qml" line="70"/>
        <source>News</source>
        <translation>Навіны</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/TabBar.qml" line="125"/>
        <source>Wall</source>
        <translation>Сьцяна</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/TabBar.qml" line="160"/>
        <source>Messages</source>
        <translation>Паведамленьні</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/TabBar.qml" line="217"/>
        <source>Audio</source>
        <translation>Аўдыё</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/TabBar.qml" line="253"/>
        <source>Friends</source>
        <translation>Сябры</translation>
    </message>
</context>
<context>
    <name>TabForm</name>
    <message>
        <location filename="../chat/tabform.ui" line="23"/>
        <location filename="../ui_tabform.h" line="285"/>
        <source>Chat window</source>
        <translation>Акно паведамленняў</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="103"/>
        <location filename="../ui_tabform.h" line="287"/>
        <source>Contact history</source>
        <translation>Гісторыя кантакта</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="138"/>
        <location filename="../ui_tabform.h" line="291"/>
        <source>Smilies</source>
        <translation>Смайлікі</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="173"/>
        <location filename="../ui_tabform.h" line="295"/>
        <source>Contact details</source>
        <translation>Профіль карыстальніка</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="183"/>
        <location filename="../ui_tabform.h" line="298"/>
        <source>Ctrl+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="208"/>
        <location filename="../ui_tabform.h" line="300"/>
        <source>Message filter</source>
        <translation>Фільтр паведамленняў</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="218"/>
        <location filename="../ui_tabform.h" line="303"/>
        <source>Ctrl+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="246"/>
        <location filename="../ui_tabform.h" line="305"/>
        <source>Swap layout</source>
        <translation>Змена раскладкi</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="256"/>
        <location filename="../ui_tabform.h" line="308"/>
        <source>Ctrl+T</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="281"/>
        <location filename="../ui_tabform.h" line="310"/>
        <source>Quote</source>
        <translation>Цытаваць</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="291"/>
        <location filename="../ui_tabform.h" line="313"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="329"/>
        <location filename="../ui_tabform.h" line="315"/>
        <source>Clear chat log</source>
        <translation>Ачысціць акно чата</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="361"/>
        <location filename="../ui_tabform.h" line="319"/>
        <source>Close Tab</source>
        <translation>Закрыць укладку</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="371"/>
        <location filename="../ui_tabform.h" line="322"/>
        <source>Ctrl+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="417"/>
        <location filename="../ui_tabform.h" line="325"/>
        <source>VK page</source>
        <translation>Старонка Ўкантакце</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="452"/>
        <location filename="../ui_tabform.h" line="329"/>
        <source>Send message on enter</source>
        <translation>Адпраўляць паведамленні па Enter</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="472"/>
        <location filename="../ui_tabform.h" line="332"/>
        <source>Symbols left: </source>
        <translation>Засталося: </translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="479"/>
        <location filename="../ui_tabform.h" line="333"/>
        <source>4096</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="520"/>
        <location filename="../ui_tabform.h" line="335"/>
        <source>Send message</source>
        <translation>Адправіць паведамленне</translation>
    </message>
    <message>
        <location filename="../chat/tabform.ui" line="523"/>
        <location filename="../ui_tabform.h" line="337"/>
        <source>Send</source>
        <translation>Адправіць</translation>
    </message>
    <message>
        <source>Send to wall</source>
        <translation type="obsolete">Адправіць на сцяну</translation>
    </message>
    <message>
        <source>Invite to TitanIM</source>
        <translation type="obsolete">Запрасіць у TitanIM</translation>
    </message>
    <message>
        <source>Photo</source>
        <translation type="obsolete">Фатаздымак</translation>
    </message>
    <message>
        <source>Music</source>
        <translation type="obsolete">Музыка</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="obsolete">Відэа</translation>
    </message>
    <message>
        <source>VK instant messenger</source>
        <translation type="obsolete">вось кліент для абмену імгненнымі паведамленнямі</translation>
    </message>
    <message>
        <location filename="../chat/tabform.cpp" line="408"/>
        <location filename="../chat/tabform.cpp" line="781"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../chat/tabform.cpp" line="493"/>
        <source>Status change: Online</source>
        <translation>Змена статусу: падключаны</translation>
    </message>
    <message>
        <location filename="../chat/tabform.cpp" line="510"/>
        <source>Status change: Offline</source>
        <translation>Змена статусу: адключаны</translation>
    </message>
    <message>
        <location filename="../chat/tabform.cpp" line="846"/>
        <source>qwertyuiop[]asdfghjkl;&apos;zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:&quot;ZXCVBNM&lt;&gt;?</source>
        <translation>йцукенгшўзх&apos;фывапролджэячсмітьбю.ЙЦУКЕНГШЎЗХ&apos;ФЫВАПРОЛДЖЭЯЧСМІТЬБЮ.</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <location filename="../data/qml-desktop/qml/ToolBar.qml" line="58"/>
        <location filename="../share/qml/ToolBar.qml" line="58"/>
        <source>Settings</source>
        <translation>Наладкі</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/ToolBar.qml" line="59"/>
        <location filename="../share/qml/ToolBar.qml" line="59"/>
        <source>Clear cache</source>
        <translation>Ачысціць кэш</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/ToolBar.qml" line="60"/>
        <location filename="../share/qml/ToolBar.qml" line="60"/>
        <source>Forget Password</source>
        <translation>Забыць пароль</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/ToolBar.qml" line="61"/>
        <location filename="../share/qml/ToolBar.qml" line="61"/>
        <source>Change profile</source>
        <translation>Зьмяніць профіль</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/ToolBar.qml" line="62"/>
        <location filename="../share/qml/ToolBar.qml" line="62"/>
        <source>About TitanIM</source>
        <translation>Пра праграму</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/ToolBar.qml" line="63"/>
        <location filename="../share/qml/ToolBar.qml" line="63"/>
        <source>Quit</source>
        <translation>Выйсьці</translation>
    </message>
</context>
<context>
    <name>VideoPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/VideoPage.qml" line="55"/>
        <source>Back</source>
        <translation>Таму</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/VideoPage.qml" line="75"/>
        <source>Video</source>
        <translation>Відэа</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/VideoPage.qml" line="111"/>
        <location filename="../data/qml-harmattan/qml/VideoPage.qml" line="114"/>
        <source>Search</source>
        <translation>Пошук</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/VideoPage.qml" line="148"/>
        <source>Cancel</source>
        <translation>Адмена</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/VideoPage.qml" line="191"/>
        <source>Loading...</source>
        <translation>Загрузка...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/VideoPage.qml" line="229"/>
        <source>No videos</source>
        <translation>Ня знойдзена аніводнага відэа</translation>
    </message>
</context>
<context>
    <name>WallCommentsPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="63"/>
        <source>Back</source>
        <translation>Таму</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="84"/>
        <source>Comments</source>
        <translation>Камэнтары</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="111"/>
        <source>Delete wall post?</source>
        <translation>Выдаліць запіс?</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="113"/>
        <source>Delete</source>
        <translation>Выдаліць</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="114"/>
        <source>Cancel</source>
        <translation>Адмена</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="280"/>
        <source>Like</source>
        <translation>Спадабалася</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="280"/>
        <source>people</source>
        <translation>чал.</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="321"/>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="346"/>
        <source>Pull down to refresh...</source>
        <translation>Пацягніце ўніз, каб абнавіць...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="328"/>
        <source>Last updated: </source>
        <translation>Абноўлена: </translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="338"/>
        <source>Release to refresh...</source>
        <translation>Адпусціце, каб абнавіць...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="449"/>
        <source>Comment...</source>
        <translation>Камэнтаваць...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallCommentsPage.qml" line="464"/>
        <source>Send</source>
        <translation>Адпр.</translation>
    </message>
</context>
<context>
    <name>WallPage</name>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="37"/>
        <source>Wall</source>
        <translation>Сьцяна</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">Выхад</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="55"/>
        <source>Settings</source>
        <translation>Наладкі</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="139"/>
        <source>What&apos;s new?</source>
        <translation>Што новага?</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="148"/>
        <source>Send</source>
        <translation>Адпр.</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="198"/>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="223"/>
        <source>Pull down to refresh...</source>
        <translation>Пацягніце ўніз, каб абнавіць...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="205"/>
        <source>Last updated: </source>
        <translation>Абноўлена: </translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="215"/>
        <source>Release to refresh...</source>
        <translation>Адпусціце, каб абнавіць...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="250"/>
        <source>Loading...</source>
        <translation>Загрузка...</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/WallPage.qml" line="285"/>
        <source>There are no posts on this wall yet</source>
        <translation>На сьцяне пакуль няма аніводнага запісу</translation>
    </message>
</context>
<context>
    <name>captchaView</name>
    <message>
        <location filename="../vk/captchaview.cpp" line="64"/>
        <source>Send</source>
        <translation>Адправіць</translation>
    </message>
</context>
<context>
    <name>chat</name>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="78"/>
        <location filename="../share/qml/chat.qml" line="78"/>
        <source>Reply Quoted</source>
        <translation>Адказаць з цытатай</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="79"/>
        <location filename="../share/qml/chat.qml" line="79"/>
        <source>Copy</source>
        <translation>Капіраваць</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="80"/>
        <location filename="../share/qml/chat.qml" line="80"/>
        <source>Copy Text</source>
        <translation>Капіраваць тэкст</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="81"/>
        <location filename="../share/qml/chat.qml" line="81"/>
        <source>Mark as Unread</source>
        <translation>Пазначыць як новае</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="82"/>
        <location filename="../share/qml/chat.qml" line="82"/>
        <source>Open VK</source>
        <translation>Адкрыць Ўкантакце</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="83"/>
        <location filename="../share/qml/chat.qml" line="83"/>
        <source>Delete</source>
        <translation>Выдаліць з сервера</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="138"/>
        <location filename="../share/qml/chat.qml" line="138"/>
        <source>Download next 30 messages</source>
        <translation>Загрузіць яшчэ 30 паведамленняў</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="139"/>
        <location filename="../share/qml/chat.qml" line="139"/>
        <source>Download next 50 messages</source>
        <translation>Загрузіць яшчэ 50 паведамленняў</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="140"/>
        <location filename="../share/qml/chat.qml" line="140"/>
        <source>Download next 100 messages</source>
        <translation>Загрузіць яшчэ 100 паведамленняў</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="329"/>
        <location filename="../share/qml/chat.qml" line="329"/>
        <source>VK page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="330"/>
        <location filename="../share/qml/chat.qml" line="330"/>
        <source>Photo</source>
        <translation type="unfinished">Фатаздымак</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="331"/>
        <location filename="../share/qml/chat.qml" line="331"/>
        <source>Music</source>
        <translation type="unfinished">Музыка</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="332"/>
        <location filename="../share/qml/chat.qml" line="332"/>
        <source>Video</source>
        <translation type="unfinished">Відэа</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="362"/>
        <location filename="../share/qml/chat.qml" line="362"/>
        <source>Symbols left: </source>
        <translation>Засталося: </translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/chat.qml" line="384"/>
        <location filename="../share/qml/chat.qml" line="384"/>
        <source>Send</source>
        <translation>Адправіць</translation>
    </message>
</context>
<context>
    <name>contactListModel</name>
    <message>
        <location filename="../roster/contactlistmodel.cpp" line="143"/>
        <source>Nickname</source>
        <translation>Нікнэйм</translation>
    </message>
    <message>
        <location filename="../roster/contactlistmodel.cpp" line="149"/>
        <source>Birthday</source>
        <translation>Дзень народзінаў</translation>
    </message>
    <message>
        <location filename="../roster/contactlistmodel.cpp" line="155"/>
        <source>Mobile phone</source>
        <translation>Мабільны тэлефон</translation>
    </message>
    <message>
        <location filename="../roster/contactlistmodel.cpp" line="161"/>
        <source>Home phone</source>
        <translation>Хатні тэлефон</translation>
    </message>
</context>
<context>
    <name>cvk</name>
    <message>
        <source>VK | Authorization</source>
        <translation type="obsolete">Укантакце | Аўтарызацыя</translation>
    </message>
    <message>
        <location filename="../vk/cvk.cpp" line="193"/>
        <location filename="../vk/cvk.cpp" line="351"/>
        <location filename="../vk/cvk.cpp" line="1010"/>
        <source>Server is not available</source>
        <translation>Сервер недаступны</translation>
    </message>
    <message>
        <location filename="../vk/cvk.cpp" line="238"/>
        <source>User authorization failed</source>
        <translation>Памылка аўтарызацыі</translation>
    </message>
    <message>
        <source>Authorization canceled</source>
        <translation type="obsolete">Аўтарызацыя адменена</translation>
    </message>
</context>
<context>
    <name>dialogsModel</name>
    <message>
        <location filename="../chat/dialogsmodel.cpp" line="64"/>
        <source>Monday</source>
        <translation>панядзелак</translation>
    </message>
    <message>
        <location filename="../chat/dialogsmodel.cpp" line="65"/>
        <source>Tuesday</source>
        <translation>аўторак</translation>
    </message>
    <message>
        <location filename="../chat/dialogsmodel.cpp" line="66"/>
        <source>Wednesday</source>
        <translation>серада</translation>
    </message>
    <message>
        <location filename="../chat/dialogsmodel.cpp" line="67"/>
        <source>Thursday</source>
        <translation>чацвер</translation>
    </message>
    <message>
        <location filename="../chat/dialogsmodel.cpp" line="68"/>
        <source>Friday</source>
        <translation>пятніца</translation>
    </message>
    <message>
        <location filename="../chat/dialogsmodel.cpp" line="69"/>
        <source>Saturday</source>
        <translation>субота</translation>
    </message>
    <message>
        <location filename="../chat/dialogsmodel.cpp" line="70"/>
        <source>Sunday</source>
        <translation>нядзеля</translation>
    </message>
    <message>
        <location filename="../chat/dialogsmodel.cpp" line="78"/>
        <source>yesterday</source>
        <translation>учора</translation>
    </message>
</context>
<context>
    <name>newsfeedModel</name>
    <message>
        <source>Monday</source>
        <translation type="obsolete">панядзелак</translation>
    </message>
    <message>
        <source>Tuesday</source>
        <translation type="obsolete">аўторак</translation>
    </message>
    <message>
        <source>Wednesday</source>
        <translation type="obsolete">серада</translation>
    </message>
    <message>
        <source>Thursday</source>
        <translation type="obsolete">чацвер</translation>
    </message>
    <message>
        <source>Friday</source>
        <translation type="obsolete">пятніца</translation>
    </message>
    <message>
        <source>Saturday</source>
        <translation type="obsolete">субота</translation>
    </message>
    <message>
        <source>Sunday</source>
        <translation type="obsolete">нядзеля</translation>
    </message>
    <message>
        <location filename="../newsfeed/newsfeedmodel.cpp" line="78"/>
        <source>Today at %1</source>
        <translation>сёньня а %1</translation>
    </message>
    <message>
        <location filename="../newsfeed/newsfeedmodel.cpp" line="80"/>
        <source>Yesterday at %1</source>
        <translation>учора а %1</translation>
    </message>
</context>
<context>
    <name>popupWindow</name>
    <message>
        <location filename="../notification/popupwindow.ui" line="14"/>
        <location filename="../ui_popupwindow.h" line="60"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>profileForm</name>
    <message>
        <location filename="../profileform.ui" line="26"/>
        <location filename="../ui_profileform.h" line="262"/>
        <source>Contact details</source>
        <translation>Профіль карыстальніка</translation>
    </message>
    <message>
        <location filename="../profileform.ui" line="74"/>
        <location filename="../profileform.ui" line="117"/>
        <location filename="../profileform.ui" line="163"/>
        <location filename="../profileform.ui" line="209"/>
        <location filename="../profileform.ui" line="255"/>
        <location filename="../profileform.ui" line="301"/>
        <location filename="../profileform.ui" line="350"/>
        <location filename="../ui_profileform.h" line="264"/>
        <location filename="../ui_profileform.h" line="266"/>
        <location filename="../ui_profileform.h" line="268"/>
        <location filename="../ui_profileform.h" line="270"/>
        <location filename="../ui_profileform.h" line="272"/>
        <location filename="../ui_profileform.h" line="274"/>
        <location filename="../ui_profileform.h" line="276"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../profileform.ui" line="107"/>
        <location filename="../ui_profileform.h" line="265"/>
        <source>Sex:</source>
        <translation>Пол:</translation>
    </message>
    <message>
        <location filename="../profileform.ui" line="153"/>
        <location filename="../ui_profileform.h" line="267"/>
        <source>Birthday:</source>
        <translation>Дзень народзінаў:</translation>
    </message>
    <message>
        <location filename="../profileform.ui" line="199"/>
        <location filename="../ui_profileform.h" line="269"/>
        <source>Mobile phone:</source>
        <translation>Мабільны тэлефон:</translation>
    </message>
    <message>
        <location filename="../profileform.ui" line="245"/>
        <location filename="../ui_profileform.h" line="271"/>
        <source>Home phone:</source>
        <translation>Хатні тэлефон:</translation>
    </message>
    <message>
        <location filename="../profileform.ui" line="291"/>
        <location filename="../ui_profileform.h" line="273"/>
        <source>University:</source>
        <translation>ВНУ:</translation>
    </message>
    <message>
        <location filename="../profileform.ui" line="340"/>
        <location filename="../ui_profileform.h" line="275"/>
        <source>Faculty:</source>
        <translation>Факультэт:</translation>
    </message>
    <message>
        <location filename="../profileform.ui" line="400"/>
        <location filename="../ui_profileform.h" line="277"/>
        <source>Close</source>
        <translation>Закрыць</translation>
    </message>
    <message>
        <source>Contact details - %1</source>
        <translation type="obsolete">Профіль карыстальніка - %1</translation>
    </message>
    <message>
        <source>Male</source>
        <translation type="obsolete">мужчынскі</translation>
    </message>
    <message>
        <source>Female</source>
        <translation type="obsolete">жаночы</translation>
    </message>
    <message>
        <location filename="../profileform.cpp" line="116"/>
        <source>last seen</source>
        <comment>woman</comment>
        <translation>была тут</translation>
    </message>
    <message>
        <location filename="../profileform.cpp" line="116"/>
        <source>last seen</source>
        <comment>man</comment>
        <translation>быў тут</translation>
    </message>
    <message>
        <location filename="../profileform.cpp" line="127"/>
        <source>minutes ago</source>
        <comment>5</comment>
        <translation>хвілін таму</translation>
    </message>
    <message>
        <location filename="../profileform.cpp" line="129"/>
        <source>minute ago</source>
        <comment>1</comment>
        <translation>хвіліну таму</translation>
    </message>
    <message>
        <location filename="../profileform.cpp" line="131"/>
        <source>minutes ago</source>
        <comment>2</comment>
        <translation>хвіліны таму</translation>
    </message>
    <message>
        <location filename="../profileform.cpp" line="139"/>
        <source>today at %1</source>
        <translation>сёньня а %1</translation>
    </message>
    <message>
        <location filename="../profileform.cpp" line="143"/>
        <source>yesterday at %1</source>
        <translation>учора а %1</translation>
    </message>
</context>
<context>
    <name>settingsForm</name>
    <message>
        <location filename="../settingsform.ui" line="69"/>
        <location filename="../ui_settingsform.h" line="926"/>
        <source>General</source>
        <translation>Галоўныя</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="78"/>
        <location filename="../ui_settingsform.h" line="928"/>
        <source>Contact List</source>
        <translation>Спіс кантактаў</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="87"/>
        <location filename="../ui_settingsform.h" line="930"/>
        <source>Chat window</source>
        <translation>Акно паведамленняў</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="96"/>
        <location filename="../ui_settingsform.h" line="932"/>
        <source>Notification</source>
        <translation>Апавяшчэнне</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="123"/>
        <location filename="../ui_settingsform.h" line="938"/>
        <source>Sound</source>
        <translation>Гук</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="132"/>
        <location filename="../ui_settingsform.h" line="940"/>
        <source>Spell checker</source>
        <translation>Арфаграфія</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="141"/>
        <location filename="../ui_settingsform.h" line="942"/>
        <source>Proxy</source>
        <translation>Проксі</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="180"/>
        <location filename="../ui_settingsform.h" line="945"/>
        <source>Autoconnect on start</source>
        <translation>Аўтаматычна падключацца пры запуску</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="187"/>
        <location filename="../ui_settingsform.h" line="946"/>
        <source>Reconnect after disconnect</source>
        <translation>Падключацца пасля разрыву сувязі</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="194"/>
        <location filename="../ui_settingsform.h" line="947"/>
        <source>Hide on startup</source>
        <translation>Хаваць пры запуску</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="201"/>
        <location filename="../ui_settingsform.h" line="948"/>
        <source>Thick window title</source>
        <translation>Звычайны загаловак акна</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="208"/>
        <location filename="../ui_settingsform.h" line="949"/>
        <source>Always on top</source>
        <translation>Заўсёды наверсе</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="220"/>
        <location filename="../ui_settingsform.h" line="950"/>
        <source> Language:</source>
        <translation> Мова (Language):</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1084"/>
        <location filename="../ui_settingsform.h" line="1010"/>
        <source>Russian (RU)</source>
        <translation>Рускі (RU)</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="278"/>
        <location filename="../ui_settingsform.h" line="951"/>
        <source>Show offline friends</source>
        <translation>Паказваць адключаных карыстальнікаў</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="105"/>
        <location filename="../ui_settingsform.h" line="934"/>
        <source>Events</source>
        <translation>Падзеі</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="114"/>
        <location filename="../settingsform.ui" line="936"/>
        <location filename="../ui_settingsform.h" line="936"/>
        <location filename="../ui_settingsform.h" line="1000"/>
        <source>Smilies</source>
        <translation>Смайлікі</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="285"/>
        <location filename="../ui_settingsform.h" line="952"/>
        <source>Draw the background with using alternating colors</source>
        <translation>Маляваць фон альтэрнатыўнымі колерамі</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="292"/>
        <location filename="../ui_settingsform.h" line="953"/>
        <source>Show avatars</source>
        <translation>Паказваць аватары</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="299"/>
        <location filename="../ui_settingsform.h" line="954"/>
        <source>Show statuses</source>
        <translation>Паказваць статусы</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="311"/>
        <location filename="../ui_settingsform.h" line="955"/>
        <source> Avatar size:</source>
        <translation> Памер аватара:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="343"/>
        <location filename="../ui_settingsform.h" line="956"/>
        <source>30</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="409"/>
        <location filename="../ui_settingsform.h" line="957"/>
        <source>Your name in the chat window:</source>
        <translation>Ваша імя ў вакне паведамленняў:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="416"/>
        <location filename="../ui_settingsform.h" line="958"/>
        <source>You</source>
        <translation>Я</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="428"/>
        <location filename="../ui_settingsform.h" line="959"/>
        <source>Don&apos;t play incoming message sound if message window is active</source>
        <translation>Без гуку пры ўваходным паведамленні, калі ўкладка актыўная</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="435"/>
        <location filename="../ui_settingsform.h" line="960"/>
        <source>Send message on Ctrl+Enter</source>
        <translation>Адпраўляць па Ctrl + Enter</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="442"/>
        <location filename="../ui_settingsform.h" line="961"/>
        <source>Close the chat window after sending a message</source>
        <translation>Закрыць акно чата пасля адпраўкі паведамлення</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="449"/>
        <location filename="../ui_settingsform.h" line="962"/>
        <source>Show the events in the chat window</source>
        <translation>Адлюстроўваць падзеі ў вакне паведамленняў</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="480"/>
        <location filename="../ui_settingsform.h" line="963"/>
        <source>Colors</source>
        <translation>Колеры</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="495"/>
        <location filename="../ui_settingsform.h" line="964"/>
        <source>Your name</source>
        <translation>Сваё імя</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="502"/>
        <location filename="../ui_settingsform.h" line="965"/>
        <source>Contact&apos;s name</source>
        <translation>Імя кантакту</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="509"/>
        <location filename="../ui_settingsform.h" line="966"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="516"/>
        <location filename="../ui_settingsform.h" line="967"/>
        <source>Your background</source>
        <translation>Свой фон</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="523"/>
        <location filename="../ui_settingsform.h" line="968"/>
        <source>Contact&apos;s background</source>
        <translation>Фон кантакту</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="530"/>
        <location filename="../ui_settingsform.h" line="969"/>
        <source>Separator</source>
        <translation>Падзельнік</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="577"/>
        <location filename="../ui_settingsform.h" line="970"/>
        <source>Sound notifications</source>
        <translation>Гукавыя апавяшчэнні</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="592"/>
        <location filename="../settingsform.ui" line="791"/>
        <location filename="../ui_settingsform.h" line="971"/>
        <location filename="../ui_settingsform.h" line="992"/>
        <source>Incoming message</source>
        <translation>Уваходнае паведамленне</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="648"/>
        <location filename="../ui_settingsform.h" line="972"/>
        <source>Tray messages window</source>
        <translation>Усплываючае вакно</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="684"/>
        <location filename="../ui_settingsform.h" line="973"/>
        <source>Show time:</source>
        <translation>Час паказу:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="691"/>
        <location filename="../ui_settingsform.h" line="974"/>
        <source> s</source>
        <translation> сек</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="707"/>
        <location filename="../ui_settingsform.h" line="975"/>
        <source>Position:</source>
        <translation>Пазіцыя:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="718"/>
        <location filename="../ui_settingsform.h" line="978"/>
        <source>Upper left corner</source>
        <translation>Верхні левы кут</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="723"/>
        <location filename="../ui_settingsform.h" line="979"/>
        <source>Upper right corner</source>
        <translation>Верхні правы кут</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="728"/>
        <location filename="../ui_settingsform.h" line="980"/>
        <source>Lower left corner</source>
        <translation>Ніжні левы кут</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="733"/>
        <location filename="../ui_settingsform.h" line="981"/>
        <source>Lower right corner</source>
        <translation>Ніжні правы кут</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="741"/>
        <location filename="../ui_settingsform.h" line="983"/>
        <source>Style:</source>
        <translation>Стыль:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="752"/>
        <location filename="../ui_settingsform.h" line="986"/>
        <source>No slide</source>
        <translation>Без слізгацення</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="757"/>
        <location filename="../ui_settingsform.h" line="987"/>
        <source>Slide vertically</source>
        <translation>Вертыкальнае слізгаценне</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="762"/>
        <location filename="../ui_settingsform.h" line="988"/>
        <source>Slide horizontally</source>
        <translation>Гарызантальнае слізгаценне</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="777"/>
        <location filename="../ui_settingsform.h" line="990"/>
        <source>Online contact</source>
        <translation>Кантакт online</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="784"/>
        <location filename="../ui_settingsform.h" line="991"/>
        <source>Offline contact</source>
        <translation>Кантакт offline</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="833"/>
        <location filename="../ui_settingsform.h" line="993"/>
        <source>Events VK</source>
        <translation>Падзеі УКантакце</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="860"/>
        <location filename="../ui_settingsform.h" line="994"/>
        <source>My Friends</source>
        <translation>Мае Сябры</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="867"/>
        <location filename="../ui_settingsform.h" line="995"/>
        <source>My Photos</source>
        <translation>Мае Фатаздымкі</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="874"/>
        <location filename="../ui_settingsform.h" line="996"/>
        <source>My Videos</source>
        <translation>Мае Відэазапісы</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="881"/>
        <location filename="../ui_settingsform.h" line="997"/>
        <source>My Notes</source>
        <translation>Мае Нататкі</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="888"/>
        <location filename="../ui_settingsform.h" line="998"/>
        <source>My Groups</source>
        <translation>Мае Суполкі</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="895"/>
        <location filename="../ui_settingsform.h" line="999"/>
        <source>My Gifts</source>
        <translation>Мае Падарункі</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="948"/>
        <location filename="../ui_settingsform.h" line="1001"/>
        <source>Select smilies:</source>
        <translation>Выбраць смайлікі:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1006"/>
        <location filename="../ui_settingsform.h" line="1003"/>
        <source>Custom sound player</source>
        <translation>Знешняя праграма прайгравання гукаў</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1015"/>
        <location filename="../ui_settingsform.h" line="1004"/>
        <source>Command:</source>
        <translation>Каманда:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1022"/>
        <location filename="../ui_settingsform.h" line="1005"/>
        <source>aplay -q &quot;%1&quot;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1064"/>
        <location filename="../ui_settingsform.h" line="1006"/>
        <source>Spell checker (Hunspell)</source>
        <translation>Правяраць арфаграфію (Hunspell)</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1076"/>
        <location filename="../ui_settingsform.h" line="1007"/>
        <source>Select dictionary:</source>
        <translation>Слоўнік:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1089"/>
        <location filename="../ui_settingsform.h" line="1011"/>
        <source>English (En)</source>
        <translation>Ангельская (En)</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="975"/>
        <location filename="../ui_settingsform.h" line="1002"/>
        <source>*You must restart TitanIM for the changes to take effect</source>
        <translation>*Змены набудуць моц пасля перазапуску праграмы</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1140"/>
        <location filename="../ui_settingsform.h" line="1013"/>
        <source>Type:</source>
        <translation>Тып:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1148"/>
        <location filename="../ui_settingsform.h" line="1016"/>
        <source>None</source>
        <translation>Няма</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1153"/>
        <location filename="../ui_settingsform.h" line="1017"/>
        <source>HTTP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1158"/>
        <location filename="../ui_settingsform.h" line="1018"/>
        <source>SOCKS 5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1166"/>
        <location filename="../ui_settingsform.h" line="1020"/>
        <source>Host:</source>
        <translation>Хост:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1180"/>
        <location filename="../ui_settingsform.h" line="1021"/>
        <source>Port:</source>
        <translation>Порт:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1211"/>
        <location filename="../ui_settingsform.h" line="1022"/>
        <source>Authentication (optionally)</source>
        <translation>Аўтэнтыфікацыя (апцыянальна)</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1220"/>
        <location filename="../ui_settingsform.h" line="1023"/>
        <source>User name:</source>
        <translation>Імя карыстальніка:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1234"/>
        <location filename="../ui_settingsform.h" line="1024"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1286"/>
        <location filename="../ui_settingsform.h" line="1025"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1293"/>
        <location filename="../ui_settingsform.h" line="1026"/>
        <source>Cancel</source>
        <translation>Адмена</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="1300"/>
        <location filename="../ui_settingsform.h" line="1027"/>
        <source>Apply</source>
        <translation>Прымяніць</translation>
    </message>
    <message>
        <location filename="../settingsform.ui" line="20"/>
        <location filename="../ui_settingsform.h" line="921"/>
        <source>Settings</source>
        <translation>Наладкі</translation>
    </message>
    <message>
        <location filename="../settingsform.cpp" line="291"/>
        <source>You must restart TitanIM for the changes to take effect. Restart now?</source>
        <translation>Змены набудуць моц пасля перазапуску праграмы. Перазапусціць цяпер?</translation>
    </message>
</context>
<context>
    <name>statusForm</name>
    <message>
        <location filename="../statusform.ui" line="20"/>
        <location filename="../statusform.ui" line="106"/>
        <location filename="../ui_statusform.h" line="113"/>
        <location filename="../ui_statusform.h" line="120"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../statusform.ui" line="72"/>
        <location filename="../ui_statusform.h" line="115"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../statusform.ui" line="116"/>
        <location filename="../ui_statusform.h" line="121"/>
        <source>Wall</source>
        <translation>Сьцяна</translation>
    </message>
    <message>
        <location filename="../statusform.ui" line="126"/>
        <location filename="../ui_statusform.h" line="122"/>
        <source>Send</source>
        <translation>Адправіць</translation>
    </message>
</context>
<context>
    <name>titanim</name>
    <message>
        <location filename="../data/qml-desktop/qml/titanim.qml" line="135"/>
        <location filename="../share/qml/titanim.qml" line="135"/>
        <source>Send message</source>
        <translation>Паведамленне</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/titanim.qml" line="136"/>
        <location filename="../share/qml/titanim.qml" line="136"/>
        <source>Contact details</source>
        <translation>Профіль</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/titanim.qml" line="137"/>
        <location filename="../share/qml/titanim.qml" line="137"/>
        <source>VK page</source>
        <translation>Старонка</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/titanim.qml" line="138"/>
        <location filename="../share/qml/titanim.qml" line="138"/>
        <source>Photo</source>
        <translation>Фатаздымак</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/titanim.qml" line="139"/>
        <location filename="../share/qml/titanim.qml" line="139"/>
        <source>Music</source>
        <translation>Музыка</translation>
    </message>
    <message>
        <location filename="../data/qml-desktop/qml/titanim.qml" line="140"/>
        <location filename="../share/qml/titanim.qml" line="140"/>
        <source>Video</source>
        <translation>Відэа</translation>
    </message>
    <message>
        <location filename="../data/qml-harmattan/qml/titanim.qml" line="104"/>
        <source>New message</source>
        <translation>Новае паведамленне</translation>
    </message>
</context>
<context>
    <name>wallCommentsModel</name>
    <message>
        <location filename="../wall/wallcommentsmodel.cpp" line="54"/>
        <source>Today at %1</source>
        <translation>сёньня а %1</translation>
    </message>
    <message>
        <location filename="../wall/wallcommentsmodel.cpp" line="56"/>
        <source>Yesterday at %1</source>
        <translation>учора а %1</translation>
    </message>
</context>
<context>
    <name>wallModel</name>
    <message>
        <source>Monday</source>
        <translation type="obsolete">панядзелак</translation>
    </message>
    <message>
        <source>Tuesday</source>
        <translation type="obsolete">аўторак</translation>
    </message>
    <message>
        <source>Wednesday</source>
        <translation type="obsolete">серада</translation>
    </message>
    <message>
        <source>Thursday</source>
        <translation type="obsolete">чацвер</translation>
    </message>
    <message>
        <source>Friday</source>
        <translation type="obsolete">пятніца</translation>
    </message>
    <message>
        <source>Saturday</source>
        <translation type="obsolete">субота</translation>
    </message>
    <message>
        <source>Sunday</source>
        <translation type="obsolete">нядзеля</translation>
    </message>
    <message>
        <source>yesterday</source>
        <translation type="obsolete">учора</translation>
    </message>
    <message>
        <location filename="../wall/wallmodel.cpp" line="75"/>
        <source>Today at %1</source>
        <translation>сёньня а %1</translation>
    </message>
    <message>
        <location filename="../wall/wallmodel.cpp" line="77"/>
        <source>Yesterday at %1</source>
        <translation>учора а %1</translation>
    </message>
</context>
</TS>
