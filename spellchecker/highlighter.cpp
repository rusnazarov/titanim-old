/****************************************************************************
 **
 ** Copyright (C) 2005-2008 Trolltech ASA. All rights reserved.
 **
 ** This file is part of the example classes of the Qt Toolkit.
 **
 ** This file may be used under the terms of the GNU General Public
 ** License versions 2.0 or 3.0 as published by the Free Software
 ** Foundation and appearing in the files LICENSE.GPL2 and LICENSE.GPL3
 ** included in the packaging of this file.  Alternatively you may (at
 ** your option) use any later version of the GNU General Public
 ** License if such license has been publicly approved by Trolltech ASA
 ** (or its successors, if any) and the KDE Free Qt Foundation. In
 ** addition, as a special exception, Trolltech gives you certain
 ** additional rights. These rights are described in the Trolltech GPL
 ** Exception version 1.1, which can be found at
 ** http://www.trolltech.com/products/qt/gplexception/ and in the file
 ** GPL_EXCEPTION.txt in this package.
 **
 ** Please review the following information to ensure GNU General
 ** Public Licensing requirements will be met:
 ** http://trolltech.com/products/qt/licenses/licensing/opensource/. If
 ** you are unsure which license is appropriate for your use, please
 ** review the following information:
 ** http://trolltech.com/products/qt/licenses/licensing/licensingoverview
 ** or contact the sales department at sales@trolltech.com.
 **
 ** In addition, as a special exception, Trolltech, as the sole
 ** copyright holder for Qt Designer, grants users of the Qt/Eclipse
 ** Integration plug-in the right for the Qt/Eclipse Integration to
 ** link to functionality provided by Qt Designer and its related
 ** libraries.
 **
 ** This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
 ** INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR
 ** A PARTICULAR PURPOSE. Trolltech reserves all rights not expressly
 ** granted herein.
 **
 ** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 ** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 **
 ***************************************************************************
 * Modified by Nazarov Ruslan <818151@mail.ru> 19/11/2010
 * for TitanIM http://titanim.ru
 ***************************************************************************/

#include <QtGui>
#include <iostream>
#include "highlighter.h"

Highlighter::Highlighter(QTextDocument *parent, Hunspell *checker, bool spellCheckState) :
	QSyntaxHighlighter(parent) {
    HighlightingRule rule;

    spellCheckFormat.setUnderlineColor(QColor(Qt::red));
    spellCheckFormat.setUnderlineStyle(QTextCharFormat::WaveUnderline);

    pChecker = checker;
    spell_encoding=QString(pChecker->get_dic_encoding());
    codec = QTextCodec::codecForName(spell_encoding.toLatin1());

    if (pChecker == 0)
        spellCheckActive=false;
    else spellCheckActive=true;

    spellerError=!spellCheckActive;
    spellCheckActive=spellCheckActive && spellCheckState;
}

Highlighter::~Highlighter() {
}


void Highlighter::highlightBlock(const QString &text) {

    spellCheck(text);
}

void Highlighter::enableSpellChecking(const bool state)
{
    bool old=spellCheckActive;
    if(!spellerError) spellCheckActive=state;
    if(old!=spellCheckActive) rehighlight();
}

void Highlighter::spellCheck(const QString &text)
{
    if (spellCheckActive) {
        // split text into words
        QString str = text.simplified();
        if (!str.isEmpty()) {
            QStringList Checkliste = str.split(QRegExp("([^\\w,^\\\\]|(?=\\\\))+"),
                                               QString::SkipEmptyParts);
            int l,number;
            // check all words
            for (int i=0; i<Checkliste.size(); ++i) {
                str = Checkliste.at(i);
                if (str.length()>1 &&!str.startsWith('\\'))
                {
                    if (!checkWord(str)) {
                        number = text.count(QRegExp("\\b" + str + "\\b"));
                        l=-1;
                        // underline all incorrect occurences of misspelled word
                        for (int j=0;j < number; ++j)
                        {
                            l = text.indexOf(QRegExp("\\b" + str + "\\b"),l+1);
                            if (l>=0)
                                setFormat(l, str.length(), spellCheckFormat);
                        } // for j
                    } // if spell chek error
                } // if str.length > 1
            } // for
        } // if str.isEmpty
    } // initial check
}

bool Highlighter::checkWord(QString word)
{
    int check;
    /*switch(check=mWords.value(word,-1)){
	case -1:
    {
		QByteArray encodedString;
		encodedString = codec->fromUnicode(word);
		check = pChecker->spell(encodedString.data());
		mWords[word]=check;
		break;
    }
	default:
		break;
	}
        */
    QByteArray encodedString;
    encodedString = codec->fromUnicode(word);
    check = pChecker->spell(encodedString.data());
    return bool(check);
}

bool Highlighter::setDict(Hunspell *checker)
{
    bool spell;

    pChecker = checker;
    spell_encoding=QString(pChecker->get_dic_encoding());
    codec = QTextCodec::codecForName(spell_encoding.toLatin1());

    //spellCheckFormat.setForeground(Qt::red);//faster Cursoroperation ...
    //spellCheckFormat.setUnderlineColor(QColor(Qt::red));
    //spellCheckFormat.setUnderlineStyle(QTextCharFormat::SpellCheckUnderline);

    if (pChecker == 0)
        spell=false;
    else spell=true;

    spellerError=!spell;
    spellCheckActive=spellCheckActive && spell;
    rehighlight();
    return spell;
}

void Highlighter::slot_addWord(QString word)
{
    rehighlight();
}
