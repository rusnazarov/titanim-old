/****************************************************************************
 **
 ** Copyright (C) 2005-2008 Trolltech ASA. All rights reserved.
 **
 ** This file is part of the example classes of the Qt Toolkit.
 **
 ** This file may be used under the terms of the GNU General Public
 ** License versions 2.0 or 3.0 as published by the Free Software
 ** Foundation and appearing in the files LICENSE.GPL2 and LICENSE.GPL3
 ** included in the packaging of this file.  Alternatively you may (at
 ** your option) use any later version of the GNU General Public
 ** License if such license has been publicly approved by Trolltech ASA
 ** (or its successors, if any) and the KDE Free Qt Foundation. In
 ** addition, as a special exception, Trolltech gives you certain
 ** additional rights. These rights are described in the Trolltech GPL
 ** Exception version 1.1, which can be found at
 ** http://www.trolltech.com/products/qt/gplexception/ and in the file
 ** GPL_EXCEPTION.txt in this package.
 **
 ** Please review the following information to ensure GNU General
 ** Public Licensing requirements will be met:
 ** http://trolltech.com/products/qt/licenses/licensing/opensource/. If
 ** you are unsure which license is appropriate for your use, please
 ** review the following information:
 ** http://trolltech.com/products/qt/licenses/licensing/licensingoverview
 ** or contact the sales department at sales@trolltech.com.
 **
 ** In addition, as a special exception, Trolltech, as the sole
 ** copyright holder for Qt Designer, grants users of the Qt/Eclipse
 ** Integration plug-in the right for the Qt/Eclipse Integration to
 ** link to functionality provided by Qt Designer and its related
 ** libraries.
 **
 ** This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
 ** INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR
 ** A PARTICULAR PURPOSE. Trolltech reserves all rights not expressly
 ** granted herein.
 **
 ** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 ** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 **
 ***************************************************************************
 * Modified by Nazarov Ruslan <818151@mail.ru> 19/11/2010
 * for TitanIM http://titanim.ru
 ***************************************************************************/

#include "SpellTextEdit.h"
#include <QTextCursor>
#include <QTextBlock>
#include <QMenu>
#include <QContextMenuEvent>
#include <QFileInfo>
#include <QTextCodec>
#include <QSettings>
#include <QTextStream>
#include <iostream>

SpellTextEdit::SpellTextEdit(QWidget *parent,QString SpellDic)
    : QTextEdit(parent)
{
    createActions();

    spell_dic=SpellDic.left(SpellDic.length()-4);
    pChecker = new Hunspell(spell_dic.toLatin1()+".aff",spell_dic.toLatin1()+".dic");

    QFileInfo fi(SpellDic);
    if (!(fi.exists() && fi.isReadable())){
        delete pChecker;
        pChecker=0;
    }

    QString filePath = appSettings::instance()->homeDir()+"dictionaries";
    filePath=filePath+"/User_"+QFileInfo(spell_dic.toLatin1()+".dic").fileName();
    fi=QFileInfo(filePath);
    if (fi.exists() && fi.isReadable()){
        pChecker->add_dic(filePath.toLatin1());
    }
    else filePath="";

    addedWords.clear();

    highlighter = new Highlighter(this->document(), pChecker, true);
    connect(this, SIGNAL(addWord(QString)), highlighter, SLOT(slot_addWord(QString)));
}

SpellTextEdit::~SpellTextEdit()
{
    QString fileName = appSettings::instance()->homeDir()+"dictionaries";
    fileName=fileName+"/User_"+QFileInfo(spell_dic.toLatin1()+".dic").fileName();

    QString spell_encoding=QString(pChecker->get_dic_encoding());
    QTextCodec *codec = QTextCodec::codecForName(spell_encoding.toLatin1());

    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&file);
        in.setCodec(codec);
        in.readLine();
        while (!in.atEnd()) {
            QString line = in.readLine();
            if(!addedWords.contains(line)) addedWords << line;
        }
        file.close();
    }
    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream out(&file);
        out.setCodec(codec);
        out << addedWords.count() << "\n";
        foreach(QString elem, addedWords){
            out << elem << "\n";
        }
        file.close();
    }

    delete highlighter;
    delete pChecker;
}

bool SpellTextEdit::setDict(const QString SpellDic)
{
    if(SpellDic!=""){
        spell_dic=SpellDic.left(SpellDic.length()-4);
        delete pChecker;
        pChecker = new Hunspell(spell_dic.toLatin1()+".aff",spell_dic.toLatin1()+".dic");
    }
    else spell_dic="";

    QFileInfo fi(SpellDic);
    if (!(fi.exists() && fi.isReadable())){
        delete pChecker;
        pChecker=0;
    }

    QString filePath = appSettings::instance()->homeDir()+"dictionaries";
    filePath=filePath+"/User_"+QFileInfo(spell_dic.toLatin1()+".dic").fileName();
    fi=QFileInfo(filePath);
    if (fi.exists() && fi.isReadable()){
        pChecker->add_dic(filePath.toLatin1());
    }
    else filePath="";

    highlighter->setDict(pChecker);

    return (spell_dic!="");
}

QString SpellTextEdit::getDict(){
    QFileInfo fi(spell_dic.toLatin1()+".dic");
    if (fi.exists() && fi.isReadable())
        return fi.fileName();
    else return "";
}

void SpellTextEdit::createActions() {
    for (int i = 0; i < MaxWords; ++i) {
        misspelledWordsActs[i] = new QAction(this);
        misspelledWordsActs[i]->setVisible(false);
        connect(misspelledWordsActs[i], SIGNAL(triggered()), this, SLOT(correctWord()));
    }
}

void SpellTextEdit::correctWord() {
    QAction *action = qobject_cast<QAction *>(sender());
    if (action)
    {
        QString replacement = action->text();
        QTextCursor cursor = cursorForPosition(lastPos);
        //QTextCursor cursor = textCursor();
        QString zeile = cursor.block().text();
        cursor.select(QTextCursor::WordUnderCursor);
        cursor.deleteChar();
        cursor.insertText(replacement);
    }
}

void SpellTextEdit::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu *menu = new QMenu(this);
    lastPos=event->pos();
    QTextCursor cursor = cursorForPosition(lastPos);
    QString zeile = cursor.block().text();
    int pos = cursor.columnNumber();
    int end = zeile.indexOf(QRegExp("\\W+"),pos);
    int begin = zeile.lastIndexOf(QRegExp("\\W+"),pos);
    zeile=zeile.mid(begin+1,end-begin-1);
    QStringList liste = getWordPropositions(zeile);
    if (!liste.isEmpty())
    {
        QFont font;
        font.setBold(true);
        for (int i = 0; i < qMin(int(MaxWords),liste.size()); ++i) {
            misspelledWordsActs[i]->setText(liste.at(i).trimmed());
            misspelledWordsActs[i]->setFont(font);
            misspelledWordsActs[i]->setVisible(true);
            menu->addAction(misspelledWordsActs[i]);
        }
        
        menu->addSeparator();
        QAction *a;
        a = menu->addAction(tr("Ignore"), this, SLOT(slot_ignoreWord()));
        a = menu->addAction(tr("Add"), this, SLOT(slot_addWord()));
        menu->addSeparator();
    }

    menu->addActions(createStandardContextMenu()->actions());
    menu->exec(event->globalPos());
    delete menu;
}

QStringList SpellTextEdit::getWordPropositions(const QString word)
{
    QStringList wordList;
    if(pChecker){
        QByteArray encodedString;
        QString spell_encoding=QString(pChecker->get_dic_encoding());
        QTextCodec *codec = QTextCodec::codecForName(spell_encoding.toLatin1());
        encodedString = codec->fromUnicode(word);
        bool check = pChecker->spell(encodedString.data());
        if(!check){
            char ** wlst;
            int ns = pChecker->suggest(&wlst,encodedString.data());
            if (ns > 0)
            {
                for (int i=0; i < ns; i++)
                {
                    wordList.append(codec->toUnicode(wlst[i]));
                    //free(wlst[i]);
                }
                //free(wlst);
                pChecker->free_list(&wlst, ns);
            }// if ns >0
        }// if check
    }
    return wordList;
}

void SpellTextEdit::slot_addWord()
{
    QTextCursor cursor = cursorForPosition(lastPos);
    QString zeile = cursor.block().text();
    int pos = cursor.columnNumber();
    int end = zeile.indexOf(QRegExp("\\W+"),pos);
    int begin = zeile.left(pos).lastIndexOf(QRegExp("\\W+"),pos);
    zeile=zeile.mid(begin+1,end-begin-1);
    QByteArray encodedString;
    QString spell_encoding=QString(pChecker->get_dic_encoding());
    QTextCodec *codec = QTextCodec::codecForName(spell_encoding.toLatin1());
    encodedString = codec->fromUnicode(zeile);
    pChecker->add(encodedString.data());
    addedWords.append(zeile);
    emit addWord(zeile);
}

void SpellTextEdit::slot_ignoreWord()
{
    QTextCursor cursor = cursorForPosition(lastPos);
    QString zeile = cursor.block().text();
    int pos = cursor.columnNumber();
    int end = zeile.indexOf(QRegExp("\\W+"),pos);
    int begin = zeile.left(pos).lastIndexOf(QRegExp("\\W+"),pos);
    zeile=zeile.mid(begin+1,end-begin-1);
    QByteArray encodedString;
    QString spell_encoding=QString(pChecker->get_dic_encoding());
    QTextCodec *codec = QTextCodec::codecForName(spell_encoding.toLatin1());
    encodedString = codec->fromUnicode(zeile);
    pChecker->add(encodedString.data());
    emit addWord(zeile);
}
