#ifndef VIDEO_H
#define VIDEO_H

#include <QObject>
#include <QProcess>
#include <QDesktopServices>
#include <QFile>
#include "videomodel.h"
#include "../vk/cvkStruct.h"

class Video : public QObject
{
Q_OBJECT

public:
    videoModel *model;

public:
    explicit Video(QObject *parent = 0);
    ~Video();

public slots:
    void appendVideos(const QVector<sVideo> &videos);
    void replaceVideos(const QVector<sVideo> &videos);
    void playVideo(const QString &url);

signals:
};

#endif // VIDEO_H
