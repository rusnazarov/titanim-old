#include "videomodel.h"

videoModel::videoModel(QObject *parent) :
    QAbstractListModel(parent)
{
    QHash<int, QByteArray> roles = roleNames();
    roles[vidRole] = "vid";
    roles[ownerIdRole] = "ownerId";
    roles[descriptionRole] = "description";
    roles[durationRole] = "duration";
    roles[urlRole] = "url";
    roles[domainRole] = "domain";
    setRoleNames(roles);
}

void videoModel::appendVideos(QVector<sVideo> videos){
    // ==========================================================================
    // добавление видеозаписей
    // ==========================================================================

    if (!videos.count())
        return;

    //добавляем новые
    beginInsertRows(QModelIndex(), this->videos.count(), this->videos.count() + videos.count() - 1);
    this->videos << videos;
    endInsertRows();
}

void videoModel::replaceVideos(QVector<sVideo> videos){
    // ==========================================================================
    // замена видеозаписей
    // ==========================================================================

    //очищаем модель
    removeVideo(0, rowCount());

    //добавляем новые
    appendVideos(videos);
}

bool videoModel::removeVideo(int row, int count){
    // ==========================================================================
    // удаляем с модели данные
    // ==========================================================================

    beginRemoveRows(QModelIndex(), row, row+count-1);

    for (int i = 0; i < count; ++i) {
        videos.remove(row);
    }

    endRemoveRows();
    return true;
}

QVariant videoModel::data(const QModelIndex &index, int role) const{
    // ==========================================================================
    // данные модели
    // ==========================================================================

    if (!index.isValid() || index.row() >= videos.size()) {
        return QVariant();
    }

    switch (role) {
    case Qt::DisplayRole:
        return videos.at(index.row()).title;

    case Qt::DecorationRole:
        return videos.at(index.row()).image;

    case vidRole:
        return videos.at(index.row()).vid;

    case ownerIdRole:
        return videos.at(index.row()).owner_id;

    case descriptionRole:
        return videos.at(index.row()).description;

    case durationRole:{
        QTime time(0, 0, 0);
        time = time.addSecs(videos.at(index.row()).duration);
        if (time.hour())
            return time.toString("hh:mm:ss");
        else
            return time.toString("mm:ss");
    }

    case urlRole:
        return videos.at(index.row()).url;

    case domainRole:
        return videos.at(index.row()).url.host().contains("userapi") ? "" : videos.at(index.row()).url.host();
    }

    return QVariant();
}

bool videoModel::setData(const QModelIndex &index, const QVariant &value, int role){
    // ==========================================================================
    // редактирование данных модели
    // ==========================================================================

    if (!index.isValid() && !value.isValid() && index.row() >= videos.size()) {
        return false;
    }

    return false;
}

int videoModel::rowCount(const QModelIndex &parent) const{
    // ==========================================================================
    // количество строк в модели
    // ==========================================================================

    Q_UNUSED(parent);
    return videos.count();
}

Qt::ItemFlags videoModel::flags(const QModelIndex &index) const{
    // ==========================================================================
    // флаги модели
    // ==========================================================================

    if (index.isValid())
      return (Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    else
        return Qt::NoItemFlags;
}
