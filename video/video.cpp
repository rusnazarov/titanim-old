#include "video.h"

Video::Video(QObject *parent) :
    QObject(parent)
{
    model = new videoModel();
}

Video::~Video()
{
    delete model;
}

void Video::appendVideos(const QVector<sVideo> &videos){
    // ==========================================================================
    // пришли видеозаписи
    // ==========================================================================

    model->appendVideos(videos);
}

void Video::replaceVideos(const QVector<sVideo> &videos){
    // ==========================================================================
    // пришли видеозаписи
    // ==========================================================================

    model->replaceVideos(videos);
}

void Video::playVideo(const QString &url){
    // ==========================================================================
    // проиграть видеозапись
    // ==========================================================================

    if (url.right(4) == ".mp4"){
        QProcess::startDetached("/usr/bin/videosheetplayer", QStringList(url));
//        QProcess::startDetached("/usr/bin/video-suite", QStringList(url));
    } else if (QUrl(url).host() != "vk.com" || !QFile::exists("/opt/fennec/lib/fennec-13.0/fennec")){
        QDesktopServices::openUrl(QUrl(url));
    } else {
        QProcess::startDetached("/opt/fennec/lib/fennec-13.0/fennec", QStringList(url));
    }
}
