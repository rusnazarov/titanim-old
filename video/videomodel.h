#ifndef VIDEOMODEL_H
#define VIDEOMODEL_H

#include <QAbstractListModel>
#include "vk/cvkStruct.h"

class videoModel : public QAbstractListModel
{
Q_OBJECT

private:
    QVector<sVideo> videos;

public:
    explicit videoModel(QObject *parent = 0);
    void appendVideos(QVector<sVideo> videos);
    void replaceVideos(QVector<sVideo> videos);
    bool removeVideo(int row, int count);
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant& value, int role = Qt::EditRole);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    enum VideosRole {
        vidRole = Qt::UserRole,
        ownerIdRole,
        descriptionRole,
        durationRole,
        urlRole,
        domainRole
    };

public slots:

signals:
};

#endif // VIDEOMODEL_H
