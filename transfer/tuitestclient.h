#ifndef TUITESTCLIENT_H
#define TUITESTCLIENT_H

#include <QObject>
#if defined(MEEGO_EDITION_HARMATTAN)
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDesktopServices>
#include <QPixmap>
#include <QUrl>
#include <TransferUI/Client>
#include <TransferUI/Transfer>

using namespace TransferUI;

class TUITestClient : public QObject
{
    Q_OBJECT

private:
    Client *client;
    Transfer *uploadTransfer;
    Transfer *downloadTransfer;
    QNetworkAccessManager *manager;
    QString name;

public:
    TUITestClient(QObject *parent = 0);
    virtual ~TUITestClient();
    bool initTUIClient();
    bool downloadImg(const QUrl &url);
    void showUploadTransfer(const QString &name);
    void hideUploadTransfer();

private slots:
    void slotDownloadProgress(qint64 bytesReceived, qint64 bytesTotal);
    void slotResponse(QNetworkReply *networkReply);

public Q_SLOTS:
    void transferCancelled(Transfer *transfer);
};

#endif //MEEGO_EDITION_HARMATTAN
#endif // TUITESTCLIENT_H
