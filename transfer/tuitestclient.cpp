#include "tuitestclient.h"
#if defined(MEEGO_EDITION_HARMATTAN)

TUITestClient::TUITestClient(QObject *parent) : client(0) , uploadTransfer(0),
    downloadTransfer(0), manager(0), QObject(parent) {
}

TUITestClient::~TUITestClient(){
    if(uploadTransfer) {
        delete uploadTransfer;
    }
    uploadTransfer = 0;

    if(downloadTransfer) {
        delete downloadTransfer;
    }
    downloadTransfer = 0;
}

bool TUITestClient::initTUIClient(){
    // ==========================================================================
    // инициализация клиента transfer
    // ==========================================================================

    client = new Client(this);

    if(!client->init()){
        delete client;
        client = 0;
        return false;
    }

    connect(client, SIGNAL(cancelTransfer(Transfer*)), this, SLOT(transferCancelled(Transfer*)));
    return true;
}

bool TUITestClient::downloadImg(const QUrl &url){
    // ==========================================================================
    // загружаем изображение в галерею
    // ==========================================================================

    //если уже загружается
    if (!name.isEmpty()){
        return false;
    }

    //если NAM не создан - создаем
    if (!manager){
        manager = new QNetworkAccessManager(this);
        connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(slotResponse(QNetworkReply*)));
    }

    //имя  файла
    name = url.toString();
    name = name.right(name.count() - name.lastIndexOf("/") - 1);

    //регистрируем transfer
    downloadTransfer = client->registerTransfer(name, Client::TRANSFER_TYPES_DOWNLOAD);
    downloadTransfer->setIcon("icon-m-transfer-download");
    downloadTransfer->setActive(0.0);

    //отправляем запрос на файл и отображаем прогресс загрузки
    QNetworkReply *networkReply =  manager->get(QNetworkRequest(url));
    connect(networkReply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(slotDownloadProgress(qint64,qint64)));

    //отображаем ui
    client->showUI();
    return true;
}

void TUITestClient::showUploadTransfer(const QString &name){
    // ==========================================================================
    // отображаем индикатор отправки
    // ==========================================================================

    uploadTransfer = client->registerTransfer(name, Client::TRANSFER_TYPES_UPLOAD);
    uploadTransfer->setIcon("icon-m-transfer-upload");
    uploadTransfer->setActive(0.0);
}

void TUITestClient::hideUploadTransfer(){
    // ==========================================================================
    // скрываем индикатор отправки
    // ==========================================================================

    if(uploadTransfer) {
        uploadTransfer->markDone();
        client->removeTransfer(uploadTransfer->transferId());
        delete uploadTransfer;
        uploadTransfer = 0;
    }
}

void TUITestClient::slotDownloadProgress(qint64 bytesReceived, qint64 bytesTotal){
    // ==========================================================================
    // прогресс получения файла
    // ==========================================================================

    downloadTransfer->setProgress(bytesTotal / bytesReceived);
    downloadTransfer->setSize(bytesTotal);
}

void TUITestClient::slotResponse(QNetworkReply *networkReply){
    // ==========================================================================
    // сохраняем полученное изображение в галерею
    // ==========================================================================

    if (name.isEmpty()){
        return;
    }

    if(networkReply->error() != QNetworkReply::NoError){
        //qDebug() << networkReply->errorString();
        networkReply->deleteLater();
        downloadTransfer->markFailure("Error", networkReply->errorString());
        return;
    }

    //сохраняем на диск
    QImage img;
    img.loadFromData(networkReply->readAll());
    networkReply->deleteLater();
    QString fileName = QDesktopServices::storageLocation(QDesktopServices::PicturesLocation) + "/" + name;
    img.save(fileName, 0, 100);

    //помечаем как завершено успешно
    downloadTransfer->setThumbnailForFile(fileName, "image/jpeg");
    downloadTransfer->markCompleted(true, "", fileName);
    name.clear();
}

void TUITestClient::transferCancelled(Transfer *transfer){
    // ==========================================================================
    // пользователь отменил transfer
    // ==========================================================================

    name.clear();
    transfer->markCancelled();
    client->removeTransfer(transfer->transferId());
    delete transfer;
    transfer = 0;
}

#endif //MEEGO_EDITION_HARMATTAN
