/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QApplication>
#include <QSettings>
#include <QDir>
#include <QDesktopServices>

class appSettings : public QObject
{

private:
    static appSettings *aInstance;
    QString home;
    QString homeApp;
    QString uid;
    QString name;
    QString shortName;
    QString photo;

private:
    appSettings();

public:
    static appSettings *instance();
    static void destroy();
    QString uids();
    void setUid(const QString &uid);
    void setProfileName(const QString &name);
    void setProfileShortName(const QString &name);
    void setProfilePhoto(const QString &url);
    QString cacheDir();
    QString homeDir();
    QString homeAppDir();
    QString profileName();
    QString profileShortName();
    QString profilePhoto();
    void creatDir();
    void saveProfile(const QString &key, const QVariant &value);
    void saveMain(const QString &key, const QVariant &value);
    QVariant loadProfile(const QString &key, const QVariant &defaultValue);
    QVariant loadMain(const QString &key, const QVariant &defaultValue);
};

#endif // SETTINGS_H
