/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef PROFILEFORM_H
#define PROFILEFORM_H

#include <QObject>
#include "vk/cvkStruct.h"

class profileForm : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString uid READ uid NOTIFY profileChanged)
    Q_PROPERTY(QString name READ name NOTIFY profileChanged)
    Q_PROPERTY(QString photo READ photo NOTIFY profileChanged)
    Q_PROPERTY(QString photoBig READ photoBig NOTIFY profileChanged)
    Q_PROPERTY(QString activity READ activity NOTIFY profileChanged)
    Q_PROPERTY(QString bDate READ bDate NOTIFY profileChanged)
    Q_PROPERTY(QString univerName READ univerName NOTIFY profileChanged)
    Q_PROPERTY(QString facultyName READ facultyName NOTIFY profileChanged)
    Q_PROPERTY(QString mPhone READ mPhone NOTIFY profileChanged)
    Q_PROPERTY(QString hPhone READ hPhone NOTIFY profileChanged)
    Q_PROPERTY(bool status READ status NOTIFY profileChanged)
    Q_PROPERTY(QString lastSeen READ lastSeen NOTIFY profileChanged)
    Q_PROPERTY(int friendsCount READ friendsCount NOTIFY profileChanged)
    Q_PROPERTY(int mutualFriendsCount READ mutualFriendsCount NOTIFY profileChanged)
    Q_PROPERTY(int photosCount READ photosCount NOTIFY profileChanged)
    Q_PROPERTY(int videosCount READ videosCount NOTIFY profileChanged)
    Q_PROPERTY(int audiosCount READ audiosCount NOTIFY profileChanged)
    Q_PROPERTY(bool canPost READ canPost NOTIFY profileChanged)
    Q_PROPERTY(bool canWriteMessage READ canWriteMessage NOTIFY profileChanged)

private:
    sProfile m_profile;

private:
    QString uid();
    QString name();
    QString photo();
    QString photoBig();
    QString activity();
    QString bDate();
    QString univerName();
    QString facultyName();
    QString mPhone();
    QString hPhone();
    bool status();
    QString lastSeen();
    int friendsCount();
    int mutualFriendsCount();
    int photosCount();
    int videosCount();
    int audiosCount();
    bool canPost();
    bool canWriteMessage();

public:
    profileForm(QObject *parent = 0);

public slots:
    void slotProfile(const sProfile &profile);
    void slotClear();

signals:
    void profileChanged();
};

#endif // PROFILEFORM_H
