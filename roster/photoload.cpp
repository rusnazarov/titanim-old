/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "photoload.h"

//определяем static-члены
QQueue<sAvatar> photoLoad::queuePhoto;
QString photoLoad::m_cacheDir;
QMutex photoLoad::mutex;

photoLoad::photoLoad(QObject *parent) :
        QThread(parent)
{
}

void photoLoad::run(){
    // ==========================================================================
    // цикл потока
    // ==========================================================================

    QNetworkAccessManager manager;

    work = true;
    while (work){
        mutex.lock();
        if (queuePhoto.count() > 0){
            photo = queuePhoto.dequeue();
        }
        mutex.unlock();

        if (!photo.url.isEmpty()){
            QNetworkReply *networkReply = manager.get(QNetworkRequest(photo.url));

            QEventLoop loop;
            QObject::connect(networkReply, SIGNAL(finished()), &loop, SLOT(quit()));
            loop.exec();

            if(networkReply->error() != QNetworkReply::NoError){
                //qDebug() << networkReply->errorString();
                networkReply->deleteLater();
                photo.url.clear();
                continue;
            }

            QByteArray bytes = networkReply->readAll();
            networkReply->deleteLater();
            emit loaded(bytes, photo.id);

            photo.url.clear();
        } else work = false;
    }
}

void photoLoad::setStop(){
    // ==========================================================================
    // просим поток остановиться
    // ==========================================================================

    work = false;
}

void photoLoad::setCacheDir(const QString &cacheDir){
    m_cacheDir = cacheDir;
}

void photoLoad::append(const QUrl &url, int id){
    // ==========================================================================
    // добавляем в очередь
    // ==========================================================================

    sAvatar temp;
    temp.url = url;
    temp.id = id;
    queuePhoto.enqueue(temp);
}

void photoLoad::appendBegin(const QUrl &url, int id){
    // ==========================================================================
    // добавляем в начало очереди
    // ==========================================================================

    sAvatar temp;
    temp.url = url;
    temp.id = id;
    queuePhoto.insert(queuePhoto.begin(), temp);
}

int photoLoad::count(){
    return queuePhoto.count();
}
