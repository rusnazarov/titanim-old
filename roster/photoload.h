/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef PHOTOLOAD_H
#define PHOTOLOAD_H

#include <QThread>
#include <QQueue>
#include <QMutex>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QEventLoop>
#include <QUrl>

struct sAvatar {
    QUrl url;
    int id;
};

class photoLoad : public QThread
{
Q_OBJECT

private:
    static QQueue<sAvatar> queuePhoto;
    static QString m_cacheDir;
    static QMutex mutex;
    sAvatar photo;
    bool work;

public:
    explicit photoLoad(QObject *parent = 0);
    void run();
    void setStop();
    static void setCacheDir(const QString &cacheDir);
    static void append (const QUrl &url, int id);
    static void appendBegin (const QUrl &url, int id);
    static int count();

signals:
    void loaded(const QByteArray &bytes, int id);
};

#endif // PHOTOLOAD_H
