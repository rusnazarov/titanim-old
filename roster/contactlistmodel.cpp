/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "contactlistmodel.h"

contactListModel::contactListModel(QObject *parent) :
    QAbstractListModel(parent)
{
    visibleAvatar = false;
    cacheDir = appSettings::instance()->cacheDir();
    QHash<int, QByteArray> roles = roleNames();
    roles[statusRole] = "status";
    roles[activityRole] = "activity";
    roles[uidRole] = "uid";
    roles[filterListsRole] = "filterLists";
    roles[alphabetRole] = "alphabet";
    setRoleNames(roles);
}

void contactListModel::appendContacts(QVector<sContact> contacts){
    // ==========================================================================
    // добавление контактов в контакт лист
    // ==========================================================================

    if (!contacts.count())
        return;

    if (visibleAvatar){
        for (int i=0; i < contacts.count(); i++){
            if (contacts.at(i).avatarImg.isNull() &&
                QFile::exists(cacheDir+contacts.at(i).avatarUrl.path().remove(0,1).replace("/","."))){
                QPixmap img(cacheDir+contacts.at(i).avatarUrl.path().remove(0,1).replace("/","."));
                //исправляем не квадратные аватарки
                img = img.copy(sizeToRect(img.size()));
                //обновляем контакт
                contacts[i].avatarImg = img;
            }
        }
    }

    beginInsertRows(QModelIndex(), this->contacts.count(), this->contacts.count() + contacts.count()-1);
    this->contacts << contacts;
    endInsertRows();
}

void contactListModel::appendContact(sContact contact){
    // ==========================================================================
    // добавление контакта в контакт лист
    // ==========================================================================

    if (contact.uid.isEmpty())
        return;

    if (visibleAvatar){
        if (contact.avatarImg.isNull() &&
            QFile::exists(cacheDir+contact.avatarUrl.path().remove(0,1).replace("/","."))){
            QPixmap img(cacheDir+contact.avatarUrl.path().remove(0,1).replace("/","."));
            //исправляем не квадратные аватарки
            img = img.copy(sizeToRect(img.size()));
            //обновляем контакт
            contact.avatarImg = img;
        }
    }

    beginInsertRows(QModelIndex(), contacts.count(), contacts.count());
    contacts << contact;
    endInsertRows();
}

void contactListModel::replaceContacts(QVector<sContact> contacts){
    // ==========================================================================
    // замена контактов
    // ==========================================================================

    //очищаем модель
    removeContacts(0, rowCount());

    //добавляем новые
    appendContacts(contacts);
}

bool contactListModel::removeContacts(int row, int count){
    // ==========================================================================
    // удаляем с модели данные
    // ==========================================================================

    beginRemoveRows(QModelIndex(), row, row+count-1);

    for (int i = 0; i < count; ++i) {
        contacts.remove(row);
    }

    endRemoveRows();
    return true;
}

QString contactListModel::toolTip(const sContact &contact) const{
    // ==========================================================================
    // создает строку для отображения в QToolTip
    // ==========================================================================

    QString fileAvatar = cacheDir+contact.avatarUrl.path().remove(0,1).replace("/",".");

    QString html;
    html = "<table cellspacing=\"0\" cellpadding=\"0\">";

    //иконка статуса
    if (contact.status == "1")
        html += "<tr><td><img border=\"0\" src=\":/Icon_18.png\"></td>";
    else
        html += "<tr><td><img border=\"0\" src=\":/Icon_17.png\"></td>";
    
    //имя, фамилия
    html += QString("<td colspan=\"2\" width=\"200\"><b>%1</b> (%2)</td>")
            .arg(contact.firstName+" "+contact.lastName)
            .arg(contact.uid);
    
    //аватарка
    if (QFile::exists(fileAvatar))
        html += QString("<td rowspan=6><img border=\"1\" src=\"%1\"></td>")
            .arg(fileAvatar);

    html += "</tr>";

    //расширенный статус
    if (!contact.activity.isEmpty())
        html += QString("<tr><td></td><td colspan=\"2\"><small>%1</small></td></tr>")
            .arg(contact.activity);

    //никнейм:
    if (!contact.nickName.isEmpty())
        html += QString("<tr><td></td><td><b><small>%1:</small><b></td><td><small>%2</small></td></tr>")
        .arg(tr("Nickname"))
        .arg(contact.nickName);

    //день рождения
    if (!contact.bDate.isEmpty())
        html += QString("<tr><td></td><td><b><small>%1:</small><b></td><td><small>%2</small></td></tr>")
        .arg(tr("Birthday"))
        .arg(contact.bDate);

    //мобильный телефон
    if (!contact.mPhone.isEmpty())
        html += QString("<tr><td></td><td><b><small>%1:</small><b></td><td><small>%2</small></td></tr>")
        .arg(tr("Mobile phone"))
        .arg(contact.mPhone);

    //домашний телефон
    if (!contact.hPhone.isEmpty())
        html += QString("<tr><td></td><td><b><small>%1:</small><b></td><td><small>%2</small></td></tr>")
        .arg(tr("Home phone"))
        .arg(contact.hPhone);

    html += "</table>";
    return html;
}

QVariant contactListModel::data(const QModelIndex &index, int role) const{
    // ==========================================================================
    // данные модели
    // ==========================================================================

    if (!index.isValid() || index.row() >= contacts.size()) {
        return QVariant();
    }

    switch (role) {
    case Qt::DisplayRole:
        return contacts.at(index.row()).firstName.isEmpty() ?
                contacts.at(index.row()).uid :
                contacts.at(index.row()).firstName + " " + contacts.at(index.row()).lastName;

    case Qt::DecorationRole:
        if (!visibleAvatar)
            return contacts.at(index.row()).status == "1" ? "qrc:/Icon_18.png" : "qrc:/Icon_17.png";
        return contacts.at(index.row()).avatarUrl;

    case Qt::ToolTipRole:
        return toolTip(contacts.at(index.row()));

    case Qt::FontRole:
        return QFont();

    case Qt::ForegroundRole:
        return contacts.at(index.row()).status == "0" ? QColor("#FF0000") : QVariant();

    case Qt::SizeHintRole:
        return QVariant();

    case statusRole:
        return contacts.at(index.row()).status;

    case activityRole:
        return contacts.at(index.row()).activity;

    case uidRole:
        return contacts.at(index.row()).uid;

    case filterListsRole: //листы + статус
        return "," + contacts.at(index.row()).lists + ", " +contacts.at(index.row()).status;

    case alphabetRole: //первая буква имени
        return QString(contacts.at(index.row()).firstName[0]);
    }

    return QVariant();
}

bool contactListModel::setData(const QModelIndex &index, const QVariant &value, int role){
    // ==========================================================================
    // редактирование данных модели
    // ==========================================================================

    if (!index.isValid() && !value.isValid() && index.row() >= contacts.size()) {
        return false;
    }

    //изменяем аватар
    if (role == Qt::DecorationRole){
        contacts[index.row()].avatarImg = value.value<QPixmap>();
        emit dataChanged(index, index);
        return true;
    }

    //изменяем статус
    if (role == statusRole){
        contacts[index.row()].status = QString::number(value.toInt());
        emit dataChanged(index, index);
        return true;
    }

    //изменяем расширенный статус
    if (role == activityRole){
        contacts[index.row()].activity = value.toString();
        emit dataChanged(index, index);
        return true;
    }

    return false;
}

int contactListModel::rowCount(const QModelIndex &parent) const{
    // ==========================================================================
    // количество строк в модели
    // ==========================================================================

    Q_UNUSED(parent);
    return contacts.count();
}

Qt::ItemFlags contactListModel::flags(const QModelIndex &index) const{
    // ==========================================================================
    // флаги модели
    // ==========================================================================

    if (index.isValid())
      return (Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    else
      return Qt::NoItemFlags;
}

QRect contactListModel::sizeToRect(const QSize &size) const{
    // ==========================================================================
    // возращает квадрат для размера аватара
    // ==========================================================================

    if (size.width() < size.height())
        return QRect(0, 0, size.width(), size.width());
    else
        return QRect(0, 0, size.height(), size.height());
}

void contactListModel::setVisibleAvatar(const bool &visible){
    // ==========================================================================
    // нужно ли показывать аватары
    // ==========================================================================

    if (visible){
        for (int i=0; i < contacts.count(); i++){
            if (contacts.at(i).avatarImg.isNull() &&
                QFile::exists(cacheDir+contacts.at(i).avatarUrl.path().remove(0,1).replace("/","."))){
                QPixmap img(cacheDir+contacts.at(i).avatarUrl.path().remove(0,1).replace("/","."));
                //исправляем не квадратные аватарки
                img = img.copy(sizeToRect(img.size()));
                //обновляем контакт
                QModelIndex idx = index(i, 0);
                setData(idx, img, Qt::DecorationRole);
            }
        }
    }

    if (visible != visibleAvatar){
        visibleAvatar = visible;
        reset();
    }
}

bool contactListModel::isVisibleAvatar() const{
    // ==========================================================================
    // показаны ли аватары
    // ==========================================================================

    return visibleAvatar;
}

void contactListModel::getPhoto(){
    // ==========================================================================
    // загружаем аватарки с сервера
    // ==========================================================================

    for (int i=0; i < contacts.count(); i++){
        if (!QFile::exists(cacheDir+contacts.at(i).avatarUrl.path().remove(0,1).replace("/",".")) &&
            contacts.at(i).avatarUrl.path().right(3) != "gif"){
            if (contacts.at(i).status == "1")
                photoLoad::appendBegin(contacts.at(i).avatarUrl, i);
            else
                photoLoad::append(contacts.at(i).avatarUrl, i);
        }
    }

    //устанавливаю каталог кеша
    photoLoad::setCacheDir(cacheDir);

    //загружаю аватары которых нет в кеше
    int k = qMin(10, photoLoad::count());
    for (int i=0; i < k; i++){
        photo[i] = new photoLoad(this);
        connect(photo[i], SIGNAL(loaded(QByteArray,int)), this, SLOT(slotPhotoLoaded(QByteArray,int)));
        photo[i]->start();
    }
}

void contactListModel::slotPhotoLoaded(const QByteArray &bytes, int id){
    // ==========================================================================
    // пришло изображение (аватар) с сервера
    // ==========================================================================

    //для внутренних нужд (показ профиля итд)
    if (id >= 50000)
        return;

    QPixmap img;
    img.loadFromData(bytes);

    //сохраняю в кеш
    img.save(cacheDir+contacts.at(id).avatarUrl.path().remove(0,1).replace("/","."), 0, 100);

    //исправляем не квадратные аватарки
    img = img.copy(sizeToRect(img.size()));

    //обновляем контакт
    QModelIndex idx = index(id, 0);
    setData(idx, img, Qt::DecorationRole);
}

sContact contactListModel::getContact(const QString &uid) const{
    // ==========================================================================
    // запросили данные контакта
    // ==========================================================================

    if (!contacts.isEmpty() && !uid.isEmpty()){
        //ищем контакт по uid
        for (int i=0; i<contacts.count(); i++){
            if (contacts.at(i).uid == uid){
                return contacts.at(i);
            }
        }
    }

    //если не найдено
    sContact contact;
    contact.uid = uid;
    return contact;
}

QStringList contactListModel::getAllUidOnline(){
    // ==========================================================================
    // получаем uid всех кто онлайн
    // ==========================================================================

    QStringList uids;

    for (int i=0; i<contacts.count(); i++){
        if (contacts.at(i).status == "1"){
            uids.append(contacts.at(i).uid);
        }
    }

    return uids;
}

void contactListModel::setUserOnline(const QString &uid){
    // ==========================================================================
    // обновляем контакт, теперь он в сети
    // ==========================================================================

    //ищем uid контакта и изменяем статус
    // 1 - в сети, 0 - не в сети
    for (int i=0; i<contacts.count(); i++){
        if (contacts.at(i).uid == uid){
            QModelIndex idx = index(i, 0);
            setData(idx, 1, statusRole);
            break;
        }
    }
}

void contactListModel::setUserOffline(const QString &uid, int flag){
    // ==========================================================================
    // обновляем контакт, теперь он не в сети
    // ==========================================================================

    Q_UNUSED(flag);

    //ищем uid контакта и изменяем статус
    // 1 - в сети, 0 - не в сети
    for (int i=0; i<contacts.count(); i++){
        if (contacts.at(i).uid == uid){
            QModelIndex idx = index(i, 0);
            setData(idx, 0, statusRole);
            break;
        }
    }
}

void contactListModel::setActivity(const QList<sActivity> &activity){
    // ==========================================================================
    // устанавливаем расширенные статусы
    // ==========================================================================

    //ищем uid контакта и изменяем статус
    for (int i=0; i<activity.count(); i++){
        for (int j=0; j<contacts.count(); j++){
            if (contacts.at(j).uid == activity.value(i).uid){
                QModelIndex idx = index(j, 0);
                setData(idx, activity.value(i).activity, activityRole);
                break;
            }
        }
    }
}

void contactListModel::messageReceived(const QString &uid){
    // ==========================================================================
    // пришло новое сообщение
    // ==========================================================================

    //ищем контакт по uid
    for (int i=0; i<contacts.count(); i++){
        if (contacts.at(i).uid == uid){
            QModelIndex idx = index(i, 0);
            setData(idx, QPixmap(":/mail.png"), Qt::DecorationRole);
            break;
        }
    }
}
