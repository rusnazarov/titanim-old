/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef CONTACTLISTMODEL_H
#define CONTACTLISTMODEL_H

#include <QAbstractListModel>
#include "vk/cvkStruct.h"
#include "settings.h"
#include "photoload.h"
#include <QFont>
#include <QFile>

class contactListModel : public QAbstractListModel
{
    Q_OBJECT

private:
    QVector<sContact> contacts;
    bool visibleAvatar;
    QString cacheDir;
    photoLoad *photo[10];

private:
    QString toolTip (const sContact &contact) const;
    QRect sizeToRect(const QSize &size) const;

public:
    explicit contactListModel(QObject *parent = 0);
    void appendContacts(QVector<sContact> contacts);
    void appendContact(sContact contact);
    void replaceContacts(QVector<sContact> contacts);
    bool removeContacts(int row, int count);
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant& value, int role = Qt::EditRole);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    void setVisibleAvatar(const bool &visible);
    bool isVisibleAvatar() const;
    void getPhoto();
    sContact getContact(const QString &uid) const;
    QStringList getAllUidOnline();
    void setUserOnline(const QString &uid);
    void setUserOffline(const QString &uid, int flag);
    void setActivity(const QList<sActivity> &activity);
    void messageReceived(const QString &uid);

    enum contactRole {
        statusRole = Qt::UserRole,
        activityRole = Qt::UserRole+1,
        uidRole = Qt::UserRole+2,
        filterListsRole = Qt::UserRole+3,
        alphabetRole = Qt::UserRole+4
    };

private slots:
    void slotPhotoLoaded(const QByteArray &bytes, int id);
};

#endif // CONTACTLISTMODEL_H
