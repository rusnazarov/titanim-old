/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "contactlistdelegate.h"

contactListDelegate::contactListDelegate(const bool &showStatus, QObject *parent) :
        QStyledItemDelegate(parent)
{
    this->showStatus = showStatus;
}

void contactListDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const{
    if(!index.isValid()){
        return;
    }

    //Шрифт и цвет имени
    QFont nameFont = option.font;
    QColor nameFontColor = index.data(Qt::ForegroundRole).value<QColor>();
    if (!nameFontColor.isValid())
        nameFontColor = painter->pen().color();

    //Шрифт и цвет статуса
    QFont statusFont = option.font;
    statusFont.setPointSize(7);
    QColor statusFontColor("#808080");

    painter->save();

    //если выбрано
    if (option.state & QStyle::State_Selected)
    {
        //градиент в стили ВК
        QLinearGradient gradient(0, option.rect.y(), 0, option.rect.y()+option.rect.height());
        gradient.setColorAt(0, QColor("#4e79a1"));
        gradient.setColorAt(1, QColor("#174774"));

        //градиент не зависит от палитры, поэтому и текст белый
        nameFontColor = QColor(Qt::white);
        statusFontColor = QColor(Qt::white);

        //красим
        QBrush brush(gradient);
        painter->fillRect(option.rect, brush);
    }

    //получаем фото
    QPixmap foto = index.data(Qt::DecorationRole).value<QPixmap>();
    foto = foto.scaledToHeight(option.decorationSize.height(), Qt::SmoothTransformation);

    //получаем имя и статус
    QString name = index.data(Qt::DisplayRole).toString();
    QString status;
    if (showStatus)
        status = index.data(Qt::UserRole+1).toString();

    //определяем где рисовать
    int x = option.rect.x();
    int w = option.rect.width();

    //рисуем фото
    int y = option.rect.y()+((option.rect.height()-foto.rect().height())/2);
    painter->drawPixmap(x+3, y, foto);

    //размер шрифта имени
    QFontMetrics fm1(nameFont);
    QRect rect1 = fm1.boundingRect(name);

    //размер шрифта статуса
    QFontMetrics fm2(statusFont);
    QRect rect2 = fm2.boundingRect(status);

    //определяем где рисовать
    x = x+foto.size().width()+9;
    y = option.rect.y()+((option.rect.height()-(rect1.height()+rect2.height()))/2);

    //рисуем имя
    painter->setPen(QPen(nameFontColor));
    painter->setFont(nameFont);
    painter->drawText(QRect(x, y, w-x, rect1.height()), Qt::TextWrapAnywhere, fm1.elidedText(name, Qt::ElideRight, w-x));

    //рисуем статус
    if (showStatus){
        painter->setPen(QPen(statusFontColor));
        painter->setFont(statusFont);
        painter->drawText(QRect(x, y+rect1.height(), w-x, rect2.height()), Qt::TextWrapAnywhere, fm2.elidedText(status, Qt::ElideRight, w-x));
    }

    painter->restore();
}

QSize contactListDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const{
    if(!index.isValid()){
        return QSize();
    }

    QSize newSize = QStyledItemDelegate::sizeHint(option, index);

    //размер шрифта имени
    QFont nameFont = option.font;
    QFontMetrics fm1(nameFont);
    int nameH = fm1.height();

    //размер шрифта статуса
    int statusH = 0;
    if (showStatus){
        QFont statusFont = option.font;
        statusFont.setPointSize(7);
        QFontMetrics fm2(statusFont);
        statusH = fm2.height();
    }

    newSize.setHeight(option.decorationSize.height()+2 > nameH+statusH ? option.decorationSize.height()+2 : nameH+statusH);

    return newSize;
}

void contactListDelegate::setShowStatus(const bool &show){
    // ==========================================================================
    // нужно ли показывать расширенные статусы
    // ==========================================================================

    this->showStatus = show;
}























