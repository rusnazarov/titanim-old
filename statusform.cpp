/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "statusform.h"
#include "ui_statusform.h"

statusForm::statusForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::statusForm)
{
    ui->setupUi(this);

    http = new QNetworkAccessManager(this);
    connect(http, SIGNAL(finished(QNetworkReply*)),this, SLOT(slotFinished(QNetworkReply*)));
    connect(ui->textEdit, SIGNAL(textChanged()), this, SLOT(slotCountChar()));
    connect(ui->statusCheck, SIGNAL(stateChanged(int)), this, SLOT(slotStateChanged(int)));
    connect(ui->wallCheck, SIGNAL(stateChanged(int)), this, SLOT(slotStateChanged(int)));
}

statusForm::~statusForm()
{
    delete http;
    delete ui;
}

void statusForm::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void statusForm::closeEvent(QCloseEvent *pe){
    // ==========================================================================
    // по закрытию скрываем окно
    // ==========================================================================

    hide();
    pe->ignore();
}

void statusForm::setPhoto(const QUrl &url){
    // ==========================================================================
    // загружаем фото текущего пользователя
    // ==========================================================================

    QNetworkRequest request;
    request.setUrl(url);
    http->get(request);
}

void statusForm::setStatus(const QString &status){
    // ==========================================================================
    // установка статуса на форму
    // ==========================================================================

    ui->textEdit->setText(status);
}

void statusForm::slotFinished(QNetworkReply *networkReply){
    // ==========================================================================
    // изображение загрузилось
    // ==========================================================================

    if(networkReply->error() != QNetworkReply::NoError){
        //qDebug() << networkReply->errorString();
        ui->photo->setPixmap(QPixmap(":/avatar.png"));
        return;
    }

    QByteArray bytes = networkReply->readAll();
    QPixmap img;
    img.loadFromData(bytes);
    ui->photo->setPixmap(img);
    resize(size().width(), 70);
    toolsHeight = height() - ui->textEdit->height();
    networkReply->deleteLater();
}

void statusForm::slotCountChar(){
    // ==========================================================================
    // количество символов
    // ==========================================================================

    //максимум для статуса 140 симв.
    if (140 - ui->textEdit->toPlainText().count() < 0){
        ui->statusCheck->setChecked(false);
        ui->statusCheck->setEnabled(false);
        //максимум для записи на стене 255 симв.
        if (255 - ui->textEdit->toPlainText().count() < 0){
            ui->wallCheck->setChecked(false);
            ui->wallCheck->setEnabled(false);
        } else {
            ui->wallCheck->setChecked(true);
            ui->wallCheck->setEnabled(true);
        }
    } else {
        ui->statusCheck->setEnabled(true);
        ui->wallCheck->setEnabled(true);
    }

    int height = ui->textEdit->document()->size().height();
    resize(width(), height + toolsHeight);
}

void statusForm::slotStateChanged(int state){
    // ==========================================================================
    // изменилось состояние флажка
    // ==========================================================================

    Q_UNUSED(state);

    if (!ui->statusCheck->isChecked() && !ui->wallCheck->isChecked())
        ui->pushButton->setEnabled(false);
    else
        ui->pushButton->setEnabled(true);
}

void statusForm::on_pushButton_clicked(){
    // ==========================================================================
    // отправка статуса на сайт
    // ==========================================================================

    if (ui->statusCheck->isChecked())
        emit sendStatus(ui->textEdit->toPlainText());

    if (ui->wallCheck->isChecked())
        emit sendWall(ui->textEdit->toPlainText());

    hide();
}
