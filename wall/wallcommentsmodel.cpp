/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "wallcommentsmodel.h"

wallCommentsModel::wallCommentsModel(QObject *parent) :
    QAbstractListModel(parent)
{
    QHash<int, QByteArray> roles = roleNames();
    roles[cidRole] = "cid";
    roles[uidRole] = "uid";
    roles[dateRole] = "date";
    roles[dateStrRole] = "dateStr";
    roles[textRole] = "text";
    roles[replyToUidRole] = "replyToUid";
    roles[replyToCidRole] = "replyToCid";
    setRoleNames(roles);
}


void wallCommentsModel::replaceComments(QVector<sWallComments> comments){
    // ==========================================================================
    // замена комментариев
    // ==========================================================================

    //очищаем модель
    removeComments(0, rowCount());

    //добавляем новые
    beginInsertRows(QModelIndex(), 0, comments.count() - 1);
    this->comments << comments;
    endInsertRows();
}

QString wallCommentsModel::dateText(QDateTime date) const{
    // ==========================================================================
    // переводит дату в удобную для человеку
    // ==========================================================================

    int days = date.daysTo(QDateTime::currentDateTime());

    switch (days) {
    case 0:
        return tr("Today at %1").arg(date.time().toString("hh:mm"));
    case 1:
        return tr("Yesterday at %1").arg(date.time().toString("hh:mm"));
    default:
        return date.toString("dd.MM.yy hh:mm");
    }
}

bool wallCommentsModel::removeComments(int row, int count){
    // ==========================================================================
    // удаляем с модели данные
    // ==========================================================================

    beginRemoveRows(QModelIndex(), row, row+count-1);

    for (int i = 0; i < count; ++i) {
        comments.remove(row);
    }

    endRemoveRows();
    return true;
}

QVariant wallCommentsModel::data(const QModelIndex &index, int role) const{
    // ==========================================================================
    // данные модели
    // ==========================================================================

    if (!index.isValid() || index.row() >= comments.size()) {
        return QVariant();
    }

    switch (role) {
    case Qt::DisplayRole:
        return comments.at(index.row()).firstName.isEmpty() ?
                comments.at(index.row()).uid :
                comments.at(index.row()).firstName + " " + comments.at(index.row()).lastName;

    case Qt::DecorationRole:
        return comments.at(index.row()).avatarUrl;

    case cidRole:
        return comments.at(index.row()).cid;

    case uidRole:
        return comments.at(index.row()).uid;

    case dateRole:
        return comments.at(index.row()).date;

    case dateStrRole:
        return dateText(comments.at(index.row()).date);

    case textRole:
        return comments.at(index.row()).text;

    case replyToUidRole:
        return comments.at(index.row()).replyToUid;

    case replyToCidRole:
        return comments.at(index.row()).replyToCid;
    }

    return QVariant();
}

bool wallCommentsModel::setData(const QModelIndex &index, const QVariant &value, int role){
    // ==========================================================================
    // редактирование данных модели
    // ==========================================================================

    if (!index.isValid() && !value.isValid() && index.row() >= comments.size()) {
        return false;
    }

    return false;
}

int wallCommentsModel::rowCount(const QModelIndex &parent) const{
    // ==========================================================================
    // количество строк в модели
    // ==========================================================================

    Q_UNUSED(parent);
    return comments.count();
}

Qt::ItemFlags wallCommentsModel::flags(const QModelIndex &index) const{
    // ==========================================================================
    // флаги модели
    // ==========================================================================

    if (index.isValid())
      return (Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    else
      return Qt::NoItemFlags;
}


