/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef WALLMODEL_H
#define WALLMODEL_H

#include <QAbstractListModel>
#include "vk/cvkStruct.h"

class wallModel : public QAbstractListModel
{
Q_OBJECT

private:
    QVector<sWall> wall;

public:
    explicit wallModel(QObject *parent = 0);
    void appendWall(QVector<sWall> wall);
    void replaceWall(QVector<sWall> wall);
    QString dateText(QDateTime date) const;
    bool removeWall(int row, int count);
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant& value, int role = Qt::EditRole);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    void setLikesCount(const QString &ownerId, const QString &postId, const QString &countLikes, bool isAdd);

    enum WallRole {
        idRole = Qt::UserRole,
        toIdRole,
        fromIdRole,
        dateRole,
        dateStrRole,
        textRole,
        shortTextRole,
        commentsRole,
        canPostRole,
        likesRole,
        isLikedRole,
        photosRole,
        photosBigRole,
        geoRole,
        copyOwnerIdRole,
        copyPostIdRole,
        linkUrlRole
    };

public slots:

signals:
};

#endif // WALLMODEL_H
