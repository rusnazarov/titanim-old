/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "wallmodel.h"

wallModel::wallModel(QObject *parent) :
    QAbstractListModel(parent)
{
    QHash<int, QByteArray> roles = roleNames();
    roles[idRole] = "id";
    roles[toIdRole] = "toId";
    roles[fromIdRole] = "fromId";
    roles[dateRole] = "date";
    roles[dateStrRole] = "dateStr";
    roles[textRole] = "text";
    roles[shortTextRole] = "shortText";
    roles[commentsRole] = "comments";
    roles[canPostRole] = "canPost";
    roles[likesRole] = "likes";
    roles[isLikedRole] = "isLiked";
    roles[photosRole] = "photos";
    roles[photosBigRole] = "photosBig";
    roles[geoRole] = "geo";
    roles[copyOwnerIdRole] = "copyOwnerId";
    roles[copyPostIdRole] = "copyPostId";
    roles[linkUrlRole] = "linkUrl";
    setRoleNames(roles);
}

void wallModel::appendWall(QVector<sWall> wall){
    // ==========================================================================
    // добавление новостей
    // ==========================================================================

    if (!wall.count())
        return;

    //добавляем новые
    beginInsertRows(QModelIndex(), this->wall.count(), this->wall.count() + wall.count() - 1);
    this->wall << wall;
    endInsertRows();
}

void wallModel::replaceWall(QVector<sWall> wall){
    // ==========================================================================
    // замена новостей
    // ==========================================================================

    //очищаем модель
    removeWall(0, rowCount());

    //добавляем новые
    appendWall(wall);
}

QString wallModel::dateText(QDateTime date) const{
    // ==========================================================================
    // переводит дату в удобную для человеку
    // ==========================================================================

    int days = date.daysTo(QDateTime::currentDateTime());

    switch (days) {
    case 0:
        return tr("Today at %1").arg(date.time().toString("hh:mm"));
    case 1:
        return tr("Yesterday at %1").arg(date.time().toString("hh:mm"));
    default:
        return date.toString("dd.MM.yy hh:mm");
    }
}

bool wallModel::removeWall(int row, int count){
    // ==========================================================================
    // удаляем с модели данные
    // ==========================================================================

    beginRemoveRows(QModelIndex(), row, row+count-1);

    for (int i = 0; i < count; ++i) {
        wall.remove(row);
    }

    endRemoveRows();
    return true;
}

QVariant wallModel::data(const QModelIndex &index, int role) const{
    // ==========================================================================
    // данные модели
    // ==========================================================================

    if (!index.isValid() || index.row() >= wall.size()) {
        return QVariant();
    }

    switch (role) {
    case Qt::DisplayRole:
        return wall.at(index.row()).firstName.isEmpty() ?
                wall.at(index.row()).fromId :
                wall.at(index.row()).firstName + " " + wall.at(index.row()).lastName;

    case Qt::DecorationRole:
        return wall.at(index.row()).avatarUrl;

    case idRole:
        return wall.at(index.row()).id;

    case toIdRole:
        return wall.at(index.row()).toId;

    case fromIdRole:
        return wall.at(index.row()).fromId;

    case dateRole:
        return wall.at(index.row()).date;

    case dateStrRole:
        return dateText(wall.at(index.row()).date);

    case textRole:
        return wall.at(index.row()).text;

    case shortTextRole:
        return wall.at(index.row()).text.length() > 200 ?
                wall.at(index.row()).text.left(200) + "..." :
                wall.at(index.row()).text;

    case commentsRole:
        return wall.at(index.row()).comments;

    case canPostRole:
        return wall.at(index.row()).canPost;

    case likesRole:
        return wall.at(index.row()).likes;

    case isLikedRole:
        return wall.at(index.row()).isLiked;

    case photosRole:
        return QVariant::fromValue(wall.at(index.row()).photos.split(","));

    case photosBigRole:
        return QVariant::fromValue(wall.at(index.row()).photosBig.split(","));

    case geoRole:
        return wall.at(index.row()).geo;

    case copyOwnerIdRole:
        return wall.at(index.row()).copyOwnerId;

    case copyPostIdRole:
        return wall.at(index.row()).copyPostId;

    case linkUrlRole:
        return wall.at(index.row()).linkUrl;
    }

    return QVariant();
}

bool wallModel::setData(const QModelIndex &index, const QVariant &value, int role){
    // ==========================================================================
    // редактирование данных модели
    // ==========================================================================

    if (!index.isValid() && !value.isValid() && index.row() >= wall.size()) {
        return false;
    }

    //изменяем количество лайков
    if (role == likesRole){
        wall[index.row()].likes = value.toString();
        emit dataChanged(index, index);
        return true;
    }

    //изменяем isLiked
    if (role == isLikedRole){
        wall[index.row()].isLiked = value.toBool();
        emit dataChanged(index, index);
        return true;
    }

    return false;
}

int wallModel::rowCount(const QModelIndex &parent) const{
    // ==========================================================================
    // количество строк в модели
    // ==========================================================================

    Q_UNUSED(parent);
    return wall.count();
}

Qt::ItemFlags wallModel::flags(const QModelIndex &index) const{
    // ==========================================================================
    // флаги модели
    // ==========================================================================

    if (index.isValid())
      return (Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    else
        return Qt::NoItemFlags;
}

void wallModel::setLikesCount(const QString &ownerId, const QString &postId, const QString &countLikes, bool isAdd){
    // ==========================================================================
    // обновляем количество лайков
    // ==========================================================================

    //ищем ownerId и postId записи и изменяем количество
    for (int i=0; i<wall.count(); i++){
        if (wall.at(i).toId == ownerId && wall.at(i).id == postId){
            QModelIndex idx = index(i, 0);
            setData(idx, countLikes, likesRole);
            setData(idx, isAdd, isLikedRole);
            break;
        }
    }
}

