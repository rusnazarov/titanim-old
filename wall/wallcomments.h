#ifndef WALLCOMMENTS_H
#define WALLCOMMENTS_H

#include <QObject>
#include <QSortFilterProxyModel>
#include "wallcommentsmodel.h"
#include "../vk/cvkStruct.h"

class WallComments : public QObject
{
Q_OBJECT

public:
    wallCommentsModel *model;
    QSortFilterProxyModel *proxy;

public:
    explicit WallComments(QObject *parent = 0);
    ~WallComments();

public slots:
    void slotWallComments(const QVector<sWallComments> &comments);

signals:
};

#endif // WALLCOMMENTS_H
