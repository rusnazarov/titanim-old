/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "wallcomments.h"

WallComments::WallComments(QObject *parent) :
    QObject(parent)
{
    model = new wallCommentsModel();

    proxy = new QSortFilterProxyModel(this);
    proxy->setDynamicSortFilter(true);
    proxy->setSortCaseSensitivity(Qt::CaseInsensitive);
    proxy->setFilterCaseSensitivity(Qt::CaseInsensitive);
    proxy->setSortRole(Qt::UserRole+2);
    proxy->sort(0);
    proxy->setSourceModel(model);
}

WallComments::~WallComments(){
    delete proxy;
    delete model;
}

void WallComments::slotWallComments(const QVector<sWallComments> &comments){
    // ==========================================================================
    // пришли комментарии
    // ==========================================================================

    model->replaceComments(comments);
}
