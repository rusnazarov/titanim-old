/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef WALL_H
#define WALL_H

#include <QObject>
#include <QSortFilterProxyModel>
#include "wallmodel.h"
#include "../vk/cvkStruct.h"

class Wall : public QObject
{
Q_OBJECT

public:
    wallModel *model;
    QSortFilterProxyModel *proxy;

public:
    explicit Wall(QObject *parent = 0);
    ~Wall();

public slots:
    void appendWall(const QVector<sWall> &wall);
    void replaceWall(const QVector<sWall> &wall);

signals:
};

#endif // WALL_H
