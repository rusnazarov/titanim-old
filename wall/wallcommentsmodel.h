#ifndef WALLCOMMENTSMODEL_H
#define WALLCOMMENTSMODEL_H

#include <QAbstractListModel>
#include "vk/cvkStruct.h"

class wallCommentsModel : public QAbstractListModel
{
Q_OBJECT

private:
    QVector<sWallComments> comments;

public:
    explicit wallCommentsModel(QObject *parent = 0);
    void replaceComments(QVector<sWallComments> comments);
    QString dateText(QDateTime date) const;
    bool removeComments(int row, int count);
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant& value, int role = Qt::EditRole);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    enum WallCommentsRole {
        cidRole = Qt::UserRole,
        uidRole = Qt::UserRole+1,
        dateRole = Qt::UserRole+2,
        dateStrRole = Qt::UserRole+3,
        textRole = Qt::UserRole+4,
        replyToUidRole = Qt::UserRole+5,
        replyToCidRole = Qt::UserRole+6
    };

public slots:

signals:
};

#endif // WALLCOMMENTSMODEL_H
