/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "wall.h"

Wall::Wall(QObject *parent) :
    QObject(parent)
{
    model = new wallModel();

    proxy = new QSortFilterProxyModel(this);
    proxy->setDynamicSortFilter(true);
    proxy->setSortCaseSensitivity(Qt::CaseInsensitive);
    proxy->setFilterCaseSensitivity(Qt::CaseInsensitive);
    proxy->setSortRole(Qt::UserRole+3);
    proxy->sort(0, Qt::DescendingOrder);
    proxy->setSourceModel(model);
}

Wall::~Wall(){
    delete proxy;
    delete model;
}

void Wall::appendWall(const QVector<sWall> &wall){
    // ==========================================================================
    // пришли новости
    // ==========================================================================

    model->appendWall(wall);
}

void Wall::replaceWall(const QVector<sWall> &wall){
    // ==========================================================================
    // пришли новости
    // ==========================================================================

    model->replaceWall(wall);
}
