/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************

 ****************************************************************************
 * I'm sorry for the russian comments
 ***************************************************************************

*/

#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QmlApplicationViewer(parent)
{
    //язык
    translator = new QTranslator(this);
    translator->load("titanim_" + appSettings::instance()->loadMain("main/language", QLocale::system().name()).toString(),
                     appSettings::instance()->homeAppDir() + "translations");
    qApp->installTranslator(translator);
    qtTranslator = new QTranslator(this);
    qtTranslator->load("qt_" + appSettings::instance()->loadMain("main/language", QLocale::system().name()).toString(),
                     appSettings::instance()->homeAppDir() + "translations");
    qApp->installTranslator(qtTranslator);

    //обнуляем
    status = offline;
    positionInStack = 0;
    positionInStackD = 0;
    model = 0;
    proxy = 0;

    //таймер для мигания иконки в трее
    timerMessageTray = new QTimer(this);
    timerMessageTray->setInterval(500);
    connect(timerMessageTray, SIGNAL(timeout()), this, SLOT(slotTimerMessageTray()));

    //таймер для автоподключения после обрыва
    timerReconnect = new QTimer(this);
    connect(timerReconnect, SIGNAL(timeout()), this, SLOT(slotTimerReconnect()));

    //системный трей
    trayMenu = new QMenu(this);
    QMenu *statusTrayMenu = trayMenu->addMenu(QIcon(":/Icon_47.ico"), tr("Status"));
    statusTrayMenu->addAction(QIcon(":/Icon_18.png"), tr("Online"), this, SLOT(setOnline()));
    statusTrayMenu->addAction(QIcon(":/Icon_17.png"), tr("Offline"), this, SLOT(setOffline()));
    statusTrayMenu->addSeparator();
    statusTrayMenu->addAction(QIcon(":/Icon_35.ico"), tr("Edit text"), this, SLOT(on_statusButton_clicked()));
    trayMenu->addSeparator();
    trayMenu->addAction(QIcon(":/wrench-screwdriver.png"), tr("Settings"), this, SLOT(on_settingsButton_clicked()));
    trayMenu->addAction(QIcon(":/bell--exclamation.png"), tr("Quiet mode"), this, SLOT(slotQuietMode()))->setCheckable(true);
    trayMenu->addAction(QIcon(":/users.png"), tr("Change profile"), this, SLOT(slotChangeProfile()));
    trayMenu->addSeparator();
    trayMenu->addAction(QIcon(":/door.png"), tr("Quit"), qApp, SLOT(quit()));
    tray = new QSystemTrayIcon(QIcon(":/Icon_48.png"), this);
    connect(tray, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(slotTrayActivated(QSystemTrayIcon::ActivationReason)));
    tray->setContextMenu(trayMenu);
    tray->setToolTip("TitanIM");
    tray->show();

    //класс ВКонтакте (передаем id приложения)
    vk = new cvk("1950109", this);
    connect(vk, SIGNAL(errorString(eError,QString,bool)), this, SLOT(slotErrorString(eError,QString,bool)));
    connect(vk, SIGNAL(connected(QString,QString)), this, SLOT(slotConnected(QString,QString)));
    connect(vk, SIGNAL(friends(QVector<sContact>)), this, SLOT(slotFriends(QVector<sContact>)));
    connect(vk, SIGNAL(friendsLists(QList<QString>)), this, SLOT(slotFriendsLists(QList<QString>)));
    connect(vk, SIGNAL(friendsFinished()), this, SLOT(slotFriendsFinished()));
    connect(vk, SIGNAL(friendsFinished()), this, SIGNAL(friendsFinished()));
    connect(vk, SIGNAL(profileSelf(sProfile)), this, SLOT(slotProfileSelf(sProfile)));
    connect(vk, SIGNAL(sendMessagesError(int,QString,QString)), this, SLOT(slotSendMessagesError(int,QString,QString)));
    connect(vk, SIGNAL(captcha(QString,QString)), this, SIGNAL(captcha(QString,QString)));
    connect(vk, SIGNAL(resetMessageFlags(QString,int,QString)), this, SLOT(slotResetMessageFlags(QString,int,QString)));
    connect(vk, SIGNAL(messageReceived(sMessage)), this, SLOT(slotMessageReceived(sMessage)));
    connect(vk, SIGNAL(userOnline(QString)), this, SLOT(slotUserOnline(QString)));
    connect(vk, SIGNAL(userOffline(QString,int)), this, SLOT(slotUserOffline(QString,int)));
    connect(vk, SIGNAL(activity(QList<sActivity>)), this, SLOT(slotActivity(QList<sActivity>)));
    connect(vk, SIGNAL(counters(sCounters)), this, SLOT(slotCounters(sCounters)));
    connect(vk, SIGNAL(history(QString,QList<sMessage>)), this, SLOT(slotHistory(QString,QList<sMessage>)));
    connect(vk, SIGNAL(wallRefresh(QString)), this, SLOT(slotWallGet(QString)));
    connect(vk, SIGNAL(wallPostFinished(QString,bool)), this, SLOT(slotWallPostFinished(QString,bool)));
    connect(vk, SIGNAL(likes(QString,QString,QString,bool)), this, SLOT(slotLike(QString,QString,QString,bool)));

    //окно табов (чаты, аудио итд)
    tabForm = new TabForm();
    tabForm->setGeometry(appSettings::instance()->loadMain("tabForm/geometry", QRect(
            (QApplication::desktop()->width() - 561)/2,
            (QApplication::desktop()->height() - 387)/2,561,387)).toRect());
    connect(tabForm, SIGNAL(messagesSend(QString,QString)), vk, SLOT(apiMessagesSend(QString,QString)));
    connect(tabForm, SIGNAL(messageTrayStart(sMessage)), this, SLOT(slotMessageTrayStart(sMessage)));
    connect(tabForm, SIGNAL(messageTrayStop()), this, SLOT(slotMessageTrayStop()));
    connect(vk, SIGNAL(userOnline(QString)), tabForm, SLOT(slotUserOnline(QString)));
    connect(vk, SIGNAL(userOffline(QString,int)), tabForm, SLOT(slotUserOffline(QString,int)));
    connect(vk, SIGNAL(userTyping(QString,int)), tabForm, SLOT(slotUserTyping(QString,int)));
    connect(tabForm, SIGNAL(messagesGetHistory(QString,int,int)), vk, SLOT(apiMessagesGetHistory(QString,int,int)));
    connect(tabForm, SIGNAL(messagesMark(QString,bool)), vk, SLOT(apiMessagesMark(QString,bool)));
    connect(tabForm, SIGNAL(messagesDelete(QString)), vk, SLOT(apiMessagesDelete(QString)));
    connect(tabForm, SIGNAL(showProfile(QString)),vk,SLOT(apiGetProfiles(QString)));
    connect(tabForm, SIGNAL(wallPost(QString,QString)),vk,SLOT(apiWallPost(QString,QString)));
    connect(tabForm, SIGNAL(messagesSetActivity(QString,QString,QString)),vk,SLOT(apiMessagesSetActivity(QString,QString,QString)));

    //диалоги
    dialogs = new chatDialogs();
    connect(vk, SIGNAL(dialogs(QVector<sDialog>)), this, SLOT(slotDialogs(QVector<sDialog>)));

    //новости
    newsfeed = new Newsfeed();
    connect(vk, SIGNAL(news(QVector<sNew>,QString)), this, SLOT(slotNews(QVector<sNew>,QString)));

    //стена
    wall = new Wall();
    wallUser = new Wall();
    connect(vk, SIGNAL(wall(QVector<sWall>,QString)), this, SLOT(slotWall(QVector<sWall>,QString)));

    //комментарии
    wallComments = new WallComments();
    connect(vk, SIGNAL(wallComments(QVector<sWallComments>)), wallComments, SLOT(slotWallComments(QVector<sWallComments>)));

    //аудиозаписи
    audio = new Audio();
    connect(vk, SIGNAL(audio(QVector<sAudio>)), this, SLOT(slotAudio(QVector<sAudio>)));
    connect(audio, SIGNAL(audioAdd(QString,QString,QString)), vk, SLOT(apiAudioAdd(QString,QString,QString)));

    //фотографии
    photo = new Photo();
    connect(vk, SIGNAL(photos(QVector<sPhoto>)), this, SLOT(slotPhotos(QVector<sPhoto>)));

    //видеозаписи
    video = new Video();
    connect(vk, SIGNAL(video(QVector<sVideo>)), this, SLOT(slotVideo(QVector<sVideo>)));

    //друзья друзей
    rosterUserModel = new contactListModel(this);
    rosterUserModel->setVisibleAvatar(true);
    rosterUserProxy = new QSortFilterProxyModel(this);
    rosterUserProxy->setDynamicSortFilter(true);
    rosterUserProxy->setSortCaseSensitivity(Qt::CaseInsensitive);
    rosterUserProxy->setFilterCaseSensitivity(Qt::CaseInsensitive);
    connect(vk, SIGNAL(friendsUser(QVector<sContact>)), this, SLOT(slotFriendsUser(QVector<sContact>)));
    connect(vk, SIGNAL(friendsMutual(QStringList)), this, SLOT(slotFriendsMutual(QStringList)));

    //форма статуса
    StatusForm = new statusForm();
    connect(StatusForm, SIGNAL(sendStatus(QString)), vk, SLOT(apiActivitySet(QString)));
    connect(StatusForm, SIGNAL(sendWall(QString)), vk, SLOT(apiWallPost(QString)));

    //профиль пользователя
    profile = new profileForm();
    connect(vk, SIGNAL(profile(sProfile)), profile, SLOT(slotProfile(sProfile)));

    //настройки
    settings = new settingsForm();
    connect(settings, SIGNAL(settingsChanged()), this, SLOT(slotSettingsChanged()));
    connect(settings, SIGNAL(settingsChanged()), tabForm, SLOT(slotSettingsChanged()));

    //о программе
    about = new About();

    //Transfer harmattan
    #if defined(MEEGO_EDITION_HARMATTAN)
    clientTransfer = new TUITestClient(this);
    clientTransfer->initTUIClient();
    #endif

    //dbus
    QDBusConnection::sessionBus().registerService("com.rnazarov.titanimpro");

    //устанавливаем интерфейс
    setWindowTitle("TitanIM");
    setWindowIcon(QIcon(":/Icon_47.ico"));
    setMinimumSize(200, 100);
    engine()->setNetworkAccessManagerFactory(new NetworkAccessManagerFactory);
    setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    rootContext()->setContextProperty("titanim", this);
    rootContext()->setContextProperty("rosterModel", proxy);
    rootContext()->setContextProperty("chat", tabForm);
    rootContext()->setContextProperty("profile", profile);
    rootContext()->setContextProperty("chatModel", tabForm->proxy);
    rootContext()->setContextProperty("dialogsModel", dialogs->proxy);
    rootContext()->setContextProperty("newsModel", newsfeed->proxy);
    rootContext()->setContextProperty("wallModel", wall->proxy);
    rootContext()->setContextProperty("wallCommentsModel", wallComments->proxy);
    rootContext()->setContextProperty("audioCpp", audio);
    rootContext()->setContextProperty("audioModel", audio->model);
    rootContext()->setContextProperty("photoModel", photo->model);
    rootContext()->setContextProperty("wallUserModel", wallUser->proxy);
    rootContext()->setContextProperty("videoCpp", video);
    rootContext()->setContextProperty("videoModel", video->model);
    rootContext()->setContextProperty("settings", settings);
    rootContext()->setContextProperty("rosterUserModel", rosterUserProxy);
//    setMainQmlFile(appSettings::instance()->homeAppDir() + "qml/titanim.qml");
    setSource(QUrl("qrc:/qml/titanim.qml"));

    //загружаю настройки программы
    if (!appSettings::instance()->loadMain("main/normWinType", true).toBool())
        setWindowFlags(windowFlags() |= Qt::Tool);
    if (appSettings::instance()->loadMain("main/onTop", false).toBool())
        setWindowFlags(windowFlags() |= Qt::WindowStaysOnTopHint);
    QRect rect = appSettings::instance()->loadMain("mainForm/geometry", QRect()).toRect();
    if (rect.isNull()){
        resize(207, 500);
        showExpanded();
        move(QApplication::desktop()->availableGeometry().width() - frameGeometry().width(),
             QApplication::desktop()->availableGeometry().height() - frameGeometry().height());
    } else setGeometry(rect);
    if (!appSettings::instance()->loadMain("main/hideStart", false).toBool() && isVisible() == false)
        showExpanded();

    //устанавливаю прокси если необходимо
    setProxyApp();

    bearer = new QNetworkConfigurationManager(this);
    connect(bearer, SIGNAL(onlineStateChanged(bool)), SLOT(slotOnlineChanged(bool)));
    #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
        QList<QNetworkConfiguration> activeConfigs = bearer->allConfigurations(QNetworkConfiguration::Active);
        if (activeConfigs.count() && activeConfigs.at(0).bearerType() == QNetworkConfiguration::BearerWLAN)
            countDownloadItems = 20;
        else
            countDownloadItems = 10;
    #else
            countDownloadItems = 20;
    #endif

    if (appSettings::instance()->loadMain("connection/autoConnect", false).toBool())
        setOnline();
}

MainWindow::~MainWindow()
{
    //сохраняю настройки
    if (!mySetting.uid.isEmpty()){
        appSettings::instance()->saveProfile("main/uid", mySetting.uid);
        appSettings::instance()->saveProfile("main/token", mySetting.token);
        appSettings::instance()->saveProfile("contactlist/showActivity", mySetting.showActivity);
        appSettings::instance()->saveProfile("contactlist/showAvatar", mySetting.showAvatar);
        appSettings::instance()->saveProfile("notifications/quietMode", false);
        appSettings::instance()->saveProfile("newsfeed/idReadNews", newsfeed->getId(indexReadNews));
        appSettings::instance()->saveProfile("newsfeed/dateReadNews", newsfeed->getDateTime(indexReadNews + 5));
        appSettings::instance()->saveMain("profiles/last", mySetting.uid);
    }
    appSettings::instance()->saveMain("mainForm/geometry", geometry());

    delete tabForm;
    delete dialogs;
    delete newsfeed;
    delete wall;
    delete wallUser;
    delete wallComments;
    delete audio;
    delete StatusForm;
    delete profile;
    delete settings;
    delete about;
    appSettings::destroy();
}

void MainWindow::closeEvent(QCloseEvent *pe){
    // ==========================================================================
    // скрываем ростер вместо его закрытия
    // ==========================================================================

    #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
      pe->accept();
    #else
    if (tray->isVisible()){
        hide();
        pe->ignore();
    }
    #endif
}

bool MainWindow::showActivity(){
    // ==========================================================================
    // показывать ли расширенный статус пользователя
    // ==========================================================================

    return mySetting.showActivity;
}

int MainWindow::iconSize(){
    // ==========================================================================
    // размер аватара
    // ==========================================================================

      return mySetting.iconSize;
}

int MainWindow::fontPointSize(){
    // ==========================================================================
    // размер шрифта в контакт листе
    // ==========================================================================

    return mySetting.textSize;
}

bool MainWindow::alternatingRowColors(){
    // ==========================================================================
    // рисовать фон альтернативными цветами
    // ==========================================================================

    return mySetting.alternatingRowColors;
}

bool MainWindow::sound(){
    // ==========================================================================
    // вкл/выкл звук
    // ==========================================================================

    return mySetting.sound;
}

bool MainWindow::vibro(){
    // ==========================================================================
    // вкл/выкл вибро
    // ==========================================================================

    return appSettings::instance()->loadProfile("contactlist/vibro",true).toBool();
}

bool MainWindow::autoConnect(){
    // ==========================================================================
    // автоматически подключаться
    // ==========================================================================

    return appSettings::instance()->loadMain("connection/autoConnect", false).toBool();
}

QString MainWindow::profileName(){
    // ==========================================================================
    // имя/фамилия текущего пользователя
    // ==========================================================================

    return appSettings::instance()->profileName();
}

QString MainWindow::profilePhoto(){
    // ==========================================================================
    // фотография текущего пользователя
    // ==========================================================================

    return appSettings::instance()->profilePhoto();
}

QString MainWindow::profileUid(){
    // ==========================================================================
    // uid текущего пользователя
    // ==========================================================================

    return mySetting.uid;
}

bool MainWindow::newsfeedEnabled(){
    // ==========================================================================
    // включен ли канал harmattan
    // ==========================================================================

    return mySetting.newsfeedEnabled;
}

int MainWindow::indexLastNew(){
    // ==========================================================================
    // индекс последней прочитанной новости
    // ==========================================================================

    return newsfeed->getIndex(appSettings::instance()->loadProfile("newsfeed/idReadNews", "").toString());
}

bool MainWindow::saveLastReadNew(){
    // ==========================================================================
    // сохранять индекс последней прочитанной новости
    // ==========================================================================

    return mySetting.saveLastReadNew;
}

bool MainWindow::sortByName(){
    // ==========================================================================
    // сортировать по имени
    // ==========================================================================

    return mySetting.sortByName;
}

void MainWindow::setProxyApp(){
    // ==========================================================================
    // устанавливаем прокси если необходимо
    // ==========================================================================

    QNetworkProxy::ProxyType pType;
    switch(appSettings::instance()->loadMain("proxy/proxyType", 0).toInt()){
    case 1:
        pType = QNetworkProxy::HttpProxy;
        break;
    case 2:
        pType = QNetworkProxy::Socks5Proxy;
        break;
    case 0:
    default:
        pType = QNetworkProxy::NoProxy;
    }

    if ((QNetworkProxy::applicationProxy().type() != pType) ||
        ((pType != QNetworkProxy::NoProxy) &&
         (QNetworkProxy::applicationProxy().hostName() != appSettings::instance()->loadMain("proxy/host", "").toString() ||
          QNetworkProxy::applicationProxy().port() != appSettings::instance()->loadMain("proxy/port", 8888).toInt() ||
          QNetworkProxy::applicationProxy().user() != appSettings::instance()->loadMain("proxy/user", "").toString() ||
          QNetworkProxy::applicationProxy().password() != appSettings::instance()->loadMain("proxy/pass", "").toString()))){
        eStatus temp;
        temp = status;
        setOffline();
        QNetworkProxy appProxy(pType,
                               appSettings::instance()->loadMain("proxy/host", "").toString(),
                               appSettings::instance()->loadMain("proxy/port", 8888).toInt(),
                               appSettings::instance()->loadMain("proxy/user", "").toString(),
                               appSettings::instance()->loadMain("proxy/pass", "").toString());
        QNetworkProxy::setApplicationProxy(appProxy);
        if (temp != offline)
            setOnline();
    }
}

void MainWindow::setTextProgress(const QString &text){
    // ==========================================================================
    // устанавливаем текст уведомления в контакт листе
    // ==========================================================================

    emit progressChanged(text);
}

void MainWindow::slotTrayActivated(const QSystemTrayIcon::ActivationReason &reason){
    // ==========================================================================
    // обработчик кликов на иконке в трее
    // ==========================================================================

    switch (reason) {
    case QSystemTrayIcon::Trigger:
        {
            if (timerMessageTray->isActive()){
                tabForm->openNewMsg();
                break;
            }

            if (!isVisible()){
                setWindowState(windowState() &~ Qt::WindowMinimized);
                showExpanded();
                raise();
                activateWindow();
            } else {
                hide();
            }
            break;
        }
    case QSystemTrayIcon::MiddleClick:
        on_statusButton_clicked();
        break;
    case QSystemTrayIcon::DoubleClick:
    default:
        break;
    }
}

void MainWindow::slotRosterMenu(const int &idMenu, const int &indexModel){
    // ==========================================================================
    // клик по меню контакт листа
    // ==========================================================================

    QModelIndex idx = proxy->index(indexModel, 0);

    if (status != online || !idx.isValid()){
        return;
    }

    switch (idMenu){
    case 0:
        //открыть чат
        slotOpenChatWindow(proxy->data(idx, Qt::UserRole+2).toString());
        break;
    case 1:
        //открыть профиль пользователя
        vk->apiGetProfiles(proxy->data(idx, Qt::UserRole+2).toString());
        break;
    case 2:
        //открыть страницу пользователя в браузере
        QDesktopServices::openUrl(QUrl("http://vk.com/id"+proxy->data(idx, Qt::UserRole+2).toString()));
        break;
    case 3:
        //открыть фотографии пользователя в браузере
        QDesktopServices::openUrl(QUrl("http://vk.com/albums"+proxy->data(idx, Qt::UserRole+2).toString()));
        break;
    case 4:
        //открыть музыку пользователя в браузере
        QDesktopServices::openUrl(QUrl("http://vk.com/audio.php?id="+proxy->data(idx, Qt::UserRole+2).toString()));
        break;
    case 5:
        //открыть видео пользователя в браузере
        QDesktopServices::openUrl(QUrl("http://vk.com/video.php?id="+proxy->data(idx, Qt::UserRole+2).toString()));
        break;
    default:
        break;
    }
}

void MainWindow::slotPressedKey(const QString &filter){
    // ==========================================================================
    // Нажата клавиша в главном окне
    // ==========================================================================

    if (status == online){
        if (filter.isEmpty()){
            mySetting.filterText.clear();
            setFilterSort(mySetting.filterSort);
            return;
        }

        if (mySetting.filterText.isEmpty()){
            mySetting.filterList = 0;
            setFilterSort(1);
        }

        mySetting.filterText = filter;
        proxy->setFilterWildcard(mySetting.filterText);
    }
}

void MainWindow::slotPressedKeyRosterUser(const QString &filter){
    // ==========================================================================
    // Нажата клавиша в окне друзья друзей
    // ==========================================================================

    if (status == online){
        rosterUserProxy->setFilterWildcard(filter);
    }
}

void MainWindow::slotSettingsChanged(){
    // ==========================================================================
    // изменились настройки
    // ==========================================================================

    if (status == online){
        //загружаю настройки
        mySetting.alternatingRowColors = appSettings::instance()->loadProfile("contactlist/alternatingrowcolors",true).toBool();
        emit alternatingRowColorsChanged(mySetting.alternatingRowColors);

        mySetting.filterSort = appSettings::instance()->loadProfile("contactlist/filterSort",10).toInt();
        setFilterSort(mySetting.filterSort);

        #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
          bool showActivity = appSettings::instance()->loadProfile("contactlist/showActivity",false).toBool();
        #else
          bool showActivity = appSettings::instance()->loadProfile("contactlist/showActivity",true).toBool();
        #endif
        if (showActivity != mySetting.showActivity){
            mySetting.showActivity = showActivity;
            if (mySetting.showActivity)
                vk->apiActivityGet(model->getAllUidOnline().join(","));
            emit showActivityChanged(showActivity);
        }

        mySetting.showAvatar = appSettings::instance()->loadProfile("contactlist/showAvatar",true).toBool();
        model->setVisibleAvatar(mySetting.showAvatar);
        if (!model->isVisibleAvatar()){
            mySetting.iconSize = 16;
        } else {
            #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
              mySetting.iconSize = appSettings::instance()->loadProfile("contactlist/sizeAvatar",70).toInt();
            #else
              mySetting.iconSize = appSettings::instance()->loadProfile("contactlist/sizeAvatar",30).toInt();
            #endif
        }
        emit iconSizeChanged(mySetting.iconSize);

        #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
          mySetting.textSize = appSettings::instance()->loadProfile("contactlist/sizeText",16).toInt();
        #else
          mySetting.textSize = appSettings::instance()->loadProfile("contactlist/sizeText",8).toInt();
        #endif
        emit fontPointSizeChanged(mySetting.textSize);
        mySetting.newsfeedEnabled = appSettings::instance()->loadProfile("newsfeed/enabled", true).toBool();
        emit newsfeedEnabledChanged(mySetting.newsfeedEnabled);

        mySetting.saveLastReadNew = appSettings::instance()->loadProfile("newsfeed/saveLastReadNews", false).toBool();
        emit saveLastReadNewChanged(mySetting.saveLastReadNew);

        mySetting.sortByName = appSettings::instance()->loadProfile("contactlist/sortByName", false).toBool();
        emit sortByNameChanged(mySetting.sortByName);
        if (mySetting.sortByName){
            setFilterSort(8);
        } else {
            setFilterSort(4);
        }
    }

    //поверх всех окон
    if (appSettings::instance()->loadMain("main/onTop", false).toBool() != bool(windowFlags() & Qt::WindowStaysOnTopHint)){
        QRect rect = geometry();
        if (windowFlags() & Qt::WindowStaysOnTopHint)
            setWindowFlags(windowFlags() &=~ Qt::WindowStaysOnTopHint);
        else
            setWindowFlags(windowFlags() |= Qt::WindowStaysOnTopHint);
        setGeometry(rect);
        showExpanded();
    }

    //обычный заголовок окна
    if (appSettings::instance()->loadMain("main/normWinType", true).toBool() == bool((windowFlags() & Qt::Tool) - Qt::Window)){
        QRect rect = geometry();
        if ((windowFlags() & Qt::Tool) - Qt::Window)
            setWindowFlags(windowFlags() &=~ Qt::Tool);
        else
            setWindowFlags(windowFlags() |= Qt::Tool);
        setGeometry(rect);
        showExpanded();
    }

    //устанавливаю прокси если необходимо
    setProxyApp();
}

void MainWindow::setFilterSort(int setting){
    // ==========================================================================
    // +1  статус: показывать всех
    // +2  статус: только онлайн
    // +4  сортировка: не сортировать
    // +8  сортировка: по имени
    // +16 сортировка: по статусу
    // +32 диалоги
    // ==========================================================================

    if (setting == 0){
        setting = mySetting.filterSort;
        mySetting.filterList = 0;
    }

    if (setting & 1){
        if (mySetting.filterList == 0){
            proxy->setFilterRole(Qt::DisplayRole);
            proxy->setFilterWildcard("");
        } else {
            proxy->setFilterRole(Qt::UserRole+3);
            proxy->setFilterWildcard("*,"+QString::number(mySetting.filterList)+",*");
        }
    }

    if (setting & 2){
        if (mySetting.filterList == 0){
            proxy->setFilterRole(Qt::UserRole);
            proxy->setFilterWildcard("1");
        } else {
            proxy->setFilterRole(Qt::UserRole+3);
            proxy->setFilterWildcard("*,"+QString::number(mySetting.filterList)+",* 1");
        }
    }

    if (setting & 4){
        proxy->setSortRole(Qt::DisplayRole);
        proxy->sort(1);
    }

    if ((mySetting.sortByName) && (setting & 8)){
        proxy->setSortRole(Qt::DisplayRole);
        proxy->sort(0);
    }

    if (setting & 16){
        proxy->setSortRole(Qt::UserRole);
        proxy->sort(0, Qt::DescendingOrder);
    }

    if (setting & 32){
        proxy->setSortRole(Qt::DisplayRole);
        proxy->sort(0);
        proxy->setFilterRole(Qt::UserRole+2);
        proxy->setFilterRegExp(mySetting.filterUids);
    }
}

void MainWindow::setOnline(const QString &username, const QString &password){
    // ==========================================================================
    // подключаемся
    // ==========================================================================

    if (status == offline){
        setTextProgress(tr("Downloading settings"));
        status = connecting;
        //загружаю настройки
        appSettings::instance()->setUid(appSettings::instance()->uids().split(",").at(0));
        mySetting.uid = appSettings::instance()->loadProfile("main/uid","0").toString();
        mySetting.token = appSettings::instance()->loadProfile("main/token","").toString();
        mySetting.sound = appSettings::instance()->loadProfile("contactlist/sound", true).toBool();
        emit soundChanged(mySetting.sound);
        emit vibroChanged(appSettings::instance()->loadProfile("contactlist/vibro", true).toBool());
        mySetting.filterSort = appSettings::instance()->loadProfile("contactlist/filterSort",10).toInt();
        mySetting.sortByName = appSettings::instance()->loadProfile("contactlist/sortByName", false).toBool();
        emit sortByNameChanged(mySetting.sortByName);
        #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
          mySetting.showActivity = appSettings::instance()->loadProfile("contactlist/showActivity",false).toBool();
        #else
          mySetting.showActivity = appSettings::instance()->loadProfile("contactlist/showActivity",true).toBool();
        #endif
        emit showActivityChanged(mySetting.showActivity);

        mySetting.showAvatar = appSettings::instance()->loadProfile("contactlist/showAvatar",true).toBool();
        mySetting.alternatingRowColors = appSettings::instance()->loadProfile("contactlist/alternatingrowcolors",true).toBool();
        emit alternatingRowColorsChanged(mySetting.alternatingRowColors);

        //подключаемся к серверу ВК
        setTextProgress(tr("Connecting..."));
        vk->connectToVk(mySetting.uid, mySetting.token, username, password);
    }
}

void MainWindow::setOffline(){
    // ==========================================================================
    // отключаемся
    // ==========================================================================

    if (status != offline){
        //оффлайн
        setTextProgress("");
        slotMessageTrayStop();
        tabForm->hide();
        tabForm->closeAllChat();
//        ui->listsButton->setCheckable(false);
        vk->disconnectVk();
        if (proxy){
            proxy->clear();
            delete proxy;
            proxy = 0;
        }
        if (model){
            delete model;
            model = 0;
        }

        #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
          if (dialogs->model){
              dialogs->model->removeDialog(0, dialogs->model->rowCount());
          }

          if (newsfeed->model){
              appSettings::instance()->saveProfile("newsfeed/idReadNews", newsfeed->getId(indexReadNews));
              appSettings::instance()->saveProfile("newsfeed/dateReadNews", newsfeed->getDateTime(indexReadNews + 5));
              newsfeed->model->removeNew(0, newsfeed->model->rowCount());
          }

          if (wall->model){
              wall->model->removeWall(0, wall->model->rowCount());
          }

          if (audio->model){
              audio->model->removeAudio(0, audio->model->rowCount());
          }
        #endif

        rootContext()->setContextProperty("rosterModel", proxy);
        appSettings::instance()->setProfileName("");
        appSettings::instance()->setProfileShortName("");
        appSettings::instance()->setProfilePhoto("");
        status = offline;
        emit statusChanged(false);
        tray->setIcon(QIcon(":/Icon_48.png"));

        emit showAuthForm();
    }
}

void MainWindow::showPopupWindow(const QString &uid, const QString &text){
    // ==========================================================================
    // всплывающее уведомление
    // ==========================================================================

    #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
      if (uid.isEmpty())
        emit showNotification(text);
      return;
    #endif

    //всплывающее уведомление (память освобождатся сама)
    if (!appSettings::instance()->loadProfile("notifications/quietMode",false).toBool()){
        positionInStack++;
        popupWindow *popup = new popupWindow(appSettings::instance()->loadProfile("notifications/time",5).toInt(),
                                             appSettings::instance()->loadProfile("notifications/position",3).toInt(),
                                             appSettings::instance()->loadProfile("notifications/style",2).toInt(),
                                             positionInStack);
        if (!uid.isEmpty())
            popup->setTextWindow(model->getContact(uid), text);
        else
            popup->setTextWindow(text);
        connect(popup, SIGNAL(closePopupWindow()), this, SLOT(slotClosePopupWindow()));
        connect(popup, SIGNAL(openChatWindow(QString)), this, SLOT(slotOpenChatWindow(QString)));
        popup->showTrayMessage();
    }
}

void MainWindow::on_statusCBox_currentIndexChanged(const int &index){
    // ==========================================================================
    // пользователь изменил статус (online/offline)
    // ==========================================================================

    switch(index){
    case 0:
        setOnline();
        break;
    case 1:
        setOffline();
        break;
    }
}

void MainWindow::slotErrorString(const eError error, const QString &text, const bool &fatal){
    // ==========================================================================
    // возникла ошибка при работе с API ВКонтакте
    // fatal - если ошибка фатальная и нужно сбросить статус в оффлайн
    // ==========================================================================

    if (fatal){
        //отключаемся
        setOffline();

        //если ВК отклонил авторизацию, удаляем токен и показываем форму авторизации
        if (error == userAuthorizationFailed){
            slotDeletePassword();
        }

        //пробуем подключиться повторно
        if ((error == timeoutLongPollServer || error == incorrectSignature || error == unknownMethodPassed) &&
            (appSettings::instance()->loadMain("connection/reconnect", true).toBool())){
            if (!timerReconnect->isActive()){
                setOnline();
                timerReconnect->start(15000);
            } else return;
        }
    }

    //всплывающее уведомление
    showPopupWindow("", text);
}

void MainWindow::slotOnlineChanged(bool set){
    // ==========================================================================
    // изменилось состояние подключения
    // ==========================================================================

    #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
        QList<QNetworkConfiguration> activeConfigs = bearer->allConfigurations(QNetworkConfiguration::Active);
        if (activeConfigs.count() && activeConfigs.at(0).bearerType() == QNetworkConfiguration::BearerWLAN)
            countDownloadItems = 20;
        else
            countDownloadItems = 10;
    #else
            countDownloadItems = 20;
    #endif

    if (appSettings::instance()->loadMain("connection/reconnect", true).toBool() && set){
        setOnline();
    } else {
        setOffline();
        showPopupWindow("", tr("Server is not available"));
    }
}

void MainWindow::slotConnected(const QString &token, const QString &uid){
    // ==========================================================================
    // успешно подключились к серверу
    // ==========================================================================

    if (!uid.isEmpty()){
        appSettings::instance()->setUid(uid);
        appSettings::instance()->creatDir();
        mySetting.uid = uid;
        mySetting.token = token;
        appSettings::instance()->saveProfile("main/uid", mySetting.uid);
        appSettings::instance()->saveProfile("main/token", mySetting.token);
        appSettings::instance()->saveMain("profiles/last", mySetting.uid);
    }

    //обнуляем
    counters.friends = 0;
    counters.events = 0;
    counters.gifts = 0;
    counters.groups = 0;
    counters.notes = 0;
    counters.offers = 0;
    counters.opinions = 0;
    counters.photos = 0;
    counters.questions = 0;

    timerReconnect->stop();
    emit statusChanged(true);
    tray->setIcon(QIcon(":/Icon_47.ico"));
    setTextProgress(tr("Receiving Contact List"));

    //модель
    model = new contactListModel(this);
    model->setVisibleAvatar(mySetting.showAvatar);
    proxy = new QSortFilterProxyModel(this);
    proxy->setDynamicSortFilter(true);
    proxy->setSourceModel(model);
    proxy->setSortCaseSensitivity(Qt::CaseInsensitive);
    proxy->setFilterCaseSensitivity(Qt::CaseInsensitive);

    //фильтр и сортировка
    mySetting.filterList = 0;
    setFilterSort(mySetting.filterSort);

    //размер аватара в контакт листе
    if (!model->isVisibleAvatar()){
        mySetting.iconSize = 16;
    } else {
        #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
          mySetting.iconSize = appSettings::instance()->loadProfile("contactlist/sizeAvatar",70).toInt();
        #else
          mySetting.iconSize = appSettings::instance()->loadProfile("contactlist/sizeAvatar",30).toInt();
        #endif
    }
    emit iconSizeChanged(mySetting.iconSize);

    #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
      mySetting.textSize = appSettings::instance()->loadProfile("contactlist/sizeText",16).toInt();
    #else
      mySetting.textSize = appSettings::instance()->loadProfile("contactlist/sizeText",8).toInt();
    #endif
    emit fontPointSizeChanged(mySetting.textSize);
    mySetting.newsfeedEnabled = appSettings::instance()->loadProfile("newsfeed/enabled", true).toBool();
    emit newsfeedEnabledChanged(mySetting.newsfeedEnabled);

    mySetting.saveLastReadNew = appSettings::instance()->loadProfile("newsfeed/saveLastReadNews", false).toBool();
    emit saveLastReadNewChanged(mySetting.saveLastReadNew);

    //установка модели
    rootContext()->setContextProperty("rosterModel", proxy);

    tabForm->slotSettingsChanged();

    uidsOnline.clear();

    #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
      //получаю новости
      if (mySetting.saveLastReadNew){
        QDateTime startDateTime;
        startDateTime = appSettings::instance()->loadProfile("newsfeed/dateReadNews", "").toDateTime();
        vk->apiNewsfeedGet("", "post,photo,photo_tag", QString::number(startDateTime.toTime_t()), "", 100);
      } else {
          vk->apiNewsfeedGet("", "post,photo,photo_tag", "", "", countDownloadItems);
      }

      //устанавливаем автоподключение
      appSettings::instance()->saveMain("connection/autoConnect", true);
    #endif
}

void MainWindow::slotFriends(const QVector<sContact> &contacts){
    // ==========================================================================
    // получили контакт лист
    // ==========================================================================

    //добавляем контакты в модель
    model->appendContacts(contacts);

    setTextProgress("");

    //готовлю данные для получения расширенных статусов онлайн друзей
    for (int i=0; i < contacts.count(); i++)
        if (contacts[i].status == "1") uidsOnline.append(contacts[i].uid);

    //устанавливаю статус в сети
    status = online;
}

void MainWindow::slotFriendsLists(const QList<QString> &lists){
    // ==========================================================================
    // получили списки друзей
    // ==========================================================================

    //меню списка друзей
//    listMenu = new QMenu(ui->listsButton);
//    listMenu->addAction(tr("All friends"), this, SLOT(setFilterSort()));
//    listMenu->addAction(tr("Dialogs"), vk, SLOT(apiMessagesGetDialogs()));
//    if (lists.count() > 0){
//        listMenu->addSeparator();
//        QSignalMapper *signalMapper = new QSignalMapper(listMenu);
//        for(int i = 0; i < lists.count(); i++){
//            QAction *action = new QAction(lists.at(i).split(",").at(1), listMenu);
//            action->setData(lists.at(i).split(",").at(0));
//            connect(action, SIGNAL(triggered()), signalMapper, SLOT(map()));
//            signalMapper->setMapping(action, action);
//            listMenu->addAction(action);
//        }
//        connect(signalMapper, SIGNAL(mapped(QObject*)), this, SLOT(slotListMenuTriggered(QObject*)));
//    }
//    ui->listsButton->setMenu(listMenu);
//    ui->listsButton->setCheckable(true);
}

void MainWindow::slotFriendsFinished(){
    // ==========================================================================
    // получили всех друзей
    // ==========================================================================

    //загружаем аватарки с сервера
    //model->getPhoto();

    //получаем расширенные статусы онлайн друзей
    #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
      if (mySetting.showActivity)
        vk->apiActivityGet(uidsOnline.join(","));
    #else
      vk->apiActivityGet(uidsOnline.join(","));
    #endif

    //получаем оффлайн сообщения
    vk->apiNewMessagesGet();

    //получаю статус текущего пользователя
    vk->apiActivityGet(mySetting.uid);

    setTextProgress("");
}

void MainWindow::slotProfileSelf(const sProfile &profile){
    // ==========================================================================
    // получили профиль текущего пользователя
    // ==========================================================================

    if (appSettings::instance()->profileName().isEmpty()){
        appSettings::instance()->saveProfile("main/name", profile.firstName + " " + profile.lastName);
        appSettings::instance()->setProfileName(profile.firstName + " " + profile.lastName);
        appSettings::instance()->setProfileShortName(profile.firstName);
        appSettings::instance()->saveProfile("main/photo", profile.photoRectUrl.toString());
        appSettings::instance()->setProfilePhoto(profile.photoRectUrl.toString());
        StatusForm->setPhoto(profile.photoRectUrl);

        emit profileNameChanged(profile.firstName + " " + profile.lastName);
        emit profilePhotoChanged(profile.photoRectUrl.toString());
        emit profileUidChanged(profile.uid);
    } else {
        this->profile->slotProfile(profile);
    }
}

void MainWindow::slotSendMessagesError(const int &error, const QString &uid, const QString &text){
    // ==========================================================================
    // ошибка при отправки сообщения
    // ==========================================================================

    //передаем в чат
    tabForm->sendMessagesError(error, uid, text);

    //всплывающее уведомление
    if (error == 7)
        showPopupWindow("", QString("<font color=\"red\">%1</font>").arg(tr("Privacy error")));
}

void MainWindow::slotListMenuTriggered(QObject *obj){
    // ==========================================================================
    // отображаем в ростере нужный список
    // ==========================================================================

    QAction *action = qobject_cast<QAction*>(obj);
    if(action){
        mySetting.filterList = action->data().toInt();
        setFilterSort(mySetting.filterSort);
    }
}

void MainWindow::slotResetMessageFlags(const QString &mid, const int &mask, const QString &uid){
    // ==========================================================================
    // сброс флагов сообщения
    // ==========================================================================

    //передаем в чат
    tabForm->resetMessageFlags(mid, mask, uid);
}

void MainWindow::slotMessageReceived(const sMessage &message){
    // ==========================================================================
    // пришло новое сообщение
    // ==========================================================================

    //передаем в модель
    //model->messageReceived(message.fromId);

    //передаем в чат
    tabForm->messageReceived(message, model->getContact(message.fromId));

    //всплывающее уведомление TODO
    if (appSettings::instance()->loadProfile("notifications/message", true).toBool() && !(message.flags & 2)){
        emit messageReceived(message.fromId, message.text);
    }

    //обновляю диалоги (да простят меня за трафик)
    #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
      slotMessagesGetDialogs();
    #endif
}

void MainWindow::slotMessageTrayStart(const sMessage &message){
    // ==========================================================================
    // включаем отображение в трее о новом сообщении
    // ==========================================================================

    if (!timerMessageTray->isActive()){
        tray->setIcon(QIcon(":/mail.png"));
        timerMessageTray->start();
    }

    //всплывающее уведомление
    if (appSettings::instance()->loadProfile("notifications/message", true).toBool()){
        showPopupWindow(message.fromId, message.text);
    }
}

void MainWindow::slotMessageTrayStop(){
    // ==========================================================================
    // отключаем отображение в трее о новом сообщении
    // ==========================================================================

    tray->setIcon(QIcon(":/Icon_47.ico"));
    timerMessageTray->stop();
}

void MainWindow::slotTimerMessageTray(){
    // ==========================================================================
    // таймер мигания
    // ==========================================================================

    static bool showMsg;

    if (showMsg) tray->setIcon(QIcon(":/mail.png"));
    else tray->setIcon(QIcon());

    showMsg = !showMsg;
}

void MainWindow::slotTimerReconnect(){
    // ==========================================================================
    // таймер для автоподключения после обрыва
    // ==========================================================================

    setOnline();
}

void MainWindow::slotUserOnline(const QString &uid){
    // ==========================================================================
    // друг стал онлайн
    // ==========================================================================

    //отправляем информацию в модель
    model->setUserOnline(uid);
    if (mySetting.showActivity)
        vk->apiActivityGet(uid);

    //всплывающее уведомление
    if (appSettings::instance()->loadProfile("notifications/online", true).toBool())
        showPopupWindow(uid, tr("Online"));
}

void MainWindow::slotUserOffline(const QString &uid, const int &flag){
    // ==========================================================================
    // друг стал оффлайн
    // ==========================================================================

    //отправляем информацию в модель
    model->setUserOffline(uid, flag);

    //всплывающее уведомление
    if (appSettings::instance()->loadProfile("notifications/offline", true).toBool())
        showPopupWindow(uid, tr("Offline"));
}

void MainWindow::slotActivity(const QList<sActivity> &activity){
    // ==========================================================================
    // пришли расширенные статусы
    // устанавливаем их в контакт лист
    // ==========================================================================

    if (activity.isEmpty())
        return;

    if (activity.at(0).uid == mySetting.uid){
        StatusForm->setStatus(activity.at(0).activity);
        return;
    }

    //отправляем информацию в чат
    tabForm->setActivity(activity);

    //отправляем информацию в модель
    model->setActivity(activity);
}

void MainWindow::slotCounters(const sCounters &countersNew){
    // ==========================================================================
    // пришли события с сайта
    // ==========================================================================

    if (!appSettings::instance()->loadProfile("events/eventsVK",true).toBool()){
        return;
    }

    //друзья
    if (appSettings::instance()->loadProfile("events/friends",true).toBool()){
        if (counters.friends < countersNew.friends){
            showPopupWindow("", tr("My Friends (%1)").arg(countersNew.friends));
        }
        counters.friends = countersNew.friends;
    } else counters.friends = 0;

    //фотографии
    if (!appSettings::instance()->loadProfile("events/photos",true).toBool()){
        if (counters.photos < countersNew.photos){
            showPopupWindow("", tr("My Photos (%1)").arg(countersNew.photos));
        }
        counters.photos = countersNew.photos;
    } else counters.photos = 0;

    //видео
    if (!appSettings::instance()->loadProfile("events/videos",true).toBool()){
        if (counters.videos < countersNew.videos){
            showPopupWindow("", tr("My Videos (%1)").arg(countersNew.videos));
        }
        counters.videos = countersNew.videos;
    } else counters.videos = 0;

    //заметки
    if (!appSettings::instance()->loadProfile("events/notes",true).toBool()){
        if (counters.notes < countersNew.notes){
            showPopupWindow("", tr("My Notes (%1)").arg(countersNew.notes));
        }
        counters.notes = countersNew.notes;
    } else counters.notes = 0;

    //подарки
    if (!appSettings::instance()->loadProfile("events/gifts",true).toBool()){
        if (counters.gifts < countersNew.gifts){
            showPopupWindow("", tr("My Gifts (%1)").arg(countersNew.gifts));
        }
        counters.gifts = countersNew.gifts;
    } else counters.gifts = 0;

    //группы
    if (!appSettings::instance()->loadProfile("events/groups",true).toBool()){
        if (counters.groups < countersNew.groups){
            showPopupWindow("", tr("My Groups (%1)").arg(countersNew.groups));
        }
        counters.groups = countersNew.groups;
    } else counters.groups = 0;
}

void MainWindow::slotHistory(const QString &uid, const QList<sMessage> &messages){
    // ==========================================================================
    // пришла история
    // ==========================================================================

    //передаем в чат
    tabForm->history(messages, model->getContact(uid));
}

void MainWindow::slotDialogs(const QVector<sDialog> &dialogs){
    // ==========================================================================
    // пришли диалоги
    // ==========================================================================

    if (!dialogOffset)
        this->dialogs->replaceDialogs(dialogs);
    else
        this->dialogs->appendDialogs(dialogs);

    dialogOffset += dialogs.count();
    emit dialogsFinished();
}

void MainWindow::slotNews(const QVector<sNew> &news, const QString &newFrom){
    // ==========================================================================
    // пришли новости
    // ==========================================================================

    if (newFrom == "0"){
        emit newsFinished();
        return;
    }

    if (newsfeedFrom.isEmpty())
        this->newsfeed->replaceNews(news);
    else
        this->newsfeed->appendNews(news);

    newsfeedFrom = newFrom;
    emit newsFinished();

    #if defined(MEEGO_EDITION_HARMATTAN)
    if (appSettings::instance()->loadProfile("newsfeed/lastUpdate","none").toString() == "none"){
        QDBusMessage message = QDBusMessage::createMethodCall("com.meego.msyncd", "/synchronizer", "com.meego.msyncd", "startSync");
        QList<QVariant> args;
        args << "titanimpro";
        message.setArguments(args);
        QDBusConnection bus = QDBusConnection::sessionBus();
        if (bus.isConnected()){
            bus.send(message);
        }
    }
    #endif
}

void MainWindow::slotWall(const QVector<sWall> &wall, const QString &ownerId){
    // ==========================================================================
    // пришли записи со стены
    // ==========================================================================

    if (ownerId == mySetting.uid){
        if (!wallOffset)
            this->wall->replaceWall(wall);
        else
            this->wall->appendWall(wall);
        emit wallFinished();
    }

    if (!wallOffset)
        wallUser->replaceWall(wall);
    else
        wallUser->appendWall(wall);
    emit wallUserFinished();

    wallOffset += wall.count();
}

void MainWindow::slotAudio(const QVector<sAudio> &audio){
    // ==========================================================================
    // пришли аудиозаписи
    // ==========================================================================

    if (!audioOffset)
        this->audio->replaceAudio(audio);
    else
        this->audio->appendAudio(audio);

    audioOffset += audio.count();
    emit audioFinished();
}

void MainWindow::slotPhotos(const QVector<sPhoto> &photos){
    // ==========================================================================
    // пришли фотографии
    // ==========================================================================

    if (!photoOffset)
        this->photo->replacePhoto(photos);
    else
        this->photo->appendPhoto(photos);

    photoOffset += photos.count();
    emit photoFinished();
}

void MainWindow::slotClosePopupWindow(){
    // ==========================================================================
    // закрылось окно всплывающего уведомления
    // ==========================================================================

    positionInStackD++;
    if (positionInStackD == positionInStack){
        positionInStack = 0;
        positionInStackD = 0;
    }
}

void MainWindow::slotOpenChatWindow(const QString &uid){
    // ==========================================================================
    // открываем чат с указанным uid
    // ==========================================================================

    if (!uid.isEmpty()){
        tabForm->openChat(model->getContact(uid));
    } else {
        setWindowState(windowState() &~ Qt::WindowMinimized);
        showExpanded();
        raise();
        activateWindow();
    }
}

void MainWindow::slotOpenChatWindow(const int &indexModel){
    // ==========================================================================
    // открываем чат с указанным индексом
    // ==========================================================================

    QModelIndex idx = proxy->index(indexModel, 0);
    slotOpenChatWindow(proxy->data(idx, model->uidRole).toString());
}

void MainWindow::slotShowProfile(const QString &uid){
    // ==========================================================================
    // запрашиваем профиль пользователя
    // ==========================================================================

    if (!uid.isEmpty()){
        vk->apiGetProfiles(uid);
        photoOffset = 0;
        vk->apiPhotosGetAll(uid, 0, 4);
    }
}

void MainWindow::slotMainMenu(const int &idMenu){
    // ==========================================================================
    // клик по главному меню
    // ==========================================================================

    switch (idMenu){
    case 0:
        //настройки
        on_settingsButton_clicked();
        break;
    case 1:
        //очистить кеш фотографий
        slotDeleteCacheDir();
        break;
    case 2:
        //забыть пароль
        slotDeletePassword();
        break;
    case 3:
        //сменить профиль
        slotChangeProfile();
        break;
    case 4:
        //о программе
        on_aboutButton_clicked();
        break;
    case 5:
        //выход
        QApplication::quit();
        break;
    default:
        break;
    }
}

bool MainWindow::slotDeleteCacheDir(){
    // ==========================================================================
    // удаляем фотографии в каталоге кеша
    // ==========================================================================

    #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
        showPopupWindow("", tr("Cache clear: %1 MB").
                    arg(engine()->networkAccessManager()->cache()->cacheSize() / 1024 / 1024));
        engine()->networkAccessManager()->cache()->clear();
    #else
        if (status == online){
            QDir dir(appSettings::instance()->cacheDir());
            QStringList fileList = dir.entryList(QDir::Files);
            QStringList::ConstIterator it;
            for (it = fileList.begin(); it != fileList.end(); ++it){
                if (!dir.remove(*it))
                    return false;
            }
            return true;
        } else return false;
    #endif
}

void MainWindow::slotDeletePassword(){
    // ==========================================================================
    // удалить пароль с профиля
    // ==========================================================================

    if (mySetting.uid.isEmpty())
        return;

    setOffline();

    appSettings::instance()->saveMain("profiles/last", mySetting.uid);
    appSettings::instance()->saveMain("connection/autoConnect", false);
    mySetting.uid.clear();
    mySetting.token.clear();
    appSettings::instance()->saveProfile("main/uid", mySetting.uid);
    appSettings::instance()->saveProfile("main/token", mySetting.token);
}

void MainWindow::slotQuietMode(){
    // ==========================================================================
    // тихий режим (нет звука, нет всплывающих окон)
    // ==========================================================================

    if (status != online){
        trayMenu->actions().at(3)->setChecked(false);
        return;
    }

    if (trayMenu->actions().at(3)->isChecked())
        appSettings::instance()->saveProfile("notifications/quietMode", true);
    else
        appSettings::instance()->saveProfile("notifications/quietMode", false);
}

void MainWindow::slotChangeProfile(){
    // ==========================================================================
    // сменить профиль
    // ==========================================================================

    slotDeletePassword();
}

void MainWindow::slotLike(const QString &ownerId, const QString &itemId, const QString &countLikes, bool isAdd){
    // ==========================================================================
    // пришли лайки
    // ==========================================================================

    //передаем в модель
    newsfeed->model->setLikesCount(ownerId, itemId, countLikes, isAdd);
    wall->model->setLikesCount(ownerId, itemId, countLikes, isAdd);
    wallUser->model->setLikesCount(ownerId, itemId, countLikes, isAdd);

    //передаем в GUI
    emit likes(ownerId, itemId, countLikes, isAdd);
}

void MainWindow::slotVideo(const QVector<sVideo> &video){
    // ==========================================================================
    // пришли видеозаписи
    // ==========================================================================

    if (!videoOffset)
        this->video->replaceVideos(video);
    else
        this->video->appendVideos(video);

    videoOffset += video.count();
    emit videoFinished();
}

void MainWindow::slotFriendsUser(const QVector<sContact> &contacts){
    // ==========================================================================
    // пришли друзья друга
    // ==========================================================================

    rosterUserModel->replaceContacts(contacts);
    emit friendsUserFinished();
}

void MainWindow::slotFriendsMutual(const QStringList &uids){
    // ==========================================================================
    // пришли uids общих друзей
    // ==========================================================================

    rosterUserProxy->setFilterRegExp(uids.join("|"));
    emit friendsUserFinished();
}

void MainWindow::slotWallPostFinished(const QString &uid, const bool &ok){
    // ==========================================================================
    // пришел результат отправки сообщения на стену
    // ==========================================================================

    #if defined(MEEGO_EDITION_HARMATTAN)
    clientTransfer->hideUploadTransfer();
    #endif
}

void MainWindow::on_offlineButton_clicked(const int &metod){
    // ==========================================================================
    // клик по кнопке показать оффлайн контакты
    // ==========================================================================

    if (status != online){
        return;
    }

    if ((mySetting.filterSort & 1 && metod == 0) || (metod == 1)){
        setFilterSort(10);
        mySetting.filterSort = 10;
        appSettings::instance()->saveProfile("contactlist/offlineButton", false);
    } else {
        setFilterSort(9);
        mySetting.filterSort = 9;
        appSettings::instance()->saveProfile("contactlist/offlineButton", true);
    }

    #if !defined(Q_WS_MAEMO_5) && !defined(MEEGO_EDITION_HARMATTAN)
      appSettings::instance()->saveProfile("contactlist/filterSort", mySetting.filterSort);
    #endif
}

void MainWindow::setSound(const bool &on)
{
    // ==========================================================================
    // клик по кнопке вкл/выкл звук
    // ==========================================================================

    mySetting.sound = on;
    emit soundChanged(mySetting.sound);
    appSettings::instance()->saveProfile("contactlist/sound", mySetting.sound);
}

void MainWindow::on_statusButton_clicked()
{
    // ==========================================================================
    // клик по кнопке показать форму статуса
    // ==========================================================================

    if(status == online){
        vk->apiActivityGet(mySetting.uid);
        StatusForm->show();
    }
}

void MainWindow::slotCaptchaDone(const QString &captcha_sid, const QString &captcha_key){
    // ==========================================================================
    // пользователь ввел капчу
    // ==========================================================================

    vk->captchaDone(captcha_sid, captcha_key);
}

void MainWindow::on_settingsButton_clicked(){
    // ==========================================================================
    // клик по кнопке показать настройки
    // ==========================================================================

    settings->show();
    settings->resize(520,240);
}

void MainWindow::on_aboutButton_clicked(){
    // ==========================================================================
    // клик по кнопке показать о программе
    // ==========================================================================

    about->show();
}

void MainWindow::on_roster_doubleClicked(const QModelIndex &index){
    // ==========================================================================
    // двойной клик по контакту
    // ==========================================================================

    if (index.isValid())
        slotOpenChatWindow(index.data(Qt::UserRole+2).toString());
}

void MainWindow::on_homeButton_clicked(){
    // ==========================================================================
    // открываем страницу пользователя в браузере
    // ==========================================================================

    if(status == online)
        QDesktopServices::openUrl(QUrl("http://vk.com/id"+mySetting.uid));
}

void MainWindow::on_friendsButton_clicked(){
    // ==========================================================================
    // открываем страницу пользователя в браузере
    // ==========================================================================

    QDesktopServices::openUrl(QUrl("http://vk.com/friends.php?filter=requests"));
}

void MainWindow::on_photosButton_clicked(){
    // ==========================================================================
    // открываем страницу в браузере
    // ==========================================================================

    QDesktopServices::openUrl(QUrl("http://vk.com/photos.php?act=added"));
}

void MainWindow::on_videosButton_clicked(){
    // ==========================================================================
    // открываем страницу в браузере
    // ==========================================================================

    QDesktopServices::openUrl(QUrl("http://vk.com/video.php?act=tagview"));
}

void MainWindow::on_notesButton_clicked(){
    // ==========================================================================
    // открываем страницу в браузере
    // ==========================================================================

    QDesktopServices::openUrl(QUrl("http://vk.com/notes.php?act=comms"));
}

void MainWindow::on_giftsButton_clicked(){
    // ==========================================================================
    // открываем страницу в браузере
    // ==========================================================================

    QDesktopServices::openUrl(QUrl("http://vk.com/gifts.php"));
}

void MainWindow::on_groupsButton_clicked(){
    // ==========================================================================
    // открываем страницу в браузере
    // ==========================================================================

    QDesktopServices::openUrl(QUrl("http://vk.com/groups.php?filter=invitations"));
}

void MainWindow::slotMessagesGetDialogs(){
    // ==========================================================================
    // запрашиваю диалоги
    // ==========================================================================

    dialogOffset = 0;
    vk->apiMessagesGetDialogs(dialogOffset, countDownloadItems, 55);
}

void MainWindow::slotMessagesGetNextDialogs(){
    // ==========================================================================
    // запрашиваю диалоги
    // ==========================================================================

    vk->apiMessagesGetDialogs(dialogOffset, countDownloadItems, 55);
}

void MainWindow::slotNewsfeedGet(){
    // ==========================================================================
    // запрашиваю новости
    // ==========================================================================

    newsfeedFrom.clear();
    vk->apiNewsfeedGet("", "post,photo,photo_tag", "", "", countDownloadItems);
}

void MainWindow::slotNewsfeedGetNext(){
    // ==========================================================================
    // запрашиваю новости
    // ==========================================================================

    vk->apiNewsfeedGet("", "post,photo,photo_tag", "", "", countDownloadItems, newsfeedFrom);
}

void MainWindow::slotWallGet(const QString &owner_id){
    // ==========================================================================
    // получаю записи со стены
    // ==========================================================================

    wallOffset = 0;
    vk->apiWallGet(owner_id, wallOffset, countDownloadItems, "");
}

void MainWindow::slotWallGetNext(const QString &owner_id){
    // ==========================================================================
    // получаю записи со стены
    // ==========================================================================

    vk->apiWallGet(owner_id, wallOffset, countDownloadItems, "");
}

void MainWindow::slotWallGetComments(const QString &post_id, const QString &owner_id){
    // ==========================================================================
    // получаю комментарии
    // ==========================================================================

    vk->apiWallGetComments(post_id, owner_id);
}

void MainWindow::slotWallClearComments(){
    // ==========================================================================
    // очищает модель с комментариями
    // ==========================================================================

    if (wallComments->model){
        wallComments->model->removeComments(0, wallComments->model->rowCount());
    }
}

void MainWindow::slotWallAddComment(const QString &post_id, const QString &text, const QString &owner_id, const QString &reply_to_cid){
    // ==========================================================================
    // добавляю комментарий к записи на стене пользователя
    // ==========================================================================

    vk->apiWallAddComment(post_id, text, owner_id, reply_to_cid);
}

void MainWindow::slotWallUserClear(){
    // ==========================================================================
    // очищает модель со стеной пользователей
    // ==========================================================================

    if (wallUser->model){
        wallUser->model->removeWall(0, wallUser->model->rowCount());
    }
}

void MainWindow::slotAudioGet(const QString &uid){
    // ==========================================================================
    // получаю аудиозаписи
    // ==========================================================================

    audioOffset = 0;
    vk->apiAudioGet(uid, "", "", "", 100, 0);
}

void MainWindow::slotAudioGetNext(const QString &uid){
    // ==========================================================================
    // получаю аудиозаписи
    // ==========================================================================

    vk->apiAudioGet(uid, "", "", "", 100, audioOffset);
}

void MainWindow::slotAudioSearch(const QString &search){
    // ==========================================================================
    // поиск аудиозаписий
    // ==========================================================================

    audioOffset = 0;
    vk->apiAudioSearch(search);
}

void MainWindow::slotAudioSearchNext(const QString &search){
    // ==========================================================================
    // поиск аудиозаписий
    // ==========================================================================

    vk->apiAudioSearch(search, "1", "2", 100, audioOffset);
}

void MainWindow::slotAudiosClear(){
    // ==========================================================================
    // очищает модель с аудиозаписями
    // ==========================================================================

    if (audio->model){
        audio->model->removeAudio(0, audio->model->rowCount());
    }
}

void MainWindow::slotPhotosGetAll(const QString &owner_id){
    // ==========================================================================
    // получаю фотографии
    // ==========================================================================

    photoOffset = 0;
    vk->apiPhotosGetAll(owner_id);
}

void MainWindow::slotPhotosGetAllNext(const QString &owner_id){
    // ==========================================================================
    // получаю фотографии
    // ==========================================================================

    vk->apiPhotosGetAll(owner_id, photoOffset, 100);
}

void MainWindow::slotPhotosClear(){
    // ==========================================================================
    // очищает модель с фотографиями пользователей
    // ==========================================================================

    if (photo->model){
        photo->model->removePhoto(0, photo->model->rowCount());
    }
}

void MainWindow::slotVideoGet(const QString &uid){
    // ==========================================================================
    // запрашиваю видеозаписи
    // ==========================================================================

    videoOffset = 0;
    vk->apiVideoGet("", uid, "", "", "", 100, 0);
}

void MainWindow::slotVideoGetNext(const QString &uid){
    // ==========================================================================
    // запрашиваю видеозаписи
    // ==========================================================================

    vk->apiVideoGet("", uid, "", "", "", 100, videoOffset);
}

void MainWindow::slotVideoSearch(const QString &search){
    // ==========================================================================
    // поиск видеозаписей
    // ==========================================================================

    videoOffset = 0;
    vk->apiVideoSearch(search);
}

void MainWindow::slotVideoSearchNext(const QString &search){
    // ==========================================================================
    // поиск видеозаписей
    // ==========================================================================

    vk->apiVideoSearch(search, "", "0", 100, videoOffset);
}

void MainWindow::slotVideosClear(){
    // ==========================================================================
    // очищает модель с видеозаписями
    // ==========================================================================

    if (video->model){
        video->model->removeVideo(0, video->model->rowCount());
    }
}

void MainWindow::slotRosterUserGet(const QString &uid, const bool &mutualFriends){
    // ==========================================================================
    // запрашиваю контакт лист друга
    // ==========================================================================

    if (!mutualFriends){
        rosterUserProxy->setSourceModel(rosterUserModel);
        rosterUserProxy->setFilterRole(Qt::DisplayRole);
        rosterUserProxy->setFilterWildcard("");
        vk->apiFriendsUserGet(uid, "first_name,last_name,photo_medium_rec,online", "");
    } else {
        rosterUserProxy->setSourceModel(model);
        rosterUserProxy->setSortRole(Qt::DisplayRole);
        rosterUserProxy->sort(0);
        rosterUserProxy->setFilterRole(Qt::UserRole+2);
        vk->apiFriendsGetMutual(uid);
    }
}

void MainWindow::slotRosterUserClear(){
    // ==========================================================================
    // очищает модель с друзьями друзей
    // ==========================================================================

    if (rosterUserModel){
        rosterUserModel->removeContacts(0, rosterUserModel->rowCount());
    }
}

void MainWindow::wallPost(const QString &text, const QString &uid, const QString &photo, const int &rotation){
    // ==========================================================================
    // отправка статуса на сайт
    // ==========================================================================

    vk->apiWallPost(text, uid, photo, rotation);

    //если имеется изображение отображаем transfer
    #if defined(MEEGO_EDITION_HARMATTAN)
    if (!photo.isEmpty()){
        clientTransfer->showUploadTransfer(tr("Upload image"));
    }
    #endif
}

void MainWindow::slotWallDelete(const QString &post_id, const QString &owner_id){
    // ==========================================================================
    // удаляет запись со стены пользователя
    // ==========================================================================

    vk->apiWallDelete(post_id, owner_id);
}

void MainWindow::slotOpenRegVK(){
    // ==========================================================================
    // открываем страницу регистрации в браузере
    // ==========================================================================

    QDesktopServices::openUrl(QUrl("http://vkontakte.ru/join"));
}

void MainWindow::slotOpenUrl(const QString &url){
    // ==========================================================================
    // открываем ссылку в браузере
    // ==========================================================================

    QDesktopServices::openUrl(QUrl(url));
}

void MainWindow::slotLikesGet(const QString &owner_id, const QString &type, const QString &item_id, bool add, bool repost){
    // ==========================================================================
    // добавляет/удаляет указанный объект в список Мне нравится
    // ==========================================================================

    vk->apiLikes(owner_id, type, item_id, add, repost);
}

void MainWindow::setIndexReadNews(const int &index){
    // ==========================================================================
    // пришел индекс прочитанной новости
    // ==========================================================================

    indexReadNews = index;
}

void MainWindow::slotCallPhone(const QString &phone){
    // ==========================================================================
    // открываем карточку контакта
    // ==========================================================================

    QString tempPhone = phone;
    QDesktopServices::openUrl(QUrl("tel:" + tempPhone.remove(QChar(' '))));
}

void MainWindow::slotSavePhoto(const QString &url){
    // ==========================================================================
    // сохраняем изображение
    // ==========================================================================

    #if defined(MEEGO_EDITION_HARMATTAN)
    clientTransfer->downloadImg(url);
    #endif
}

bool MainWindow::slotShare(const QString &link, const QString &title, const QString &desc){
    // ==========================================================================
    // поделиться harmattan
    // ==========================================================================

    #if defined(MEEGO_EDITION_HARMATTAN)
    MDataUri duri;
    duri.setMimeType ("text/x-url");
    duri.setTextData (link);
    duri.setAttribute ("title", title);
    duri.setAttribute ("description", desc);
    if (duri.isValid() == false) {
        return false;
    }

    QStringList items;
    items << duri.toString();
    ShareUiInterface shareIf("com.nokia.ShareUi");
    if (shareIf.isValid()) {
        shareIf.share (items);
        return true;
    } else {
        return false;
    }
    #endif
}
