#ifndef NETWORKACCESSMANAGERFACTORY_H
#define NETWORKACCESSMANAGERFACTORY_H

#include <QDeclarativeNetworkAccessManagerFactory>
#include <QNetworkDiskCache>
#include <QNetworkAccessManager>
#include "settings.h"

class NetworkAccessManagerFactory : public QDeclarativeNetworkAccessManagerFactory
{

public:
    virtual QNetworkAccessManager *create(QObject *parent);
};

#endif // NETWORKACCESSMANAGERFACTORY_H
