#include "networkaccessmanagerfactory.h"

QNetworkAccessManager *NetworkAccessManagerFactory::create(QObject *parent)
{
    QNetworkAccessManager *nam = new QNetworkAccessManager(parent);
    QNetworkDiskCache *cache = new QNetworkDiskCache(nam);
    cache->setCacheDirectory(appSettings::instance()->homeDir() + "cache");
    cache->setMaximumCacheSize(appSettings::instance()->loadMain("main/maxCacheSize", 1024 * 1024 * 100).toInt());
    nam->setCache(cache);
    return nam;
}
