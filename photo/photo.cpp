/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "photo.h"

Photo::Photo(QObject *parent) :
    QObject(parent)
{
    model = new photoModel();
}

Photo::~Photo(){
    delete model;
}

void Photo::appendPhoto(const QVector<sPhoto> &photo){
    // ==========================================================================
    // пришли фотографии
    // ==========================================================================

    model->appendPhoto(photo);
}

void Photo::replacePhoto(const QVector<sPhoto> &photo){
    // ==========================================================================
    // пришли фотографии
    // ==========================================================================

    model->replacePhoto(photo);
}
