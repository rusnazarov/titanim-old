/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef PHOTO_H
#define PHOTO_H

#include <QObject>
#include "photomodel.h"
#include "../vk/cvkStruct.h"

class Photo : public QObject
{
Q_OBJECT

public:
    photoModel *model;

public:
    explicit Photo(QObject *parent = 0);
    ~Photo();

public slots:
    void appendPhoto(const QVector<sPhoto> &photo);
    void replacePhoto(const QVector<sPhoto> &photo);

signals:
};

#endif // PHOTO_H
