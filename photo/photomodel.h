/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef PHOTOMODEL_H
#define PHOTOMODEL_H

#include <QAbstractListModel>
#include "vk/cvkStruct.h"

class photoModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString img1 READ img1 NOTIFY photoChanged)
    Q_PROPERTY(QString img2 READ img2 NOTIFY photoChanged)
    Q_PROPERTY(QString img3 READ img3 NOTIFY photoChanged)
    Q_PROPERTY(QString img4 READ img4 NOTIFY photoChanged)

private:
    QVector<sPhoto> photo;

private:
    QString img1();
    QString img2();
    QString img3();
    QString img4();

public:
    explicit photoModel(QObject *parent = 0);
    void appendPhoto(QVector<sPhoto> photo);
    void replacePhoto(QVector<sPhoto> photo);
    bool removePhoto(int row, int count);
    Q_INVOKABLE QString urlAt(int index);
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant& value, int role = Qt::EditRole);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    enum PhotoRole {
        pidRole = Qt::UserRole,
        aidRole = Qt::UserRole+1,
        ownerIdRole = Qt::UserRole+2,
        createdRole = Qt::UserRole+3,
        srcRole = Qt::UserRole+4,
        srcBigRole = Qt::UserRole+5,
        srcSmallRole = Qt::UserRole+6
    };

public slots:

signals:
    void photoChanged();
};

#endif // PHOTOMODEL_H
