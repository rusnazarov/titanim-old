/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "photomodel.h"

photoModel::photoModel(QObject *parent) :
    QAbstractListModel(parent)
{
    QHash<int, QByteArray> roles = roleNames();
    roles[pidRole] = "pid";
    roles[aidRole] = "aid";
    roles[ownerIdRole] = "ownerId";
    roles[createdRole] = "created";
    roles[srcRole] = "src";
    roles[srcBigRole] = "srcBig";
    roles[srcSmallRole] = "srcSmall";
    setRoleNames(roles);
}

void photoModel::appendPhoto(QVector<sPhoto> photo){
    // ==========================================================================
    // добавление фотографий
    // ==========================================================================

    if (!photo.count())
        return;

    //добавляем новые
    beginInsertRows(QModelIndex(), this->photo.count(), this->photo.count() + photo.count() - 1);
    this->photo << photo;
    endInsertRows();

    emit photoChanged();
}

void photoModel::replacePhoto(QVector<sPhoto> photo){
    // ==========================================================================
    // замена фотографий
    // ==========================================================================

    //очищаем модель
    removePhoto(0, rowCount());

    //добавляем новые
    appendPhoto(photo);
}

bool photoModel::removePhoto(int row, int count){
    // ==========================================================================
    // удаляем с модели данные
    // ==========================================================================

    beginRemoveRows(QModelIndex(), row, row+count-1);

    for (int i = 0; i < count; ++i) {
        photo.remove(row);
    }

    endRemoveRows();

    emit photoChanged();
    return true;
}

QString photoModel::urlAt(int index){
    // ==========================================================================
    // возвращает урл фотографии по числовому индексу
    // ==========================================================================

    if (index >= photo.size())
        return "";
    return photo.at(index).src;
}

QVariant photoModel::data(const QModelIndex &index, int role) const{
    // ==========================================================================
    // данные модели
    // ==========================================================================

    if (!index.isValid() || index.row() >= photo.size()) {
        return QVariant();
    }

    switch (role) {

    case pidRole:
        return photo.at(index.row()).pid;

    case aidRole:
        return photo.at(index.row()).aid;

    case ownerIdRole:
        return photo.at(index.row()).owner_id;

    case createdRole:
        return photo.at(index.row()).created;

    case srcRole:
        return photo.at(index.row()).src;

    case srcBigRole:{
        return photo.at(index.row()).src_big;
    }

    case srcSmallRole:
        return photo.at(index.row()).src_small;
    }

    return QVariant();
}

bool photoModel::setData(const QModelIndex &index, const QVariant &value, int role){
    // ==========================================================================
    // редактирование данных модели
    // ==========================================================================

    if (!index.isValid() && !value.isValid() && index.row() >= photo.size()) {
        return false;
    }

    return false;
}

int photoModel::rowCount(const QModelIndex &parent) const{
    // ==========================================================================
    // количество строк в модели
    // ==========================================================================

    Q_UNUSED(parent);
    return photo.count();
}

Qt::ItemFlags photoModel::flags(const QModelIndex &index) const{
    // ==========================================================================
    // флаги модели
    // ==========================================================================

    if (index.isValid())
      return (Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    else
        return Qt::NoItemFlags;
}

QString photoModel::img1(){
    if (photo.size() < 1)
        return "";
    return photo.at(0).src;
}

QString photoModel::img2(){
    if (photo.size() < 2)
        return "";
    return photo.at(1).src;
}

QString photoModel::img3(){
    if (photo.size() < 3)
        return "";
    return photo.at(2).src;
}

QString photoModel::img4(){
    if (photo.size() < 4)
        return "";
    return photo.at(3).src;
}
