# -------------------------------------------------
# Project created by QtCreator 2010-09-18T19:03:40
# -------------------------------------------------

QT += network \
    webkit \
    xml \
    dbus
DESTDIR = bin
OBJECTS_DIR = build
MOC_DIR = build
TARGET = titanimpro
TEMPLATE = app
RESOURCES = icon/icon.qrc
win32:RC_FILE = winRCFile.rc
TRANSLATIONS = translations/titanim_ru.ts \
    translations/titanim_uk.ts \
    translations/titanim_be.ts

contains(MEEGO_EDITION,harmattan):{
  DEFINES += MEEGO_EDITION_HARMATTAN
  CONFIG += qdeclarative-boostable \
            mobility \
            libtuiclient \
            shareuiinterface-maemo-meegotouch mdatauri
  MOBILITY += feedback
}

# Add more folders to ship with the application, here
dictionaries.source = data/dictionaries
dictionaries.target = share
!isEmpty(MEEGO_EDITION){
  RESOURCES += data/qml-harmattan/qml/qml.qrc
  qml.source = data/qml-harmattan/qml
} else {
  qml.source = data/qml-desktop/qml
}
qml.target = share
smilies.source = data/smilies
smilies.target = share
sounds.source = data/sounds
sounds.target = share
translations.source = data/translations
translations.target = share
splash.source = data/splash
splash.target = share

!isEmpty(MEEGO_EDITION){
  # Syncfw-plugin Harmattan
  libsyncfw.path = /usr/lib/sync
  libsyncfw.files = data/feed/lib/libtitanimpro-client.so
  client.path = /etc/sync/profiles/client
  client.files = data/feed/client/titanimpro.xml
  sync.path = /etc/sync/profiles/sync
  sync.files = data/feed/sync/titanimpro.xml
  service.path = /etc/sync/profiles/service
  service.files = data/feed/service/titanimpro.xml
  dbusservice.path = /usr/share/dbus-1/services
  dbusservice.files = data/feed/dbus/com.rnazarov.titanimpro.service

  # Setting Syncfw binary applet Harmattan
  libsettings.path = /usr/lib/duicontrolpanel/applets
  libsettings.files = data/feed/settings/libtitanimproapplet.so
  desktopsettings.path = /usr/lib/duicontrolpanel
  desktopsettings.files = data/feed/settings/*.desktop

  INSTALLS += libsyncfw client sync service dbusservice libsettings desktopsettings
}

DEPLOYMENTFOLDERS = dictionaries \
#                    qml \
                    smilies \
                    sounds \
                    translations \
                    splash

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

symbian:TARGET.UID3 = 0xEE13B59C

# Smart Installer package's UID
# This UID is from the protected range and therefore the package will
# fail to install if self-signed. By default qmake uses the unprotected
# range value if unprotected UID is defined for the application and
# 0x2002CCCF value if protected UID is given to the application
#symbian:DEPLOYMENT.installer_header = 0x2002CCCF

# Allow network access on Symbian
symbian:TARGET.CAPABILITY += NetworkServices

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    mainwindow.cpp \
    vk/cvk.cpp \
    vk/captchaview.cpp \
    roster/contactlistmodel.cpp \
    roster/photoload.cpp \
    roster/contactlistdelegate.cpp \
    settings.cpp \
    chat/chatform.cpp \
    chat/tabform.cpp \
    chat/chatmodel.cpp \
    chat/chatdelegate.cpp \
    about.cpp \
    chat/efilter.cpp \
    profileform.cpp \
    settingsform.cpp \
    statusform.cpp \
    notification/popupwindow.cpp \
    spellchecker/SpellTextEdit.cpp \
    spellchecker/highlighter.cpp \
    spellchecker/hunspell/utf_info.cxx \
    spellchecker/hunspell/suggestmgr.cxx \
    spellchecker/hunspell/phonet.cxx \
    spellchecker/hunspell/hunzip.cxx \
    spellchecker/hunspell/hunspell.cxx \
    spellchecker/hunspell/hashmgr.cxx \
    spellchecker/hunspell/filemgr.cxx \
    spellchecker/hunspell/dictmgr.cxx \
    spellchecker/hunspell/csutil.cxx \
    spellchecker/hunspell/affixmgr.cxx \
    spellchecker/hunspell/affentry.cxx \
    chat/emoticons.cpp \
    chat/chatdialogs.cpp \
    chat/dialogsmodel.cpp \
    newsfeed/newsfeed.cpp \
    newsfeed/newsfeedmodel.cpp \
    wall/wall.cpp \
    wall/wallmodel.cpp \
    wall/wallcommentsmodel.cpp \
    wall/wallcomments.cpp \
    audio/audiomodel.cpp \
    audio/audio.cpp \
    audio/meegobuttonlistener.cpp \
    photo/photo.cpp \
    photo/photomodel.cpp \
    vk/k8json.cpp \
    cache/networkaccessmanagerfactory.cpp \
    video/video.cpp \
    video/videomodel.cpp \
    transfer/tuitestclient.cpp
HEADERS += mainwindow.h \
    vk/cvk.h \
    vk/captchaview.h \
    vk/cvkStruct.h \
    roster/contactlistmodel.h \
    roster/photoload.h \
    roster/contactlistdelegate.h \
    settings.h \
    chat/chatform.h \
    chat/tabform.h \
    chat/chatmodel.h \
    chat/chatdelegate.h \
    about.h \
    chat/efilter.h \
    profileform.h \
    settingsform.h \
    statusform.h \
    notification/popupwindow.h \
    spellchecker/SpellTextEdit.h \
    spellchecker/highlighter.h \
    spellchecker/hunspell/w_char.hxx \
    spellchecker/hunspell/suggestmgr.hxx \
    spellchecker/hunspell/phonet.hxx \
    spellchecker/hunspell/langnum.hxx \
    spellchecker/hunspell/hunzip.hxx \
    spellchecker/hunspell/hunspell.hxx \
    spellchecker/hunspell/hunspell.h \
    spellchecker/hunspell/htypes.hxx \
    spellchecker/hunspell/hashmgr.hxx \
    spellchecker/hunspell/filemgr.hxx \
    spellchecker/hunspell/dictmgr.hxx \
    spellchecker/hunspell/csutil.hxx \
    spellchecker/hunspell/baseaffix.hxx \
    spellchecker/hunspell/atypes.hxx \
    spellchecker/hunspell/affixmgr.hxx \
    spellchecker/hunspell/affentry.hxx \
    chat/emoticons.h \
    chat/chatdialogs.h \
    chat/dialogsmodel.h \
    newsfeed/newsfeed.h \
    newsfeed/newsfeedmodel.h \
    wall/wall.h \
    wall/wallmodel.h \
    wall/wallcommentsmodel.h \
    wall/wallcomments.h \
    audio/audiomodel.h \
    audio/audio.h \
    audio/meegobuttonlistener.h \
    photo/photo.h \
    photo/photomodel.h \
    vk/k8json.h \
    cache/networkaccessmanagerfactory.h \
    video/video.h \
    video/videomodel.h \
    transfer/tuitestclient.h
FORMS += mainwindow.ui \
    chat/chatform.ui \
    chat/tabform.ui \
    about.ui \
    profileform.ui \
    settingsform.ui \
    statusform.ui \
    notification/popupwindow.ui

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

OTHER_FILES += \
    qtc_packaging/debian_fremantle/rules \
    qtc_packaging/debian_fremantle/README \
    qtc_packaging/debian_fremantle/copyright \
    qtc_packaging/debian_fremantle/control \
    qtc_packaging/debian_fremantle/compat \
    qtc_packaging/debian_fremantle/changelog \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog

maemo5 {
    QT += maemo5
}


























