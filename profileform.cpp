/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "profileform.h"
#include "ui_profileform.h"

profileForm::profileForm(QObject *parent) :
    QObject(parent)
{
    slotClear();
}

void profileForm::slotProfile(const sProfile &profile){
    m_profile = profile;
    emit profileChanged();
}

void profileForm::slotClear()
{
    m_profile.uid = "";
    m_profile.firstName = "";
    m_profile.lastName = "";
    m_profile.photoRectUrl = "";
    m_profile.photoBigUrl = "";
    m_profile.activity = "";
    m_profile.bDate = "";
    m_profile.univerName = "";
    m_profile.facultyName = "";
    m_profile.mPhone = "";
    m_profile.hPhone = "";
    m_profile.status = "0";
    m_profile.friendsCount = 0;
    m_profile.mutualFriendsCount = 0;
    m_profile.photosCount = 0;
    m_profile.videosCount = 0;
    m_profile.audiosCount = 0;
    m_profile.canPost = true;
    m_profile.canWriteMessage = true;
    m_profile.lastSeen = QDateTime();
    emit profileChanged();
}

QString profileForm::uid()
{
    return m_profile.uid;
}

QString profileForm::name()
{
    if (!m_profile.firstName.isEmpty())
        return m_profile.firstName + " " + m_profile.lastName;
    return m_profile.uid;
}

QString profileForm::photo()
{
    return m_profile.photoRectUrl.toString();
}

QString profileForm::photoBig()
{
    return m_profile.photoBigUrl.toString();
}

QString profileForm::activity()
{
    return m_profile.activity;
}

QString profileForm::bDate()
{
    return m_profile.bDate;
}

QString profileForm::univerName()
{
    return m_profile.univerName;
}

QString profileForm::facultyName()
{
    return m_profile.facultyName;
}

QString profileForm::mPhone()
{
    return m_profile.mPhone;
}

QString profileForm::hPhone()
{
    return m_profile.hPhone;
}

bool profileForm::status()
{
    return m_profile.status.toInt();
}

QString profileForm::lastSeen()
{
    if (m_profile.lastSeen.isNull())
        return "";

    //я пьян, todo сделать красиво
    QString prefix = m_profile.sex == 1 ? tr("last seen", "woman") : tr("last seen", "man");
    int days = m_profile.lastSeen.daysTo(QDateTime::currentDateTime());

    switch (days) {
    case 0:{
        int minute = m_profile.lastSeen.secsTo(QDateTime::currentDateTime()) / 60;

        if(minute < 60){
            QString suffix;
            int temp = QString::number(minute).right(1).toInt();
            if (temp == 0 || (minute > 4 && minute < 20) || temp > 4){
                suffix = tr("minutes ago", "5");
            } else if (temp == 1){
                suffix = tr("minute ago", "1");
            } else {
                suffix = tr("minutes ago", "2");
            }

            return QString("%1 %2 %3")
                    .arg(prefix)
                    .arg(minute)
                    .arg(suffix);
        } else {
            return prefix + " " + tr("today at %1").arg(m_profile.lastSeen.time().toString("h:mm"));
        }
    }
    case 1:
        return prefix + " " + tr("yesterday at %1").arg(m_profile.lastSeen.time().toString("h:mm"));
    default:
        return prefix + " " + m_profile.lastSeen.toString("dd MMMM yyyy h:mm");
    }
}

int profileForm::friendsCount()
{
    return m_profile.friendsCount;
}

int profileForm::mutualFriendsCount()
{
    return m_profile.mutualFriendsCount;
}

int profileForm::photosCount()
{
    return m_profile.photosCount;
}

int profileForm::videosCount()
{
    return m_profile.videosCount;
}

int profileForm::audiosCount()
{
    return m_profile.audiosCount;
}

bool profileForm::canPost()
{
    return m_profile.canPost;
}

bool profileForm::canWriteMessage()
{
    return m_profile.canWriteMessage;
}
