/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "settingsform.h"
#include "ui_settingsform.h"

settingsForm::settingsForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::settingsForm)
{
    ui->setupUi(this);
    colorDialog = new QColorDialog(this);

    connect(ui->proxyTypeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(slotChanged(int)));
    connect(ui->proxyAuthCheck, SIGNAL(clicked()), this, SLOT(slotChanged()));

    //языки
    languageMap.insert("English", "en_US");
    QDir qmDir(appSettings::instance()->homeAppDir() + "translations", "*.qm");
    QStringList fileNames = qmDir.entryList(QStringList("titanim_*.qm"));
    for (int i = 0; i < fileNames.size(); ++i) {
        QString localeStr = fileNames[i];
        localeStr.remove(0, localeStr.indexOf('_') + 1);
        localeStr.chop(3);
        QLocale l(localeStr);
        languageMap.insert(l.languageToString(l.language()), l.name());
    }
    QMap<QString, QVariant>::const_iterator i = languageMap.constBegin();
    QString localeTemp = appSettings::instance()->loadMain("main/language", QLocale::system().name()).toString();
    int k = 0;
    lngCurrentIndex = -1;
    while (i != languageMap.constEnd()) {
        if (localeTemp == i.value()){
            lngCurrentIndex = k;
            break;
        }
        ++i; k++;
    }

    //смайлы
    QDir smiliesDir(appSettings::instance()->homeAppDir() + "smilies");
    QStringList smiliesNames = smiliesDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    for (int i = 0; i < smiliesNames.size(); ++i){
        ui->smiliesCombo->addItem(smiliesNames.at(i));
        if (appSettings::instance()->loadMain("smilies/smilies","default").toString() == smiliesNames.at(i))
            ui->smiliesCombo->setCurrentIndex(i);
    }
}

settingsForm::~settingsForm()
{
    delete ui;
}

void settingsForm::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void settingsForm::showEvent(QShowEvent *){
    flagRestartApp = false;

    ui->offlineCheck->setChecked(appSettings::instance()->loadProfile("contactlist/offlineButton",false).toBool());
    ui->soundMsgCheck->setChecked(appSettings::instance()->loadProfile("contactlist/soundMsg",true).toBool());
    ui->showActivityCheck->setChecked(appSettings::instance()->loadProfile("contactlist/showActivity",true).toBool());
    ui->showAvatarCheck->setChecked(appSettings::instance()->loadProfile("contactlist/showAvatar",true).toBool());
    ui->alternatingRCCheck->setChecked(appSettings::instance()->loadProfile("contactlist/alternatingrowcolors",true).toBool());
    ui->sizeAvatarSlider->setValue(appSettings::instance()->loadProfile("contactlist/sizeAvatar",30).toInt());
    ui->sendByCtEnterCheck->setChecked(appSettings::instance()->loadProfile("chatForm/sendMsgByCtEnter",true).toBool());
    ui->closeSendMsgCheck->setChecked(appSettings::instance()->loadProfile("chatForm/closeSendMsg",false).toBool());
    ui->actNotSoundCheck->setChecked(appSettings::instance()->loadProfile("chatForm/actNotSound",false).toBool());
    ui->notifyChatCheck->setChecked(appSettings::instance()->loadProfile("chatForm/notifyChat",true).toBool());
    ui->popupTimeSpin->setValue(appSettings::instance()->loadProfile("notifications/time",5).toInt());
    ui->popupPositionCombo->setCurrentIndex(appSettings::instance()->loadProfile("notifications/position",3).toInt());
    ui->popupStyleCombo->setCurrentIndex(appSettings::instance()->loadProfile("notifications/style",2).toInt());
    ui->popupOnlineCheck->setChecked(appSettings::instance()->loadProfile("notifications/online",true).toBool());
    ui->popupOfflineCheck->setChecked(appSettings::instance()->loadProfile("notifications/offline",true).toBool());
    ui->popupMessageCheck->setChecked(appSettings::instance()->loadProfile("notifications/message",true).toBool());
    ui->eventsGroup->setChecked(appSettings::instance()->loadProfile("events/eventsVK",true).toBool());
    ui->friendsCheck->setChecked(appSettings::instance()->loadProfile("events/friends",true).toBool());
    ui->photosCheck->setChecked(appSettings::instance()->loadProfile("events/photos",true).toBool());
    ui->videosCheck->setChecked(appSettings::instance()->loadProfile("events/videos",true).toBool());
    ui->notesCheck->setChecked(appSettings::instance()->loadProfile("events/notes",true).toBool());
    ui->giftsCheck->setChecked(appSettings::instance()->loadProfile("events/gifts",true).toBool());
    ui->groupsCheck->setChecked(appSettings::instance()->loadProfile("events/groups",true).toBool());

    ui->hideStartCheck->setChecked(appSettings::instance()->loadMain("main/hideStart", false).toBool());
    ui->cmdSoundEdit->setText(appSettings::instance()->loadMain("main/cmdSound", "aplay -q \"%1\"").toString());
    ui->autoConnectCheck->setChecked(appSettings::instance()->loadMain("connection/autoConnect", false).toBool());
    ui->reconnectCheck->setChecked(appSettings::instance()->loadMain("connection/reconnect", true).toBool());
    ui->normWinTypeCheck->setChecked(appSettings::instance()->loadMain("main/normWinType", true).toBool());
    ui->onTopCheck->setChecked(appSettings::instance()->loadMain("main/onTop", false).toBool());
    ui->smiliesCheck->setChecked(appSettings::instance()->loadMain("smilies/enabled",true).toBool());
    ui->spellerCheck->setChecked(appSettings::instance()->loadMain("speller/check",false).toBool());
    //словари (TODO сделать универсальным для всех словарей в системе)
    ui->dictionaryCombo->setCurrentIndex(appSettings::instance()->loadMain("speller/dictionary","ru_RU.dic").toString() == "ru_RU.dic" ? 0 : 1);
    ui->proxyTypeCombo->setCurrentIndex(appSettings::instance()->loadMain("proxy/proxyType", 0).toInt());
    ui->proxyHostEdit->setText(appSettings::instance()->loadMain("proxy/host", "").toString());
    ui->proxyPortSpin->setValue(appSettings::instance()->loadMain("proxy/port", 8888).toInt());
    ui->proxyAuthCheck->setChecked(appSettings::instance()->loadMain("proxy/auth", false).toBool());
    ui->proxyUserEdit->setText(appSettings::instance()->loadMain("proxy/user", "").toString());
    ui->proxyPassEdit->setText(appSettings::instance()->loadMain("proxy/pass", "").toString());

    slotChanged();
}

void settingsForm::closeEvent(QCloseEvent *pe){
    // ==========================================================================
    // по закрытию скрываем окно
    // ==========================================================================

    hide();
    pe->ignore();
}

void settingsForm::setShowActivity(bool show)
{
    appSettings::instance()->saveProfile("contactlist/showActivity", show);
}

void settingsForm::setIconSize(int size)
{
    appSettings::instance()->saveProfile("contactlist/sizeAvatar", size);
}

void settingsForm::setTextSize(int size)
{
    appSettings::instance()->saveProfile("contactlist/sizeText", size);
}

void settingsForm::setNewsFeedEnabled(bool enabled)
{
    appSettings::instance()->saveProfile("newsfeed/enabled", enabled);
}

void settingsForm::setLanguage(QString language)
{
    if ((appSettings::instance()->loadMain("main/language", QLocale::system().name()).toString() != languageMap[language])){
        appSettings::instance()->saveMain("main/language", languageMap[language]);
        emit showRestartDialog();
    }
}

void settingsForm::setVisibleStatusBar(bool visible)
{
    appSettings::instance()->saveMain("main/showStatusBar", visible);
}

void settingsForm::setVisibleNativToolBar(bool visible)
{
    appSettings::instance()->saveMain("main/nativToolBar", visible);
    emit isNativToolBarChanged(visible);
}

void settingsForm::setVibro(bool on)
{
    appSettings::instance()->saveProfile("contactlist/vibro", on);
}

void settingsForm::setMessagesActivity(bool on)
{
    appSettings::instance()->saveProfile("chatForm/typing", on);
}

void settingsForm::setSaveLastReadNew(bool on)
{
    appSettings::instance()->saveProfile("newsfeed/saveLastReadNews", on);
}

void settingsForm::setSortByName(bool enabled)
{
    appSettings::instance()->saveProfile("contactlist/sortByName", enabled);
}

void settingsForm::emitSettingsChanged()
{
    emit settingsChanged();
}

void settingsForm::on_saveButton_clicked()
{
    on_applyButton_clicked();
    hide();
}

void settingsForm::on_closeButton_clicked()
{
    hide();
}

void settingsForm::on_applyButton_clicked()
{
//    if ((appSettings::instance()->loadMain("main/language", QLocale::system().name()).toString() != ui->langCombo->itemData(ui->langCombo->currentIndex()).toString()) ||
//        (appSettings::instance()->loadMain("smilies/smilies","default").toString() != ui->smiliesCombo->itemText(ui->smiliesCombo->currentIndex())) ||
//        (appSettings::instance()->loadMain("smilies/enabled",true).toBool() != ui->smiliesCheck->isChecked()))
//        flagRestartApp = true;

//    appSettings::instance()->saveProfile("contactlist/offlineButton", ui->offlineCheck->isChecked());
//    appSettings::instance()->saveProfile("contactlist/soundMsg", ui->soundMsgCheck->isChecked());
//    appSettings::instance()->saveProfile("contactlist/filterSort", ui->offlineCheck->isChecked() ? 17 : 10);
//    appSettings::instance()->saveProfile("contactlist/showActivity", ui->showActivityCheck->isChecked());
//    appSettings::instance()->saveProfile("contactlist/showAvatar", ui->showAvatarCheck->isChecked());
//    appSettings::instance()->saveProfile("contactlist/alternatingrowcolors", ui->alternatingRCCheck->isChecked());
//    appSettings::instance()->saveProfile("contactlist/sizeAvatar", ui->sizeAvatarSlider->value());
//    appSettings::instance()->saveProfile("chatForm/sendMsgByCtEnter", ui->sendByCtEnterCheck->isChecked());
//    appSettings::instance()->saveProfile("chatForm/closeSendMsg", ui->closeSendMsgCheck->isChecked());
//    appSettings::instance()->saveProfile("chatForm/actNotSound", ui->actNotSoundCheck->isChecked());
//    appSettings::instance()->saveProfile("chatForm/notifyChat", ui->notifyChatCheck->isChecked());
//    appSettings::instance()->saveProfile("notifications/time", ui->popupTimeSpin->value());
//    appSettings::instance()->saveProfile("notifications/position", ui->popupPositionCombo->currentIndex());
//    appSettings::instance()->saveProfile("notifications/style", ui->popupStyleCombo->currentIndex());
//    appSettings::instance()->saveProfile("notifications/online", ui->popupOnlineCheck->isChecked());
//    appSettings::instance()->saveProfile("notifications/offline", ui->popupOfflineCheck->isChecked());
//    appSettings::instance()->saveProfile("notifications/message", ui->popupMessageCheck->isChecked());
//    appSettings::instance()->saveProfile("events/eventsVK", ui->eventsGroup->isChecked());
//    appSettings::instance()->saveProfile("events/friends", ui->friendsCheck->isChecked());
//    appSettings::instance()->saveProfile("events/photos", ui->photosCheck->isChecked());
//    appSettings::instance()->saveProfile("events/videos", ui->videosCheck->isChecked());
//    appSettings::instance()->saveProfile("events/notes", ui->notesCheck->isChecked());
//    appSettings::instance()->saveProfile("events/gifts", ui->giftsCheck->isChecked());
//    appSettings::instance()->saveProfile("events/groups", ui->groupsCheck->isChecked());

//    appSettings::instance()->saveMain("main/hideStart", ui->hideStartCheck->isChecked());
//    appSettings::instance()->saveMain("main/cmdSound", ui->cmdSoundEdit->text());
//    appSettings::instance()->saveMain("connection/autoConnect", ui->autoConnectCheck->isChecked());
//    appSettings::instance()->saveMain("connection/reconnect", ui->reconnectCheck->isChecked());
//    appSettings::instance()->saveMain("main/normWinType", ui->normWinTypeCheck->isChecked());
//    appSettings::instance()->saveMain("main/onTop", ui->onTopCheck->isChecked());
//    appSettings::instance()->saveMain("main/language", ui->langCombo->itemData(ui->langCombo->currentIndex()).toString());
//    appSettings::instance()->saveMain("smilies/enabled", ui->smiliesCheck->isChecked());
//    appSettings::instance()->saveMain("smilies/smilies", ui->smiliesCombo->itemText(ui->smiliesCombo->currentIndex()));
//    appSettings::instance()->saveMain("speller/check", ui->spellerCheck->isChecked());
//    appSettings::instance()->saveMain("speller/dictionary", ui->dictionaryCombo->currentIndex() == 0 ? "ru_RU.dic" : "en_GB.dic");
//    appSettings::instance()->saveMain("proxy/proxyType", ui->proxyTypeCombo->currentIndex());
//    appSettings::instance()->saveMain("proxy/host", ui->proxyHostEdit->text());
//    appSettings::instance()->saveMain("proxy/port", ui->proxyPortSpin->value());
//    appSettings::instance()->saveMain("proxy/auth", ui->proxyAuthCheck->isChecked());
//    appSettings::instance()->saveMain("proxy/user", ui->proxyUserEdit->text());
//    appSettings::instance()->saveMain("proxy/pass", ui->proxyPassEdit->text());

//    if (flagRestartApp){
//        appSettings::destroy();
//        restartApp();
//    } else {
//        emit settingsChanged();
//    }
}

void settingsForm::slotChanged(int idx){
    Q_UNUSED(idx);

    ui->proxyHostEdit->setEnabled(ui->proxyTypeCombo->currentIndex());
    ui->proxyPortSpin->setEnabled(ui->proxyTypeCombo->currentIndex());
    ui->proxyAuthCheck->setEnabled(ui->proxyTypeCombo->currentIndex());
    ui->proxyUserEdit->setEnabled(ui->proxyAuthCheck->isChecked() && ui->proxyTypeCombo->currentIndex());
    ui->proxyPassEdit->setEnabled(ui->proxyAuthCheck->isChecked() && ui->proxyTypeCombo->currentIndex());
    if (!ui->proxyAuthCheck->isChecked()){
        ui->proxyUserEdit->clear();
        ui->proxyPassEdit->clear();
    }
}

void settingsForm::restartApp(){
    // ==========================================================================
    // перезапускаем приложение
    // ==========================================================================

    #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
      appSettings::destroy();
      if (QProcess::startDetached(QString("\"") + QApplication::applicationFilePath() + "\""))
          QApplication::quit();
    #else
      QMessageBox::StandardButton ret = QMessageBox::question(this, QCoreApplication::applicationName(),
      tr("You must restart TitanIM for the changes to take effect. Restart now?"),
              QMessageBox::Yes | QMessageBox::No);

      switch(ret){
      case QMessageBox::Yes:
          if (QProcess::startDetached(QString("\"") + QApplication::applicationFilePath() + "\""))
              QApplication::quit();
          break;
      case QMessageBox::No:
          reject();
          break;
      }
    #endif
}

QVariantList settingsForm::language()
{
    QVariantList temp;

    QMap<QString, QVariant>::const_iterator i = languageMap.constBegin();
    while (i != languageMap.constEnd()) {
        temp.append(i.key());
        ++i;
    }

    return temp;
}

int settingsForm::languageCurrentIndex()
{
    return lngCurrentIndex;
}

bool settingsForm::showStatusBar()
{
    return appSettings::instance()->loadMain("main/showStatusBar", true).toBool();
}

bool settingsForm::isNativToolBar()
{
    return appSettings::instance()->loadMain("main/nativToolBar", false).toBool();
}

void settingsForm::on_nameOutButton_clicked()
{
    QColor color = colorDialog->getColor(QColor("#0860ce"));
    if (color.isValid())
        appSettings::instance()->saveProfile("chatForm/colorNameOut", color);
}

void settingsForm::on_nameInButton_clicked()
{
    QColor color = colorDialog->getColor(QColor("#c83f6b"));
    if (color.isValid())
        appSettings::instance()->saveProfile("chatForm/colorNameIn", color);
}

void settingsForm::on_dateButton_clicked()
{
    QColor color = colorDialog->getColor(QColor("#000000"));
    if (color.isValid())
        appSettings::instance()->saveProfile("chatForm/colorDate", color);
}

void settingsForm::on_backgroundOutButton_clicked()
{
    QColor color = colorDialog->getColor(QColor("#eeeeee"));
    if (color.isValid())
        appSettings::instance()->saveProfile("chatForm/colorBackgroundOut", color);
}

void settingsForm::on_backgroundInButton_clicked()
{
    QColor color = colorDialog->getColor(QColor("#dbdbdb"));
    if (color.isValid())
        appSettings::instance()->saveProfile("chatForm/colorBackgroundIn", color);
}

void settingsForm::on_separatorButton_clicked()
{
    QColor color = colorDialog->getColor(QColor("#808080"));
    if (color.isValid())
        appSettings::instance()->saveProfile("chatForm/colorSeparator", color);
}
