/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "popupwindow.h"
#include "ui_popupwindow.h"

popupWindow::popupWindow(int time, int position, int style, int positionInStack, QWidget *parent) :
        QWidget(parent),
        ui(new Ui::popupWindow)
{
    ui->setupUi(this);
    uid = "";
    ui->textBrowser->findChild<QObject *>("qt_scrollarea_viewport")->installEventFilter(this);

    this->time=time;
    this->position = position;
    this->style = style;
    this->positionInStack = positionInStack;

    setWindowFlags(windowFlags() | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::ToolTip);
    setAttribute(Qt::WA_QuitOnClose, false);
    setAttribute(Qt::WA_DeleteOnClose, true);
    QTimer::singleShot((time + 1) * 1000, this, SLOT(close()));
}

popupWindow::~popupWindow()
{
    emit closePopupWindow();
    delete ui;
}

void popupWindow::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

bool popupWindow::eventFilter(QObject *obj, QEvent *event)
{
    Q_UNUSED(obj);

    if (event->type() == QEvent::MouseButtonRelease){
        if (((QMouseEvent*)event)->button() == Qt::LeftButton)
            emit openChatWindow(uid);
        close();
        return true;
    }

    return false;
}

void popupWindow::setTextWindow(const sContact &contact, const QString &text){
    this->uid = contact.uid;

    QString html;
    QString fileAvatar;

    if (!contact.avatarUrl.path().contains("question") && !contact.avatarUrl.path().isEmpty())
        fileAvatar = appSettings::instance()->cacheDir()+contact.avatarUrl.path().remove(0,1).replace("/",".");

    html = "<table cellspacing=\"0\" cellpadding=\"0\">";

    //аватарка
    if (QFile::exists(fileAvatar)){
        html += QString("<td width=\"50\"><img border=\"1\" src=\"%1\"></td>")
                .arg(fileAvatar);
        html += "<td width=\"160\" ";
    } else html += "<td width=\"210\" ";

    //имя, фамилия, текст
    html += QString("align=\"center\"><b>%1</b><br>%2</td>")
            .arg(contact.firstName.isEmpty() ? contact.uid : contact.firstName+" "+contact.lastName)
            .arg(text);
    html += "</table>";

    ui->textBrowser->setText(html);
}

void popupWindow::setTextWindow(const QString &text){
    QString html;
    html = QString("<table cellspacing=\"0\" cellpadding=\"0\"><td width=\"210\" align=\"center\"><b>TitanIM</b><br>%1</td>")
            .arg(text);
    html += "</table>";

    ui->textBrowser->setText(html);
}

void popupWindow::showTrayMessage(){
    QRect geometry = QApplication::desktop()->availableGeometry();
    moveToPointX = 0;
    moveToPointY = 0;

    switch(position){
    case 0:
        moveToPointX = geometry.left() + 5;
        moveToPointY = geometry.top() + (positionInStack - 1) * (height() + 5);
        break;
    case 1:
        moveToPointX = geometry.right() - width() - 5;
        moveToPointY = geometry.top() + (positionInStack - 1) * (height() + 5);
        break;
    case 2:
        moveToPointX = geometry.left() + 5;
        moveToPointY = geometry.bottom() - positionInStack * (height() + 5);
        break;
    default:
        moveToPointX = geometry.right() - width() - 5;
        moveToPointY = geometry.bottom() - positionInStack * (height() + 5);
    }
    slide();
}

void popupWindow::slide(){
    fromX = 0;
    fromY = 0;
    switch (style){
    case 0:
        move(moveToPointX, moveToPointY);
        show();
        break;
    case 1:
        if (position == 0 || position == 1){
            fromX = moveToPointX;
            fromY = moveToPointY - height();
            move(fromX, fromY);
            show();
            slideVerticallyDown();
        } else if (position == 2 || position == 3){
            fromX = moveToPointX;
            fromY = moveToPointY + height();
            move(fromX, fromY);
            show();
            slideVerticallyUp();
        }
        break;
        case 2:
        if (position == 0 || position == 2){
            fromX = moveToPointX - width();
            fromY = moveToPointY;
            move(fromX, fromY);
            show();
            slideHorizontallyRight();
        } else if (position == 1 || position == 3){
            fromX = moveToPointX + width();
            fromY = moveToPointY;
            move(fromX, fromY);
            show();
            slideHorizontallyLeft();
        }
        break;
        default:
        move(moveToPointX, moveToPointY);
        show();
    }
}

void popupWindow::slideVerticallyUp(){
    fromY -= 5;

    if (fromY >= moveToPointY){
        move(moveToPointX, fromY);
        QTimer::singleShot(10, this, SLOT(slideVerticallyUp()));
    }
}

void popupWindow::slideVerticallyDown(){
    fromY += 5;
    if (fromY <= moveToPointY){
        move(moveToPointX, fromY);
        QTimer::singleShot(10, this, SLOT(slideVerticallyDown()));
    }
}

void popupWindow::slideHorizontallyRight(){
    fromX += 5;
    if (fromX <= moveToPointX){
        move(fromX, moveToPointY);
        QTimer::singleShot(10, this, SLOT(slideHorizontallyRight()));
    }
}

void popupWindow::slideHorizontallyLeft(){
    fromX -= 5;
    if (fromX >= moveToPointX){
        move(fromX, moveToPointY);
        QTimer::singleShot(10, this, SLOT(slideHorizontallyLeft()));
    }
}
