/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef POPUPWINDOW_H
#define POPUPWINDOW_H

#include <QWidget>
#include "../vk/cvkStruct.h"
#include "settingsform.h"
#include <QApplication>
#include <QDesktopWidget>
#include <QFile>
#include <QTimer>

namespace Ui {
    class popupWindow;
}

class popupWindow : public QWidget
{
Q_OBJECT

private:
    Ui::popupWindow *ui;
    QString uid;
    int position;
    int style;
    int time;
    int positionInStack;
    int moveToPointX;
    int moveToPointY;
    int fromX;
    int fromY;

private:
    void slide();

public:
    popupWindow(int time, int position, int style, int positionInStack, QWidget *parent = 0);
    ~popupWindow();
    void setTextWindow(const sContact &contact, const QString &text);
    void setTextWindow(const QString &text);
    void showTrayMessage();

protected:
    void changeEvent(QEvent *e);
    bool eventFilter(QObject *obj, QEvent *event);

private slots:
    void slideVerticallyUp();
    void slideVerticallyDown();
    void slideHorizontallyRight();
    void slideHorizontallyLeft();

signals:
    void closePopupWindow();
    void openChatWindow(const QString &uid);
};

#endif // POPUPWINDOW_H
