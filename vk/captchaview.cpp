/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "captchaview.h"

captchaView::captchaView(const QString &captchaSid, const QString &captchaImg, QWidget *parent) :
    QWidget(parent)
{
    http = new QNetworkAccessManager(this);
    setAttribute(Qt::WA_QuitOnClose, false);
    setWindowModality(Qt::ApplicationModal);
    connect(http, SIGNAL(finished(QNetworkReply*)),this, SLOT(slotFinished(QNetworkReply*)));
    sid = captchaSid;
    url = captchaImg;
}

captchaView::~captchaView(){
    delete http;
    delete labelImg;
    delete lineEdit;
    delete button;
    delete layout;
}

void captchaView::load(){
    // ==========================================================================
    // загружаем изображение
    // ==========================================================================

    QNetworkRequest request;
    request.setUrl(url);
    http->get(request);
}

void captchaView::slotFinished(QNetworkReply *networkReply){
    // ==========================================================================
    // изображение загрузилось
    // ==========================================================================

    if(networkReply->error() != QNetworkReply::NoError){
        //qDebug() << networkReply->errorString();
        emit done(sid, "");
        return;
    }

    QByteArray bytes = networkReply->readAll();
    img.loadFromData(bytes);
    networkReply->deleteLater();

    //показываем форму с капчей
    labelImg = new QLabel();
    labelImg->setPixmap(img);
    lineEdit = new QLineEdit();
    button = new QPushButton(tr("Send"));
    connect(button, SIGNAL(clicked()), this, SLOT(slotClickedButton()));
    layout = new QVBoxLayout();
    layout->addWidget(labelImg);
    layout->addWidget(lineEdit);
    layout->addWidget(button);
    setLayout(layout);
    resize(layout->sizeHint());
    setWindowTitle("TitanIM");
    move((QApplication::desktop()->width() - width())/2,
         (QApplication::desktop()->height() - height())/2);
    show();
}

void captchaView::closeEvent(QCloseEvent *event)
{
    // ==========================================================================
    // при закрытия окна
    // ==========================================================================

    hide();
    event->ignore();
    emit done(sid, "");
}

void captchaView::slotClickedButton(){
    // ==========================================================================
    // пользователь ввел капчу
    // ==========================================================================

    hide();
    emit done(sid, lineEdit->text().trimmed());
}
