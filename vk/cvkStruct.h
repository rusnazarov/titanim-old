/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************

 ****************************************************************************
 * I'm sorry for the russian comments
 ***************************************************************************

*/

#ifndef CVKSTRUCT_H
#define CVKSTRUCT_H

#include <QString>
#include <QUrl>
#include <QPixmap>
#include <QDateTime>

struct sLoginVars {
    QString grant_type;     //тип авторизации, должен быть равен password
    QString client_id;      //id приложения
    QString client_secret;  //секретный ключ приложения
    QString username;       //логин (может быть пустым)
    QString password;       //пароль (может быть пустым)
    int scope;              //битовая маска настроек доступа приложения
};

struct sSessionVars {
    QString access_token;   //токен
    QString expires_in;     //время жизни токена (сек)
    QString user_id;        //id пользователя в ВКонтакте
};

struct sUrlServers {
    QUrl authServer;        //адрес входа и получения сессии
    QUrl apiServer;         //адрес взаимодействие с API
};

enum eStatus {
    offline,                //не в сети
    connecting,             //в процессе
    online                  //в сети
};

enum eMethod {
    notAPI,                 //метод не из API
    friendsGet,             //возвращает список идентификаторов друзей текущего пользователя
    friendsGetLists,        //возвращает id и названия списков друзей текущего пользователя
    getLongPollServer,      //возвращает данные, необходимые для подключения к Long Poll серверу
    execute,                //универсальный метод, который позволяет запускать последовательность других методов
    activityOnline,         //поддержка online статуса на сайте ВК
    getCounters,            //возвращает события с сайта ВК (друзья, подарки, встречи итд)
    activityGet,            //возвращает текущий статус пользователя
    activitySet,            //устанавливает статус текущему пользователю
    messagesSend,           //посылает сообщение
    newMessagesGet,         //возвращает список непрочитанных входящих сообщений
    messagesGetHistory,     //возвращает историю сообщений
    messagesMark,           //помечает сообщения, как прочитанные/непрочитанные
    messagesDelete,         //удаляет сообщение
    getProfiles,            //возвращает расширенную информацию о пользователе
    wallPost,               //публикует новую запись на своей или чужой стене
    messagesGetDialogs,     //возвращает список диалогов текущего пользователя
    newsfeedGet,            //возвращает данные, необходимые для показа списка новостей
    wallGet,                //возвращает список записей со стены пользователя
    wallGetComments,        //возвращает список комментариев к записи на стене пользователя
    wallAddComment,         //добавляет комментарий к записи на стене пользователя
    photosGetAll,           //возвращает все фотографии пользователя в антихронологическом порядке
    audioGet,               //возвращает список аудиозаписей пользователя или группы
    audioAdd,               //копирует аудиозапись на страницу пользователя или группы
    getWallUploadServer,    //возвращает адрес сервера для загрузки﻿ фотографии на стену пользователя
    saveWallPhoto,          //сохраняет фотографии после успешной загрузки на URI
    likesGet,               //добавляет/удаляет указанный объект в список Мне нравится
    messagesSetActivity,    //изменяет статус набора текста пользователем в диалоге
    videoGet,               //возвращает информацию о видеозаписях
    friendsUserGet,         //возвращает список идентификаторов друзей uid пользователя
    friendsGetMutual,       //возвращает список идентификаторов общих друзей между парой пользователей
    wallDelete              //удаляет запись со стены пользователя
};

struct sQuery {
    eMethod method;         //метод API
    QUrl request;           //строка запроса к API
    QString result;         //результат выполнения запроса
};

struct sContact {
    QString uid;            //идентификатор
    QString firstName;      //имя
    QString lastName;       //фамилия
    QString nickName;       //никнейм
    QString bDate;          //дата рождения
    QUrl avatarUrl;         //url фотографии
    QPixmap avatarImg;      //фотография (может быть null)
    QString status;         //статус (1 - в сети, 0 - не в сети)
    QString activity;       //активность
    QString lists;          //списки друзей
    QString mPhone;         //мобильный телефон
    QString hPhone;         //домашний телефон
};

struct sLongPoll {
    QString key;            //секретный ключ сессии
    QUrl server;            //адрес LongPoll сервера
    QString ts;             //номер последнего события, начиная с которого Вы хотите получать данные
    int wait;               //держать соединение (сек)
};

struct sActivity {
    QString uid;            //идентификатор пользователя
    QString activity;       //статус пользователя
};

struct sCounters {
    int friends;            //друзья
    int photos;             //фотографии
    int videos;             //видео
    int gifts;              //подарки
    int notes;              //заметки
    int events;             //встречи
    int groups;             //группы
    int offers;             //предложения
    int opinions;           //мнения
    int questions;          //вопросы
};

struct sAudio {
    QString aid;            //идентификатор аудиозаписи
    QString ownerId;        //идентификатор владельца аудиозаписи
    QString artist;         //исполнитель
    QString title;          //композиция
    int duration;           //длительность (сек)
    QString url;            //url аудиозаписи (mp3) привязка к IP
};

struct sMessage {
    QString mid;            //идентификатор сообщения
    int flags;              //флаг сообщения
    QString fromId;         //идентификатор отправителя
    QDateTime dateTime;     //дата время сообщения
    QString subject;        //тема сообщения
    QString text;           //текст сообщения
    QString photos;         //url фотографий, которые прикреплены к текущему сообщению
    QString photosBig;      //url больших фотографий, которые прикреплены к текущему сообщению
    QVector<sAudio> audio;  //url аудио, которые прикреплены к текущему сообщению
};

struct sDialog {
    QString body;           //текст сообщения
    QString title;          //тема сообщения
    QDateTime date;         //дата время сообщения
    QString uid;            //идентификатор пользователя
    QString mid;            //идентификатор сообщения
    bool readState;         //сообщение не прочитано
    bool out;               //исходящее сообщение
    QString chatId;         //id чата (если этот диалог яв-ся чатом)
    QString firstName;      //имя
    QString lastName;       //фамилия
    QString avatarUrl;      //url фотографии
};

struct sNew {
    QString type;           //тип списка новости
    QString sourceId;       //идентификатор источника новости
    QDateTime date;         //время публикации новости
    QString postId;         //идентификатор записи на стене владельца
    QString copyOwnerId;    //идентификатор владельца скопированного сообщения
    QString copyOwner;      //имя владельца скопированного сообщения
    QString copyPostId;     //идентификатор скопированного сообщения
    QString text;           //текст записи
    QString likes;          //число людей, которым понравилась данная запись
    bool isLiked;           //понравилась ли запись текущему пользователю
    QString comments;       //количесто комментариев к записи
    QStringList photos;     //url фотографий, которые прикреплены к текущей новости
    QStringList photosBig;  //url больших фотографий, которые прикреплены к текущей новости
    QList<int> photosWidth; //ширина фотографий, которые прикреплены к текущей новости
    QList<int> photosHeight;//высота фотографий, которые прикреплены к текущей новости
    QString geo;            //местоположение
    QString firstName;      //имя пользователя
    QString lastName;       //фамилия пользователя
    QString avatarUrl;      //url фотографии
    QString linkUrl;        //адрес прикрепленной ссылки
    bool canPost;           //может ли текущий пользователь добавить комментарии к записи
};

struct sWall {
    QString id;             //идентификатор записи
    QString toId;           //идентификатор владельца записи
    QString fromId;         //идентификатор пользователя, создавшего запись
    QDateTime date;         //время публикации записи
    QString text;           //текст записи
    QString comments;       //количестве комментариев к записи
    QString likes;          //количесвто людей кому понравилась данная запись
    bool isLiked;           //понравилась ли запись текущему пользователю
    QString photos;         //url фотографий, которые прикреплены к текущей записи
    QString photosBig;      //url больших фотографий, которые прикреплены к текущей записи
    QString geo;            //местоположение
    QString copyOwnerId;    //идентификатор владельца стены
    QString copyPostId;     //идентификатор скопированной записи
    QString firstName;      //имя пользователя
    QString lastName;       //фамилия пользователя
    QString avatarUrl;      //url фотографии
    QString linkUrl;        //адрес прикрепленной ссылки
    bool canPost;           //может ли текущий пользователь добавить комментарии к записи
};

struct sWallComments {
    QString cid;            //идентификатор комментария
    QString uid;            //идентификатор владельца комментария
    QDateTime date;         //время публикации комментария
    QString text;           //текст комментария
    QString replyToUid;     //идентификатор пользователя которому отвечают
    QString replyToCid;     //идентификатор комментария на который отвечают
    QString firstName;      //имя пользователя
    QString lastName;       //фамилия пользователя
    QString avatarUrl;      //url фотографии
};

struct sProfile {
    QString uid;            //идентификатор
    QString firstName;      //имя
    QString lastName;       //фамилия
    QString nickName;       //никнейм
    int sex;                //пол
    QString bDate;          //дата рождения
    QString sity;           //id города
    QString country;        //id страны
    QUrl photoRectUrl;      //url квадратной фотографии
    QUrl photoBigUrl;       //url фотографии
    QString status;         //статус
    QString activity;       //активность
    QString lists;          //списки друзей
    QString domain;         //домен
    QString mPhone;         //мобильный телефон
    QString hPhone;         //домашний телефон
    QString univerName;     //универ
    QString facultyName;    //факультет
    int friendsCount;       //количество друзей
    int mutualFriendsCount; //количество общих друзей
    int photosCount;        //количество фотографий
    int videosCount;        //количество видеозаписей
    int audiosCount;        //количество аудиозаписей
    bool canPost;           //разрешено ли оставлять записи на стене
    bool canWriteMessage;   //разрешено ли написание сообщений
    QDateTime lastSeen;     //время последнего входа пользователя
};

struct sPhoto {
    QString pid;            //идентификатор фотографии
    QString aid;            //идентификатор альбома
    QString owner_id;       //идентификатор пользователя
    QDateTime created;      //дата время фотографии
    QString src;            //url фотографии
    QString src_big;        //url фотографии большого размера
    QString src_small;      //url фотографии маленького размера
};

struct sVideo {
    QString vid;            //идентификатор видеозаписи
    QString owner_id;       //идентификатор владельца видеозаписи
    QString title;          //заголовок
    QString description;    //описание
    int duration;           //длительность (сек)
    QString image;          //превью
    QUrl url;               //url видеозаписи
};

enum eError {
    timeoutLongPollServer   = -2,
    serverIsNotAvailable    = -1,
    unknownMethodPassed     =  3,
    incorrectSignature      =  4,
    userAuthorizationFailed =  5
};

enum eLongPollAnswer {
    lpsMessageDeleted       = 0,
    lpsMessageFlagsReplaced = 1,
    lpsMessageFlagsSet      = 2,
    lpsMessageFlagsReseted  = 3,
    lpsMessageAdded         = 4,
    lpsUserOnline           = 8,
    lpsUserOffline          = 9,
    lpsUserTyping           = 61
};

#endif // CVKSTRUCT_H
