/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef CAPTCHAVIEW_H
#define CAPTCHAVIEW_H

#include <QApplication>
#include <QCloseEvent>
#include <QDesktopWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QString>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QUrl>
#include <QPixmap>

class captchaView : public QWidget
{
Q_OBJECT

private:
    QVBoxLayout *layout;
    QLabel *labelImg;
    QLineEdit *lineEdit;
    QPushButton *button;
    QNetworkAccessManager *http;
    QPixmap img;
    QString sid;
    QUrl url;

public:
    explicit captchaView(const QString &captchaSid, const QString &captchaImg, QWidget *parent = 0);
    ~captchaView();
    void load();

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void slotFinished(QNetworkReply *networkReply);
    void slotClickedButton();

signals:
    void done(const QString &captcha_sid, const QString &captcha_key);
};

#endif // CAPTCHAVIEW_H
