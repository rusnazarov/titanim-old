/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************

 ****************************************************************************
 * I'm sorry for the russian comments
 ***************************************************************************

*/

#include "cvk.h"
#include <QNetworkCookieJar>

cvk::cvk(const QString &client_id, QObject *parent) :
    QObject(parent)
{
    //переменные входа и получения сессии
    loginVars.grant_type = "password";
    loginVars.client_id = client_id;
    loginVars.client_secret = "bJKfYSu0LS6N52M0HnBo";
    loginVars.scope = 2 + 4 + 8 + 16 + 1024 + 4096 + 8192;

    //адреса взаимодействия с API
    urlServers.authServer = QString("https://api.vk.com/oauth/token?grant_type=%1&client_id=%2&client_secret=%3&scope=%4")
                             .arg(loginVars.grant_type)
                             .arg(loginVars.client_id)
                             .arg(loginVars.client_secret)
                             .arg(loginVars.scope);

    urlServers.apiServer = "https://api.vk.com/method/";

    //по умолчанию не в сети
    status = offline;

    //флаг выполнения запросов к серверу
    isProcessing = false;

    //таймеры запросов (3 запроса/1000 мс)
    stopwatch = new QTime();
    timerRequest = new QTimer();

    //таймер для проверки соединения с LongPoll
    timeoutLongPoll = new QTimer();

    //таймер для выполнения заданий по расписанию
    timerSchedule = new QTimer();
}

cvk::~cvk(){
    //удаляем таймеры запросов
    delete stopwatch;
    delete timerRequest;

    //удаляем таймер для проверки соединения с LongPoll
    delete timeoutLongPoll;

    //удаляем таймер для выполнения заданий по расписанию
    delete timerSchedule;
}

void cvk::connectToVk(const QString &uid, const QString &token, const QString &username, const QString &password){
    // ==========================================================================
    // выполняем все действия для успешной авторизации
    // авторизация проходит либо с помощью пары uid:token
    // либо c помощью username:password
    // приоритет отдается username:password авторизации
    // ==========================================================================

    if (status != offline){
        return;
    }

    status = connecting;

    //объект выполняющий запросы к API
    http = new QNetworkAccessManager(this);

    //объект выполняющий запросы к Long Poll серверу
    httpLongPoll = new QNetworkAccessManager(this);

    connect(this, SIGNAL(connected(QString,QString)), this, SLOT(slotConnected()));
    connect(http, SIGNAL(finished(QNetworkReply*)), this, SLOT(slotResponse(QNetworkReply*)));
    connect(timerRequest, SIGNAL(timeout()), this, SLOT(slotTimerRequest()));
    connect(this,SIGNAL(responseQuery(sQuery)),this,SLOT(slotResponseQuery(sQuery)));
    connect(httpLongPoll, SIGNAL(finished(QNetworkReply*)), this, SLOT(slotResponseLongPoll(QNetworkReply*)));
    connect(timeoutLongPoll, SIGNAL(timeout()), this, SLOT(slotTimeoutLongPoll()));
    connect(timerSchedule, SIGNAL(timeout()), this, SLOT(slotTimerSchedule()));

    stopwatch->start();
    timerRequest->setSingleShot(true);
    offsetFriends = 0;

    //заполняем логин/пароль
    if (!username.isEmpty()){
        loginVars.username = username;
        loginVars.password = password;
        //получаем токен
        loadToken();
    } else {
        sessionVars.user_id = uid;
        sessionVars.access_token = token;
        if (!sessionVars.access_token.isEmpty()){
            status = online;
            emit connected(sessionVars.access_token, sessionVars.user_id);
        } else
            loginFailure("");
    }
}

void cvk::disconnectVk(){
    // ==========================================================================
    // выполняем все действия для успешного выхода
    // ==========================================================================

    disconnect(this, SIGNAL(connected(QString,QString)), this, SLOT(slotConnected()));
    disconnect(http, SIGNAL(finished(QNetworkReply*)), this, SLOT(slotResponse(QNetworkReply*)));
    disconnect(timerRequest, SIGNAL(timeout()), this, SLOT(slotTimerRequest()));
    disconnect(this,SIGNAL(responseQuery(sQuery)),this,SLOT(slotResponseQuery(sQuery)));
    disconnect(httpLongPoll, SIGNAL(finished(QNetworkReply*)), this, SLOT(slotResponseLongPoll(QNetworkReply*)));
    disconnect(timeoutLongPoll, SIGNAL(timeout()), this, SLOT(slotTimeoutLongPoll()));
    disconnect(timerSchedule, SIGNAL(timeout()), this, SLOT(slotTimerSchedule()));

    stopwatch->restart();
    timerRequest->stop();
    timeoutLongPoll->stop();
    timerSchedule->stop();
    queryList.clear();
    isProcessing = false;

    http->deleteLater();
    httpLongPoll->deleteLater();

    status = offline;
}

QVariant cvk::parseJSON(const QByteArray &data){
    // ==========================================================================
    // парсим JSON
    // ==========================================================================

    QVariant res;
    int len = data.size();
    K8JSON::parseRecord(res, reinterpret_cast<const uchar *>(data.constData()), &len);
    return res;
}

void cvk::loadToken(){
    // ==========================================================================
    // получаем токен с помощью прямой авторизации
    // ==========================================================================

    httpAuth = new QNetworkAccessManager(this);
    connect(httpAuth, SIGNAL(finished(QNetworkReply*)),this,SLOT(slotTokenLoaded(QNetworkReply*)));

    if (!urlServers.authServer.queryItemValue("username").isEmpty()){
        urlServers.authServer.removeAllQueryItems("username");
        urlServers.authServer.removeAllQueryItems("password");
    }

    //отправляем на сервер
    QNetworkRequest request;
    urlServers.authServer.addQueryItem("username", loginVars.username);
    urlServers.authServer.addQueryItem("password", loginVars.password);
    request.setUrl(urlServers.authServer);
    httpAuth->get(request);
}

void cvk::slotTokenLoaded(QNetworkReply *networkReply){
    // ==========================================================================
    // получили ответ от сервера авторизации
    // при успешной авторизации - отдает токен
    // при не успешной отдает - "Host requires authentication"
    // ==========================================================================

    if(networkReply->error() != QNetworkReply::NoError){
        //qDebug() << networkReply->errorString();

        if (networkReply->error() == QNetworkReply::AuthenticationRequiredError){
            networkReply->deleteLater();
            httpAuth->deleteLater();
            loginFailure("");
        } else {
            networkReply->deleteLater();
            httpAuth->deleteLater();
            emit errorString(serverIsNotAvailable, tr("Server is not available"), true);
        }

        return;
    }

    QVariant response = parseJSON(networkReply->readAll());
    networkReply->deleteLater();

    //авторизация удалась
    if (response.toMap().contains("access_token")){
        loginSuccess(response);
    } else {
        loginFailure(response);
    }

    //удаляем объект авторизации
    httpAuth->deleteLater();
}

void cvk::loginSuccess(const QVariant &response){
    // ==========================================================================
    // авторизация удалась
    // получаем токен и uid
    // ==========================================================================

    //токен, uid, expires_in
    sessionVars.access_token = response.toMap().value("access_token").toString();
    sessionVars.user_id = response.toMap().value("user_id").toString();
    sessionVars.expires_in = "0";

    //в сети
    if (!sessionVars.access_token.isEmpty()){
        status = online;
        emit connected(sessionVars.access_token, sessionVars.user_id);
    } else
        loginFailure(response);
}

void cvk::loginFailure(const QVariant &response){
    // ==========================================================================
    // авторизация завершилась не удачей
    // ==========================================================================

    status = offline;
    emit errorString(userAuthorizationFailed, tr("User authorization failed"), true);
}

void cvk::slotConnected(){
    // ==========================================================================
    // вызывается когда пройдены все шаги авторизации и получены все
    // переменные для взаимодействие с API
    // ==========================================================================

    //первый запрос к ВКонтакте API
    firstRequest();
}

QUrl cvk::genQuery(const QString &method, const QStringList &params){
    // ==========================================================================
    // генерируется строка запроса по данному методу и параметрам
    // параметры могут отсутствовать
    // ==========================================================================

    QUrl temp = urlServers.apiServer;
    temp.setPath(temp.path() + method + ".xml");
    QStringList paramsList;
    paramsList.append("access_token=" + sessionVars.access_token);
    if (!params.isEmpty()) {
        paramsList << params;
    }

    paramsList.replaceInStrings("%","%25");
    paramsList.replaceInStrings("&","%26");
    paramsList.replaceInStrings("+","%2B");
    paramsList.replaceInStrings("#","%23");
    paramsList.replaceInStrings("\n","%0A");
    paramsList.replaceInStrings(" ","+");
    foreach(QString param, paramsList){
        QByteArray key;
        key.append(param.mid(0, param.indexOf("=")));
        QByteArray value;
        value.append(param.mid(param.indexOf("=")+1));
        temp.addEncodedQueryItem(key, value);
    }
    return temp;
}

void cvk::appendQuery(const eMethod &method, const QUrl &query){
    // ==========================================================================
    // добавляется запрос в очередь
    // все запросы должны проходить через этот метод
    // для добавления обычного запроса используйте метод "notAPI"
    // ==========================================================================

    if (status != online)
        return;

    //временная структура для заполнения очереди
    sQuery temp;
    temp.method = method;
    temp.request = query;

    queryList.enqueue(temp);

    //активируем выполнения запросов
    if (!isProcessing){
        execQuery();
    }
}

void cvk::execQuery(){
    // ==========================================================================
    // выполняем запрос в порядке очереди
    // так же соблюдаем условие 3 запр/сек
    // ==========================================================================

    if (status != online || queryList.isEmpty()){
        return;
    }

    //если после последнего запроса прошло меньше 340 мс (1000/3)
    //запускаю таймер по истечении которого снова вызовем
    //функцию выполнения запросов
    if (stopwatch->elapsed() < 340 || isProcessing){
        if (!timerRequest->isActive()){
            timerRequest->setInterval(340 - qMin(stopwatch->elapsed(),330));
            timerRequest->start();
        }
        return;
    }

    //флаг выполнения запросов
    isProcessing = true;

    //отправляем на сервер следующий запрос из очереди
    QNetworkRequest request;
    request.setUrl(queryList.head().request);
    //qDebug() << queryList.head().request;
    http->get(request);
    stopwatch->restart();
}

void cvk::slotResponse(QNetworkReply *networkReply){
    // ==========================================================================
    // пришел ответ на наш запрос
    // проверяем на ошибки
    // ==========================================================================

    if (status != online){
        networkReply->deleteLater();
        return;
    }

    if(networkReply->error() != QNetworkReply::NoError){
        //qDebug() << networkReply->errorString();
        if (networkReply->error() == QNetworkReply::UnknownNetworkError){
            networkReply->deleteLater();
            emit errorString(serverIsNotAvailable, tr("Server is not available"), true);
            return;
        } else {
            isProcessing = false;
            execQuery();
            return;
        }
    }

    QString response = networkReply->readAll();
    //qDebug() << response;
    networkReply->deleteLater();

    //проверяем на "Captcha is needed"
    if (response.contains("<error_code>14</error_code>")){
        //заполняем структуру ответом от сервера ВК
        queryList.head().result = response;
        //показываем капчу пользователю
        showCaptcha(queryList.head().result);
        return;
    }

    //фатальные ошибки
    //проверяем на "Incorrect signature"
    if (response.contains("<error_code>4</error_code>")){
        emit errorString(incorrectSignature, "Incorrect signature", true);
        return;
    }

    //проверяем на "Unknown method passed"
    if (response.contains("<error_code>3</error_code>")){
        emit errorString(unknownMethodPassed, "Unknown method passed", true);
        return;
    }

    //проверяем на "User authorization failed"
    if (response.contains("<error_code>5</error_code>")){
        emit errorString(userAuthorizationFailed, "User authorization failed", true);
        return;
    }

    isProcessing = false;

    //проверяем на "Too many requests per second"
    if (!response.contains("<error_code>6</error_code>")){
        //подмены
        response.replace(QRegExp("&(?!.{2,5};)"), "&amp;");
        response.replace("&amp;lt;br&amp;gt;","\n");
        response.replace("&amp;quot;","\"");
        //заполняем структуру ответом от сервера ВК
        queryList.head().result = response;
        //сигнал что получили и обработали данные на запрос
        emit responseQuery(queryList.dequeue());
    }

    //если в очереди запросов еще есть что выполнять, то
    //вызываем функцию выполнения запросов
    if (!queryList.isEmpty()){
        execQuery();
    }
}

void cvk::slotResponseQuery(const sQuery &query){
    // ==========================================================================
    // вызывается когда получили и обработали ответ от сервера
    // в качестве параметра структура с данными запроса и ответа на него
    // определяем на какой запрос пришел ответ и если необходимо дергаем
    // соответствующие сигналы или методы
    // ==========================================================================

    if (query.result.isEmpty())
        return;

    switch (query.method){
    case friendsGet:
        //парсим ответ и извлекаем данные друзей
        int count;
        count = getFriends(query.result);
        if (count == 0){
            offsetFriends = 0;
            emit friendsFinished();
            //получаем списки друзей
            apiFriendsGetLists();
            //получаем данные, необходимые для подключения к Long Poll серверу
            apiMessagesGetLongPollServer();
            //запрашиваем события с сайта ВК
            apiGetCounters();
            //запускаем таймер для выполнения заданий по расписанию
            if (!timerSchedule->isActive())
                timerSchedule->start(30000);
        } else {
            //запрашиваем следующую пачку друзей
            offsetFriends += 100;
            apiFriendsGet("first_name,last_name,bdate,photo_medium_rec,contacts", "", offsetFriends);
        }
        break;

    case friendsGetLists:
        //парсим ответ и извлекаем списки друзей
        getFriendsLists(query.result);
        break;

    case getLongPollServer:
        //парсим ответ и извлекаем поля "key", "server", "ts"
        getParamsLongPoll(query.result);
        //подключаемся к серверу Long Poll
        longPoll();
        break;

    case activityGet:
        //парсим ответ и извлекаем статусы друзей
        getActivity(query.result);
        break;

    case getCounters:
        //парсим ответ и извлекаем события (друзья, подарки итд)
        countersGet(query.result);
        break;

    case messagesSend:
        //парсим ответ на ошибки
        sendMsg(query.result);
        break;

    case newMessagesGet:
        //парсим ответ и извлекаем сообщения
        getNewMessages(query.result);
        break;

    case messagesGetHistory:
        //парсим ответ и извлекаем сообщения
        getMessagesHistory(query.result);
        break;

    case getProfiles:
        //парсим ответ и извлекаем профиль
        profilesGet(query.result);
        break;

    case wallPost:
        //парсим ответ на ошибки
        postWall(query.result);
        break;

    case messagesGetDialogs:
        //парсим ответ и извлекаем диалоги
        getMessagesDialogs(query.result);
        break;

    case newsfeedGet:
        //парсим ответ и извлекаем новости
        getNewsfeed(query.result);
        break;

    case wallGet:
        //парсим ответ и извлекаем записи
        getWall(query.result);
        break;

    case wallGetComments:
        //парсим ответ и извлекаем комментарии
        getWallComments(query.result);
        break;

    case photosGetAll:
        //парсим ответ и извлекаем фотографии
        getPhotosGetAll(query.result);
        break;

    case audioGet:
        //парсим ответ и извлекаем аудиозаписи
        getAudio(query.result);
        break;

    case getWallUploadServer:
        //парсим ответ и извлекаем url для загрузки фото
        wallUploadServerGet(query.result);
        break;

    case saveWallPhoto:
        //парсим ответ и извлекаем url фотографий
        wallSavePhoto(query.result);
        break;

    case likesGet:
        //парсим ответ и извлекаем количество лайков
        getLikes(query.result);
        break;

    case videoGet:
        //парсим ответ и извлекаем видео
        getVideo(query.result);
        break;

    case friendsUserGet:
        //парсим ответ и извлекаем друзей
        getFriendsUser(query.result);
        break;

    case friendsGetMutual:
        //парсим ответ и извлекаем uid друзей
        getFriendsMutual(query.result);
        break;

    case wallDelete:
        //парсим ответ и извлекаем результат удаления
        deleteWall(query.result);
        break;

    default:
        break;
    }
}

void cvk::slotTimerRequest(){
    // ==========================================================================
    // слот таймера запросов
    // ==========================================================================

    //вызываем функцию выполнения запросов
    execQuery();
}

void cvk::firstRequest(){
    // ==========================================================================
    // первый запрос к ВКонтакте API
    // ==========================================================================

    //получаем информацию о текущем пользователе
    apiGetProfiles(sessionVars.user_id, "photo_medium_rec");

    //получаем список друзей текущего пользователя
    apiFriendsGet("first_name,last_name,bdate,photo_medium_rec,contacts");

    //устанавливаем online статус на сайте
    apiActivityOnline();
}

int cvk::getFriends(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем данные друзей
    // вся подготовка контакт листа должна производится в этом методе
    // ==========================================================================

    //контакт лист
    QVector<sContact> contacts;
    sContact temp;

    //получаем контакты
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    QDomNodeList users = domElem.elementsByTagName ("user");
    contacts.resize(users.count());
    for(int i=0; i < users.count(); i++){
        QDomElement userElem = users.at(i).toElement();
        temp.lists.clear();
        temp.uid = userElem.elementsByTagName("uid").at(0).toElement().text();
        temp.firstName = userElem.elementsByTagName("first_name").at(0).toElement().text();
        temp.lastName = userElem.elementsByTagName("last_name").at(0).toElement().text();
        temp.nickName = userElem.elementsByTagName("nickname").at(0).toElement().text();
        temp.bDate = userElem.elementsByTagName("bdate").at(0).toElement().text();
        temp.avatarUrl = userElem.elementsByTagName("photo_medium_rec").at(0).toElement().text();
        temp.status = userElem.elementsByTagName("online").at(0).toElement().text();
        QDomElement lists = userElem.elementsByTagName("lists").at(0).toElement();
        QDomNodeList items = lists.elementsByTagName ("lid");
        for(int j=0; j < items.count(); j++)
            temp.lists += "," + items.at(j).toElement().text();
        temp.lists.remove(0,1);
        temp.mPhone = userElem.elementsByTagName("mobile_phone").at(0).toElement().text();
        temp.hPhone = userElem.elementsByTagName("home_phone").at(0).toElement().text();
        contacts[i] = temp;
    }

    //сигнал с контакт листом
    emit friends(contacts);

    //возвращаем количество обработанных друзей
    return contacts.count();
}

void cvk::getFriendsLists(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем списки друзей
    // ==========================================================================

    //списки
    QList<QString> lists;

    //получаем списки
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    QDomNodeList nLists = domElem.elementsByTagName ("list");
    for(int i=0; i < nLists.count(); i++){
        QDomElement listElem = nLists.at(i).toElement();
        lists.append(listElem.elementsByTagName("lid").at(0).toElement().text()+","+
                     listElem.elementsByTagName("name").at(0).toElement().text());
    }

    //сигнал со списками
    emit friendsLists(lists);
}

void cvk::getParamsLongPoll(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем поля "key", "server", "ts"
    // ==========================================================================

    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    QDomNode domNode = domElem.firstChild();
    while(!domNode.isNull()) {
        QDomElement e = domNode.toElement();
        if(!e.isNull()) {
            if (e.tagName()=="key"){
                longPollVars.key = e.text();
            } else
            if (e.tagName()=="server"){
                longPollVars.server = "http://" + e.text();
            } else
            if (e.tagName()=="ts"){
                longPollVars.ts = e.text();
            }
        }
        domNode = domNode.nextSibling();
    }
    longPollVars.wait = 25;
}

void cvk::longPoll(){
    // ==========================================================================
    // подключение к LongPoll серверу
    // ==========================================================================

    if (status != online)
        return;

    //параметры
    QString param;
    param = QString ("?act=a_check&key=%1&ts=%2&wait=%3&mode=2")
            .arg(longPollVars.key)
            .arg(longPollVars.ts)
            .arg(longPollVars.wait);

    //подключение к LongPoll серверу
    QNetworkRequest request;
    request.setUrl(QUrl(longPollVars.server.toString()+param));
    httpLongPoll->get(request);

    //запускаем таймер
    if (!timeoutLongPoll->isActive())
        timeoutLongPoll->start(longPollVars.wait*1000+65000);
}

void cvk::slotResponseLongPoll(QNetworkReply *networkReply){
    // ==========================================================================
    // пришли данные с Long Poll сервера
    // ==========================================================================

    if (status != online){
        networkReply->deleteLater();
        return;
    }

    if(networkReply->error() != QNetworkReply::NoError){
        //qDebug() << networkReply->errorString();
        longPoll();
        return;
    }

    //останавливаем таймер
    timeoutLongPoll->stop();

    QMap<QString, QVariant> response = parseJSON(networkReply->readAll()).toMap();
    //qDebug() << response;
    networkReply->deleteLater();

    if (status != online){
        return;
    }

    //проверяем на ошибки
    if (response.isEmpty() || response.contains("failed")){
        //переподключаемся
        apiMessagesGetLongPollServer();
        return;
    }

    //получаем номер cобытия
    longPollVars.ts = response.value("ts").toString();

    //получаем события
    QVariantList updates = response.value("updates").toList();
    for (int i=0; i < updates.size(); i++) {
        QVariantList update = updates.at(i).toList();
        int code = update.value(0, -1).toInt();

        switch(code){
        case lpsMessageDeleted:       //удаление сообщения с указанным message_id [0,$message_id,0]

            break;

        case lpsMessageFlagsReplaced: //замена флагов сообщения (FLAGS:=$flags) [1,$message_id,$flags]

            break;

        case lpsMessageFlagsSet:      //установка флагов сообщения (FLAGS|=$mask) [2,$message_id,$mask,$user_id]

            break;

        case lpsMessageFlagsReseted:  //сброс флагов сообщения (FLAGS&=~$mask) [3,$message_id,$mask,$user_id]
            longPollResetMessageFlags(update);
            break;

        case lpsMessageAdded:         //добавление нового сообщения [4,$message_id,$flags,$from_id,$timestamp,"$subject","$text"]
            longPollMessageReceived(update);
            break;

        case lpsUserOnline:           //друг стал онлайн [8,-$user_id,0]
            longPollUserOnline(update);
            break;

        case lpsUserOffline:          //друг стал оффлайн [9,-$user_id,$flags]
            longPollUserOffline(update);
            break;

        case lpsUserTyping:           //друг стал набирать текст [61,$user_id,$flags]
            longPollUserTyping(update);
            break;

        default:
            break;
        }
    }

    longPoll();
}

void cvk::longPollResetMessageFlags(QVariantList &update){
    // ==========================================================================
    // сброс флагов сообщения
    // $message_id,$mask,$user_id
    // ==========================================================================

    QString mid = update.value(1).toString();
    int mask = update.value(2).toInt();
    QString uid = update.value(3).toString();
    emit resetMessageFlags(mid, mask, uid);
}

void cvk::longPollMessageReceived(QVariantList &update){
    // ==========================================================================
    // добавление нового сообщения
    // $message_id,$flags,$from_id,$timestamp,"$subject","$text"
    // ==========================================================================

    sMessage message;
    //идентификатор сообщения
    message.mid = update.value(1).toString();
    //флаг сообещния
    message.flags = update.value(2).toInt();
    //идентификатор отправителя
    message.fromId = update.value(3).toString();
    //дата время сообщения
    message.dateTime = QDateTime::fromTime_t(update.value(4).toLongLong());
    //тема сообщения
    message.subject = update.value(5).toString();
    //текст сообщения
    message.text = update.value(6).toString();

    QTextDocument textDocument;
    textDocument.setHtml(message.text);
    message.text = textDocument.toPlainText();
    message.text.replace("<br>","\n");
    message.text.replace("\\\\","\\");
    message.text.replace("\\/","/");

    emit messageReceived(message);

    //прикрепления да простят меня за трафик
    if (!update.value(7).toMap().isEmpty() && message.fromId != sessionVars.user_id){
        apiMessagesGetHistory(message.fromId, 3, 0);
    }
}

void cvk::longPollUserOnline(QVariantList &update){
    // ==========================================================================
    // друг стал онлайн -$user_id,0
    // ==========================================================================

    QString uid = update.value(1).toString().remove("-");
    emit userOnline(uid);
}

void cvk::longPollUserOffline(QVariantList &update){
    // ==========================================================================
    // друг стал оффлайн -$user_id,$flags
    // $flags: 0 - пользователь покинул сайт (например, нажал выход)
    //         1 - оффлайн по таймауту (например, статус away)
    // ==========================================================================

    QString uid = update.value(1).toString().remove("-");
    int flag = update.value(2).toInt();
    emit userOffline(uid, flag);
}

void cvk::longPollUserTyping(QVariantList &update){
    // ==========================================================================
    // друг начал набирать текст -$user_id,$flags
    // $flags = 1
    // ==========================================================================

    QString uid = update.value(1).toString().remove("-");
    int flag = update.value(2).toInt();
    emit userTyping(uid, flag);
}

void cvk::showCaptcha(const QString &result){
    // ==========================================================================
    // парсим xml с ошибкой "Captcha needed" получаем "captcha_sid",
    // "captcha_img" и показываем капчу пользователю
    // ==========================================================================

    //получаем "captcha_sid" и "captcha_img"
    QString captchaSid;
    QString captchaImg;
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    QDomNode domNode = domElem.firstChild();
    while(!domNode.isNull()) {
        QDomElement e = domNode.toElement();
        if(!e.isNull()) {
            if (e.tagName()=="captcha_sid"){
                captchaSid = e.text();
            } else
            if (e.tagName()=="captcha_img"){
                captchaImg = e.text();
            }
        }
        domNode = domNode.nextSibling();
    }

    //показываем капчу пользователю
    emit captcha(captchaSid, captchaImg);
}

void cvk::captchaDone(const QString &captcha_sid, const QString &captcha_key){
    // ==========================================================================
    // пользователь ввел капчу
    // ==========================================================================

    //если пользователь не ввел текст капчи либо просто закрыл окно
    if (captcha_key.isEmpty()){
        //удаляем этот запрос
        queryList.dequeue();
        isProcessing = false;
        execQuery();
        return;
    }

    QUrl query = queryList.head().request;

    //если капча уже есть в параметрах то удаляем
    if (query.queryItemValue("captcha_sid") != ""){
        query.removeAllQueryItems("captcha_sid");
        query.removeAllQueryItems("captcha_key");
    }

    //добавляем к параметрам запроса капчу
    query.addQueryItem("captcha_sid", captcha_sid);
    query.addQueryItem("captcha_key", captcha_key);

    //устанавливаем новые параметры на этот запрос
    queryList.head().request = query;

    //выполняем повторно запрос
    isProcessing = false;
    execQuery();
}

void cvk::uploadFile(const QUrl &url, const QString &fileName, const QString &field){
    // ==========================================================================
    // загрузка файла на указанный url метод POST файл помещается в поле field
    // ==========================================================================

    QString FileName = fileName;
    FileName = FileName.remove("file://");

    if (!QFile::exists(FileName))
        return;

    QFileInfo fileInfo(FileName);

    QNetworkAccessManager httpUploader;
    QEventLoop *loop = new QEventLoop();

    QNetworkRequest request;
    request.setUrl(url);
    QString boundary = "---------------------------193971182219750";
    QByteArray datas(QString("--" + boundary + "\r\n").toAscii());
    datas += "Content-Disposition: form-data; name=\""+field+"\"; filename=\""+fileInfo.fileName()+"\"\r\n";
    datas += "Content-Type: image/"+fileInfo.suffix().toLower()+"\r\n\r\n";

    QImage img(FileName);
    if (wallPhotoRotation){
        QMatrix mat;
        mat.rotate(wallPhotoRotation);
        img = img.transformed(mat, Qt::SmoothTransformation);
    }
    if (qMax(img.size().width(), img.size().height()) > 1024)
        img = img.scaled(QSize(1024, 1024), Qt::KeepAspectRatio, Qt::SmoothTransformation);
    QByteArray byteArray;
    QBuffer buffer(&byteArray);
    buffer.open(QIODevice::ReadWrite);
    img.save(&buffer, fileInfo.suffix().toUpper().toAscii().data());

    datas += byteArray;
    datas += "\r\n";
    datas += QString("--" + boundary + "\r\n").toAscii();
    datas += "Content-Disposition: form-data; name=\"upload\"\r\n\r\n";
    datas += "Uploader\r\n";
    datas += QString("--" + boundary + "--\r\n").toAscii();

    request.setRawHeader("POST", QByteArray().append(url.path() + "?" + url.encodedQuery()));
    request.setRawHeader("Host", QByteArray().append(url.host()));
    request.setRawHeader("Content-Type", QByteArray().append("multipart/form-data; boundary=" + boundary));
    request.setRawHeader("Content-Length", QByteArray().append(QString::number(datas.length())));

    QNetworkReply *reply = httpUploader.post(request, datas);
    QObject::connect(reply, SIGNAL(finished()), loop, SLOT(quit()));

    loop->exec();

    delete loop;
    if(reply->error() != QNetworkReply::NoError){
        //qDebug() << reply->errorString();
        return;
    }
    QVariant response = parseJSON(reply->readAll());
    reply->deleteLater();

    //парсим ответ
    QString server = response.toMap().value("server").toString();
    QString hash = response.toMap().value("hash").toString();
    QString photo = response.toMap().value("photo").toString();

    //получаем данные о загруженной фотографии
    apiPhotosSaveWallPhoto(server, photo, hash, wallUidTemp);
}

void cvk::slotTimeoutLongPoll(){
    // ==========================================================================
    // соединение с LongPoll потеряно, вероятнее всего пропал интернет
    // ==========================================================================

    timeoutLongPoll->stop();
    emit errorString(timeoutLongPollServer, tr("Server is not available"), true);
}

void cvk::slotTimerSchedule(){
    // ==========================================================================
    // слот таймера для выполнения заданий по расписанию
    // все задания выполняющие по циклу должны запускаться
    // только в этом методе
    // ==========================================================================

    if (status != online)
        return;

    //запрашиваем события с сайта ВК
    apiGetCounters();

    //поддержка онлайн статуса на сайте ВК (каждые 5 минут)
    static int k = 0;
    if (k == 10) {
        apiActivityOnline();
        k = 0;
    } else k++;
}

void cvk::getActivity(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем статусы друзей
    // ==========================================================================

    //статусы
    QList<sActivity> m_activity;
    sActivity temp;

    //получаем статусы
    QStringList uids;
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    QDomNode domNode = domElem.firstChild();
    while(!domNode.isNull()) {
        QDomElement e = domNode.toElement();
        if(!e.isNull())
            if (e.tagName()=="uids"){
                uids << e.text().split(",");
                break;
            }
        domNode = domNode.nextSibling();
    }
    QDomNodeList item = domElem.elementsByTagName ("item");
    for(int i=0; i < item.count(); i++){
        QDomElement userElem = item.at(i).toElement();
        temp.uid = uids.at(i);
        temp.activity = userElem.elementsByTagName("text").at(0).toElement().text();
        m_activity.append(temp);
    }

    //сигнал со статусами
    emit activity(m_activity);
}

void cvk::countersGet(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем события (друзья, подарки итд)
    // ==========================================================================

    //события сайта ВК
    sCounters Counters;

    //получаем события "friends" "gifts" итд
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    Counters.friends = domElem.elementsByTagName("friends").at(0).toElement().text().toInt();
    Counters.photos = domElem.elementsByTagName("photos").at(0).toElement().text().toInt();
    Counters.videos = domElem.elementsByTagName("videos").at(0).toElement().text().toInt();
    Counters.gifts = domElem.elementsByTagName("gifts").at(0).toElement().text().toInt();
    Counters.notes = domElem.elementsByTagName("notes").at(0).toElement().text().toInt();
    Counters.events = domElem.elementsByTagName("events").at(0).toElement().text().toInt();
    Counters.groups = domElem.elementsByTagName("groups").at(0).toElement().text().toInt();
    Counters.offers = domElem.elementsByTagName("offers").at(0).toElement().text().toInt();
    Counters.opinions = domElem.elementsByTagName("opinions").at(0).toElement().text().toInt();
    Counters.questions = domElem.elementsByTagName("questions").at(0).toElement().text().toInt();

    emit counters(Counters);
}

void cvk::sendMsg(const QString &result){
    // ==========================================================================
    // парсим ответ на ошибки
    // ==========================================================================

    //проверяем на "Permission to perform this action is denied by user"
    if (result.contains("<error_code>7</error_code>")){
        QString uid;
        QString message;
        QDomDocument domDoc;
        domDoc.setContent(result);
        QDomElement domElem = domDoc.documentElement();
        QDomNodeList params = domElem.elementsByTagName ("param");
        for(int i=0; i < params.count(); i++){
            QDomElement listElem = params.at(i).toElement();
            if (listElem.elementsByTagName("key").at(0).toElement().text() == "message")
                message = listElem.elementsByTagName("value").at(0).toElement().text();
            else if (listElem.elementsByTagName("key").at(0).toElement().text() == "uid")
                uid = listElem.elementsByTagName("value").at(0).toElement().text();
        }

        if (!uid.isEmpty())
            emit sendMessagesError(7, uid, message);
    }
}

void cvk::getNewMessages(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем сообщения
    // ==========================================================================

    //сообщение
    sMessage message;

    //получаем сообщения
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    QDomNodeList msg = domElem.elementsByTagName ("message");
    for(int i=0; i < msg.count(); i++){
        QDomElement messageElem = msg.at(i).toElement();
        message.mid = messageElem.elementsByTagName("mid").at(0).toElement().text();
        message.flags = 1;
        message.fromId = messageElem.elementsByTagName("uid").at(0).toElement().text();
        message.dateTime = QDateTime::fromTime_t(messageElem.elementsByTagName("date").at(0).toElement().text().toLongLong());
        message.subject = messageElem.elementsByTagName("title").at(0).toElement().text();
        message.text = messageElem.elementsByTagName("body").at(0).toElement().text();
        QTextDocument textDocument;
        textDocument.setHtml(message.text);
        message.text = textDocument.toPlainText();
        message.text.replace("<br>","\n");
        message.text.replace("\\\\","\\");
        message.text.replace("\\/","/");
        emit messageReceived(message);
    }
}

void cvk::getMessagesHistory(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем сообщения
    // ==========================================================================

    //сообщения
    QList<sMessage> messages;

    //пользователь с кем история
    QString uid;

    //получаем сообщения
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    QDomNode domNode = domElem.firstChild();
    while(!domNode.isNull()) {
        QDomElement e = domNode.toElement();
        if(!e.isNull())
            if (e.tagName()=="uid"){
                uid = e.text();
                break;
            }
        domNode = domNode.nextSibling();
    }
    QDomNodeList msg = domElem.elementsByTagName ("item");
    for(int i=0; i < msg.count(); i++){
        sMessage temp;
        QDomElement messageElem = msg.at(i).toElement();
        temp.mid = messageElem.elementsByTagName("mid").at(0).toElement().text();
        temp.fromId = messageElem.elementsByTagName("uid").at(0).toElement().text();
        temp.flags = messageElem.elementsByTagName("read_state").at(0).toElement().text().toInt() ? 0 : 1;
        if (temp.fromId == sessionVars.user_id) temp.flags += 2;
        temp.dateTime = QDateTime::fromTime_t(messageElem.elementsByTagName("date").at(0).toElement().text().toLongLong());
        temp.subject = " ... ";
        temp.text = messageElem.elementsByTagName("body").at(0).toElement().text();
        QDomNodeList attachments = messageElem.elementsByTagName("attachments");
        attachments = attachments.at(0).toElement().elementsByTagName("item");
        for(int i=0; i < attachments.count(); i++){
            QDomElement attachElem = attachments.at(i).toElement();
            QString typeAttach = attachElem.elementsByTagName("type").at(0).toElement().text();

            if(typeAttach == "photo"){
                temp.photos += attachElem.elementsByTagName("src").at(0).toElement().text() + ",";
                temp.photosBig += attachElem.elementsByTagName("src_big").at(0).toElement().text() + ",";
            }

            if(typeAttach == "audio"){
                sAudio audio;
                audio.aid = attachElem.elementsByTagName("aid").at(0).toElement().text();
                audio.ownerId = attachElem.elementsByTagName("owner_id").at(0).toElement().text();
                audio.artist = attachElem.elementsByTagName("performer").at(0).toElement().text();
                audio.title = attachElem.elementsByTagName("title").at(0).toElement().text();
                audio.duration = attachElem.elementsByTagName("duration").at(0).toElement().text().toInt();
                audio.url = attachElem.elementsByTagName("url").at(0).toElement().text();
                temp.audio.append(audio);
            }

            if(typeAttach == "video" && temp.text.isEmpty()){
                temp.text = "Video: " + attachElem.elementsByTagName("title").at(0).toElement().text();
            }

            if(typeAttach == "doc" && temp.text.isEmpty()){
                temp.text = "Doc: " + attachElem.elementsByTagName("title").at(0).toElement().text();
            }
        }
        temp.photos.remove(temp.photos.count() - 1, 1);
        temp.photosBig.remove(temp.photosBig.count() - 1, 1);

        if (!temp.mid.isEmpty())
            messages.append(temp);
    }
    if (!messages.isEmpty()){
        //сигнал с сообщениями
        emit history(uid, messages);
    }
}

void cvk::profilesGet(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем профиль
    // ==========================================================================

    //профиль
    sProfile Profile;

    //получаем профиль
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    QDomNodeList users = domElem.elementsByTagName ("user");
    for(int i=0; i < users.count(); i++){
        QDomElement userElem = users.at(i).toElement();
        Profile.uid = userElem.elementsByTagName("uid").at(0).toElement().text();
        Profile.firstName = userElem.elementsByTagName("first_name").at(0).toElement().text();
        Profile.lastName = userElem.elementsByTagName("last_name").at(0).toElement().text();
        Profile.nickName = userElem.elementsByTagName("nickname").at(0).toElement().text();
        Profile.activity = userElem.elementsByTagName("activity").at(0).toElement().text();
        Profile.sex = userElem.elementsByTagName("sex").at(0).toElement().text().toInt();
        QStringList tempDate = userElem.elementsByTagName("bdate").at(0).toElement().text().split(".");
        if (tempDate.count() == 3)
            Profile.bDate = QDate(tempDate.at(2).toInt(), tempDate.at(1).toInt(), tempDate.at(0).toInt()).toString("d MMMM yyyy");
        else if (tempDate.count() == 2)
            Profile.bDate = QDate(1800, tempDate.at(1).toInt(), tempDate.at(0).toInt()).toString("d MMMM");
        Profile.photoRectUrl = userElem.elementsByTagName("photo_medium_rec").at(0).toElement().text();
        Profile.photoBigUrl = userElem.elementsByTagName("photo_big").at(0).toElement().text();
        Profile.status = userElem.elementsByTagName("online").at(0).toElement().text();
        Profile.mPhone = userElem.elementsByTagName("mobile_phone").at(0).toElement().text();
        Profile.hPhone = userElem.elementsByTagName("home_phone").at(0).toElement().text();
        Profile.univerName = userElem.elementsByTagName("university_name").at(0).toElement().text();
        Profile.facultyName = userElem.elementsByTagName("faculty_name").at(0).toElement().text();
        Profile.friendsCount = userElem.elementsByTagName("friends").at(0).toElement().text().toInt();
        Profile.mutualFriendsCount = userElem.elementsByTagName("mutual_friends").at(0).toElement().text().toInt();
        Profile.photosCount = userElem.elementsByTagName("photos").at(0).toElement().text().toInt();
        Profile.videosCount = userElem.elementsByTagName("videos").at(0).toElement().text().toInt();
        Profile.audiosCount = userElem.elementsByTagName("audios").at(0).toElement().text().toInt();
        Profile.canPost = userElem.elementsByTagName("can_post").at(0).toElement().text().toInt();
        Profile.canWriteMessage = userElem.elementsByTagName("can_write_private_message").at(0).toElement().text().toInt();
        Profile.lastSeen = QDateTime::fromTime_t(userElem.elementsByTagName("last_seen").at(0).toElement().text().toLongLong());
    }

    //сигнал с профилем
    if (Profile.uid == sessionVars.user_id)
        emit profileSelf(Profile);
    else
        emit profile(Profile);
}

void cvk::postWall(const QString &result){
    // ==========================================================================
    // парсим ответ на ошибки
    // ==========================================================================

    //проверяем на "Permission to perform this action is denied by user"
    //и "Access to adding post denied"
    if (result.contains("<error_code>7</error_code>") || result.contains("<error_code>214</error_code>")){
        QString uid;
        QString message;
        QDomDocument domDoc;
        domDoc.setContent(result);
        QDomElement domElem = domDoc.documentElement();
        QDomNodeList params = domElem.elementsByTagName ("param");
        for(int i=0; i < params.count(); i++){
            QDomElement listElem = params.at(i).toElement();
            if (listElem.elementsByTagName("key").at(0).toElement().text() == "message")
                message = listElem.elementsByTagName("value").at(0).toElement().text();
            else if (listElem.elementsByTagName("key").at(0).toElement().text() == "owner_id")
                uid = listElem.elementsByTagName("value").at(0).toElement().text();
        }

        if (!uid.isEmpty()){
            emit sendMessagesError(7, uid, message);
            emit wallPostFinished(uid, false);
        }

        return;
    }

    emit wallRefresh(wallUidTemp);
    emit wallPostFinished(wallUidTemp, true);
}

void cvk::getMessagesDialogs(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем диалоги
    // ==========================================================================

    QVector<sDialog> Dialogs;
    sDialog temp;

    QMap<QString, sProfile> Profiles;
    sProfile profile;

    //получаем диалоги
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    QDomNodeList dialogsList = domElem.elementsByTagName ("dialogs");
    QDomElement messageElem = dialogsList.at(0).toElement();
    QDomNodeList profilesList = domElem.elementsByTagName ("profiles");
    QDomElement profileElem = profilesList.at(0).toElement();
    QDomNodeList prof = profileElem.elementsByTagName ("item");
    for(int i=0; i < prof.count(); i++){
        QDomElement profElem = prof.at(i).toElement();
        profile.uid = profElem.elementsByTagName("uid").at(0).toElement().text();
        profile.firstName = profElem.elementsByTagName("first_name").at(0).toElement().text();
        profile.lastName = profElem.elementsByTagName("last_name").at(0).toElement().text();
        profile.photoRectUrl = profElem.elementsByTagName("photo_medium_rec").at(0).toElement().text();
        Profiles[profile.uid] = profile;
    }
    QDomElement elt = messageElem.firstChildElement("item");
    elt = elt.nextSiblingElement("item");
    for (; !elt.isNull(); elt = elt.nextSiblingElement("item")) {
        temp.body = elt.elementsByTagName("body").at(0).toElement().text();
        temp.title = elt.elementsByTagName("title").at(0).toElement().text();
        temp.date = QDateTime::fromTime_t(elt.elementsByTagName("date").at(0).toElement().text().toLongLong());
        temp.uid = elt.elementsByTagName("uid").at(0).toElement().text();
        temp.mid = elt.elementsByTagName("mid").at(0).toElement().text();
        temp.readState = elt.elementsByTagName("read_state").at(0).toElement().text().toInt();
        temp.out = elt.elementsByTagName("out").at(0).toElement().text().toInt();
        temp.chatId = elt.elementsByTagName("chat_id").at(0).toElement().text();
        temp.firstName = Profiles[temp.uid].firstName;
        temp.lastName = Profiles[temp.uid].lastName;
        temp.avatarUrl = Profiles[temp.uid].photoRectUrl.toString();
        if (temp.chatId.isEmpty()) //чаты пока недоступны
            Dialogs.append(temp);
    }

    //сигнал с диалогами
    emit dialogs(Dialogs);
}

void cvk::getNewsfeed(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем новости
    // ==========================================================================

    QVector<sNew> News;

    QMap<QString, sProfile> Profiles;
    sProfile profile;

    QString newFrom;

    //получаем новости
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    QDomNodeList items = domElem.elementsByTagName ("item");
    QDomNodeList profiles = domElem.elementsByTagName ("user");
    QDomNodeList groups = domElem.elementsByTagName ("group");
    QDomNodeList new_from = domElem.elementsByTagName ("new_from");
    QDomElement newsFromElem = new_from.at(0).toElement();
    newFrom = newsFromElem.text();
    for(int i=0; i < profiles.count(); i++){
        QDomElement profElem = profiles.at(i).toElement();
        profile.uid = profElem.elementsByTagName("uid").at(0).toElement().text();
        profile.firstName = profElem.elementsByTagName("first_name").at(0).toElement().text();
        profile.lastName = profElem.elementsByTagName("last_name").at(0).toElement().text();
        profile.photoRectUrl = profElem.elementsByTagName("photo_medium_rec").at(0).toElement().text();
        Profiles[profile.uid] = profile;
    }
    for(int i=0; i < groups.count(); i++){
        QDomElement groupElem = groups.at(i).toElement();
        profile.uid = "-" + groupElem.elementsByTagName("gid").at(0).toElement().text();
        profile.firstName = groupElem.elementsByTagName("name").at(0).toElement().text();
        profile.lastName = "";
        profile.photoRectUrl = groupElem.elementsByTagName("photo_medium").at(0).toElement().text();
        Profiles[profile.uid] = profile;
    }
    News.resize(items.count());
    for(int i=0; i < items.count(); i++){
        sNew temp;
        QDomElement newElem = items.at(i).toElement();
        temp.postId.clear();
        temp.text.clear();
        temp.photos.clear();
        temp.photosBig.clear();
        temp.photosWidth.clear();
        temp.photosHeight.clear();
        temp.copyOwner.clear();
        temp.linkUrl.clear();
        temp.type = newElem.elementsByTagName("type").at(0).toElement().text();
        temp.sourceId = newElem.elementsByTagName("source_id").at(0).toElement().text();
        temp.date = QDateTime::fromTime_t(newElem.elementsByTagName("date").at(0).toElement().text().toLongLong());

        if (temp.type == "post"){
            temp.postId = newElem.elementsByTagName("post_id").at(0).toElement().text();
            temp.copyOwnerId = newElem.elementsByTagName("copy_owner_id").at(0).toElement().text();
            if (!temp.copyOwnerId.isEmpty())
                temp.copyOwner = Profiles[temp.copyOwnerId].firstName + " " + Profiles[temp.copyOwnerId].lastName;
            temp.copyPostId = newElem.elementsByTagName("copy_post_id").at(0).toElement().text();
            temp.text = newElem.elementsByTagName("text").at(0).toElement().text();
            temp.geo = newElem.elementsByTagName("geo").at(0).toElement().text();
            QDomElement likes = newElem.elementsByTagName("likes").at(0).toElement();
            temp.likes = likes.elementsByTagName("count").at(0).toElement().text();
            temp.isLiked = likes.elementsByTagName("user_likes").at(0).toElement().text().toInt();
            QDomElement comments = newElem.elementsByTagName("comments").at(0).toElement();
            temp.comments = comments.elementsByTagName("count").at(0).toElement().text();
            temp.canPost = comments.elementsByTagName("can_post").at(0).toElement().text().toInt();
            QDomNodeList attachments = newElem.elementsByTagName("attachments");
            attachments = attachments.at(0).toElement().elementsByTagName("attachment");
            for(int i=0; i < attachments.count(); i++){
                QDomElement attachElem = attachments.at(i).toElement();
                QString typeAttach = attachElem.elementsByTagName("type").at(0).toElement().text();

                if(typeAttach == "audio" && temp.text.isEmpty()){
                    temp.text = "Audio: " + attachElem.elementsByTagName("performer").at(0).toElement().text() +
                            " - " + attachElem.elementsByTagName("title").at(0).toElement().text();
                }

                if(typeAttach == "video" && temp.text.isEmpty()){
                    temp.text = "Video: " + attachElem.elementsByTagName("title").at(0).toElement().text();
                }

                if(typeAttach == "doc" && temp.text.isEmpty()){
                    temp.text = "Doc: " + attachElem.elementsByTagName("title").at(0).toElement().text();
                }

                if(typeAttach == "photo" || typeAttach == "posted_photo" || typeAttach == "graffiti" || typeAttach == "app"){
                    temp.photos.append(attachElem.elementsByTagName("src").at(0).toElement().text());
                    temp.photosBig.append(attachElem.elementsByTagName("src_big").at(0).toElement().text());
                    int width = attachElem.elementsByTagName("width").at(0).toElement().text().toInt();
                    int height = attachElem.elementsByTagName("height").at(0).toElement().text().toInt();
                    if (width == 0){
                        temp.photosWidth.append(0);
                        temp.photosHeight.append(0);
                    } else if (width > height){
                        temp.photosWidth.append(130);
                        temp.photosHeight.append(130 * height / width);
                    } else {
                        temp.photosWidth.append(130 * width / height);
                        temp.photosHeight.append(130);
                    }
                }

                if(typeAttach == "link"){
                    if (temp.text.isEmpty()){
                        temp.text = attachElem.elementsByTagName("title").at(0).toElement().text();
                    }
                    temp.linkUrl = attachElem.elementsByTagName("url").at(0).toElement().text();
                }
            }
        } else if (temp.type == "photo"){
            QDomNodeList photos = newElem.elementsByTagName("photos");
            photos = photos.at(0).toElement().elementsByTagName("photo");
            for(int i=0; i < photos.count(); i++){
                QDomElement photoElem = photos.at(i).toElement();
                temp.photos.append(photoElem.elementsByTagName("src").at(0).toElement().text());
                temp.photosBig.append(photoElem.elementsByTagName("src_big").at(0).toElement().text());
                int width = photoElem.elementsByTagName("width").at(0).toElement().text().toInt();
                int height = photoElem.elementsByTagName("height").at(0).toElement().text().toInt();
                if (width == 0){
                    temp.photosWidth.append(0);
                    temp.photosHeight.append(0);
                } else if (width > height){
                    temp.photosWidth.append(130);
                    temp.photosHeight.append(130 * height / width);
                } else {
                    temp.photosWidth.append(130 * width / height);
                    temp.photosHeight.append(130);
                }
            }
        } else if (temp.type == "photo_tag"){
            QDomNodeList photosTag = newElem.elementsByTagName("photo_tags");
            photosTag = photosTag.at(0).toElement().elementsByTagName("photo_tag");
            for(int i=0; i < photosTag.count(); i++){
                QDomElement photoElem = photosTag.at(i).toElement();
                temp.photos.append(photoElem.elementsByTagName("src").at(0).toElement().text());
                temp.photosBig.append(photoElem.elementsByTagName("src_big").at(0).toElement().text());
                int width = photoElem.elementsByTagName("width").at(0).toElement().text().toInt();
                int height = photoElem.elementsByTagName("height").at(0).toElement().text().toInt();
                if (width == 0){
                    temp.photosWidth.append(0);
                    temp.photosHeight.append(0);
                } else if (width > height){
                    temp.photosWidth.append(130);
                    temp.photosHeight.append(130 * height / width);
                } else {
                    temp.photosWidth.append(130 * width / height);
                    temp.photosHeight.append(130);
                }
            }
        }

        temp.firstName = Profiles[temp.sourceId].firstName;
        temp.lastName = Profiles[temp.sourceId].lastName;
        temp.avatarUrl = Profiles[temp.sourceId].photoRectUrl.toString();
        QTextDocument textDocument;
        textDocument.setHtml(temp.text);
        temp.text = textDocument.toPlainText();
        temp.text.replace("<br>","\n");
        temp.text.replace("\\\\","\\");
        temp.text.replace("\\/","/");
        News[i] = temp;
    }

    //сигнал с новостями
    emit news(News, newFrom);
}

void cvk::getWall(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем записи со стены
    // ==========================================================================

    QVector<sWall> Wall;
    QString owner_id;

    QMap<QString, sProfile> Profiles;
    sProfile profile;

    //получаем записи
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    owner_id = domElem.elementsByTagName ("uidOwner").at(0).toElement().text();
    QDomNodeList wallList = domElem.elementsByTagName ("wall");
    QDomElement messageElem = wallList.at(0).toElement();
    QDomNodeList profilesList = domElem.elementsByTagName ("profiles");
    QDomElement profileElem = profilesList.at(0).toElement();
    QDomNodeList prof = profileElem.elementsByTagName ("item");
    for(int i=0; i < prof.count(); i++){
        QDomElement profElem = prof.at(i).toElement();
        profile.uid = profElem.elementsByTagName("uid").at(0).toElement().text();
        profile.firstName = profElem.elementsByTagName("first_name").at(0).toElement().text();
        profile.lastName = profElem.elementsByTagName("last_name").at(0).toElement().text();
        profile.photoRectUrl = profElem.elementsByTagName("photo_medium_rec").at(0).toElement().text();
        Profiles[profile.uid] = profile;
    }
    QDomElement elt = messageElem.firstChildElement("item");
    elt = elt.nextSiblingElement("item");
    for (; !elt.isNull(); elt = elt.nextSiblingElement("item")) {
        sWall temp;
        temp.text.clear();
        temp.photos.clear();
        temp.photosBig.clear();
        temp.linkUrl.clear();
        temp.id = elt.elementsByTagName("id").at(0).toElement().text();
        temp.toId = elt.elementsByTagName("to_id").at(0).toElement().text();
        temp.fromId = elt.elementsByTagName("from_id").at(0).toElement().text();
        temp.date = QDateTime::fromTime_t(elt.elementsByTagName("date").at(0).toElement().text().toLongLong());
        temp.text = elt.elementsByTagName("text").at(0).toElement().text();
        QDomElement likes = elt.elementsByTagName("likes").at(0).toElement();
        temp.likes = likes.elementsByTagName("count").at(0).toElement().text();
        temp.isLiked = likes.elementsByTagName("user_likes").at(0).toElement().text().toInt();
        QDomElement comments = elt.elementsByTagName("comments").at(0).toElement();
        temp.comments = comments.elementsByTagName("count").at(0).toElement().text();
        temp.canPost = comments.elementsByTagName("can_post").at(0).toElement().text().toInt();
        QDomNodeList attachments = elt.elementsByTagName("attachments");
        attachments = attachments.at(0).toElement().elementsByTagName("item");
        for(int i=0; i < attachments.count(); i++){
            QDomElement attachElem = attachments.at(i).toElement();
            QString typeAttach = attachElem.elementsByTagName("type").at(0).toElement().text();

            if(typeAttach == "audio" && temp.text.isEmpty()){
                temp.text = "Audio: " + attachElem.elementsByTagName("performer").at(0).toElement().text() +
                                " - " + attachElem.elementsByTagName("title").at(0).toElement().text();
            }

            if(typeAttach == "video" && temp.text.isEmpty()){
                temp.text = "Video: " + attachElem.elementsByTagName("title").at(0).toElement().text();
            }

            if(typeAttach == "doc" && temp.text.isEmpty()){
                temp.text = "Doc: " + attachElem.elementsByTagName("title").at(0).toElement().text();
            }

            if(typeAttach == "photo" || typeAttach == "posted_photo" || typeAttach == "graffiti" || typeAttach == "app"){
                temp.photos += attachElem.elementsByTagName("src").at(0).toElement().text() + ",";
                temp.photosBig += attachElem.elementsByTagName("src_big").at(0).toElement().text() + ",";
            }

            if(typeAttach == "link"){
                temp.photos += attachElem.elementsByTagName("image_src").at(0).toElement().text() + ",";
                temp.photosBig += attachElem.elementsByTagName("image_src").at(0).toElement().text() + ",";
                temp.linkUrl = attachElem.elementsByTagName("url").at(0).toElement().text();
            }
        }
        temp.photos.remove(temp.photos.count() - 1, 1);
        temp.photosBig.remove(temp.photosBig.count() - 1, 1);
        temp.geo = elt.elementsByTagName("geo").at(0).toElement().text();
        temp.copyOwnerId = elt.elementsByTagName("copy_owner_id").at(0).toElement().text().toInt();
        temp.copyPostId = elt.elementsByTagName("copy_post_id").at(0).toElement().text().toInt();
        temp.firstName = Profiles[temp.fromId].firstName;
        temp.lastName = Profiles[temp.fromId].lastName;
        temp.avatarUrl = Profiles[temp.fromId].photoRectUrl.toString();
        Wall.append(temp);
    }

    //сигнал с диалогами
    emit wall(Wall, owner_id);
}

void cvk::getWallComments(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем комментарии
    // ==========================================================================


    //комментарии
    QVector<sWallComments> Comments;
    sWallComments temp;

    QMap<QString, sProfile> Profiles;
    sProfile profile;

    //получаем записи
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    QDomNodeList wallList = domElem.elementsByTagName ("wall");
    QDomElement messageElem = wallList.at(0).toElement();
    QDomNodeList profilesList = domElem.elementsByTagName ("profiles");
    QDomElement profileElem = profilesList.at(0).toElement();
    QDomNodeList prof = profileElem.elementsByTagName ("item");
    for(int i=0; i < prof.count(); i++){
        QDomElement profElem = prof.at(i).toElement();
        profile.uid = profElem.elementsByTagName("uid").at(0).toElement().text();
        profile.firstName = profElem.elementsByTagName("first_name").at(0).toElement().text();
        profile.lastName = profElem.elementsByTagName("last_name").at(0).toElement().text();
        profile.photoRectUrl = profElem.elementsByTagName("photo_medium_rec").at(0).toElement().text();
        Profiles[profile.uid] = profile;
    }
    QDomElement elt = messageElem.firstChildElement("item");
    elt = elt.nextSiblingElement("item");
    for (; !elt.isNull(); elt = elt.nextSiblingElement("item")) {
        temp.cid = elt.elementsByTagName("cid").at(0).toElement().text();
        temp.uid = elt.elementsByTagName("uid").at(0).toElement().text();
        temp.date = QDateTime::fromTime_t(elt.elementsByTagName("date").at(0).toElement().text().toLongLong());
        temp.text = elt.elementsByTagName("text").at(0).toElement().text();
        temp.replyToUid = elt.elementsByTagName("reply_to_uid").at(0).toElement().text();
        temp.replyToCid = elt.elementsByTagName("reply_to_сid").at(0).toElement().text();
        temp.firstName = Profiles[temp.uid].firstName;
        temp.lastName = Profiles[temp.uid].lastName;
        temp.avatarUrl = Profiles[temp.uid].photoRectUrl.toString();
        Comments.append(temp);
    }

    //сигнал с диалогами
    emit wallComments(Comments);
}

void cvk::getPhotosGetAll(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем фотографии
    // ==========================================================================

    //фотографии
    QVector<sPhoto> Photos;
    sPhoto temp;

    //получаем фотографии
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    QDomNodeList ph = domElem.elementsByTagName ("photo");
    for(int i=0; i < ph.count(); i++){
        QDomElement photoElem = ph.at(i).toElement();
        temp.pid = photoElem.elementsByTagName("pid").at(0).toElement().text();
        temp.aid = photoElem.elementsByTagName("aid").at(0).toElement().text();
        temp.owner_id = photoElem.elementsByTagName("owner_id").at(0).toElement().text();
        temp.created = QDateTime::fromTime_t(photoElem.elementsByTagName("created").at(0).toElement().text().toLongLong());
        temp.src = photoElem.elementsByTagName("src").at(0).toElement().text();
        temp.src_big = photoElem.elementsByTagName("src_big").at(0).toElement().text();
        temp.src_small = photoElem.elementsByTagName("src_small").at(0).toElement().text();
        Photos.append(temp);
    }

    emit photos(Photos);
}

void cvk::getAudio(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем url аудиозаписей
    // ==========================================================================

    //аудиозаписи
    QVector<sAudio> Audio;
    sAudio temp;

    //получаем аудиозаписи
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    QDomNodeList au = domElem.elementsByTagName ("audio");
    for(int i=0; i < au.count(); i++){
        QDomElement audioElem = au.at(i).toElement();
        temp.aid = audioElem.elementsByTagName("aid").at(0).toElement().text();
        temp.ownerId = audioElem.elementsByTagName("owner_id").at(0).toElement().text();
        temp.artist = audioElem.elementsByTagName("artist").at(0).toElement().text();
        temp.title = audioElem.elementsByTagName("title").at(0).toElement().text();
        temp.duration = audioElem.elementsByTagName("duration").at(0).toElement().text().toInt();
        temp.url = audioElem.elementsByTagName("url").at(0).toElement().text();
        Audio.append(temp);
    }

    emit audio(Audio);
}

void cvk::wallUploadServerGet(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем url для загрузки фотографии
    // ==========================================================================

    QUrl uploadUrl;

    //получаем url
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    uploadUrl = domElem.elementsByTagName("upload_url").at(0).toElement().text();

    //отправляем фото на сервер
    uploadFile(uploadUrl, wallPhotoTemp, "photo");
}

void cvk::wallSavePhoto(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем url фотографий
    // ==========================================================================

    //получаем id
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();

    //постим запись с фото
    //параметры
    QStringList params;
    params.append("owner_id="+wallUidTemp);
    params.append("message="+wallMsgTemp);
    params.append("attachments="+domElem.elementsByTagName("id").at(0).toElement().text());

    //генерируем строку запроса
    QUrl query;
    query = genQuery("wall.post", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(wallPost, query);
}

void cvk::getLikes(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем лайки
    // ==========================================================================

    QString itemId, ownerId;
    QString countLikes;
    bool add;

    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    QDomNode domNode = domElem.firstChild();
    while(!domNode.isNull()) {
        QDomElement e = domNode.toElement();
        if (e.tagName()=="itemId"){
            itemId = e.text();
        }
        else if (e.tagName()=="ownerId"){
            ownerId = e.text();
        }
        else if (e.tagName()=="add"){
            add = e.text().toInt();
        }
        domNode = domNode.nextSibling();
    }
    QDomNodeList l = domElem.elementsByTagName ("l");
    QDomElement likesElem = l.at(0).toElement();
    countLikes = likesElem.elementsByTagName("likes").at(0).toElement().text();

    if (!countLikes.isEmpty()){
        emit likes(ownerId, itemId, countLikes, add);
    }
}

void cvk::getVideo(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем видео
    // ==========================================================================

    //видео
    QVector<sVideo> Videos;
    sVideo temp;

    //получаем видеозаписи
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    QDomNodeList vi = domElem.elementsByTagName ("video");
    for(int i=0; i < vi.count(); i++){
        QDomElement videoElem = vi.at(i).toElement();
        temp.vid = videoElem.elementsByTagName("vid").at(0).toElement().text();
        if (temp.vid.isEmpty()){
            temp.vid = videoElem.elementsByTagName("id").at(0).toElement().text();
        }
        temp.owner_id = videoElem.elementsByTagName("owner_id").at(0).toElement().text();
        temp.title = videoElem.elementsByTagName("title").at(0).toElement().text();
        temp.description = videoElem.elementsByTagName("description").at(0).toElement().text();
        temp.duration = videoElem.elementsByTagName("duration").at(0).toElement().text().toInt();
        temp.image = videoElem.elementsByTagName("image").at(0).toElement().text();
        if (temp.image.isEmpty()){
            temp.image = videoElem.elementsByTagName("thumb").at(0).toElement().text();
        }

        temp.url = videoElem.elementsByTagName("mp4_360").at(0).toElement().text();
        if (temp.url.isEmpty()){
            temp.url = videoElem.elementsByTagName("mp4_240").at(0).toElement().text();
            if(temp.url.isEmpty()){
                temp.url = videoElem.elementsByTagName("external").at(0).toElement().text();
                if(temp.url.isEmpty()){
                    temp.url = videoElem.elementsByTagName("player").at(0).toElement().text();
                }
            }
        }

        Videos.append(temp);
    }

    emit video(Videos);
}

void cvk::getFriendsUser(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем друзей
    // ==========================================================================

    //контакт лист
    QVector<sContact> contacts;
    sContact temp;

    //получаем контакты
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    QDomNodeList users = domElem.elementsByTagName ("user");
    contacts.resize(users.count());
    for(int i=0; i < users.count(); i++){
        QDomElement userElem = users.at(i).toElement();
        temp.uid = userElem.elementsByTagName("uid").at(0).toElement().text();
        temp.firstName = userElem.elementsByTagName("first_name").at(0).toElement().text();
        temp.lastName = userElem.elementsByTagName("last_name").at(0).toElement().text();
        temp.avatarUrl = userElem.elementsByTagName("photo_medium_rec").at(0).toElement().text();
        temp.status = userElem.elementsByTagName("online").at(0).toElement().text();
        contacts[i] = temp;
    }

    //сигнал с контакт листом
    emit friendsUser(contacts);
}

void cvk::getFriendsMutual(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем uid друзей
    // ==========================================================================

    QStringList uids;
    uids.clear();

    //получаем контакты
    QDomDocument domDoc;
    domDoc.setContent(result);
    QDomElement domElem = domDoc.documentElement();
    QDomNodeList users = domElem.elementsByTagName ("uid");
    for(int i=0; i < users.count(); i++){
        QDomElement userElem = users.at(i).toElement();
        uids.append(userElem.text());
    }

    //сигнал с uids
    emit friendsMutual(uids);
}

void cvk::deleteWall(const QString &result){
    // ==========================================================================
    // парсим ответ и извлекаем результат удаления
    // ==========================================================================

    //если успешно
    if (result.contains("<response>1</response>"))
        emit wallRefresh(wallUidTemp);
}

/////////////////////////////////////////////////////////////////////////////////

void cvk::apiFriendsGet(const QString &fields, const QString &name_case, const int &offset){
    // ==========================================================================
    // возвращает список идентификаторов друзей текущего пользователя
    // "fields" - перечисленные через запятую поля анкет, необходимые для получения
    // "name_case" - падеж для склонения имени и фамилии пользователя
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("fields="+fields);
    params.append("name_case="+name_case);
    params.append("count=100");
    params.append("order=hints");
    params.append("offset="+QString::number(offset));

    //генерируем строку запроса
    QUrl query;
    query = genQuery("friends.get",params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(friendsGet, query);
}

void cvk::apiFriendsGetLists(){
    // ==========================================================================
    // возвращает список меток друзей текущего пользователя
    // ==========================================================================

    //генерируем строку запроса
    QUrl query;
    query = genQuery("friends.getLists");

    //добавляем новый запрос в очередь на выполнение
    appendQuery(friendsGetLists, query);
}

void cvk::apiMessagesGetLongPollServer(){
    // ==========================================================================
    // возвращает данные, необходимые для подключения к Long Poll серверу
    // Long Poll подключение позволит Вам моментально узнавать о приходе
    // новых сообщений и других событий.
    // ==========================================================================

    //генерируем строку запроса
    QUrl query;
    query = genQuery("messages.getLongPollServer");

    //добавляем новый запрос в очередь на выполнение
    appendQuery(getLongPollServer, query);
}

void cvk::apiExecute(const QString &code){
    // ==========================================================================
    // универсальный метод, который позволяет запускать последовательность других методов
    // "code" - код алгоритма в VKScript (максимум 21 метод, максимум 4000 символов)
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("code="+code);

    //генерируем строку запроса
    QUrl query;
    query = genQuery("execute",params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(execute, query);
}

void cvk::apiActivityOnline(){
    // ==========================================================================
    // поддержка online статуса на сайте ВК
    // ==========================================================================

    //генерируем строку запроса
    QUrl query;
    query = genQuery("activity.online");

    //добавляем новый запрос в очередь на выполнение
    appendQuery(activityOnline, query);
}

void cvk::apiGetCounters(){
    // ==========================================================================
    // возвращает события с сайта ВК (друзья, подарки, встречи итд)
    // ==========================================================================

    //генерируем строку запроса
    QUrl query;
    query = genQuery("getCounters");

    //добавляем новый запрос в очередь на выполнение
    appendQuery(getCounters, query);
}

void cvk::apiActivityGet(const QString &uids){
    // ==========================================================================
    // запрашиваем текущий статус пользователей
    // "uids" - перечисленные через запятую ID пользователей
    // запрашиваем через "execute", т.к нет нужного метода в API
    // ==========================================================================

    if (uids.isEmpty())
        return;

    QStringList uidsList;
    uidsList << uids.split(",");

    //пробегаем по всем пользователям и пачками отправляем
    //запрос на получения статусов
    QStringList metods;
    QString m_uids = "";
    for (int i=0; i < uidsList.count(); i+=20){
        for (int j=i; j<qMin(uidsList.count(),i+20); j++){
            m_uids += uidsList[j]+",";
            metods.append("API.status.get({\"uid\":" + uidsList[j] + "})");
        }
        //параметры
        QStringList params;
        m_uids.remove(m_uids.count()-1,1);
        params.append("code=return{\"uids\":\""+m_uids+"\",\"activity\":[" + metods.join(",") + "]};");

        //генерируем строку запроса
        QUrl query;
        query = genQuery("execute", params);

        //добавляем новый запрос в очередь на выполнение
        appendQuery(activityGet, query);

        //очистить пачку методов
        m_uids="";
        metods.clear();
    }
}

void cvk::apiActivitySet(const QString &text){
    // ==========================================================================
    // устанавливает статус текущему пользователю
    // "text" - текст статуса (максимум 140 символов)
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("text="+text);

    //генерируем строку запроса
    QUrl query;
    query = genQuery("status.set", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(activitySet, query);
}

void cvk::apiMessagesSend(const QString &uid, const QString &message){
    // ==========================================================================
    // посылает сообщение
    // "uid" - идентификатор пользователя (по-умолчанию - текущий пользователь)
    // "message" - текст сообщения (максимум 4096 символов)
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("uid="+uid);
    params.append("message="+message);
    params.append("type=1");

    //генерируем строку запроса
    QUrl query;
    query = genQuery("messages.send", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(messagesSend, query);
}

void cvk::apiNewMessagesGet(const QString &count){
    // ==========================================================================
    // возвращает список непрочитанных входящих сообщений
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("filters=1");
    params.append("preview_length=0");
    params.append("count="+count);

    //генерируем строку запроса
    QUrl query;
    query = genQuery("messages.get", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(newMessagesGet, query);
}

void cvk::apiMessagesGetHistory(const QString &uid, const int &count, const int &offset){
    // ==========================================================================
    // возвращает историю сообщений для данного пользователя
    // "uid" - идентификатор пользователя, переписки с которым необходимо вернуть
    // ==========================================================================

    QString code;
    code = "return{\"uid\":\""+uid+"\",\"history\":API.messages.getHistory({\"uid\":\""+uid
           +"\",\"offset\":\""+QString::number(offset)
           +"\",\"count\":\""+QString::number(count)+"\"})};";

    //параметры
    QStringList params;
    params.append("code="+code);

    //генерируем строку запроса
    QUrl query;
    query = genQuery("execute", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(messagesGetHistory, query);
}

void cvk::apiMessagesMark(const QString &mids, const bool &newState){
    // ==========================================================================
    // помечает сообщения, как прочитанные/непрочитанные
    // "mids" - список идентификаторов сообщений, разделенных запятой
    // "newState" - помечает true - непрочитанным; false - прочитанным
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("mids="+mids);

    //генерируем строку запроса
    QUrl query;
    if (!newState) query = genQuery("messages.markAsRead", params);
    else query = genQuery("messages.markAsNew", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(messagesMark, query);
}

void cvk::apiMessagesDelete(const QString &mid){
    // ==========================================================================
    // удаляет сообщение
    // "mid" - идентификатор сообщения
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("mid="+mid);

    //генерируем строку запроса
    QUrl query;
    query = genQuery("messages.delete", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(messagesDelete, query);
}

void cvk::apiGetProfiles(const QString &uid, const QString &fields){
    // ==========================================================================
    // возвращает расширенную информацию о пользователе
    // "uid" - идентификатор пользователя
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("uids="+uid);
    params.append("fields="+fields);

    //генерируем строку запроса
    QUrl query;
    query = genQuery("getProfiles", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(getProfiles, query);
}

void cvk::apiWallPost(const QString &message, const QString &uid, const QString &photo, const int &rotation){
    // ==========================================================================
    // публикует новую запись на своей или чужой стене
    // ==========================================================================

    if (photo.isEmpty()){
        //параметры
        QStringList params;
        params.append("owner_id=" + (uid.isEmpty() ? sessionVars.user_id : uid));
        params.append("message="+message);
        wallUidTemp = uid.isEmpty() ? sessionVars.user_id : uid;

        //генерируем строку запроса
        QUrl query;
        query = genQuery("wall.post", params);

        //добавляем новый запрос в очередь на выполнение
        appendQuery(wallPost, query);
    } else {
        //если перед публикацией нужно загрузить фотографию
        //TODO сделать очередь
        wallUidTemp = uid.isEmpty() ? sessionVars.user_id : uid;
        wallMsgTemp = message;
        wallPhotoTemp = photo;
        wallPhotoRotation = rotation;
        apiPhotosGetWallUploadServer(wallUidTemp, "");
    }
}

void cvk::apiMessagesGetDialogs(int offset, int count, int preview_length){
    // ==========================================================================
    // возвращает список диалогов текущего пользователя
    // ==========================================================================

    //параметры
    QStringList params;
    params.append(QString("code=var dialogs=API.messages.getDialogs({\"offset\":\"%1\",\"count\":\"%2\",\"preview_length\":\"%3\"});"
                  "var prof=API.getProfiles({\"uids\":dialogs@.uid,\"fields\":\"photo_medium_rec\"});"
                  "return{\"dialogs\":dialogs,\"profiles\":prof};")
                  .arg(offset)
                  .arg(count)
                  .arg(preview_length));

    //генерируем строку запроса
    QUrl query;
    query = genQuery("execute", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(messagesGetDialogs, query);
}

void cvk::apiNewsfeedGet(const QString &source_ids, const QString &filters, const QString &start_time, const QString &end_time, const int count, const QString &new_from){
    // ==========================================================================
    // возвращает данные, необходимые для показа списка новостей
    // ==========================================================================

    QString startTime;
    if (start_time.isEmpty())
        startTime = QString::number(QDateTime::currentDateTime().addDays(-20).toTime_t());
    else
        startTime = start_time;

    //параметры
    QStringList params;
    params.append("source_ids="+source_ids);
    params.append("filters="+filters);
    params.append("start_time="+startTime);
    params.append("end_time="+end_time);
    params.append("count="+QString::number(count));
    params.append("from="+new_from);
    params.append("max_photos=10");

    //генерируем строку запроса
    QUrl query;
    query = genQuery("newsfeed.get", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(newsfeedGet, query);
}

void cvk::apiWallGet(const QString &owner_id, const int &offset, const int &count, const QString &filter){
    // ==========================================================================
    // возвращает список записей со стены пользователя
    // ==========================================================================

    QString owner_uid;
    owner_uid = owner_id;

    if (owner_uid.isEmpty())
        owner_uid = sessionVars.user_id;

    //параметры
    QStringList params;
    params.append(QString("code=var wall=API.wall.get({\"owner_id\":\"%1\",\"offset\":\"%2\",\"count\":\"%3\",\"filter\":\"%4\"});"
                  "var prof=API.getProfiles({\"uids\":wall@.from_id,\"fields\":\"photo_medium_rec\"});"
                  "return{\"wall\":wall,\"profiles\":prof,\"uidOwner\":%1};")
                  .arg(owner_uid)
                  .arg(offset)
                  .arg(count)
                  .arg(filter));

    //генерируем строку запроса
    QUrl query;
    query = genQuery("execute", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(wallGet, query);
}

void cvk::apiWallGetComments(const QString &post_id, const QString &owner_id, const QString &sort, const int &offset, const int &count){
    // ==========================================================================
    // возвращает список комментариев к записи на стене пользователя
    // ==========================================================================

    //параметры
    QStringList params;
    params.append(QString("code=var wall=API.wall.getComments({\"owner_id\":\"%1\",\"post_id\":\"%2\",\"sort\":\"%3\",\"offset\":\"%4\",\"count\":\"%5\"});"
                  "var prof=API.getProfiles({\"uids\":wall@.uid,\"fields\":\"photo_medium_rec\"});"
                  "return{\"wall\":wall,\"profiles\":prof};")
                  .arg(owner_id)
                  .arg(post_id)
                  .arg(sort)
                  .arg(offset)
                  .arg(count));

    //генерируем строку запроса
    QUrl query;
    query = genQuery("execute", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(wallGetComments, query);
}

void cvk::apiWallAddComment(const QString &post_id, const QString &text, const QString &owner_id, const QString &reply_to_cid){
    // ==========================================================================
    // добавляет комментарий к записи на стене пользователя
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("owner_id="+owner_id);
    params.append("post_id="+post_id);
    params.append("text="+text);
    params.append("reply_to_cid="+reply_to_cid);

    //генерируем строку запроса
    QUrl query;
    query = genQuery("wall.addComment", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(wallAddComment, query);
}

void cvk::apiPhotosGetAll(const QString &owner_id, const int offset, const int count){
    // ==========================================================================
    // возвращает все фотографии пользователя в антихронологическом порядке
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("owner_id="+owner_id);
    params.append("offset="+QString::number(offset));
    params.append("count="+QString::number(count));

    //генерируем строку запроса
    QUrl query;
    query = genQuery("photos.getAll", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(photosGetAll, query);
}

void cvk::apiAudioGet(const QString &uid, const QString &gid, const QString &album_id, const QString &aids, const int &count, const int &offset){
    // ==========================================================================
    // возвращает список аудиозаписей пользователя или группы
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("uid="+uid);
    params.append("gid="+gid);
    params.append("album_id="+album_id);
    params.append("aids="+aids);
    params.append("count="+QString::number(count));
    params.append("offset="+QString::number(offset));

    //генерируем строку запроса
    QUrl query;
    query = genQuery("audio.get", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(audioGet, query);
}

void cvk::apiAudioSearch(const QString &q, const QString &auto_complete, const QString &sort, const int &count, const int &offset){
    // ==========================================================================
    // возвращает список аудиозаписей в соответствии с заданным критерием поиска
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("q="+q);
    params.append("auto_complete="+auto_complete);
    params.append("sort="+sort);
    params.append("count="+QString::number(count));
    params.append("offset="+QString::number(offset));

    //генерируем строку запроса
    QUrl query;
    query = genQuery("audio.search", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(audioGet, query);
}

void cvk::apiAudioAdd(const QString &aid, const QString &oid, const QString &gid){
    // ==========================================================================
    // копирует аудиозапись на страницу пользователя или группы
    // если gid не указан аудиозапись копируется на страницу текущего пользователя
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("aid="+aid);
    params.append("oid="+oid);
    params.append("gid="+gid);

    //генерируем строку запроса
    QUrl query;
    query = genQuery("audio.add", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(audioAdd, query);
}

void cvk::apiPhotosGetWallUploadServer(const QString &uid, const QString &gid){
    // ==========================================================================
    // возвращает адрес сервера для загрузки﻿ фотографии на стену пользователя
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("uid="+uid);
    params.append("gid="+gid);

    //генерируем строку запроса
    QUrl query;
    query = genQuery("photos.getWallUploadServer", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(getWallUploadServer, query);
}

void cvk::apiPhotosSaveWallPhoto(const QString &server, const QString &photo, const QString &hash, const QString &uid, const QString &gid){
    // ==========================================================================
    // сохраняет фотографии после успешной загрузки на URI
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("server="+server);
    params.append("photo="+photo);
    params.append("hash="+hash);
    params.append("uid="+uid);
    params.append("gid="+gid);

    //генерируем строку запроса
    QUrl query;
    query = genQuery("photos.saveWallPhoto", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(saveWallPhoto, query);
}

void cvk::apiLikes(const QString &owner_id, const QString &type, const QString &item_id, bool &add, bool &repost){
    // ==========================================================================
    // добавляет/удаляет указанный объект в список Мне нравится
    // ==========================================================================

    QString code;

    if (!repost){
        QString oper;
        if (add) oper = "likes.add";
        else oper = "likes.delete";
        code = "return{\"l\":API."+oper+"({\"owner_id\":\""+owner_id
                +"\",\"type\":\""+type
                +"\",\"item_id\":\""+item_id+"\"}),\"itemId\":\""+item_id+"\","
                +"\"add\":\""+QString::number(add)+"\","
                +"\"ownerId\":\""+owner_id+"\"};";
    } else {
        code = "return{\"l\":API.wall.addLike({\"owner_id\":\""+owner_id
                +"\",\"post_id\":\""+item_id
                +"\",\"repost\":\"1\"}),\"itemId\":\""+item_id+"\","
                +"\"add\":\""+QString::number(add)+"\","
                +"\"ownerId\":\""+owner_id+"\"};";
    }

    //параметры
    QStringList params;
    params.append("code="+code);

    //генерируем строку запроса
    QUrl query;
    query = genQuery("execute", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(likesGet, query);

    //если был репост, обновляем свою стену для удобства
    if (repost)
        apiWallGet();
}

void cvk::apiMessagesSetActivity(const QString &uid, const QString &chat_id, const QString &type){
    // ==========================================================================
    // изменяет статус набора текста пользователем в диалоге
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("uid="+uid);
    params.append("chat_id="+chat_id);
    params.append("type="+type);

    //генерируем строку запроса
    QUrl query;
    query = genQuery("messages.setActivity", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(messagesSetActivity, query);
}

void cvk::apiVideoGet(const QString &videos, const QString &uid, const QString &gid, const QString &aid, const QString &width, const int &count, const int &offset){
    // ==========================================================================
    // возвращает информацию о видеозаписях
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("videos="+videos);
    params.append("uid="+uid);
    params.append("gid="+gid);
    params.append("aid="+aid);
    params.append("width="+width);
    params.append("count="+QString::number(count));
    params.append("offset="+QString::number(offset));

    //генерируем строку запроса
    QUrl query;
    query = genQuery("video.get", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(videoGet, query);
}

void cvk::apiVideoSearch(const QString &q, const QString &sort, const QString &hd, const int &count, const int &offset){
    // ==========================================================================
    // возвращает список видеозаписей в соответствии с заданным критерием поиска
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("q="+q);
    params.append("sort="+sort);
    params.append("hd="+hd);
    params.append("count="+QString::number(count));
    params.append("offset="+QString::number(offset));

    //генерируем строку запроса
    QUrl query;
    query = genQuery("video.search", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(videoGet, query);
}

void cvk::apiFriendsUserGet(const QString &uid, const QString &fields, const QString &name_case){
    // ==========================================================================
    // возвращает список идентификаторов друзей пользователя
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("uid="+uid);
    params.append("fields="+fields);
    params.append("name_case="+name_case);
    params.append("order=name");

    //генерируем строку запроса
    QUrl query;
    query = genQuery("friends.get", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(friendsUserGet, query);
}

void cvk::apiFriendsGetMutual(const QString &target_uid, const QString &source_uid){
    // ==========================================================================
    // возвращает список идентификаторов общих друзей между парой пользователей
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("target_uid="+target_uid);
    params.append("source_uid="+source_uid);

    //генерируем строку запроса
    QUrl query;
    query = genQuery("friends.getMutual", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(friendsGetMutual, query);
}

void cvk::apiWallDelete(const QString &post_id, const QString &owner_id){
    // ==========================================================================
    // удаляет запись со стены пользователя
    // ==========================================================================

    //параметры
    QStringList params;
    params.append("post_id="+post_id);
    params.append("owner_id=" + (owner_id.isEmpty() ? sessionVars.user_id : owner_id));
    wallUidTemp = owner_id.isEmpty() ? sessionVars.user_id : owner_id;

    //генерируем строку запроса
    QUrl query;
    query = genQuery("wall.delete", params);

    //добавляем новый запрос в очередь на выполнение
    appendQuery(wallDelete, query);
}
