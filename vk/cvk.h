/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************

 ****************************************************************************
 * I'm sorry for the russian comments
 ***************************************************************************

*/

#ifndef CVK_H
#define CVK_H

#include "cvkStruct.h"
#include <QApplication>
#include <QObject>
#include <QDesktopWidget>
#include <QWebView>
#include <QWebFrame>
#include <QWebElement>
#include <QTextDocument>
#include <QNetworkReply>
#include <QQueue>
#include <QTime>
#include <QTimer>
#include <QCryptographicHash>
#include <QDomDocument>
#include <QDomElement>
#include <QDomNode>
#include <QStringList>
#include <QFile>
#include <QBuffer>
#include <QFileInfo>
#include "captchaview.h"
#include "k8json.h"

class cvk : public QObject
{
Q_OBJECT

private:
    sLoginVars loginVars;
    sSessionVars sessionVars;
    sUrlServers urlServers;
    sLongPoll longPollVars;
    eStatus status;
    QNetworkAccessManager *httpAuth;
    QNetworkAccessManager *http;
    QQueue<sQuery> queryList;
    QTime *stopwatch;
    QTimer *timerRequest;
    bool isProcessing;
    QNetworkAccessManager *httpLongPoll;
    QTimer *timeoutLongPoll;
    QTimer *timerSchedule;
    int offsetFriends;
    QString wallUidTemp;
    QString wallMsgTemp;
    QString wallPhotoTemp;
    int wallPhotoRotation;

private:
    QVariant parseJSON(const QByteArray &data);
    void loadToken();
    void loginSuccess(const QVariant &response);
    void loginFailure(const QVariant &response);
    QUrl genQuery(const QString &method, const QStringList &params = QStringList());
    void appendQuery(const eMethod &method, const QUrl &query);
    void execQuery();
    void apiMessagesGetLongPollServer();
    void firstRequest();
    int getFriends(const QString &result);
    void getFriendsLists(const QString &result);
    void getParamsLongPoll(const QString &result);
    void longPoll();
    void longPollResetMessageFlags(QVariantList &update);
    void longPollMessageReceived(QVariantList &update);
    void longPollUserOnline(QVariantList &update);
    void longPollUserOffline(QVariantList &update);
    void longPollUserTyping(QVariantList &update);
    void showCaptcha(const QString &result);
    void getActivity(const QString &result);
    void countersGet(const QString &result);
    void sendMsg(const QString &result);
    void getNewMessages(const QString &result);
    void getMessagesHistory(const QString &result);
    void profilesGet(const QString &result);
    void postWall(const QString &result);
    void getMessagesDialogs(const QString &result);
    void getNewsfeed(const QString &result);
    void getWall(const QString &result);
    void getWallComments(const QString &result);
    void getPhotosGetAll(const QString &result);
    void getAudio(const QString &result);
    void wallUploadServerGet(const QString &result);
    void wallSavePhoto(const QString &result);
    void getLikes(const QString &result);
    void getVideo(const QString &result);
    void getFriendsUser(const QString &result);
    void getFriendsMutual(const QString &result);
    void deleteWall(const QString &result);

public:
    explicit cvk(const QString &client_id, QObject *parent = 0);
    ~cvk();
    void connectToVk(const QString &uid="", const QString &token="", const QString &username="", const QString &password="");
    void disconnectVk();
    void captchaDone(const QString &captcha_sid, const QString &captcha_key);
    void uploadFile(const QUrl &url, const QString &fileName, const QString &field);
    void apiFriendsGet(const QString &fields="", const QString &name_case="", const int &offset=0);
    void apiFriendsGetLists();
    void apiExecute(const QString &code);
    void apiActivityOnline();
    void apiGetCounters();

private slots:
    void slotTokenLoaded(QNetworkReply *networkReply);
    void slotConnected();
    void slotResponse(QNetworkReply *networkReply);
    void slotTimerRequest();
    void slotResponseQuery(const sQuery &query);
    void slotResponseLongPoll(QNetworkReply *networkReply);
    void slotTimeoutLongPoll();
    void slotTimerSchedule();

public slots:
    void apiActivityGet(const QString &uids);
    void apiActivitySet(const QString &text);
    void apiMessagesSend(const QString &uid, const QString &message);
    void apiNewMessagesGet(const QString &count="100");
    void apiMessagesGetHistory(const QString &uid, const int &count=10, const int &offset=0);
    void apiMessagesMark(const QString &mids, const bool &newState);
    void apiMessagesDelete(const QString &mid);
    void apiGetProfiles(const QString &uid, const QString &fields="activity,online,sex,bdate,photo_medium_rec,photo_big,contacts,education,counters,can_post,can_write_private_message,last_seen");
    void apiWallPost(const QString &message, const QString &uid="", const QString &photo="", const int &rotation=0);
    void apiMessagesGetDialogs(const int offset=0, const int count=10, const int preview_length=55);
    void apiNewsfeedGet(const QString &source_ids="", const QString &filters="", const QString &start_time="", const QString &end_time="", const int count=100, const QString &new_from="");
    void apiWallGet(const QString &owner_id="", const int &offset=0, const int &count=10, const QString &filter="");
    void apiWallGetComments(const QString &post_id, const QString &owner_id="", const QString &sort="asc", const int &offset=0, const int &count=100);
    void apiWallAddComment(const QString &post_id, const QString &text, const QString &owner_id="", const QString &reply_to_cid="");
    void apiPhotosGetAll(const QString &owner_id="", const int offset=0, const int count=100);
    void apiAudioGet(const QString &uid="", const QString &gid="", const QString &album_id="", const QString &aids="", const int &count=100, const int &offset=0);
    void apiAudioSearch(const QString &q, const QString &auto_complete="1", const QString &sort="2", const int &count=100, const int &offset=0);
    void apiAudioAdd(const QString &aid, const QString &oid, const QString &gid="");
    void apiPhotosGetWallUploadServer(const QString &uid="", const QString &gid="");
    void apiPhotosSaveWallPhoto(const QString &server, const QString &photo, const QString &hash, const QString &uid="", const QString &gid="");
    void apiLikes(const QString &owner_id, const QString &type, const QString &item_id, bool &add, bool &repost);
    void apiMessagesSetActivity(const QString &uid="", const QString &chat_id="", const QString &type="typing");
    void apiVideoGet(const QString &videos="", const QString &uid="", const QString &gid="", const QString &aid="", const QString &width="", const int &count=100, const int &offset=0);
    void apiVideoSearch(const QString &q, const QString &sort="", const QString &hd="0", const int &count=100, const int &offset=0);
    void apiFriendsUserGet(const QString &uid="", const QString &fields="", const QString &name_case="");
    void apiFriendsGetMutual(const QString &target_uid, const QString &source_uid="");
    void apiWallDelete(const QString &post_id, const QString &owner_id="");

signals:
    void errorString(const eError error, const QString &text, const bool &fatal);
    void connected(const QString &token, const QString &uid);
    void responseQuery(const sQuery &query);
    void friends(const QVector<sContact> &contacts);
    void friendsLists(const QList<QString> &lists);
    void friendsFinished();
    void sendMessagesError(const int &error, const QString &uid, const QString &text);
    void captcha(const QString &captchaSid, const QString &captchaImg);
    void resetMessageFlags(const QString &mid, const int &mask, const QString &uid);
    void messageReceived(const sMessage &message);
    void userOnline(const QString &uid);
    void userOffline(const QString &uid, const int &flag);
    void userTyping(const QString &uid, const int &flag);
    void activity(const QList<sActivity> &activity);
    void counters(const sCounters &counters);
    void history(const QString &uid, const QList<sMessage> &messages);
    void profileSelf(const sProfile &profile);
    void profile(const sProfile &profile);
    void dialogs(const QVector<sDialog> &dialogs);
    void news(const QVector<sNew> &news, const QString &newFrom);
    void wall(const QVector<sWall> &wall, const QString &ownerId);
    void wallComments(const QVector<sWallComments> &comments);
    void wallRefresh(const QString &uid);
    void wallPostFinished(const QString &uid, const bool &ok);
    void photos(const QVector<sPhoto> &photos);
    void audio(const QVector<sAudio> &audio);
    void likes(const QString &ownerId, const QString &itemId, const QString &countLikes, bool isAdd);
    void video(const QVector<sVideo> &video);
    void friendsUser(const QVector<sContact> &contacts);
    void friendsMutual(const QStringList &uids);
};

#endif // CVK_H
