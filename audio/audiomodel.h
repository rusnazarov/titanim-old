/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef AUDIOMODEL_H
#define AUDIOMODEL_H

#include <QAbstractListModel>
#include "vk/cvkStruct.h"

class audioModel : public QAbstractListModel
{
Q_OBJECT

private:
    QVector<sAudio> audio;

public:
    explicit audioModel(QObject *parent = 0);
    void appendAudio(QVector<sAudio> audio);
    void replaceAudio(QVector<sAudio> audio);
    bool removeAudio(int row, int count);
    Q_INVOKABLE QString urlAt(int index);
    Q_INVOKABLE int durationAt(int index);
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant& value, int role = Qt::EditRole);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    enum AudioRole {
        aidRole = Qt::UserRole,
        ownerIdRole = Qt::UserRole+1,
        artistRole = Qt::UserRole+2,
        titleRole = Qt::UserRole+3,
        durationRole = Qt::UserRole+4,
        durationStrRole = Qt::UserRole+5,
        urlRole = Qt::UserRole+6
    };

public slots:

signals:
};

#endif // AUDIOMODEL_H
