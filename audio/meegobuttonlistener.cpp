/*
    Copyright (c) 2011 by Ilya Skriblovsky specially for TitanIM

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "audio/meegobuttonlistener.h"

MeegoButtonListener::MeegoButtonListener()
    : _lastHeadsetPress(0)
{
    QDBusConnection::systemBus().connect(
                "org.freedesktop.Hal",
                "/org/freedesktop/Hal/devices/computer_logicaldev_input",
                "org.freedesktop.Hal.Device",
                "Condition",
                this,
                SLOT(onCondition(QString, QString))
                );
    QDBusConnection::systemBus().connect(
                "org.freedesktop.Hal",
                "/org/freedesktop/Hal/devices/platform_twl4030_keypad_logicaldev_input",
                "org.freedesktop.Hal.Device",
                "Condition",
                this,
                SLOT(onCondition(QString, QString))
                );
    QDBusConnection::systemBus().connect(
                "org.freedesktop.Hal",
                "/org/freedesktop/Hal/devices/platform_twl4030_pwrbutton_logicaldev_input",
                "org.freedesktop.Hal.Device",
                "Condition",
                this,
                SLOT(onCondition(QString, QString))
                );
}

void MeegoButtonListener::onCondition(QString event, QString button)
{
    if (event == "ButtonPressed"  &&  button == "volume-up")  volumeUpPressed();
    if (event == "ButtonPressed"  &&  button == "volume-down")  volumeDownPressed();
    if (event == "ButtonPressed"  &&  button == "power")  powerButtonPressed();
    if (event == "ButtonPressed"  &&  button == "forward")  forwardButtonPressed();
    if (event == "ButtonPressed"  &&  button == "rewind")  rewindButtonPressed();
    if (event == "ButtonPressed"  &&  (button == "phone" || button == "play-pause"))
    {
        uint now = QDateTime::currentDateTime().toTime_t();
        if (now - _lastHeadsetPress > 0)
        {
            headsetButtonPressed();
            _lastHeadsetPress = now;
        }
    }
}
