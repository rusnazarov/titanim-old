/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef AUDIO_H
#define AUDIO_H

#include <QObject>
#include "audiomodel.h"
#include "meegobuttonlistener.h"
#include "../vk/cvkStruct.h"

class Audio : public QObject
{
Q_OBJECT

private:
    MeegoButtonListener *buttonListener;

public:
    audioModel *model;

public:
    explicit Audio(QObject *parent = 0);
    ~Audio();

public slots:
    void replaceAudio(const QVector<sAudio> &audio);
    void appendAudio(const QVector<sAudio> &audio);
    void slotAudioAdd(const QString &aid, const QString &oid, const QString &gid);

signals:
    void headsetButtonPressed();
    void volumeUpPressed();
    void volumeDownPressed();
    void powerButtonPressed();
    void forwardButtonPressed();
    void rewindButtonPressed();
    void audioAdd(const QString &aid, const QString &oid, const QString &gid="");
};

#endif // AUDIO_H
