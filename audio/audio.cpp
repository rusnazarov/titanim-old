/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "audio.h"

Audio::Audio(QObject *parent) :
    QObject(parent)
{
    model = new audioModel();
    buttonListener = new MeegoButtonListener();
    connect(buttonListener, SIGNAL(headsetButtonPressed()), this, SIGNAL(headsetButtonPressed()));
    connect(buttonListener, SIGNAL(powerButtonPressed()), this, SIGNAL(powerButtonPressed()));
    connect(buttonListener, SIGNAL(volumeDownPressed()), this, SIGNAL(volumeDownPressed()));
    connect(buttonListener, SIGNAL(volumeUpPressed()), this, SIGNAL(volumeUpPressed()));
    connect(buttonListener, SIGNAL(forwardButtonPressed()), this, SIGNAL(forwardButtonPressed()));
    connect(buttonListener, SIGNAL(rewindButtonPressed()), this, SIGNAL(rewindButtonPressed()));
}

Audio::~Audio(){
    delete model;
    delete buttonListener;
}

void Audio::replaceAudio(const QVector<sAudio> &audio){
    // ==========================================================================
    // пришли аудиозаписи
    // ==========================================================================

    model->replaceAudio(audio);
}

void Audio::appendAudio(const QVector<sAudio> &audio){
    // ==========================================================================
    // пришли аудиозаписи
    // ==========================================================================

    model->appendAudio(audio);
}

void Audio::slotAudioAdd(const QString &aid, const QString &oid, const QString &gid){
    // ==========================================================================
    // копирует аудиозапись на страницу пользователя или группы
    // если gid не указан аудиозапись копируется на страницу текущего пользователя
    // ==========================================================================

    emit audioAdd(aid, oid, gid);
}
