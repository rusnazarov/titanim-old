/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "audiomodel.h"

audioModel::audioModel(QObject *parent) :
    QAbstractListModel(parent)
{
    QHash<int, QByteArray> roles = roleNames();
    roles[aidRole] = "aid";
    roles[ownerIdRole] = "ownerId";
    roles[artistRole] = "artist";
    roles[titleRole] = "title";
    roles[durationRole] = "duration";
    roles[durationStrRole] = "durationStr";
    roles[urlRole] = "url";
    setRoleNames(roles);
}

void audioModel::appendAudio(QVector<sAudio> audio){
    // ==========================================================================
    // добавление аудиозаписи
    // ==========================================================================

    if (!audio.count())
        return;

    //добавляем новые
    beginInsertRows(QModelIndex(), this->audio.count(), this->audio.count() + audio.count() - 1);
    this->audio << audio;
    endInsertRows();
}

void audioModel::replaceAudio(QVector<sAudio> audio){
    // ==========================================================================
    // замена аудиозаписей
    // ==========================================================================

    //очищаем модель
    removeAudio(0, rowCount());

    //добавляем новые
    appendAudio(audio);
}

bool audioModel::removeAudio(int row, int count){
    // ==========================================================================
    // удаляем с модели данные
    // ==========================================================================

    beginRemoveRows(QModelIndex(), row, row+count-1);

    for (int i = 0; i < count; ++i) {
        audio.remove(row);
    }

    endRemoveRows();
    return true;
}

QString audioModel::urlAt(int index){
    // ==========================================================================
    // возвращает урл компазиции по числовому индексу
    // ==========================================================================

    if (index >= audio.size())
        return "error";
    return audio.at(index).url;
}

int audioModel::durationAt(int index){
    // ==========================================================================
    // возвращает длительность компазиции по числовому индексу
    // ==========================================================================

    if (index >= audio.size())
        return 0;
    return audio.at(index).duration;
}

QVariant audioModel::data(const QModelIndex &index, int role) const{
    // ==========================================================================
    // данные модели
    // ==========================================================================

    if (!index.isValid() || index.row() >= audio.size()) {
        return QVariant();
    }

    switch (role) {

    case aidRole:
        return audio.at(index.row()).aid;

    case ownerIdRole:
        return audio.at(index.row()).ownerId;

    case artistRole:
        return audio.at(index.row()).artist;

    case titleRole:
        return audio.at(index.row()).title;

    case durationRole:
        return audio.at(index.row()).duration;

    case durationStrRole:{
        int m = audio.at(index.row()).duration / 60;
        int s = audio.at(index.row()).duration - (m * 60);
        return QString("%1:%2").arg(m).arg(QString().number(s).rightJustified(2, '0'));
    }

    case urlRole:
        return audio.at(index.row()).url;
    }

    return QVariant();
}

bool audioModel::setData(const QModelIndex &index, const QVariant &value, int role){
    // ==========================================================================
    // редактирование данных модели
    // ==========================================================================

    if (!index.isValid() && !value.isValid() && index.row() >= audio.size()) {
        return false;
    }

    return false;
}

int audioModel::rowCount(const QModelIndex &parent) const{
    // ==========================================================================
    // количество строк в модели
    // ==========================================================================

    Q_UNUSED(parent);
    return audio.count();
}

Qt::ItemFlags audioModel::flags(const QModelIndex &index) const{
    // ==========================================================================
    // флаги модели
    // ==========================================================================

    if (index.isValid())
      return (Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    else
      return Qt::NoItemFlags;
}
