/*
    Copyright (c) 2011 by Ilya Skriblovsky specially for TitanIM

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef __MEEGOBUTTONLISTENER_H
#define __MEEGOBUTTONLISTENER_H

#include <QObject>
#include <QtDBus/QDBusConnection>
#include <QString>
#include <QDateTime>

class MeegoButtonListener: public QObject
{
    Q_OBJECT

public:
    MeegoButtonListener();

signals:
    void headsetButtonPressed();
    void volumeUpPressed();
    void volumeDownPressed();
    void powerButtonPressed();
    void forwardButtonPressed();
    void rewindButtonPressed();

private slots:
    void onCondition(QString event, QString button);

private:
    uint _lastHeadsetPress;
};

#endif
