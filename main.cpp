/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************

 ****************************************************************************
 * I'm sorry for the russian comments
 ***************************************************************************

*/

#include <QtGui/QApplication>
#include <QTextCodec>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    #if defined(Q_WS_X11) && !defined(MEEGO_EDITION_HARMATTAN)
      QApplication::setGraphicsSystem("raster");
    #endif
    QApplication app(argc, argv);
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForCStrings(codec);
    QCoreApplication::setApplicationName("TitanIM");
    QCoreApplication::setApplicationVersion("0.1.7");
    QCoreApplication::setOrganizationName("Nazarov Ruslan");
    QCoreApplication::setOrganizationDomain("http://titanim.ru");
    MainWindow w;
    return app.exec();
}
