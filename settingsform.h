/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef SETTINGSFORM_H
#define SETTINGSFORM_H

#include <QDialog>
#include <QColorDialog>
#include <QCloseEvent>
#include <QMessageBox>
#include <QProcess>
#include "settings.h"

namespace Ui {
    class settingsForm;
}

class settingsForm : public QDialog
{
Q_OBJECT

    Q_PROPERTY(QVariantList language READ language NOTIFY languageChanged)
    Q_PROPERTY(int languageCurrentIndex READ languageCurrentIndex NOTIFY languageCurrentIndexChanged)
    Q_PROPERTY(bool showStatusBar READ showStatusBar NOTIFY showStatusBarChanged)
    Q_PROPERTY(bool isNativToolBar READ isNativToolBar NOTIFY isNativToolBarChanged)

private:
    Ui::settingsForm *ui;
    QColorDialog *colorDialog;
    bool flagRestartApp;
    QVariantMap languageMap;
    int lngCurrentIndex;

private:
    QVariantList language();
    int languageCurrentIndex();
    bool showStatusBar();
    bool isNativToolBar();

public:
    settingsForm(QWidget *parent = 0);
    ~settingsForm();

protected:
    void changeEvent(QEvent *e);
    void showEvent(QShowEvent *);
    void closeEvent(QCloseEvent *pe);

public slots:
    void restartApp();
    void setShowActivity(bool show);
    void setIconSize(int size);
    void setTextSize(int size);
    void setNewsFeedEnabled(bool enabled);
    void setLanguage(QString language);
    void setVisibleStatusBar(bool visible);
    void setVisibleNativToolBar(bool visible);
    void setVibro(bool on);
    void setMessagesActivity(bool on);
    void setSaveLastReadNew(bool on);
    void setSortByName(bool enabled);
    void emitSettingsChanged();

private slots:
    void on_separatorButton_clicked();
    void on_backgroundInButton_clicked();
    void on_backgroundOutButton_clicked();
    void on_dateButton_clicked();
    void on_nameInButton_clicked();
    void on_nameOutButton_clicked();
    void on_applyButton_clicked();
    void on_saveButton_clicked();
    void on_closeButton_clicked();
    void slotChanged(int idx=0);

signals:
    void settingsChanged();
    void languageChanged(QVariantList list);
    void languageCurrentIndexChanged(int index);
    void showRestartDialog();
    void showStatusBarChanged(const bool &show);
    void isNativToolBarChanged(const bool &show);
};

#endif // SETTINGSFORM_H
