/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef STATUSFORM_H
#define STATUSFORM_H

#include <QDialog>
#include <QCloseEvent>
#include <QNetworkAccessManager>
#include <QNetworkReply>

namespace Ui {
    class statusForm;
}

class statusForm : public QDialog
{
Q_OBJECT

private:
    Ui::statusForm *ui;
    QNetworkAccessManager *http;
    int toolsHeight;

public:
    statusForm(QWidget *parent = 0);
    ~statusForm();
    void setPhoto(const QUrl &url);
    void setStatus(const QString &status);

protected:
    void changeEvent(QEvent *e);
    void closeEvent(QCloseEvent *pe);

private slots:
    void slotFinished(QNetworkReply *networkReply);
    void slotCountChar();
    void slotStateChanged(int state);
    void on_pushButton_clicked();

signals:
    void sendStatus(const QString &status);
    void sendWall(const QString &text);
};

#endif // STATUSFORM_H
