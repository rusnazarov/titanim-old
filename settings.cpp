/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "settings.h"

appSettings *appSettings::aInstance = 0;

appSettings *appSettings::instance()
{
   if (!aInstance) aInstance = new appSettings();
   return aInstance;
}

void appSettings::destroy() {
   if (aInstance)
      delete aInstance, aInstance = 0;
}

appSettings::appSettings(){
    //настройки пользователей
    if (!QFile::exists(QApplication::applicationDirPath() + "/settings.ini"))
        //домашний каталог пользователя
        home = QDesktopServices::storageLocation(QDesktopServices::HomeLocation)+"/.TitanIM/";
    else
        //каталог программы
        home = QApplication::applicationDirPath()  + "/";

    //домашний каталог программы
    if (QApplication::applicationDirPath() == "/opt/titanimpro/bin")
        homeApp = "/opt/titanimpro/share/";
    else
        homeApp = QApplication::applicationDirPath() + "/";
}

QString appSettings::uids(){
    // ==========================================================================
    // возвращаем всех пользователей
    // ==========================================================================

    //все пользователи
    QDir dir(home+"profiles/");
    QStringList listUid = dir.entryList(QDir::Dirs);
    listUid.removeOne(".");
    listUid.removeOne("..");

    if (listUid.count() > 0){
        //последний пользователь
        QSettings settingsMain(home+"settings.ini", QSettings::IniFormat);
        settingsMain.beginGroup("profiles");
        QString last = settingsMain.value("last", "0").toString();
        settingsMain.endGroup();
        if (listUid.contains(last)){
            listUid.swap(listUid.indexOf(last),0);
            return listUid.join(",");
        }
    }
    return "0";
}

void appSettings::setUid(const QString &uid){
    this->uid = uid;
}

void appSettings::setProfileName(const QString &name){
    this->name = name;
}

void appSettings::setProfileShortName(const QString &name){
    this->shortName = name;
}

void appSettings::setProfilePhoto(const QString &url){
    photo = url;
}

QString appSettings::cacheDir(){
    return home+"profiles/"+uid+"/cache/";
}

QString appSettings::homeDir(){
    return home;
}

QString appSettings::homeAppDir(){
    return homeApp;
}

QString appSettings::profileName(){
    return name;
}

QString appSettings::profileShortName(){
    return shortName;
}

QString appSettings::profilePhoto(){
    return photo;
}

void appSettings::creatDir(){
    //создаем если необходимо рабочие каталоги
    QDir dir;
    if (!dir.exists(home+"profiles/"+uid+"/cache"))
        dir.mkpath(home+"profiles/"+uid+"/cache");
    if (!dir.exists(home+"dictionaries"))
        dir.mkpath(home+"dictionaries");
}

void appSettings::saveProfile(const QString &key, const QVariant &value){
    //настройки профиля
    QSettings settings(home+"profiles/"+uid+"/profilesettings.ini", QSettings::IniFormat);
    settings.setValue(key, value);
}

void appSettings::saveMain(const QString &key, const QVariant &value){
    //настройки программы
    QSettings settingsMain(home+"settings.ini", QSettings::IniFormat);
    settingsMain.setValue(key, value);
}

QVariant appSettings::loadProfile(const QString &key, const QVariant &defaultValue){
    //настройки профиля
    QSettings settings(home+"profiles/"+uid+"/profilesettings.ini", QSettings::IniFormat);
    return settings.value(key, defaultValue);
}

QVariant appSettings::loadMain(const QString &key, const QVariant &defaultValue){
    //настройки программы
    QSettings settingsMain(home+"settings.ini", QSettings::IniFormat);
    return settingsMain.value(key, defaultValue);
}

























