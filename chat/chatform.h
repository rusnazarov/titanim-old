/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef CHATFORM_H
#define CHATFORM_H

#include <QDeclarativeContext>
#include <QSound>
#include <QProcess>
#include <QResizeEvent>
#include <QSortFilterProxyModel>
#include <QMenu>
#include <QClipboard>
#include "qmlapplicationviewer.h"
#include "chatmodel.h"
#include "chatdelegate.h"
#include "emoticons.h"
#include "../settings.h"

class ChatForm : public QmlApplicationViewer
{
Q_OBJECT

private:
    QMenu *chatLogMenu;
    chatModel *model;
    QSortFilterProxyModel *proxy;

private:
    void addEvent(const QString &uid, const QString &text);

public:
    QString uid;
    QString inputEditText;
    bool newMessage;

public:
    ChatForm(const sContact &contact, QWidget *parent = 0);
    ~ChatForm();
    sContact getContact();
    void addMessage(sMessage message, const sContact &contact);
    void resetMessageFlags(const QString &mid, int mask);
    void messageReceived(const sMessage &message, const sContact &contact);
    QString unreadMessages();
    void clearMessages();
    void history(const QList<sMessage> &messages, const sContact &contact);
    int rowCount();
    void userOnline(const QString &uid);
    void userOffline(const QString &uid, int flag);
    void setActivity(const sActivity &activity);
    void setFilterMsg(bool visible);

protected:
    void resizeEvent(QResizeEvent *);

private slots:
    void slotClearSelection();
    void slotChatLogMenu(const QPoint &point);
    void slotCopyMsg();
    void slotCopyTextMsg();
    void slotMessagesMarkAsNew();
    void slotDeleteMsg();
    void slotOpenMsg();

public slots:
    void slotQuote();

signals:
    void inputEditAppend(const QString &text);
    void messagesMark(const QString &mids, const bool &newState);
    void messagesDelete(const QString &mid);
};

#endif // CHATFORM_H
