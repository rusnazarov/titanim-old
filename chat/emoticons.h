/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef EMOTICONS_H
#define EMOTICONS_H

#include <QScrollArea>
#include <QGridLayout>
#include <QLabel>
#include <QImageReader>
#include <QTextStream>
#include "../settings.h"

class emoticons : public QScrollArea
{
Q_OBJECT

private:
    static emoticons *aInstance;
    QVector<QString> smiles[2];
    QGridLayout *gridLayout;
    QWidget *myWidget;

private:
    emoticons();
    bool eventFilter(QObject *obj, QEvent *event);

public:
    static emoticons *instance();
    static void destroy();
    QString toEmoticons(const QString &text);
    QString fromEmoticons(const QString &text);
    int countEmoticons();

signals:
    void insertSmile(const QString &smile);
};

#endif // EMOTICONS_H
