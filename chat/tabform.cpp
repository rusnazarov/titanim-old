/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "tabform.h"

TabForm::TabForm(QWidget *parent) :
    QmlApplicationViewer(parent)
{
    proxy = new QSortFilterProxyModel(this);
    proxy->setDynamicSortFilter(true);
    proxy->setSortCaseSensitivity(Qt::CaseInsensitive);
    proxy->setFilterCaseSensitivity(Qt::CaseInsensitive);
    proxy->setSortRole(Qt::UserRole+2);
    proxy->sort(0);

    countUnreadMessages = 0;
    #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
     vibro = 0;
    #endif

   #if !defined(Q_WS_MAEMO_5) && !defined(MEEGO_EDITION_HARMATTAN)
    setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    rootContext()->setContextProperty("chat", this);
    rootContext()->setContextProperty("chatModel", proxy);
    setMainQmlFile(appSettings::instance()->homeAppDir() + "qml/chat.qml");
   #endif

    connect(this, SIGNAL(currentTab(QString)), this, SLOT(slotCurrentTab(QString)));

    //по умолчанию фильтр скрыт
//    ui->filterEdit->setVisible(false);

    //фильтр
//    connect(ui->filterEdit, SIGNAL(textChanged(QString)), proxy, SLOT(setFilterWildcard(QString)));

//    //фильтр событий
//    filter = new eFilter(this);
//    connect(filter, SIGNAL(sendMsg()), this, SLOT(on_sendButton_clicked()));
//    connect(filter, SIGNAL(changeTab()), this, SLOT(slotNextTab()));
//    connect(filter, SIGNAL(hideTabForm()), this, SLOT(hide()));

//    //контекстное меню смайлов
//    if (emoticons::instance()->countEmoticons() != 0){
//        smilesMenu = new QMenu(ui->smilesButton);
//        QWidgetAction *smilesAction = new QWidgetAction(smilesMenu);
//        smilesAction->setDefaultWidget(emoticons::instance());
//        smilesMenu->addAction(smilesAction);
//        ui->smilesButton->setMenu(smilesMenu);
//        connect(emoticons::instance(), SIGNAL(insertSmile(QString)), this, SLOT(slotInsertSmile(QString)));
//    } else {
//        ui->smilesButton->setVisible(false);
//    }

//    //контекстное меню кнопки отправить
//    sendMenu = new QMenu(ui->sendButton);
//    sendMenu->addAction(QIcon(":/mail--arrow.png"), tr("Send message"), this, SLOT(on_sendButton_clicked()));
//    sendMenu->addAction(QIcon(":/wall--arrow.png"), tr("Send to wall"), this, SLOT(slotWallPost()));
//    sendMenu->addAction(QIcon(":/logo.png"), tr("Invite to TitanIM"), this, SLOT(slotInvite()));
//    ui->sendButton->setMenu(sendMenu);

//    //настройка интерфейса
//    ui->splitter->setStretchFactor(0, 1);
//    ui->splitter->setSizes(QList<int>() << 1 << 105);
//    ui->splitter->restoreState(appSettings::instance()->loadMain("chatForm/splitter", 0).toByteArray());
//    connect(ui->splitter, SIGNAL(splitterMoved(int,int)), this, SLOT(slotSplitterMoved(int,int)));
//    ui->sendByCtEnterButton->setChecked(!appSettings::instance()->loadProfile("chatForm/sendMsgByCtEnter",true).toBool());
}

TabForm::~TabForm()
{
    appSettings::instance()->saveMain("tabForm/geometry", geometry());
    emoticons::destroy();
}

void TabForm::resizeEvent(QResizeEvent *e){
    // ==========================================================================
    // изменение размеров окна
    // ==========================================================================

    QmlApplicationViewer::resizeEvent(e);
    emit scrollToBottom();
}

sContact TabForm::getCurrentContact(){
    // ==========================================================================
    // возвращаем пользователя с кем диалог
    // ==========================================================================

    return currentChat()->contact;
}

QString TabForm::getNameCurrentContact(){
    // ==========================================================================
    // возвращаем имя пользователя с кем диалог
    // ==========================================================================

    if (currentChat())
        return currentChat()->contact.firstName + " " + currentChat()->contact.lastName;
    else
        return "";
}

bool TabForm::sendMsgByCtEnter(){
    // ==========================================================================
    // отправлять сообщения по ctrl + enter
    // ==========================================================================

    return appSettings::instance()->loadProfile("chatForm/sendMsgByCtEnter",true).toBool();
}

void TabForm::addEvent(const QString &uid, const QString &text){
    // ==========================================================================
    // отображаем в чате событие
    // ==========================================================================

    if (appSettings::instance()->loadProfile("chatForm/notifyChat",true).toBool()){
        sMessage event;
        event.mid = "-1";
        event.flags = 65536; //флаг события
        event.fromId = uid;
        event.dateTime = QDateTime::currentDateTime();
        event.text = text;
        chats[uid]->addEvent(event);
        if (uid == currentUid){
            emit scrollToBottom();
        }
    }
}

void TabForm::setFilterMsg(bool visible){
    // ==========================================================================
    // Фильтр сообщений
    // ==========================================================================

//    ui->filterEdit->setVisible(visible);

//    if (visible){
//        ui->filterEdit->clear();
//        ui->filterEdit->setFocus();
//        ui->chatLogView->scrollToBottom();
//    } else {
//        proxy->setFilterWildcard("");
//        ui->chatLogView->scrollToBottom();
//    }
}

void TabForm::clearMessages(){
    // ==========================================================================
    // очищает лог чата
    // ==========================================================================

    currentChat()->clearMessages();
}

void TabForm::slotCopyMsg(const QModelIndex &idx){
    // ==========================================================================
    // копирую выделенные сообщения в буфер
    // ==========================================================================

    QString text="";

    text += proxy->data(idx,Qt::UserRole+1).toString();
    text += " (" + proxy->data(idx,Qt::UserRole+2).toDateTime().toString("dd.MM.yyyy hh:mm:ss") + ")\n";
    text += emoticons::instance()->fromEmoticons(proxy->data(idx,Qt::DisplayRole).toString().replace("<br>", "\n"));
    text += "\n";

    if (!text.isEmpty())
        QApplication::clipboard()->setText(text);
}

void TabForm::slotCopyTextMsg(const QModelIndex &idx){
    // ==========================================================================
    // копирую текст выделенных сообщений в буфер
    // ==========================================================================

    QString text="";

    text += emoticons::instance()->fromEmoticons(proxy->data(idx,Qt::DisplayRole).toString().replace("<br>", "\n"));
    text += "\n";

    if (!text.isEmpty())
        QApplication::clipboard()->setText(text);
}

void TabForm::slotQuote(const QModelIndex &idx){
    // ==========================================================================
    // цитируем выделенные сообщения
    // ==========================================================================

    if (!idx.isValid())
        return;

    QString text="";

    text += "> " + proxy->data(idx, Qt::UserRole+1).toString();
    text += " (" + proxy->data(idx,Qt::UserRole+2).toDateTime().toString("dd.MM.yyyy hh:mm:ss") + ")\n";
    text += "> " + emoticons::instance()->fromEmoticons(proxy->data(idx,Qt::DisplayRole).toString()).replace("<br>","\n> ");
    text += "\n";

    emit inputEditAppend(text, "currentUid");
}

void TabForm::slotMessagesMarkAsNew(const QModelIndex &idx){
    // ==========================================================================
    // отмечаем как непрочитанные
    // ==========================================================================

    QStringList mids;
    mids.append(proxy->data(idx,Qt::UserRole).toString());
    emit messagesMark(mids.join(","), true);
}

void TabForm::slotDeleteMsg(const QModelIndex &idx){
    // ==========================================================================
    // удаляем выделенные сообщения
    // ==========================================================================

    QString mid;
    mid = proxy->data(idx,Qt::UserRole).toString();
    emit messagesDelete(mid);
    currentChat()->removeMessage(mid);
}

void TabForm::slotOpenMsg(const QModelIndex &idx){
    // ==========================================================================
    // открываем выделенные сообщения в браузере
    // ==========================================================================

    QString mid;
    mid = proxy->data(idx,Qt::UserRole).toString();
    QDesktopServices::openUrl(QUrl("http://vk.com/mail.php?act=show&id="+mid));
}

void TabForm::addMessage(sMessage message, const sContact &contact){
    // ==========================================================================
    // Обрабатываем сообщение и добавляем в модель
    // все сообщения должны проходить через этот метод
    // ==========================================================================

    //обрабатываем
    message.text = message.text.replace("\n", "<br>");
    message.text = emoticons::instance()->toEmoticons(message.text);

    //добавляем в модель
    chats[contact.uid]->addMessage(message, contact);
}

int TabForm::rowCount(){
    // ==========================================================================
    // Количество сообщений в логе чата
    // ==========================================================================

    if (currentChat())
        return currentChat()->rowCount();
    else return 0;
}

void TabForm::closeEvent(QCloseEvent *pe){
    // ==========================================================================
    // по закрытию скрываем окно
    // ==========================================================================

    hide();
    pe->ignore();
}

chatModel *TabForm::currentChat(){
    // ==========================================================================
    // возвращает указатель на модель текущего чата
    // ==========================================================================

    if (chats.contains(currentUid))
        return chats[currentUid];
    else return 0;
}

void TabForm::windowActivationChange(bool e){
    // ==========================================================================
    // по активации окна
    // ==========================================================================

    if (!e && chats.count()){
        QString unreadMessages;
        unreadMessages = currentChat()->unreadMessages().join(",");
        if (!unreadMessages.isEmpty()){
            emit setTabIcon(currentUid, getCurrentContact().status.toInt() ? "qrc:/Icon_18.png" : "qrc:/Icon_17.png");
            emit messageTrayStop();
            emit messagesMark(unreadMessages, false);
        }
    }

    QmlApplicationViewer::windowActivationChange(e);
}

void TabForm::keyPressEvent(QKeyEvent *e){
    // ==========================================================================
    // скрытие по кнопке esc
    // ==========================================================================

    if (e->key() == Qt::Key_Escape)
    {
        hide();
        return;
    }

    QmlApplicationViewer::keyPressEvent(e);
}

void TabForm::slotCloseTab(const QString &uid){
    // ==========================================================================
    // клик по кнопке закрытия
    // ==========================================================================

    currentUid = "";

    #if !defined(Q_WS_MAEMO_5) && !defined(MEEGO_EDITION_HARMATTAN)
      chats[uid]->clearMessages();
      delete chats[uid];
      chats.remove(uid);
        if(chats.count() == 0){
            hide();
            return;
        }
    #endif
}

void TabForm::slotCloseTab(){
    // ==========================================================================
    // закрыть текущий таб
    // ==========================================================================

    slotCloseTab(currentUid);
}

void TabForm::slotCurrentTab(const QString &uid){
    // ==========================================================================
    // изменить текущий таб
    // ==========================================================================

    proxy->setSourceModel(chats[uid]);
    currentUid = uid;
    emit nameCurrentContactChanged(currentChat()->contact.firstName);
    emit currentUidChanged(currentUid);
    setWindowTitle(chats[uid]->contact.firstName.isEmpty() ? chats[uid]->contact.uid : chats[uid]->contact.firstName+" "+ chats[uid]->contact.lastName);
    emit setTabIcon(uid, chats[uid]->contact.status.toInt() ? "qrc:/Icon_18.png" : "qrc:/Icon_17.png");
    setWindowIcon(chats[uid]->contact.status.toInt() ? QIcon(":/Icon_18.png") : QIcon(":/Icon_17.png"));
    QString unreadMessages;
    unreadMessages = chats[uid]->unreadMessages().join(",");
    if (!unreadMessages.isEmpty()){
        emit messageTrayStop();
        emit messagesMark(unreadMessages, false);
    }
    //ui->photo->setPixmap(pChat->getCurrentContact().avatarImg.isNull() ? QPixmap(":/avatar.png") : pChat->getCurrentContact().avatarImg);
}

void TabForm::creatChat(const sContact &contact){
    // ==========================================================================
    // создаем чат в памяти
    // ==========================================================================

    #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
    if (!vibro) //так надо QFeedbackHapticsEffect - дурной else QDebug -> disabled
        vibro = new QFeedbackHapticsEffect(this);
    #endif

    chats[contact.uid] = new chatModel(contact, this);
    chats[contact.uid]->setObjectName(contact.uid);

    //получаю историю сообщений
    emit messagesGetHistory(contact.uid, 30, 0);
}

void TabForm::openChat(const sContact &contact){
    // ==========================================================================
    // открываем окно с чатом
    // ==========================================================================

    //отменяем отображение набора текста
    if (currentUid != contact.uid)
        emit userTyping(currentUid, 0);

    if (chats.contains(contact.uid)){
        emit currentTab(contact.uid);
        setWindowState(windowState() &~ Qt::WindowMinimized);
        #if !defined(Q_WS_MAEMO_5) && !defined(MEEGO_EDITION_HARMATTAN)
          showExpanded();
          raise();
          activateWindow();
        #endif
        return;
    } else {
        creatChat(contact);
        //показываем расширенный статус
        sActivity temp;
        temp.uid = contact.uid;
        temp.activity = contact.activity;
        if (!temp.activity.isEmpty())
            addEvent(temp.uid, QString("%1: \""+temp.activity+"\"").arg(tr("Status")));
    }

    emit addTab(contact.status.toInt() ? "qrc:/Icon_18.png" : "qrc:/Icon_17.png",
                contact.uid, contact.firstName.isEmpty() ? contact.uid : contact.firstName+" "+contact.lastName);
    emit currentTab(contact.uid);

    setWindowState(windowState() &~ Qt::WindowMinimized);
    #if !defined(Q_WS_MAEMO_5) && !defined(MEEGO_EDITION_HARMATTAN)
      showExpanded();
      raise();
      activateWindow();
    #endif
}

void TabForm::closeAllChat(){
    // ==========================================================================
    // закрыть все табы
    // ==========================================================================

    if (!chats.count())
        return;

    emit clearTab();

    QMap<QString, chatModel*>::const_iterator i = chats.constBegin();
    while (i != chats.constEnd()) {
        chats[i.key()]->clearMessages();
        delete chats[i.key()];
        ++i;
    }

    chats.clear();
}

void TabForm::openNewMsg(){
    // ==========================================================================
    // открываем вкладку с первым не прочитанным сообщением
    // ==========================================================================

    QMap<QString, chatModel*>::const_iterator i = chats.constBegin();
    QString unreadMessages;
    while (i != chats.constEnd()) {
        unreadMessages = i.value()->unreadMessages().join(",");
        if (!unreadMessages.isEmpty()){
            QString uid = i.key();
            emit currentTab(uid);

            setWindowState(windowState() &~ Qt::WindowMinimized);
            #if !defined(Q_WS_MAEMO_5) && !defined(MEEGO_EDITION_HARMATTAN)
              showExpanded();
              raise();
              activateWindow();
            #endif
            break;
        }
        ++i;
        QApplication::processEvents();
    }
}

void TabForm::slotSplitterMoved(int pos, int idx){
    // ==========================================================================
    // сохраняем позицию сплитера
    // ==========================================================================

    Q_UNUSED(pos);
    Q_UNUSED(idx);

//    appSettings::instance()->saveMain("chatForm/splitter", ui->splitter->saveState());
}

void TabForm::slotUserOnline(const QString &uid){
    // ==========================================================================
    // друг вошел в сеть
    // ==========================================================================

    if (chats.contains(uid)){
        emit setTabIcon(uid, "qrc:/Icon_18.png");
        if (currentUid == uid)
            setWindowIcon(QIcon(":/Icon_18.png"));
        //отправляем в модель изменение статуса
        chats[uid]->userOnline(uid);

        //отображаем в чате событие
        addEvent(uid, tr("Status change: Online"));
    }
}

void TabForm::slotUserOffline(const QString &uid, int flag){
    // ==========================================================================
    // друг вышел из сети
    // ==========================================================================

    if (chats.contains(uid)){
        emit setTabIcon(uid, "qrc:/Icon_17.png");
        if (currentUid == uid)
            setWindowIcon(QIcon(":/Icon_17.png"));
        //отправляем в модель
        chats[uid]->userOffline(uid, flag);

        //отображаем в чате событие
        addEvent(uid, tr("Status change: Offline"));
    }
}

void TabForm::slotHistoryMenu(const int &idMenu){
    // ==========================================================================
    // клик по меню истории
    // ==========================================================================

    switch (idMenu){
    case 0:
        emit messagesGetHistory(currentUid, 30, rowCount());
        break;
    case 1:
        emit messagesGetHistory(currentUid, 50, rowCount());
        break;
    case 2:
        emit messagesGetHistory(currentUid, 100, rowCount());
        break;
    default:
        break;
    }
}

void TabForm::slotHistoryNext(){
    // ==========================================================================
    // загрузка истории
    // ==========================================================================

    emit messagesGetHistory(currentUid, 10, rowCount());
}

void TabForm::slotInsertSmile(const QString &smile){
    // ==========================================================================
    // вставляем смайл
    // ==========================================================================

//    chatInputEdit->insertPlainText(smile);
//    chatInputEdit->setFocus();
}

void TabForm::slotUserMenu(const int &idMenu){
    // ==========================================================================
    // клик по меню пользователя
    // ==========================================================================

    switch (idMenu){
    case 0: //открываем страницу пользователя в браузере
        slotShowUserPage();
        break;
    case 1: //открываем фотографии пользователя в браузере
        slotShowFotoPage();
        break;
    case 2: //открываем музыку пользователя в браузере
        slotShowAudioPage();
        break;
    case 3: //открываем видео пользователя в браузере
        slotShowVideoPage();
        break;
    default:
        break;
    }
}

void TabForm::slotUserTyping(const QString &uid, const int &flag){
    // ==========================================================================
    // друг начал набирать текст
    // ==========================================================================

    if (currentUid == uid)
        emit userTyping(uid, flag);
}

void TabForm::slotMessagesSetActivity(){
    // ==========================================================================
    // оповещаем о наборе в активном чате
    // ==========================================================================

    if (isTyping)
        emit messagesSetActivity(currentUid);
}

void TabForm::slotShowUserPage(){
    // ==========================================================================
    // открываем страницу пользователя в браузере
    // ==========================================================================

    QDesktopServices::openUrl(QUrl("http://vk.com/id"+currentUid));
}

void TabForm::slotShowFotoPage(){
    // ==========================================================================
    // открываем фотографии пользователя в браузере
    // ==========================================================================

    QDesktopServices::openUrl(QUrl("http://vk.com/albums"+currentUid));
}

void TabForm::slotShowAudioPage(){
    // ==========================================================================
    // открываем музыку пользователя в браузере
    // ==========================================================================

    QDesktopServices::openUrl(QUrl("http://vk.com/audio.php?id="+currentUid));
}

void TabForm::slotShowVideoPage(){
    // ==========================================================================
    // открываем видео пользователя в браузере
    // ==========================================================================

    QDesktopServices::openUrl(QUrl("http://vk.com/video.php?id="+currentUid));
}

void TabForm::slotWallPost(){
    // ==========================================================================
    // опубликовать запись на стене у текущего собеседника
    // ==========================================================================

//    if (chatInputEdit->toPlainText().isEmpty())
//        return;

//    //отправляю сообщение
//    emit wallPost(chatInputEdit->toPlainText(), currentUid);
//    chatInputEdit->clear();
//    if (appSettings::instance()->loadProfile("chatForm/closeSendMsg",false).toBool())
//        hide();
}

void TabForm::slotInvite(){
    // ==========================================================================
    // пригласить в TitanIM текущего собеседника
    // ==========================================================================

//    emit messagesSend(currentUid, QString("%1, %2 - http://vk.com/titanim")
//                      .arg(currentChat()->getCurrentContact().firstName)
//                      .arg(tr("VK instant messenger")));
}

void TabForm::slotSettingsChanged(){
    // ==========================================================================
    // изменились настройки
    // ==========================================================================

    isTyping = appSettings::instance()->loadProfile("chatForm/typing", true).toBool();
    emit messagesActivityChanged(isTyping);
}

void TabForm::on_profileButton_clicked(){
    // ==========================================================================
    // клик по кнопке просмотреть профиль
    // ==========================================================================

    emit showProfile(currentUid);
}

void TabForm::on_filterButton_clicked()
{
    // ==========================================================================
    // клик по кнопке фильтр
    // ==========================================================================

//    currentChat()->setFilterMsg(ui->filterButton->isChecked());
}

void TabForm::on_quoteButton_clicked(const int &indexModel){
    // ==========================================================================
    // клик по кнопке цитирования
    // ==========================================================================

    QModelIndex idx = proxy->index(indexModel, 0);
    slotQuote(idx);
}

void TabForm::on_clearButton_clicked(){
    // ==========================================================================
    // очищаем окно с сообщениями
    // ==========================================================================

    currentChat()->clearMessages();
}

void TabForm::on_closeButton_clicked(){
    // ==========================================================================
    // клик по кнопке закрыть таб
    // ==========================================================================

    slotCloseTab(currentUid);
}

void TabForm::slotSendMsg(const QString &text){
    // ==========================================================================
    // отправить сообщение текущему собеседнику
    // ==========================================================================

    if (text.isEmpty())
        return;

    //отправляю сообщение
    emit messagesSend(currentUid, text);
    if (appSettings::instance()->loadProfile("chatForm/closeSendMsg",false).toBool())
        hide();
}

void TabForm::slotChatListMenu(const int &idMenu, const int &indexModel){
    // ==========================================================================
    // клик по меню чата
    // ==========================================================================

    QModelIndex idx = proxy->index(indexModel, 0);

    switch (idMenu){
    case 0:
        //цитируем выделенные сообщения
        slotQuote(idx);
        break;
    case 1:
        //копирую выделенные сообщения в буфер
        slotCopyMsg(idx);
        break;
    case 2:
        //копирую текст выделенных сообщений в буфер
        slotCopyTextMsg(idx);
        break;
    case 3:
        //отмечаем как непрочитанные
        slotMessagesMarkAsNew(idx);
        break;
    case 4:
        //открываем выделенные сообщения в браузере
        slotOpenMsg(idx);
        break;
    case 5:
        //удаляем выделенные сообщения
        slotDeleteMsg(idx);
        break;
    default:
        break;
    }
}

void TabForm::on_sendByCtEnterButton_clicked()
{
    // ==========================================================================
    // изменена раскладка для отправки сообщения
    // ==========================================================================

    appSettings::instance()->saveProfile("chatForm/sendMsgByCtEnter", !appSettings::instance()->loadProfile("chatForm/sendMsgByCtEnter",true).toBool());
}

void TabForm::on_translitButton_clicked(const QString &text){
    // ==========================================================================
    // клик по кнопке изменить раскладку текста
    // ==========================================================================

    QString txt;
    txt = invertMessage(text);
    emit inputEditAppend(txt, "currentUid");
}

void TabForm::setActivity(const QList<sActivity> &activity){
    // ==========================================================================
    // пришли расширенные статусы
    // ==========================================================================

    if (chats.count() == 0)
        return;

    for (int i=0; i < activity.count(); i++){
        if (!activity.at(i).activity.isEmpty() && chats.contains(activity.at(i).uid)){
            //отображаем в чате событие
            addEvent(activity.at(i).uid, QString("%1: \""+activity.at(i).activity+"\"").arg(tr("Status")));
        }
        QApplication::processEvents();
    }
}

void TabForm::sendMessagesError(int error, const QString &uid, const QString &text){
    // ==========================================================================
    // ошибка при отправки сообщения
    // ==========================================================================

    Q_UNUSED(error);

    if (currentUid == uid)
        emit inputEditAppend(text, "currentUid");
    else if (chats.contains(uid))
        emit inputEditAppend(text, uid);
}

void TabForm::resetMessageFlags(const QString &mid, int mask, const QString &uid){
    // ==========================================================================
    // сброс флагов сообщения
    // ==========================================================================

    if (chats.contains(uid)){
        chats[uid]->resetMessageFlags(mid, mask);

        if ((mask == 1) && (!(chats[uid]->getMessage(mid).flags & 2)))
            decCountUnreadMessages();
    }
}

void TabForm::playSound(const QString &cmd, const QString &fileName){
    // ==========================================================================
    // проигрываем звук
    // ==========================================================================

    if (!appSettings::instance()->loadProfile("notifications/quietMode",false).toBool()){
        if (appSettings::instance()->loadProfile("contactlist/sound",true).toBool()){
            #if defined(Q_WS_WIN) || defined(Q_WS_MAC)
                QSound::play(fileName);
            #elif defined(Q_WS_X11)
                QProcess::startDetached(cmd.arg(fileName));
            #endif
        }

        #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
        if (appSettings::instance()->loadProfile("contactlist/vibro",true).toBool()){
            vibro->setAttackIntensity(0.1);
            vibro->setAttackTime(150);
            vibro->setIntensity(0.75);
            vibro->setDuration(400);
            vibro->setFadeTime(150);
            vibro->setFadeIntensity(0.1);
            vibro->start();
        }
        #endif
    }
}

QString TabForm::invertMessage(const QString &text){
    // ==========================================================================
    // изменить раскладку текста
    // ==========================================================================

    QString tableR=tr("qwertyuiop[]asdfghjkl;'zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>?");
    QString tableE="qwertyuiop[]asdfghjkl;'zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>?";
    QString txt = text;
    for(int i=0; i < txt.length(); i++){
        if(txt.at(i) <= QChar('z')){
            for (int j=0; j < tableE.length(); j++)
                if(txt.at(i) == tableE.at(j)){
                    txt[i] = tableR.at(j);
                    break;
                }
        } else {
            for (int j=0; j < tableR.length(); j++)
                if(txt.at(i) == tableR.at(j)){
                    txt[i] = tableE.at(j);
                    break;
                }
        }
    }
    return txt;
}

bool TabForm::messagesActivity(){
    // ==========================================================================
    // печатает
    // ==========================================================================

    return isTyping;
}

QString TabForm::getCurrentUid(){
    // ==========================================================================
    // текущий uid
    // ==========================================================================

    return currentUid;
}

int TabForm::getCountUnreadMessages(){
    // ==========================================================================
    // общее количество непрочитанных сообщений
    // ==========================================================================

    return countUnreadMessages;
}

void TabForm::incCountUnreadMessages(){
    // ==========================================================================
    // увеличиваем количество непрочитанных сообщений
    // ==========================================================================

    countUnreadMessages++;
    emit countUnreadMessagesChanged(countUnreadMessages);
}

void TabForm::decCountUnreadMessages(){
    // ==========================================================================
    // увеличиваем количество непрочитанных сообщений
    // ==========================================================================

    countUnreadMessages--;
    emit countUnreadMessagesChanged(countUnreadMessages);
}

void TabForm::messageReceived(const sMessage &message, const sContact &contact){
    // ==========================================================================
    // пришло сообщение
    // ==========================================================================

    //создаем чат в памяти и добавляем если нужно вкладку в табе
    if (!chats.contains(message.fromId)){
        creatChat(contact);
        emit addTab(contact.status.toInt() ? "qrc:/Icon_18.png" : "qrc:/Icon_17.png",
                    message.fromId, contact.firstName.isEmpty() ? message.fromId : contact.firstName+" "+contact.lastName);
    }

    //отменяем отображение набора текста
    if ((currentUid == message.fromId) && (!(message.flags & 2)))
        emit userTyping(message.fromId, 0);

    //увеличиваем счетчик
    if (!(message.flags & 2))
        incCountUnreadMessages();

    //отображаем в чате сообщение
    addMessage(message, contact);
    if (message.fromId == currentUid){
        emit scrollToBottom();
    }

    //устанавливаем иконку нового сообщения
    if (!(message.flags & 2)){
      #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
        if (currentUid != message.fromId){
            emit setTabIcon(message.fromId, "qrc:/mail.png");
            emit messageTrayStart(message);
        } else
            emit messagesMark(chats[message.fromId]->unreadMessages().join(","), false);
      #else
        if ((!isActiveWindow() || currentUid != message.fromId)){
            emit setTabIcon(message.fromId, "qrc:/mail.png");
            emit messageTrayStart(message);
        } else
            emit messagesMark(chats[message.fromId]->unreadMessages().join(","), false);
      #endif
    }

    if ((message.flags & 2) || (!appSettings::instance()->loadProfile("contactlist/soundMsg",true).toBool()) || (isActiveWindow()
        && currentUid != message.fromId && appSettings::instance()->loadProfile("chatForm/actNotSound",false).toBool()))
       return;

    playSound(appSettings::instance()->loadMain("main/cmdSound", "aplay -q \"%1\"").toString(),
                        appSettings::instance()->homeAppDir() + "sounds/message.wav");
}

void TabForm::history(const QList<sMessage> &messages, const sContact &contact){
    // ==========================================================================
    // пришла история
    // ==========================================================================

    for (int i=0; i < messages.count(); i++){
        addMessage(messages.value(i), contact);
        QApplication::processEvents();
    }

    if (contact.uid == currentUid){
        emit scrollToBottom();
    }
}
