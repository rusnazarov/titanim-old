/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "chatdialogs.h"

chatDialogs::chatDialogs(QObject *parent) :
    QObject(parent)
{
    model = new dialogsModel();

    proxy = new QSortFilterProxyModel(this);
    proxy->setDynamicSortFilter(true);
    proxy->setSortCaseSensitivity(Qt::CaseInsensitive);
    proxy->setFilterCaseSensitivity(Qt::CaseInsensitive);
    proxy->setSortRole(Qt::UserRole+2);
    proxy->sort(0, Qt::DescendingOrder);
    proxy->setSourceModel(model);
}

chatDialogs::~chatDialogs(){
    delete proxy;
    delete model;
}

void chatDialogs::appendDialogs(const QVector<sDialog> &dialogs){
    // ==========================================================================
    // пришли диалоги
    // ==========================================================================

    model->appendDialogs(dialogs);
}

void chatDialogs::replaceDialogs(const QVector<sDialog> &dialogs){
    // ==========================================================================
    // пришли диалоги
    // ==========================================================================

    model->replaceDialogs(dialogs);
}
