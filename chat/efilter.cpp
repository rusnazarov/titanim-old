/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "efilter.h"

eFilter::eFilter(QObject *pobj) :
    QObject(pobj)
{
}

//FACEPALM
bool eFilter::eventFilter(QObject *pobj, QEvent *pe){
    switch (pe->type()){
    case QEvent::FocusIn:
        emit clear();
        return false;
    case QEvent::KeyPress:
        //отправка сообщения
        if (((QKeyEvent*)pe)->key() == Qt::Key_Return || ((QKeyEvent*)pe)->key() == Qt::Key_Enter){
            if ((appSettings::instance()->loadProfile("chatForm/sendMsgByCtEnter",true).toBool() && ((QKeyEvent*)pe)->modifiers() & Qt::ControlModifier) ||
                (!appSettings::instance()->loadProfile("chatForm/sendMsgByCtEnter",true).toBool() && !(((QKeyEvent*)pe)->modifiers() & Qt::ControlModifier))){
                emit sendMsg();
                return true;
            } else {
                qobject_cast<QTextEdit*>(pobj)->insertPlainText("\n");
                return true;
            }
        }

        //переключение табов
        if (((QKeyEvent*)pe)->key() == Qt::Key_Tab && (((QKeyEvent*)pe)->modifiers() & Qt::ControlModifier)){
            emit changeTab();
            return true;
        }

        //esc - скрытие окна чата
        if (((QKeyEvent*)pe)->key() == Qt::Key_Escape){
            emit hideTabForm();
            return true;
        }

        return false;
    default:
        return false;
    }
}
