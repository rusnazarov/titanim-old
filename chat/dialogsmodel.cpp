/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "dialogsmodel.h"

dialogsModel::dialogsModel(QObject *parent) :
    QAbstractListModel(parent)
{
    QHash<int, QByteArray> roles = roleNames();
    roles[textRole] = "text";
    roles[titleRole] = "title";
    roles[dateRole] = "date";
    roles[dateStrRole] = "dateStr";
    roles[uidRole] = "uid";
    roles[midRole] = "mid";
    roles[readStateRole] = "read";
    roles[outRole] = "out";
    setRoleNames(roles);
}

void dialogsModel::appendDialogs(QVector<sDialog> dialogs){
    // ==========================================================================
    // добавление диалогов
    // ==========================================================================

    if (!dialogs.count())
        return;

    //проверяем уникальность только по одному элементу //todo
    for (int i = 0; i < this->dialogs.count(); i++){
        if (!dialogs.at(0).mid.isEmpty() && dialogs.at(0).mid == this->dialogs.at(i).mid)
            return;
    }

    //добавляем новые
    beginInsertRows(QModelIndex(), this->dialogs.count(), this->dialogs.count() + dialogs.count() - 1);
    this->dialogs << dialogs;
    endInsertRows();
}

void dialogsModel::replaceDialogs(QVector<sDialog> dialogs){
    // ==========================================================================
    // замена диалогов
    // ==========================================================================

    //очищаем модель
    removeDialog(0, rowCount());

    //добавляем новые
    appendDialogs(dialogs);
}

QString dialogsModel::dateText(QDateTime date) const{
    // ==========================================================================
    // переводит дату в удобную для человеку
    // ==========================================================================

    QVector<QString> daysName;
    daysName.resize(8);
    daysName[1] = tr("Monday");
    daysName[2] = tr("Tuesday");
    daysName[3] = tr("Wednesday");
    daysName[4] = tr("Thursday");
    daysName[5] = tr("Friday");
    daysName[6] = tr("Saturday");
    daysName[7] = tr("Sunday");

    int days = date.daysTo(QDateTime::currentDateTime());

    switch (days) {
    case 0:
        return date.time().toString("hh:mm");
    case 1:
        return tr("yesterday");
    default:
        if (days < 7)
            return daysName.at(date.date().dayOfWeek());
        return date.date().toString("dd.MM.yy");
    }
}

bool dialogsModel::removeDialog(int row, int count){
    // ==========================================================================
    // удаляем с модели данные
    // ==========================================================================

    beginRemoveRows(QModelIndex(), row, row+count-1);

    for (int i = 0; i < count; ++i) {
        dialogs.remove(row);
    }

    endRemoveRows();
    return true;
}

QVariant dialogsModel::data(const QModelIndex &index, int role) const{
    // ==========================================================================
    // данные модели
    // ==========================================================================

    if (!index.isValid() || index.row() >= dialogs.size()) {
        return QVariant();
    }

    switch (role) {
    case Qt::DisplayRole:
        return dialogs.at(index.row()).firstName.isEmpty() ?
                dialogs.at(index.row()).uid :
                dialogs.at(index.row()).firstName + " " + dialogs.at(index.row()).lastName;

    case Qt::DecorationRole:
        return dialogs.at(index.row()).avatarUrl;

    case textRole:
        return dialogs.at(index.row()).body;

    case titleRole:
        return dialogs.at(index.row()).title;

    case dateRole:
        return dialogs.at(index.row()).date;

    case dateStrRole:
        return dateText(dialogs.at(index.row()).date);

    case uidRole:
        return dialogs.at(index.row()).uid;

    case midRole:
        return dialogs.at(index.row()).mid;

    case readStateRole:
        return dialogs.at(index.row()).readState;

    case outRole:
        return dialogs.at(index.row()).out;
    }

    return QVariant();
}

bool dialogsModel::setData(const QModelIndex &index, const QVariant &value, int role){
    // ==========================================================================
    // редактирование данных модели
    // ==========================================================================

    if (!index.isValid() && !value.isValid() && index.row() >= dialogs.size()) {
        return false;
    }

    return false;
}

int dialogsModel::rowCount(const QModelIndex &parent) const{
    // ==========================================================================
    // количество строк в модели
    // ==========================================================================

    Q_UNUSED(parent);
    return dialogs.count();
}

Qt::ItemFlags dialogsModel::flags(const QModelIndex &index) const{
    // ==========================================================================
    // флаги модели
    // ==========================================================================

    if (index.isValid())
      return (Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    else
      return Qt::NoItemFlags;
}
