/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef DIALOGSMODEL_H
#define DIALOGSMODEL_H

#include <QAbstractListModel>
#include "vk/cvkStruct.h"

class dialogsModel : public QAbstractListModel
{
Q_OBJECT

private:
    QVector<sDialog> dialogs;

public:
    explicit dialogsModel(QObject *parent = 0);
    void appendDialogs(QVector<sDialog> dialogs);
    void replaceDialogs(QVector<sDialog> dialogs);
    QString dateText(QDateTime date) const;
    bool removeDialog(int row, int count);
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant& value, int role = Qt::EditRole);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    enum dialogsRole {
        textRole = Qt::UserRole,
        titleRole = Qt::UserRole+1,
        dateRole = Qt::UserRole+2,
        dateStrRole = Qt::UserRole+3,
        uidRole = Qt::UserRole+4,
        midRole = Qt::UserRole+5,
        readStateRole = Qt::UserRole+6,
        outRole = Qt::UserRole+7
    };

public slots:

signals:
};

#endif // DIALOGSMODEL_H
