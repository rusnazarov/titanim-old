/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "chatform.h"
#include "ui_chatform.h"

ChatForm::ChatForm(const sContact &contact, QWidget *parent) :
    QmlApplicationViewer(parent)
{
    newMessage = false;

    //по умолчанию фильтр скрыт
//    ui->filterEdit->setVisible(false);

    //запоминаем uid кому принадлежит этот чат
    this->uid = contact.uid;

    //очищаем строку ввода сообщений
    inputEditText = "";

    //контекстное меню чата
//    chatLogMenu = new QMenu(ui->chatLogView);
//    chatLogMenu->addAction(QIcon(":/Icon_36.ico"), tr("Reply Quoted"), this, SLOT(slotQuote()));
//    chatLogMenu->addAction(QIcon(":/clipboard.png"), tr("Copy"), this, SLOT(slotCopyMsg()));
//    chatLogMenu->addAction(QIcon(":/clipboard-list.png"), tr("Copy Text"), this, SLOT(slotCopyTextMsg()));
//    chatLogMenu->addAction(QIcon(":/mail--exclamation.png"), tr("Mark as Unread"), this, SLOT(slotMessagesMarkAsNew()));
//    chatLogMenu->addAction(QIcon(":/Icon_46.ico"), tr("Open VK"), this, SLOT(slotOpenMsg()));
//    chatLogMenu->addSeparator();
//    chatLogMenu->addAction(QIcon(":/cross-script.png"), tr("Delete"), this, SLOT(slotDeleteMsg()));
//    connect(ui->chatLogView, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotChatLogMenu(QPoint)));

    //настройка интерфейса
//    ui->chatLogView->setVerticalScrollMode(ui->chatLogView->ScrollPerPixel);

    //модель
    model = new chatModel(contact, this);
    model->setColor(appSettings::instance()->loadProfile("chatForm/colorNameIn", QColor("#c83f6b")).value<QColor>(),
                    appSettings::instance()->loadProfile("chatForm/colorNameOut", QColor("#0860ce")).value<QColor>(),
                    appSettings::instance()->loadProfile("chatForm/colorBackgroundIn", QColor("#dbdbdb")).value<QColor>(),
                    appSettings::instance()->loadProfile("chatForm/colorBackgroundOut", QColor("#eeeeee")).value<QColor>(),
                    appSettings::instance()->loadProfile("chatForm/colorDate", QColor("#000000")).value<QColor>(),
                    appSettings::instance()->loadProfile("chatForm/colorSeparator", QColor("#808080")).value<QColor>());
    proxy = new QSortFilterProxyModel(this);
    proxy->setDynamicSortFilter(true);
    proxy->setSourceModel(model);
    proxy->setSortCaseSensitivity(Qt::CaseInsensitive);
    proxy->setFilterCaseSensitivity(Qt::CaseInsensitive);
    proxy->setSortRole(Qt::UserRole+2);
    proxy->sort(0);

    setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    rootContext()->setContextProperty("chat", this);
    rootContext()->setContextProperty("chatModel", proxy);
    setMainQmlFile(appSettings::instance()->homeAppDir() + "qml/chat.qml");

    //фильтр
//    connect(ui->filterEdit, SIGNAL(textChanged(QString)), proxy, SLOT(setFilterWildcard(QString)));

    //устанавливаем модель
//    ui->chatLogView->setModel(proxy);

    //устанавливаем делегат
//    ui->chatLogView->setItemDelegate(new chatDelegate(ui->chatLogView));
}

ChatForm::~ChatForm()
{
}

void ChatForm::resizeEvent(QResizeEvent *){
    // ==========================================================================
    // изменение размеров окна
    // ==========================================================================

//    ui->chatLogView->reset();
//    ui->chatLogView->scrollToBottom();
}

sContact ChatForm::getContact(){
    // ==========================================================================
    // возвращаем пользователя с кем диалог
    // ==========================================================================

    return model->contact;
}

void ChatForm::addEvent(const QString &uid, const QString &text){
    // ==========================================================================
    // отображаем в чате событие
    // ==========================================================================

    if (appSettings::instance()->loadProfile("chatForm/notifyChat",true).toBool()){
        sMessage event;
        event.mid = "-1";
        event.flags = 65536; //флаг события
        event.fromId = uid;
        event.dateTime = QDateTime::currentDateTime();
        event.text = text;
        model->addEvent(event);
//        ui->chatLogView->scrollToBottom();
    }
}

void ChatForm::userOnline(const QString &uid){
    // ==========================================================================
    // друг вошел в сеть
    // ==========================================================================

    //отправляем в модель изменение статуса
    model->userOnline(uid);

    //отображаем в чате событие
    addEvent(uid, tr("Status change: Online"));
}

void ChatForm::userOffline(const QString &uid, int flag){
    // ==========================================================================
    // друг вышел из сети
    // ==========================================================================

    //отправляем в модель
    model->userOffline(uid, flag);

    //отображаем в чате событие
    addEvent(uid, tr("Status change: Offline"));
}

void ChatForm::setActivity(const sActivity &activity){
    // ==========================================================================
    // пришел расширенный статус
    // ==========================================================================

    //отображаем в чате событие
    if (!activity.activity.isEmpty())
        addEvent(activity.uid, QString("%1: \""+activity.activity+"\"").arg(tr("Status")));
}

void ChatForm::setFilterMsg(bool visible){
    // ==========================================================================
    // Фильтр сообщений
    // ==========================================================================

//    ui->filterEdit->setVisible(visible);

//    if (visible){
//        ui->filterEdit->clear();
//        ui->filterEdit->setFocus();
//        ui->chatLogView->scrollToBottom();
//    } else {
//        proxy->setFilterWildcard("");
//        ui->chatLogView->scrollToBottom();
//    }
}

void ChatForm::slotClearSelection(){
    // ==========================================================================
    // убираем выделение с лога чата
    // ==========================================================================

//    ui->chatLogView->clearSelection();
}

void ChatForm::clearMessages(){
    // ==========================================================================
    // очищает лог чата
    // ==========================================================================

    model->clearMessages();
}

void ChatForm::slotChatLogMenu(const QPoint &point){
    // ==========================================================================
    // показываем меню для сообщений
    // ==========================================================================

//    chatLogMenu->exec(ui->chatLogView->mapToGlobal(point));
}

void ChatForm::slotCopyMsg(){
    // ==========================================================================
    // копирую выделенные сообщения в буфер
    // ==========================================================================

//    QString text="";

//    for (int i=0; i < ui->chatLogView->selectionModel()->selectedIndexes().count(); i++){
//        text += ui->chatLogView->selectionModel()->selectedIndexes().at(i).data(Qt::UserRole+1).toString();
//        text += " (" + ui->chatLogView->selectionModel()->selectedIndexes().at(i).data(Qt::UserRole+2).toDateTime().toString("dd.MM.yyyy hh:mm:ss") + ")\n";
//        text += emoticons::instance()->fromEmoticons(ui->chatLogView->selectionModel()->selectedIndexes().at(i).data(Qt::DisplayRole).toString().replace("<br>", "\n"));
//        text += "\n";
//    }

//    if (!text.isEmpty())
//        QApplication::clipboard()->setText(text);
}

void ChatForm::slotCopyTextMsg(){
    // ==========================================================================
    // копирую текст выделенных сообщений в буфер
    // ==========================================================================

//    QString text="";

//    for (int i=0; i < ui->chatLogView->selectionModel()->selectedIndexes().count(); i++){
//        text += emoticons::instance()->fromEmoticons(ui->chatLogView->selectionModel()->selectedIndexes().at(i).data(Qt::DisplayRole).toString().replace("<br>", "\n"));
//        text += "\n";
//    }

//    if (!text.isEmpty())
//        QApplication::clipboard()->setText(text);
}

void ChatForm::slotQuote(){
    // ==========================================================================
    // цитируем выделенные сообщения
    // ==========================================================================

//    QString text="";

//    for (int i=0; i < ui->chatLogView->selectionModel()->selectedIndexes().count(); i++){
//        text += "> " + ui->chatLogView->selectionModel()->selectedIndexes().at(i).data(Qt::UserRole+1).toString();
//        text += " (" + ui->chatLogView->selectionModel()->selectedIndexes().at(i).data(Qt::UserRole+2).toDateTime().toString("dd.MM.yyyy hh:mm:ss") + ")\n";
//        text += "> " + emoticons::instance()->fromEmoticons(ui->chatLogView->selectionModel()->selectedIndexes().at(i).data(Qt::DisplayRole).toString()).replace("<br>","\n> ");
//        text += "\n";
//    }

//    if (!text.isEmpty())
//        emit inputEditAppend(text);
}

void ChatForm::slotMessagesMarkAsNew(){
    // ==========================================================================
    // отмечаем как непрочитанные
    // ==========================================================================

//    QStringList mids;
//    for (int i=0; i < ui->chatLogView->selectionModel()->selectedIndexes().count(); i++)
//        mids.append(ui->chatLogView->selectionModel()->selectedIndexes().at(i).data(Qt::UserRole).toString());

//    emit messagesMark(mids.join(","), true);
}

void ChatForm::slotDeleteMsg(){
    // ==========================================================================
    // удаляем выделенные сообщения
    // ==========================================================================

//    QString mid;
//    int count =  ui->chatLogView->selectionModel()->selectedIndexes().count();
//    for (int i=0; i < count; i++){
//        mid = ui->chatLogView->selectionModel()->selectedIndexes().at(0).data(Qt::UserRole).toString();
//        emit messagesDelete(mid);
//        model->removeMessage(mid);
//    }
}

void ChatForm::slotOpenMsg(){
    // ==========================================================================
    // открываем выделенные сообщения в браузере
    // ==========================================================================

//    QString mid;
//    for (int i=0; i < ui->chatLogView->selectionModel()->selectedIndexes().count(); i++){
//        mid = ui->chatLogView->selectionModel()->selectedIndexes().at(i).data(Qt::UserRole).toString();
//        QDesktopServices::openUrl(QUrl("http://vk.com/mail.php?act=show&id="+mid));
//    }
}

void ChatForm::addMessage(sMessage message, const sContact &contact){
    // ==========================================================================
    // Обрабатываем сообщение и добавляем в модель
    // все сообщения должны проходить через этот метод
    // ==========================================================================

    //обрабатываем
    message.text = message.text.replace("\n", "<br>");
    message.text = emoticons::instance()->toEmoticons(message.text);

    //добавляем в модель
    model->addMessage(message, contact);
}

void ChatForm::resetMessageFlags(const QString &mid, int mask){
    // ==========================================================================
    // сброс флагов сообщения
    // ==========================================================================

    model->resetMessageFlags(mid, mask);
}

void ChatForm::messageReceived(const sMessage &message, const sContact &contact){
    // ==========================================================================
    // пришло сообщение
    // ==========================================================================

    addMessage(message, contact);
//    ui->chatLogView->scrollToBottom();
}

QString ChatForm::unreadMessages(){
    // ==========================================================================
    // возращает не прочитанные сообщения
    // ==========================================================================

    return model->unreadMessages().join(",");
}

void ChatForm::history(const QList<sMessage> &messages, const sContact &contact){
    // ==========================================================================
    // пришла история
    // ==========================================================================

    for (int i=0; i < messages.count(); i++)
        addMessage(messages.value(i), contact);

//    ui->chatLogView->scrollToBottom();
}

int ChatForm::rowCount(){
    // ==========================================================================
    // Количество сообщений в логе чата
    // ==========================================================================

    return model->rowCount();
}
