/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef TABFORM_H
#define TABFORM_H

#include <QDeclarativeContext>
#include <QResizeEvent>
#include <QSortFilterProxyModel>
#include <QProcess>
#include <QWidgetAction>
#include <QSound>
#include <QMenu>
#include <QClipboard>
#include "qmlapplicationviewer.h"
#include "chatmodel.h"
#include "emoticons.h"
#include "efilter.h"
#include "../settings.h"

#if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
#include <QFeedbackHapticsEffect>
QT_USE_NAMESPACE
QTM_USE_NAMESPACE
#endif

class TabForm : public QmlApplicationViewer
{
    Q_OBJECT
    Q_PROPERTY(bool sendMsgByCtEnter READ sendMsgByCtEnter NOTIFY sendMsgByCtEnterChanged)
    Q_PROPERTY(QString nameCurrentContact READ getNameCurrentContact NOTIFY nameCurrentContactChanged)
    Q_PROPERTY(bool messagesActivity READ messagesActivity NOTIFY messagesActivityChanged)
    Q_PROPERTY(QString getCurrentUid READ getCurrentUid NOTIFY currentUidChanged)
    Q_PROPERTY(int countUnreadMessages READ getCountUnreadMessages NOTIFY countUnreadMessagesChanged)

private:
    QMap <QString, chatModel*> chats;
    eFilter *filter;
    QMenu *smilesMenu;
    QMenu *sendMenu;
    bool isTyping;
    int countUnreadMessages;
    #if defined(Q_WS_MAEMO_5) || defined(MEEGO_EDITION_HARMATTAN)
    QFeedbackHapticsEffect *vibro;
    #endif

private:
    bool sendMsgByCtEnter();
    void addEvent(const QString &uid, const QString &text);
    chatModel *currentChat();
    void playSound(const QString &cmd, const QString &fileName);
    QString invertMessage(const QString &text);
    bool messagesActivity();
    QString getCurrentUid();
    int getCountUnreadMessages();
    void incCountUnreadMessages();
    void decCountUnreadMessages();

public:
    QString currentUid;
    QSortFilterProxyModel *proxy;

public:
    TabForm(QWidget *parent = 0);
    ~TabForm();
    sContact getCurrentContact();
    QString getNameCurrentContact();
    void addMessage(sMessage message, const sContact &contact);
    void clearMessages();
    int rowCount();
    void setFilterMsg(bool visible);
    void creatChat(const sContact &contact);
    void openChat(const sContact &contact);
    void closeAllChat();
    void openNewMsg();
    void sendMessagesError(int error, const QString &uid, const QString &text);
    void resetMessageFlags(const QString &mid, int mask, const QString &uid);
    void messageReceived(const sMessage &message, const sContact &contact);
    void history(const QList<sMessage> &messages, const sContact &contact);
    void setActivity(const QList<sActivity> &activity);

protected:
    void resizeEvent(QResizeEvent *e);
    void closeEvent(QCloseEvent *pe);
    void windowActivationChange(bool e);
    void keyPressEvent(QKeyEvent *e);

private slots:
    void slotCopyMsg(const QModelIndex &idx);
    void slotCopyTextMsg(const QModelIndex &idx);
    void slotMessagesMarkAsNew(const QModelIndex &idx);
    void slotDeleteMsg(const QModelIndex &idx);
    void slotOpenMsg(const QModelIndex &idx);
    void slotSplitterMoved(int pos, int idx);
    void slotUserOnline(const QString &uid);
    void slotUserOffline(const QString &uid, int flag);
    void slotInsertSmile(const QString &smile);
    void slotShowUserPage();
    void slotShowFotoPage();
    void slotShowAudioPage();
    void slotShowVideoPage();
    void slotWallPost();
    void slotInvite();
    void on_filterButton_clicked();

public slots:
    void slotQuote(const QModelIndex &idx);
    void slotSettingsChanged();
    void slotCloseTab(const QString &uid);
    void slotCloseTab();
    void slotCurrentTab(const QString &uid);
    void slotSendMsg(const QString &text);
    void slotChatListMenu(const int &idMenu, const int &indexModel);
    void slotHistoryMenu(const int &idMenu);
    void slotHistoryNext();
    void slotUserMenu(const int &idMenu);
    void slotUserTyping(const QString &uid, const int &flag);
    void slotMessagesSetActivity();
    void on_profileButton_clicked();
    void on_translitButton_clicked(const QString &text);
    void on_quoteButton_clicked(const int &indexModel);
    void on_clearButton_clicked();
    void on_closeButton_clicked();
    void on_sendByCtEnterButton_clicked();

signals:
    void sendMsgByCtEnterChanged(const bool &on);
    void nameCurrentContactChanged(const QString &name);
    void currentUidChanged(const QString &uid);
    void inputEditAppend(const QString &text, const QString &uid);
    void messagesSend(const QString &uid, const QString &text);
    void messageTrayStart(const sMessage &message);
    void messageTrayStop();
    void messagesGetHistory(const QString &uid, int count=10, int offset=0);
    void messagesMark(const QString &mids, const bool &newState);
    void messagesDelete(const QString &mid);
    void showProfile(const QString &uid);
    void wallPost(const QString &text, const QString &uid);
    void scrollToBottom();
    void addTab(const QString &icon, const QString &uid, const QString &label);
    void setTabIcon(const QString &uid, const QString &icon);
    void currentTab(const QString &uid);
    void clearTab();
    void userTyping(const QString &uid, const int &flag);
    void messagesSetActivity(const QString &uid="", const QString &chat_id="", const QString &type="typing");
    void messagesActivityChanged(bool on);
    void countUnreadMessagesChanged(const int &count);
};

#endif // TABFORM_H
