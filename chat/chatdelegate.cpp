/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "chatdelegate.h"

chatDelegate::chatDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
}

void chatDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const{
    if(!index.isValid()){
        return;
    }

    //Шрифт и цвет имени
    QFont nameFont = option.font;
    nameFont.setBold(true);
    QColor nameFontColor = index.data(Qt::ForegroundRole).value<QColor>();
    if (!nameFontColor.isValid())
        nameFontColor = painter->pen().color();

    //Шрифт и цвет текста
    QFont textFont = option.font;
    QColor textFontColor = painter->pen().color();

    //Шрифт и цвет даты
    QFont dateFont = option.font;
    dateFont.setPointSize(8);
    QColor dateFontColor = index.data(Qt::UserRole+3).value<QColor>();

    painter->save();

    //рисуем фон
    painter->fillRect(option.rect, QColor(index.data(Qt::BackgroundRole).value<QColor>()));

    //если выбрано
    if (option.state & QStyle::State_Selected)
    {
        nameFontColor = option.palette.color(QPalette::HighlightedText);
        textFontColor = option.palette.color(QPalette::HighlightedText);
        dateFontColor = option.palette.color(QPalette::HighlightedText);
        painter->fillRect(option.rect, option.palette.brush(QPalette::Highlight));
    }

    //получаем иконку
    QPixmap icon = index.data(Qt::DecorationRole).value<QPixmap>();

    //получаем имя текст и дату
    QString name = index.data(Qt::UserRole+1).toString();
    QString text = index.data(Qt::DisplayRole).toString();
    QString date = index.data(Qt::UserRole+2).toDateTime().toString("dd.MM.yyyy hh:mm:ss");

    //определяем где рисовать
    int x = option.rect.x();
    int y = option.rect.y();
    int w = option.rect.width();

    //рисуем иконку
    painter->drawPixmap(x+5, y+3, icon);

    //размер шрифта имени
    QFontMetrics fm1(nameFont);
    int nameH = fm1.height();

    //размер шрифта даты
    QFontMetrics fm3(dateFont);
    QSize dateS = fm3.size(Qt::TextDontClip, date);

    //определяем где рисовать
    x = x+icon.size().width()+9;

    //рисуем имя
    painter->setPen(QPen(nameFontColor));
    painter->setFont(nameFont);
    painter->drawText(QRect(x, y+2, w-x, nameH), Qt::TextDontClip, fm1.elidedText(name+":", Qt::ElideRight, w-x));

    //рисуем дату
    painter->setPen(QPen(dateFontColor));
    painter->setFont(dateFont);
    painter->drawText(QRect(w-dateS.width()-5, y+2, dateS.width(), dateS.height()), Qt::TextDontClip, date);

    //рисуем текст
    x = option.rect.x();
    QTextDocument doc;
    doc.setHtml(text);
    doc.setTextWidth(w);
    doc.setDefaultFont(textFont);
    QPixmap pix(doc.size().toSize());
    pix.fill(Qt::transparent);
    QPainter p(&pix);
    p.setPen(Qt::NoPen);
    QAbstractTextDocumentLayout::PaintContext ctx;
    QPalette myPalette = option.palette;
    myPalette.setColor(QPalette::Text, textFontColor);
    ctx.palette = myPalette;
    QRect r = pix.rect();
    if (r.isValid()){
        p.setClipRect(r);
        ctx.clip = r;
    }
    doc.documentLayout()->draw(&p, ctx);
    painter->drawPixmap(x, y+nameH, pix);

    //линия разделитель
    QColor separatorColor = index.data(Qt::UserRole+4).value<QColor>();
    painter->setPen(QPen(separatorColor));
    painter->drawLine(x,y+option.rect.height()-1,option.rect.width(),y+option.rect.height()-1);

    painter->restore();
}

QSize chatDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const{
    if(!index.isValid()){
        return QSize();
    }
    QSize newSize;

    //размер шрифта имени
    QFont nameFont = option.font;
    nameFont.setBold(true);
    QFontMetrics fm1(nameFont);
    int nameH = fm1.height();

    //размер шрифта текста
    QString text = index.data(Qt::DisplayRole).toString();
    QTextDocument doc;
    doc.setHtml(text);
    doc.setTextWidth(option.rect.width() - 10);
    doc.setDefaultFont(option.font);

    newSize.setWidth(option.rect.width());
    newSize.setHeight(nameH + doc.size().toSize().height() + 2);

    return newSize;
}

QWidget *chatDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const{
    Q_UNUSED(option);
    Q_UNUSED(index);

    QTextEdit *editor = new QTextEdit(parent);
    editor->setReadOnly(true);
    return editor;
}

void chatDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const{
    QString text = index.model()->data(index, Qt::EditRole).toString().replace("<br>", "\n");
    QTextEdit *textEdit = static_cast<QTextEdit*>(editor);
    textEdit->setPlainText(emoticons::instance()->fromEmoticons(text));
}

void chatDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const{
    Q_UNUSED(index);

    editor->setGeometry(option.rect);
}


























