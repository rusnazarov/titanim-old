/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef CHATMODEL_H
#define CHATMODEL_H

#include <QAbstractListModel>
#include <QFont>
#include "../vk/cvkStruct.h"
#include "../settings.h"

class chatModel : public QAbstractListModel
{
Q_OBJECT

private:
  QList<sMessage> messages;
  QColor nameIn, nameOut, backgroundIn, backgroundOut, date, separator;

public:
  sContact contact;

public:
    explicit chatModel(const sContact &contact, QObject *parent = 0);
    void setColor(const QColor &nameIn, const QColor &nameOut, const QColor &backgroundIn, const QColor &backgroundOut, const QColor &date, const QColor &separator);
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant& value, int role = Qt::EditRole);
    bool insertMessage(int row, const sMessage &message, const sContact &contact);
    bool changeMessage(const QModelIndex &index, const sMessage &message, const sContact &contact);
    bool removeMessage(int row, int count);
    bool removeMessage(const QString &mid);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    void resetMessageFlags(const QString &mid, int mask);
    void userOnline(const QString &uid);
    void userOffline(const QString &uid, int flag);
    void addMessage(const sMessage &message, const sContact &contact);
    void addEvent(const sMessage &event);
    void clearMessages();
    sMessage getMessage(const QString &mid);

    enum chatRole {
        midRole = Qt::UserRole,
        nameRole = Qt::UserRole+1,
        dateRole = Qt::UserRole+2,
        flagsRole = Qt::UserRole+5,
        uidRole = Qt::UserRole+6,
        photosRole = Qt::UserRole+7,
        photosBigRole = Qt::UserRole+8,
        audioRole = Qt::UserRole+9
    };

public slots:
    QStringList unreadMessages();
    int countUnreadMessages();
};

#endif // CHATMODEL_H
