/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "emoticons.h"

emoticons *emoticons::aInstance = 0;

emoticons *emoticons::instance()
{
    if (!aInstance) aInstance = new emoticons();
    return aInstance;
}

void emoticons::destroy() {
    if (aInstance)
        delete aInstance, aInstance = 0;
}

emoticons::emoticons()
{
    if (!appSettings::instance()->loadMain("smilies/enabled",true).toBool())
        return;
    QString smiliesName = appSettings::instance()->loadMain("smilies/smilies","default").toString();
    if (!QFile::exists(appSettings::instance()->homeAppDir() + "smilies/" + smiliesName))
        smiliesName = "default";
    myWidget = new QWidget;
    gridLayout = new QGridLayout(myWidget);
    gridLayout->setSpacing(4);
    myWidget->setLayout(gridLayout);
    setMaximumHeight(275);
    setFrameStyle(QFrame::NoFrame);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    //файлы смайлов
    QDir qmDir(appSettings::instance()->homeAppDir() + "smilies/" + smiliesName);
    QStringList filters;
    foreach (QByteArray format, QImageReader::supportedImageFormats())
        filters += "*." + format;
    QStringList fileNames = qmDir.entryList(filters, QDir::Files);

    //файл описания смайлов
    QFile file(appSettings::instance()->homeAppDir() + "smilies/" + smiliesName + "/_define.ini");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;
    QStringList smileList;
    QTextStream in(&file);
    while (!in.atEnd())
        smileList.append(in.readLine());
    file.close();

    int r = 0;
    int c = 0;
    for (int i=0; i < qMin(fileNames.count(), smileList.count()); i++){
        foreach (QString str, smileList.at(i).split(",")){
            smiles[0].append(str);
            smiles[1].append(appSettings::instance()->homeAppDir() + "smilies/" + smiliesName + "/" + fileNames.at(i));
        }

        QLabel *smileLabel = new QLabel();
        smileLabel->setPixmap(QPixmap(appSettings::instance()->homeAppDir() + "smilies/" + smiliesName + "/" + fileNames.at(i)));
        smileLabel->setToolTip(smileList.at(i).split(",").at(0));
        gridLayout->addWidget(smileLabel, r, c, Qt::AlignHCenter | Qt::AlignTop);
        smileLabel->installEventFilter(this);

        if (c == 6){
            c = 0;
            r++;
        } else c++;
    }

    //устанавливаем цвет фона
    myWidget->setStyleSheet("QWidget {background-color: #ffffff;}");
    setStyleSheet("QScrollArea {background-color: #ffffff;}");

    setWidget(myWidget);
}

bool emoticons::eventFilter(QObject *obj, QEvent *event){
    // ==========================================================================
    // фильтр событий на QLabel
    // ==========================================================================

    if(event->type() == QEvent::MouseButtonPress){
        QLabel *label = qobject_cast<QLabel*>(obj);
        if (label)
            emit insertSmile(label->toolTip());
        return false;
    } else {
        return QObject::eventFilter(obj, event);
    }
}

QString emoticons::toEmoticons(const QString &text){
    // ==========================================================================
    // вставляет html вставки в текст
    // ==========================================================================

    QString temp = text;

    for (int i=0; i < smiles[0].count(); i++){
        temp = temp.replace(smiles[0].at(i), "<img src=\"" + smiles[1].at(i) + "\"/>");
    }

    return temp;
}

QString emoticons::fromEmoticons(const QString &text){
    // ==========================================================================
    // убирает html вставки из текста
    // ==========================================================================

    QString temp = text;

    for (int i=0; i < smiles[0].count(); i++){
        temp = temp.replace("<img src=\"" + smiles[1].at(i) + "\"/>", smiles[0].at(i));
    }

    return temp;
}

int emoticons::countEmoticons(){
    // ==========================================================================
    // количество смайлов
    // ==========================================================================

    return smiles[0].count();
}
