/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#include "chatmodel.h"

chatModel::chatModel(const sContact &contact, QObject *parent) :
    QAbstractListModel(parent)
{
    this->contact = contact;
    QHash<int, QByteArray> roles = roleNames();
    roles[midRole] = "mid";
    roles[nameRole] = "name";
    roles[dateRole] = "date";
    roles[flagsRole] = "flags";
    roles[uidRole] = "uid";
    roles[photosRole] = "photos";
    roles[photosBigRole] = "photosBig";
    roles[audioRole] = "audio";
    setRoleNames(roles);
}

void chatModel::setColor(const QColor &nameIn, const QColor &nameOut, const QColor &backgroundIn, const QColor &backgroundOut, const QColor &date, const QColor &separator){
    this->nameIn = nameIn;
    this->nameOut = nameOut;
    this->backgroundIn = backgroundIn;
    this->backgroundOut = backgroundOut;
    this->date = date;
    this->separator = separator;
}

QVariant chatModel::data(const QModelIndex &index, int role) const{
    // ==========================================================================
    // данные модели
    // ==========================================================================

    if (!index.isValid()) {
        return QVariant();
    }

    switch (role) {
    case Qt::DisplayRole:
        return messages.value(index.row()).text;

    case Qt::EditRole:
        return messages.value(index.row()).text;

    case Qt::DecorationRole:
        //исходящее сообщение
        return messages.value(index.row()).flags & 2 ? appSettings::instance()->profilePhoto() : contact.avatarUrl.toString();
//        if (messages.value(index.row()).flags & 2){
//            if (messages.value(index.row()).flags & 1)
//                return QPixmap(":/mail.png");
//            else return QPixmap(":/mail-open-document-text.png");
//        }

        //событие
        if (messages.value(index.row()).flags & 65536){
            return QPixmap(":/balloon.png");
        }

        //входящее сообщение
        if (!(messages.value(index.row()).flags & 2)){
            return QPixmap(":/mail.png");
        }

    case Qt::ToolTipRole:
        return QVariant();

    case Qt::FontRole:
        return QFont();

    case Qt::BackgroundRole:
        return messages.value(index.row()).flags & 2 ? backgroundOut : backgroundIn;

    case Qt::ForegroundRole:
        return messages.value(index.row()).flags & 2 ? nameOut : nameIn;

    case Qt::SizeHintRole:
        return QVariant();

    case midRole: //идентификатор сообщения
        return messages.value(index.row()).mid;

    case nameRole: //имя
        return messages.value(index.row()).flags & 2 ? appSettings::instance()->profileShortName() : contact.firstName.isEmpty() ? messages.value(index.row()).fromId : contact.firstName;

    case dateRole: //дата
        return messages.value(index.row()).dateTime;

    case Qt::UserRole+3: //цвет даты
        return date;

    case Qt::UserRole+4: //цвет линии разделителя
        return separator;

    case flagsRole: //флаги
        return messages.value(index.row()).flags;

    case uidRole: //uid
        return messages.value(index.row()).fromId;

    case photosRole: //фотографии
        return QVariant::fromValue(messages.value(index.row()).photos.split(","));

    case photosBigRole: //большие фотографии
        return QVariant::fromValue(messages.value(index.row()).photosBig.split(","));

    case audioRole:{ //аудио
        if (!messages.value(index.row()).audio.count())
            return QVariant();

        //пусть лучше тут будет for чем в памяти всегда держать list QVariantMap'ов
        QVariantList variantList;
        QVariantMap variantMap;
        for (int i = 0; i < messages.value(index.row()).audio.count(); i++){
            variantMap["aid"] = messages.value(index.row()).audio[i].aid;
            variantMap["oid"] = messages.value(index.row()).audio[i].ownerId;
            variantMap["artist"] = messages.value(index.row()).audio[i].artist;
            variantMap["title"] = messages.value(index.row()).audio[i].title;
            variantMap["duration"] = messages.value(index.row()).audio[i].duration;
            variantMap["url"] = messages.value(index.row()).audio[i].url;
            int m = messages.value(index.row()).audio[i].duration / 60;
            int s = messages.value(index.row()).audio[i].duration - (m * 60);
            variantMap["durationStr"] = QString("%1:%2").arg(m).arg(QString().number(s).rightJustified(2, '0'));
            variantList.append(variantMap);
        }
        return variantList;
    }
    }

    return QVariant();
}

bool chatModel::setData(const QModelIndex &index, const QVariant &value, int role){
    // ==========================================================================
    // редактирование данных модели
    // ==========================================================================

    if (!index.isValid() && !value.isValid()) {
        return false;
    }

    //изменяем флаги
    if (role == Qt::UserRole+3){
        messages[index.row()].flags = value.toInt();
        emit dataChanged(index, index);
        return true;
    }

    return false;
}

bool chatModel::insertMessage(int row, const sMessage &message, const sContact &contact){
    // ==========================================================================
    // добавляем в модель данные
    // ==========================================================================

    beginInsertRows(QModelIndex(), row, row);

    messages.insert(row, message);
    this->contact = contact;

    endInsertRows();
    return true;
}

bool chatModel::changeMessage(const QModelIndex &index, const sMessage &message, const sContact &contact){
    // ==========================================================================
    // изменяем в модели данные
    // ==========================================================================

    if (!index.isValid()) {
        return false;
    }

    messages[index.row()] = message;
    this->contact = contact;
    emit dataChanged(index, index);
    return true;
}

bool chatModel::removeMessage(int row, int count){
    // ==========================================================================
    // удаляем с модели данные
    // ==========================================================================

    beginRemoveRows(QModelIndex(), row, row+count-1);

    for (int i = 0; i < count; ++i) {
        messages.removeAt(row);
    }

    endRemoveRows();
    return true;
}

bool chatModel::removeMessage(const QString &mid){
    // ==========================================================================
    // удаляем с модели данные
    // ==========================================================================

    for(int i=0; i < messages.count(); i++){
        if (messages.at(i).mid == mid){
            removeMessage(i, 1);
            return true;
        }
    }
    return false;
}

int chatModel::rowCount(const QModelIndex &parent) const{
    // ==========================================================================
    // количество строк в модели
    // ==========================================================================

    if (parent.isValid())
      return 0;
    else
      return messages.count();
}

Qt::ItemFlags chatModel::flags(const QModelIndex &index) const{
    // ==========================================================================
    // флаги модели
    // ==========================================================================

    if (index.isValid())
      return (Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable);
    else
      return Qt::NoItemFlags;
}

void chatModel::resetMessageFlags(const QString &mid, int mask){
    // ==========================================================================
    // сброс флагов сообщения
    // ==========================================================================

    //обновляем сообщение
    for(int i=0; i < messages.count(); i++){
        if (messages.at(i).mid == mid){
            int flags = messages.at(i).flags;
            flags &=~ mask;
            QModelIndex idx = index(i, 0);
            setData(idx, flags, Qt::UserRole+3);
            break;
        }
    }
}

void chatModel::userOnline(const QString &uid){
    // ==========================================================================
    // друг вошел в сеть
    // ==========================================================================

    Q_UNUSED(uid);

    contact.status = "1";
}

void chatModel::userOffline(const QString &uid, int flag){
    // ==========================================================================
    // друг вышел из сети
    // ==========================================================================

    Q_UNUSED(uid);
    Q_UNUSED(flag);

    contact.status = "0";
}

void chatModel::addMessage(const sMessage &message, const sContact &contact){
    // ==========================================================================
    // добавляем в модель сообщение
    // ==========================================================================

    for(int i=0; i < messages.count(); i++){
        if (messages.at(i).mid == message.mid){
            QModelIndex idx = index(i, 0);
            changeMessage(idx, message, contact);
            return;
        }
    }

    //добавляем сообщение
    insertMessage(rowCount(), message, contact);
}

void chatModel::addEvent(const sMessage &event){
    // ==========================================================================
    // добавляем в модель событие
    // ==========================================================================

    insertMessage(rowCount(), event, contact);
}

void chatModel::clearMessages(){
    // ==========================================================================
    // очищаем модель
    // ==========================================================================

    removeMessage(0, rowCount());
}

sMessage chatModel::getMessage(const QString &mid){
    // ==========================================================================
    // сообщение по mid
    // ==========================================================================

    sMessage msg;

    for (int i=0; i < messages.count(); i++){
        if (messages.at(i).mid == mid){
            msg = messages.at(i);
            break;
        }
    }

    return msg;
}

QStringList chatModel::unreadMessages(){
    // ==========================================================================
    // возращает не прочитанные сообщения
    // ==========================================================================

    QStringList unreadMsg;

    for (int i=0; i < messages.count(); i++){
        if (messages.at(i).flags & 1)
            unreadMsg.append(messages.at(i).mid);
    }

    return unreadMsg;
}

int chatModel::countUnreadMessages(){
    // ==========================================================================
    // возращает количество не прочитанных сообщений
    // ==========================================================================

    int count = 0;

    for (int i=0; i < messages.count(); i++){
        if (messages.at(i).flags & 1)
            count++;
    }

    return count;
}




























