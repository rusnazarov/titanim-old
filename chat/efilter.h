/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef EFILTER_H
#define EFILTER_H

#include <QObject>
#include <QEvent>
#include <QKeyEvent>
#include <QTextEdit>
#include "../settings.h"

class eFilter : public QObject
{
Q_OBJECT

public:
    eFilter(QObject *pobj = 0);

protected:
    bool eventFilter(QObject *pobj, QEvent *pe);

signals:
    void clear();
    void sendMsg();
    void changeTab();
    void hideTabForm();
};

#endif // EFILTER_H
