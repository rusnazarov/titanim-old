/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef CHATDIALOGS_H
#define CHATDIALOGS_H

#include <QObject>
#include <QSortFilterProxyModel>
#include "dialogsmodel.h"
#include "../vk/cvkStruct.h"

class chatDialogs : public QObject
{
Q_OBJECT

public:
    dialogsModel *model;
    QSortFilterProxyModel *proxy;

public:
    chatDialogs(QObject *parent = 0);
    ~chatDialogs();

public slots:
    void appendDialogs(const QVector<sDialog> &dialogs);
    void replaceDialogs(const QVector<sDialog> &dialogs);

signals:
};

#endif // CHATDIALOGS_H
