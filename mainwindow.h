/*
    Copyright (c) 2010 by Nazarov Ruslan <818151@mail.ru>

 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDeclarativeContext>
#include <QDeclarativeEngine>
#include <QTranslator>
#include <QDesktopServices>
#include <QSignalMapper>
#include <QSystemTrayIcon>
#include <QNetworkProxy>
#include <QSortFilterProxyModel>
#include <QtDBus>
#include <QNetworkConfigurationManager>
#include "qmlapplicationviewer.h"
#include "cache/networkaccessmanagerfactory.h"
#include "vk/cvk.h"
#include "roster/contactlistmodel.h"
#include "roster/contactlistdelegate.h"
#include "settings.h"
#include "chat/tabform.h"
#include "chat/chatdialogs.h"
#include "newsfeed/newsfeed.h"
#include "wall/wall.h"
#include "wall/wallcomments.h"
#include "audio/audio.h"
#include "audio/audiomodel.h"
#include "photo/photo.h"
#include "video/video.h"
#include "notification/popupwindow.h"
#include "statusform.h"
#include "profileform.h"
#include "settingsform.h"
#include "about.h"
#include "transfer/tuitestclient.h"
#if defined(MEEGO_EDITION_HARMATTAN)
#include <MDataUri>
#include <maemo-meegotouch-interfaces/shareuiinterface.h>
#endif

struct sMySetting {
    QString uid;
    QString token;
    int filterSort;
    int filterList;
    QString filterText;
    QString filterUids;
    bool showActivity;
    bool showAvatar;
    int iconSize;
    int textSize;
    bool alternatingRowColors;
    bool sound;
    bool newsfeedEnabled;
    bool saveLastReadNew;
    bool sortByName;
};

class MainWindow : public QmlApplicationViewer
{
    Q_OBJECT
    Q_PROPERTY(bool showActivity READ showActivity NOTIFY showActivityChanged)
    Q_PROPERTY(int iconSize READ iconSize NOTIFY iconSizeChanged)
    Q_PROPERTY(int fontPointSize READ fontPointSize NOTIFY fontPointSizeChanged)
    Q_PROPERTY(bool alternatingRowColors READ alternatingRowColors NOTIFY alternatingRowColorsChanged)
    Q_PROPERTY(bool sound READ sound NOTIFY soundChanged)
    Q_PROPERTY(bool vibro READ vibro NOTIFY vibroChanged)
    Q_PROPERTY(bool autoConnect READ autoConnect)
    Q_PROPERTY(QString profileName READ profileName NOTIFY profileNameChanged)
    Q_PROPERTY(QString profilePhoto READ profilePhoto NOTIFY profilePhotoChanged)
    Q_PROPERTY(QString profileUid READ profileUid NOTIFY profileUidChanged)
    Q_PROPERTY(bool newsfeedEnabled READ newsfeedEnabled NOTIFY newsfeedEnabledChanged)
    Q_PROPERTY(int indexLastNew READ indexLastNew NOTIFY indexLastNewChanged)
    Q_PROPERTY(bool saveLastReadNew READ saveLastReadNew NOTIFY saveLastReadNewChanged)
    Q_PROPERTY(bool sortByName READ sortByName NOTIFY sortByNameChanged)

private:
    QTranslator *translator;
    QTranslator *qtTranslator;
    QSystemTrayIcon *tray;
    QMenu *trayMenu;
    QNetworkConfigurationManager *bearer;
    cvk *vk;
    eStatus status;
    TabForm *tabForm;
    chatDialogs *dialogs;
    Newsfeed *newsfeed;
    Wall *wall;
    Wall *wallUser;
    WallComments *wallComments;
    Audio *audio;
    Photo *photo;
    Video *video;
    statusForm *StatusForm;
    profileForm *profile;
    settingsForm *settings;
    About *about;
    contactListModel *model;
    contactListDelegate *delegate;
    QSortFilterProxyModel *proxy;
    contactListModel *rosterUserModel;
    QSortFilterProxyModel *rosterUserProxy;
    sMySetting mySetting;
    QStringList uidsOnline;
    QTimer *timerMessageTray;
    QTimer *timerReconnect;
    int positionInStack;
    int positionInStackD;
    sCounters counters;
    int countDownloadItems;
    QString newsfeedFrom;
    int wallOffset;
    int dialogOffset;
    int photoOffset;
    int videoOffset;
    int audioOffset;
    int indexReadNews;
    #if defined(MEEGO_EDITION_HARMATTAN)
    TUITestClient *clientTransfer;
    #endif

private:
    bool showActivity();
    int iconSize();
    int fontPointSize();
    bool alternatingRowColors();
    bool sound();
    bool vibro();
    bool autoConnect();
    QString profileName();
    QString profilePhoto();
    QString profileUid();
    bool newsfeedEnabled();
    int indexLastNew();
    bool saveLastReadNew();
    bool sortByName();
    void setProxyApp();
    void setTextProgress(const QString &text);
    void showPopupWindow(const QString &uid, const QString &text);

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *pe);

private slots:
    void on_groupsButton_clicked();
    void on_giftsButton_clicked();
    void on_notesButton_clicked();
    void on_videosButton_clicked();
    void on_photosButton_clicked();
    void on_friendsButton_clicked();
    void on_roster_doubleClicked(const QModelIndex &index);
    void slotTrayActivated(const QSystemTrayIcon::ActivationReason &reason);
    void setFilterSort(int setting=0);
    void slotSettingsChanged();
    void slotErrorString(const eError error, const QString &text, const bool &fatal);
    void slotOnlineChanged(bool set);
    void slotConnected(const QString &token="", const QString &uid="");
    void slotFriends(const QVector<sContact> &contacts);
    void slotFriendsLists(const QList<QString> &lists);
    void slotFriendsFinished();
    void slotProfileSelf(const sProfile &profile);
    void slotSendMessagesError(const int &error, const QString &uid, const QString &text);
    void slotListMenuTriggered(QObject *obj);
    void slotResetMessageFlags(const QString &mid, const int &mask, const QString &uid);
    void slotMessageReceived(const sMessage &message);
    void slotMessageTrayStart(const sMessage &message);
    void slotMessageTrayStop();
    void slotTimerMessageTray();
    void slotTimerReconnect();
    void slotUserOnline(const QString &uid);
    void slotUserOffline(const QString &uid, const int &flag);
    void slotActivity(const QList<sActivity> &activity);
    void slotCounters(const sCounters &countersNew);
    void slotHistory(const QString &uid, const QList<sMessage> &messages);
    void slotDialogs(const QVector<sDialog> &dialogs);
    void slotNews(const QVector<sNew> &news, const QString &newFrom);
    void slotWall(const QVector<sWall> &wall, const QString &ownerId);
    void slotAudio(const QVector<sAudio> &audio);
    void slotPhotos(const QVector<sPhoto> &photos);
    void slotClosePopupWindow();
    void slotDeletePassword();
    void slotQuietMode();
    void slotChangeProfile();
    void slotLike(const QString &ownerId, const QString &itemId, const QString &countLikes, bool isAdd);
    void slotVideo(const QVector<sVideo> &video);
    void slotFriendsUser(const QVector<sContact> &contacts);
    void slotFriendsMutual(const QStringList &uids);
    void slotWallPostFinished(const QString &uid, const bool &ok);

public slots:
    void setOnline(const QString &username="", const QString &password="");
    void setOffline();
    void on_statusCBox_currentIndexChanged(const int &index);
    void on_offlineButton_clicked(const int &metod=0);
    void setSound(const bool &on);
    void on_settingsButton_clicked();
    void on_homeButton_clicked();
    void on_aboutButton_clicked();
    void on_statusButton_clicked();
    void slotCaptchaDone(const QString &captcha_sid, const QString &captcha_key);
    void slotMainMenu(const int &idMenu);
    bool slotDeleteCacheDir();
    void slotRosterMenu(const int &idMenu, const int &indexModel);
    void slotPressedKey(const QString &filter);
    void slotPressedKeyRosterUser(const QString &filter);
    void slotOpenChatWindow(const QString &uid);
    void slotOpenChatWindow(const int &indexModel);
    void slotShowProfile(const QString &uid);
    void slotMessagesGetDialogs();
    void slotMessagesGetNextDialogs();
    void slotNewsfeedGet();
    void slotNewsfeedGetNext();
    void slotWallGet(const QString &owner_id="");
    void slotWallGetNext(const QString &owner_id="");
    void slotWallGetComments(const QString &post_id, const QString &owner_id);
    void slotWallClearComments();
    void slotWallAddComment(const QString &post_id, const QString &text, const QString &owner_id="", const QString &reply_to_cid="");
    void slotWallUserClear();
    void slotAudioGet(const QString &uid);
    void slotAudioGetNext(const QString &uid);
    void slotAudioSearch(const QString &search);
    void slotAudioSearchNext(const QString &search);
    void slotAudiosClear();
    void slotPhotosGetAll(const QString &owner_id="");
    void slotPhotosGetAllNext(const QString &owner_id="");
    void slotPhotosClear();
    void slotVideoGet(const QString &uid);
    void slotVideoGetNext(const QString &uid);
    void slotVideoSearch(const QString &search);
    void slotVideoSearchNext(const QString &search);
    void slotVideosClear();
    void slotRosterUserGet(const QString &uid, const bool &mutualFriends);
    void slotRosterUserClear();
    void wallPost(const QString &text, const QString &uid="", const QString &photo="", const int &rotation=0);
    void slotWallDelete(const QString &post_id, const QString &owner_id="");
    void slotOpenRegVK();
    void slotOpenUrl(const QString &url);
    void slotLikesGet(const QString &owner_id, const QString &type, const QString &item_id, bool add, bool repost);
    void setIndexReadNews(const int &index);
    void slotCallPhone(const QString &phone);
    void slotSavePhoto(const QString &url);
    bool slotShare(const QString &link, const QString &title="", const QString &desc="");

signals:
    void captcha(const QString &captchaSid, const QString &captchaImg);
    void showActivityChanged(const bool &visible);
    void iconSizeChanged(const int &size);
    void fontPointSizeChanged(const int &pointSize);
    void alternatingRowColorsChanged(const bool &on);
    void soundChanged(const bool &on);
    void vibroChanged(const bool &on);
    void profileNameChanged(const QString &name);
    void profilePhotoChanged(const QString &photo);
    void profileUidChanged(const QString &uid);
    void newsfeedEnabledChanged(const bool &enabled);
    void indexLastNewChanged(const int &index);
    void saveLastReadNewChanged(const int &index);
    void sortByNameChanged(const bool &enabled);
    void statusChanged(const bool &isOnline);
    void progressChanged(const QString &progressText);
    void messageReceived(const QString &uid, const QString &text);
    void showAuthForm();
    void showNotification(const QString &text);
    void friendsFinished();
    void dialogsFinished();
    void newsFinished();
    void wallFinished();
    void wallUserFinished();
    void audioFinished();
    void photoFinished();
    void videoFinished();
    void friendsUserFinished();
    void likes(const QString &ownerId, const QString &itemId, const QString &countLikes, bool isAdd);
};

#endif // MAINWINDOW_H
